import numpy as np
import tensorflow as tf

tf.enable_eager_execution()


def my4Dmatlmul(a, b):
    # return tf.einsum("uvik,uvkj->uvij", a, b)
    return tf.matmul(a, b)


v = (np.random.rand(2, 50, 3) * 20.0).astype(np.float32)
eu = (np.random.rand(2, 50, 3) * 2 * np.pi).astype(np.float32)
order = [1, 2, 3]
freeze = -1
timeSize = 50

euSin = tf.sin(eu)
euCos = tf.cos(eu)
roll = eu[:, :, 0]
one = tf.ones_like(roll, dtype=tf.float32)
zero = tf.zeros_like(roll, dtype=tf.float32)
xAx = tf.stack([one, zero, zero], axis=2)
yAx = tf.stack([zero, one, zero], axis=2)
zAx = tf.stack([zero, zero, one], axis=2)
rot = []

for j, aa in enumerate(order):
    if aa is 1:
        rot.append(tf.stack((xAx,
                             tf.stack([zero, euCos[:, :, j], euSin[:, :, j]], axis=2),
                             tf.stack([zero, -euSin[:, :, j], euCos[:, :, j]], axis=2)), axis=2))
    elif aa is 2:
        rot.append(tf.stack((tf.stack([euCos[:, :, j], zero, -euSin[:, :, j]], axis=2),
                             yAx,
                             tf.stack([euSin[:, :, j], zero, euCos[:, :, j]], axis=2)), axis=2))
    elif aa is 3:
        rot.append(tf.stack((tf.stack([euCos[:, :, j], euSin[:, :, j], zero], axis=2),
                             tf.stack([-euSin[:, :, j], euCos[:, :, j], zero], axis=2),
                             zAx), axis=2))

# Transpose the rot_matrix to go from frame to initial
rot_matrixI2B = tf.matmul(rot[2], tf.matmul(rot[1], rot[0]))

rot_matrix = tf.transpose(rot_matrixI2B, (0, 1, 3, 2))

if freeze is not None:
    bodyRotI = rot_matrixI2B[:, freeze, :, :]
    currentRot = tf.stack([tf.squeeze(bodyRotI)] * timeSize, axis=1)
    rot_Bt = tf.matmul(currentRot, rot_matrix)
else:
    currentRot = 1
    rot_Bt = rot_matrix

v_Bt = tf.squeeze(my4Dmatlmul(rot_Bt, tf.expand_dims(v, axis=3)))

a = 1
