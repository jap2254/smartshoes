import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from Tools.GUItools import *
import xarray as xr


class stairmillSubject(object):
    def __init__(self, sessionsNames, id, shoePath, matlabPath, saveFig=False, forceSync=False):
        # sessions = ['LowSpeed', 'MidSpeed', 'HighSpeed']
        fileExist = os.path.isfile
        self.sessions = {}
        self.id = id
        self.sessionsErrors = 0
        self.pressMin = np.zeros((2, 3)) + 1000
        self.pressMax = np.zeros((2, 3))
        if saveFig:
            sessPath = shoePath
        else:
            sessPath = None
        for s in sessionsNames:
            if 'motion' in s or 'skip' in s:
                continue
            shoePreF = shoePath + s
            shoeR = shoePreF + '_R.bin'
            shoeL = shoePreF + '_L.bin'
            matF = matlabPath + s + '.mat'
            try:
                if fileExist(shoeR) and fileExist(shoeL) and fileExist(matF):
                    print('%s, %s' % (id, s))
                    self.sessions[s] = stairmillSession(shoeR, shoeL, matF, s, saveFig=sessPath, forceSync=forceSync)
                    if self.sessions[s].error:
                        del self.sessions[s]
                        print('Error in %s, %s' % (id, s))
                        self.sessionsErrors += 1
                        continue
                    for k, arr in enumerate([self.sessions[s].rPressMinMax, self.sessions[s].lPressMinMax]):
                        m = arr[0] < self.pressMin[k, :]
                        self.pressMin[k, m] = arr[0][m]
                        m = arr[1] > self.pressMax[k, :]
                        self.pressMax[k, m] = arr[1][m]
            except:
                print('Error in %s, %s' % (id, s))
                self.sessionsErrors += 1

        if saveFig:
            f, ax = plt.subplots(2, len(self.sessions), sharex=True, sharey=True)
            tPlot = np.linspace(0, 100, 400)
            if len(ax.shape) is 1:
                ax = np.expand_dims(ax, axis=1)
        for i, s in enumerate(self.sessions):
            self.sessions[s].scalePress(self.pressMin, self.pressMax)
            if saveFig:
                for j, le in enumerate(['Toe', 'Ball', 'Heel']):
                    ax[0, i].plot(tPlot, self.sessions[s].rStride['Pressure'].mean(axis=0)[:, j], label=le)
                    ax[1, i].plot(tPlot, self.sessions[s].lStride['Pressure'].mean(axis=0)[:, j], label=le)
                ax[0, i].axhline(y=1, color='red')
                ax[1, i].axhline(y=1, color='red')
                ax[0, i].axhline(y=0, color='red')
                ax[1, i].axhline(y=0, color='red')
                ax[0, i].set_title('R, ' + s)
                ax[1, i].set_title('L, ' + s)
        if saveFig:
            plt.legend()
            f.set_size_inches(16, 10)
            # f.set_figheight(10)
            # f.set_figwidth(16)
            plt.tight_layout()
            plt.savefig(shoePath + 'PressSummary.png', dpi=300)
            plt.close(f)


class SpaceSubject(stairmillSubject):
    def __init__(self, sessionsNames, id, filesPath):
        fileExist = os.path.isfile
        self.sessions = {}
        self.id = id
        self.sessionsErrors = 0
        self.pressMin = np.zeros((2, 3)) + 1000
        self.pressMax = np.zeros((2, 3))
        for s, dirName in zip(sessionsNames, filesPath):
            if 'desktop' in s:
                continue
            try:
                print('%s, %s' % (id, s))
                shoeFile = dirName + s + '.bin'
                viconFile = dirName + s + '.csv'
                self.sessions[s] = SpaceSession(shoeFile, viconFile, s)
                # if self.sessions[s].error:
                #     del self.sessions[s]
                #     print('Error in %s, %s' % (id, s))
                #     self.sessionsErrors += 1
                #     continue
            except:
                print('Error in %s, %s' % (id, s))
                self.sessionsErrors += 1


class stairmillSession(object):
    def __init__(self, rightShoeFile, leftShoeFile, matlabFile, title, strideSize=400, saveFig=None, forceSync=False):
        # use RSB for ankle right
        self.strideSize = strideSize
        self.title = title
        app = self.loadStairmillFiles(rightShoeFile, leftShoeFile, matlabFile, forceSync)
        self.error = app.error
        if not app.error:
            self.rightShoe = app.rightShoe
            self.leftShoe = app.leftShoe
            self.rAnkle = app.rAnkle
            self.lAnkle = app.lAnkle
            self.rAnkleD = np.zeros_like(app.rAnkle)
            self.rAnkleD[1:] = np.diff(self.rAnkle, axis=0)
            self.rAnkleD[0] = self.rAnkleD[1]
            self.lAnkleD = np.zeros_like(app.lAnkle)
            self.lAnkleD[1:] = np.diff(self.lAnkle, axis=0)
            self.lAnkleD[0] = self.lAnkleD[1]
            self.rSpaceAcceleration = body2spaceEuler(self.rightShoe.dataFrame.iloc[:, 3:6].values,
                                                      self.rightShoe.dataFrame.iloc[:, 9:].values, freeze=True,
                                                      rotationOrder=[1, 2, 3])
            self.lSpaceAcceleration = body2spaceEuler(self.leftShoe.dataFrame.iloc[:, 3:6].values,
                                                      self.leftShoe.dataFrame.iloc[:, 9:].values, freeze=True,
                                                      rotationOrder=[1, 2, 3])
            # rYspace = posVelFromAccODE(self.rSpaceAcceleration[:, 0], self.rSpaceAcceleration[:, 1], self.rSpaceAcceleration[:, 2],
            #                            self.rightShoe.dataFrame.index.values, self.rightShoe.dataFrame.index.values[:-1],
            #                            [*self.rAnkle[0, :], 0, 0, 0])
            # lYspace = posVelFromAccODE(self.lSpaceAcceleration[:, 0], self.lSpaceAcceleration[:, 1],
            #                            self.lSpaceAcceleration[:, 2],
            #                            self.leftShoe.dataFrame.index.values, self.leftShoe.dataFrame.index.values[:-1],
            #                            [*self.lAnkle[0, :], 0, 0, 0])
            # self.rAnkleInt = rYspace[:, :3]
            # self.lAnkleInt = lYspace[:, :3]
            self.stairTime = app.stairTime
            self.rHS = app.rHS
            self.rTO = app.rTO
            self.lHS = app.lHS
            self.lTO = app.lTO
            self.freq = app.freq
            self.rStride, self.rStrideTime = self.splitSide('Right')
            self.lStride, self.lStrideTime = self.splitSide('Left')
            self.rPressMinMax = [self.rStride['Pressure'].mean(axis=0).min(dim=['CycleIndex']),
                                 self.rStride['Pressure'].mean(axis=0).max(dim=['CycleIndex'])]
            self.lPressMinMax = [self.lStride['Pressure'].mean(axis=0).min(dim=['CycleIndex']),
                                 self.lStride['Pressure'].mean(axis=0).max(dim=['CycleIndex'])]

            if saveFig is not None:
                f, ax = plt.subplots(2)
                for axx, shoe, hs, ti in zip(ax, [self.rightShoe, self.leftShoe], [self.rHS, self.lHS],
                                             ['Right Shoe', 'Left Shoe']):
                    shoe.dataFrame.plot(y=['pToe', 'pBall', 'pHeel'], ax=axx)
                    axx.set_title(ti)
                    for hsT in hs:
                        axx.axvline(x=hsT, color='red')
                f.set_size_inches(16, 10)
                plt.tight_layout()
                plt.savefig(saveFig + title + '.png', dpi=300)
                plt.close(f)

    def splitSide(self, side):
        if side is 'Right':
            hs = np.squeeze(np.array([np.where(self.stairTime == h) for h in self.rHS]))
            ankle = self.rAnkle
            shoe = self.rightShoe
        else:
            hs = np.squeeze(np.array([np.where(self.stairTime == h) for h in self.lHS]))
            ankle = self.lAnkle
            shoe = self.leftShoe
        # strides will be of size:
        # labels: Pressure, Euler, Acceleration, Ankle
        # items: n strides
        # cycle: self.strideSize
        # values: x, y, z for Ankle, Euler, Acceleration and Toe, Ball, Heel for Pressure
        # size is: 4 X nStrides X self.strideSize X 3
        stride = np.zeros((5, hs.size - 1, self.strideSize, 3))
        labels = ['Pressure', 'Euler', 'Acceleration', 'Ankle', 'AnkleDiff']
        # strideIndex = [j for j in range(hs.size - 1)]
        # cycleIndex = [j for j in range(self.strideSize)]
        units = [['Toe', 'Ball', 'Heel'],
                 ['Roll', 'Pitch', 'Yaw'],
                 ['x', 'y', 'z'],
                 ['x', 'y', 'z']]
        # coords = {'label': labels, 'Stride': strideIndex, 'Time': cycleIndex, 'units': units}
        strideTime = np.diff(hs)
        for j, (s1, s2) in enumerate(zip(hs[:-1], hs[1:])):
            # Ankle
            # TODO: Check that this is right
            stride[3, j, :, :] = reSampleMatrix(ankle[s1:s2 + 1, :] - ankle[s1, :], self.strideSize)
            stride[4, j, 1:, :] = np.diff(stride[3, j, :, :], axis=0)
            # shoe index 0 is when the recordings started, the vicon recording is sometimes cutted and the t[0]!=0
            s1Shoe = int(self.stairTime[s1] * self.freq)
            s2Shoe = int(self.stairTime[s2] * self.freq)
            # Pressure
            stride[0, j, :, :] = reSampleMatrix(shoe.dataFrame.iloc[s1Shoe:s2Shoe + 1, :3], self.strideSize)
            # Euler
            stride[1, j, :, :] = reSampleMatrix(shoe.dataFrame.iloc[s1Shoe:s2Shoe + 1, 9:], self.strideSize)
            # Acceleration, we transform the acceleration from body to space using the euler angles
            ax = reSampleMatrix(shoe.dataFrame.iloc[s1Shoe:s2Shoe + 1, 3:6], self.strideSize)
            stride[2, j, :, :] = body2spaceEuler(ax, np.squeeze(stride[1, j, :, :]), True)
        ank = np.squeeze(stride[3, :, :, :])
        ankD = np.squeeze(stride[4, :, :, :])
        pres = np.squeeze(stride[0, :, :, :])
        eul = np.squeeze(stride[1, :, :, :])
        acc = np.squeeze(stride[2, :, :, :])
        # ds = {labels[0]: (units[0], pres),
        #       labels[1]: (units[1], eul),
        #       labels[2]: (units[2], acc),
        #       labels[3]: (units[3], ank)}
        ds = {labels[0]: (['Stride', 'CycleIndex', 'Units'], pres),
              labels[1]: (['Stride', 'CycleIndex', 'Rotations'], eul),
              labels[2]: (['Stride', 'CycleIndex', 'Gs'], acc),
              labels[3]: (['Stride', 'CycleIndex', 'Coords'], ank),
              labels[4]: (['Stride', 'CycleIndex', 'Coords'], ankD)}
        coords = {'Stride': pandas.RangeIndex(hs.size - 1) + 1,
                  'CycleIndex': pandas.RangeIndex(self.strideSize),
                  'Units': units[0],
                  'Rotations': units[1],
                  'Gs': units[2],
                  'Coords': units[3]}
        strides = xr.Dataset(ds, coords=coords)

        return strides, strideTime

    def scalePress(self, minPress, maxPress):
        self.rStride['Pressure'] = (self.rStride['Pressure'] - np.squeeze(minPress[0, :])) / np.squeeze(
            maxPress[0, :] - minPress[0, :])
        self.lStride['Pressure'] = (self.lStride['Pressure'] - np.squeeze(minPress[1, :])) / np.squeeze(
            maxPress[1, :] - minPress[1, :])

    #

    class loadStairmillFiles(object):
        def __init__(self, rightShoeFile, leftShoeFile, matlabFile, forceSync, useCenter=True):
            self.rightShoe = None
            self.leftShoe = None
            self.rAnkle = None
            self.lAnkle = None
            self.stairTime = None
            self.rHS = None
            self.rTO = None
            self.lHS = None
            self.lTO = None
            self.freq = 100.0
            self.error = False
            try:
                self.loadMatFile(matlabFile, useCenter)
                if forceSync:
                    self.loadLeftRightShoe(rightShoeFile, leftShoeFile)
                else:
                    self.loadRightShoe(rightShoeFile)
                    self.loadLeftShoe(leftShoeFile)
                    self.error = not (self.rightShoe.syncFound and self.leftShoe.syncFound)
            except:
                self.error = True

        def loadRightShoe(self, fN):
            [self.rightShoe, _] = binaryFile2python(fN, V=2)
            self.rightShoe.createPandasDataFrame(freq=self.freq, useSync=True, filter=True)
            # self.error = not self.rightShoe.syncFound

        def loadLeftShoe(self, fN):
            [_, self.leftShoe] = binaryFile2python(fN, V=2)
            self.leftShoe.createPandasDataFrame(freq=self.freq, useSync=True, filter=True)
            # self.error = not self.leftShoe.syncFound

        def loadLeftRightShoe(self, rFile, lFile):
            [rightShoe, _] = binaryFile2python(rFile, V=2)
            [_, leftShoe] = binaryFile2python(lFile, V=2)
            both = SyncedShoes(rightShoe, leftShoe, self.freq, useSync=True, filter=True)
            self.rightShoe = both.rightShoe
            self.leftShoe = both.leftShoe
            self.error = not both.syncFound

        def loadMatFile(self, fN, useCenter):
            data = sio.loadmat(fN, struct_as_record=False)
            if useCenter:
                # midSr = (RFH + RFL + RFR + RFT) / 4;
                self.rAnkle = (data['RFH'] + data['RFT'] + data['RFL'] + data['RFR']) / 4
                self.rAnkle[:, 0] = (data['RFL'][:, 0] + data['RFR'][:, 0]) / 2
                self.rAnkle[:, 1] = (data['RFH'][:, 1] + data['RFT'][:, 1]) / 2
                # self.rAnkle -= self.rAnkle[0, :]
                self.lAnkle = (data['LFH'] + data['LFT'] + data['LFL'] + data['LFR']) / 4
                self.lAnkle[:, 0] = (data['LFL'][:, 0] + data['LFR'][:, 0]) / 2
                self.lAnkle[:, 1] = (data['LFH'][:, 1] + data['LFT'][:, 1]) / 2
                # self.lAnkle -= self.lAnkle[0, :]
            else:
                self.rAnkle = data['RSB'] - data['RSB'][np.squeeze(data['RindexSt'])[0]]
                self.lAnkle = data['LSB'] - data['LSB'][np.squeeze(data['LindexSt'])[0]]
            self.rAnkle -= self.rAnkle[0, :]
            self.stairTime = np.squeeze(data['T'])
            self.rHS = self.stairTime[np.squeeze(data['RindexSt'])]
            self.rTO = self.stairTime[np.squeeze(data['Rindex'])]
            self.lHS = self.stairTime[np.squeeze(data['LindexSt'])]
            self.lTO = self.stairTime[np.squeeze(data['Lindex'])]


class SpaceSession(stairmillSession):
    def __init__(self, shoeFile, viconFile, title):
        self.strideSize = 0
        self.title = title
        self.freq = 100.0
        [r, l] = binaryFile2python(shoeFile, V=2)
        if len(r.timestamp) is 0:
            # use l
            l.createPandasDataFrame(freq=self.freq, useSync=True)
            goodShoe = l
        else:
            r.createPandasDataFrame(freq=self.freq, useSync=True)
            goodShoe = r

        self.rightShoe = goodShoe
        self.leftShoe = goodShoe

        vData = pandas.read_csv(viconFile)
        t_max = vData.shape[0]
        # make H(t)_B(t)_I of shape [t_max, 4, 4]
        H_B_I = np.zeros((t_max, 4, 4))
        # rotate to IMU initial frame
        Tr = np.squeeze(vData.loc[:, ['TRx', 'TRy', 'TRz']].values)
        Tl = np.squeeze(vData.loc[:, ['TLx', 'TLy', 'TLz']].values)
        Tm = np.squeeze(vData.loc[:, ['TMx', 'TMy', 'TMz']].values)
        Bl = np.squeeze(vData.loc[:, ['BLx', 'BLy', 'BLz']].values)

        xV = Tl - Tr
        xV = np.multiply(xV, np.expand_dims(1 / np.linalg.norm(xV, axis=1), 1))
        zV = Tm - Bl
        zV = np.multiply(zV, np.expand_dims(1 / np.linalg.norm(zV, axis=1), 1))
        yV = np.cross(zV, xV)

        # Rs is a stack of rotation matrix from space to body frame of shape [t_max, 3, 3]
        Rs_B2F = np.stack((xV, yV, zV), axis=1)
        Rs = Rs_B2F.transpose((0, 2, 1))

        columnNames = ['Cx', 'Cy', 'Cz']

        origins = np.expand_dims(vData[columnNames].values, 2)
        ankleO = vData[columnNames].values
        # origins_body is now [t_max, 3, 1]
        origins_body = -np.matmul(Rs, origins)
        # H_B_I is shape [t_max, 3, 4]
        H_B_I = np.concatenate((Rs, origins_body), axis=2)
        # H_B_I is shape [t_max, 4, 4]
        H_B_I = np.concatenate((H_B_I, np.zeros((t_max, 1, 4))), axis=1)
        H_B_I[:, -1, -1] = 1

        # H_BI transform from inertial frame to current body frame
        origins = np.concatenate((origins, np.ones((t_max, 1, 1))), axis=1)
        # ankle = np.squeeze(np.matmul(H_B_I[:-1, :, :], origins[1:, :, :])[:, :-1, :])

        # if H_B_I[0, :, :] is used, the inertial space is transformed to body frame at t=0
        ankle = np.squeeze(np.matmul(H_B_I[0, :, :], origins)[:, :-1, :])

        # rotation matrix from angles

        # eueAngles = self.rightShoe.dataFrame.iloc[:, 9:].values
        # # eueAngles[:, 2] *= -1
        # roll = eueAngles[:, 0]
        # pitch = eueAngles[:, 1]
        # yaw = eueAngles[:, 2]
        # eueAngles = np.stack((yaw, roll, pitch), axis=1)
        # euSin = np.sin(eueAngles)
        # euCos = np.cos(eueAngles)
        # one = np.ones_like(euSin[:, 0], dtype=np.float32)
        # zero = np.zeros_like(euSin[:, 0], dtype=np.float32)
        # xAx = np.stack([one, zero, zero], axis=1)
        # yAx = np.stack([zero, one, zero], axis=1)
        # zAx = np.stack([zero, zero, one], axis=1)
        # # np.concat([zero, one, zero], axis=1) is [n, t_max, 3]
        #
        # rot = []
        # order = [2, 3, 1]
        #
        # for j, aa in enumerate(order):
        #     if aa is 1:
        #         rot.append(np.stack((xAx,
        #                              np.stack([zero, euCos[:, j], -euSin[:, j]], axis=1),
        #                              np.stack([zero, euSin[:, j], euCos[:, j]], axis=1)), axis=1))
        #     elif aa is 2:
        #         rot.append(np.stack((np.stack([euCos[:, j], zero, euSin[:, j]], axis=1),
        #                              yAx,
        #                              np.stack([-euSin[:, j], zero, euCos[:, j]], axis=1)), axis=1))
        #     elif aa is 3:
        #         rot.append(np.stack((np.stack([euCos[:, j], -euSin[:, j], zero], axis=1),
        #                              np.stack([euSin[:, j], euCos[:, j], zero], axis=1),
        #                              zAx), axis=1))
        #
        #
        # # Transpose the rot_matrix to go from frame to initial
        # rot_matrixI2B = np.matmul(rot[2], np.matmul(rot[1], rot[0]))
        # rotB0 = np.stack([rot_matrixI2B[0, :, :]] * rot_matrixI2B.shape[0], axis=0)
        # r2 = rot_matrixI2B.transpose((0, 2, 1))
        # spAcc = np.matmul(r2, np.expand_dims(self.rightShoe.dataFrame.iloc[:, 3:6].values, 2))
        # spAcc = np.matmul(rotB0, spAcc)
        # spB = np.matmul(Rs[Rs.shape[0]-spAcc.shape[0]:,:,:], spAcc)
        #
        # f, axx = plt.subplots(3, 2, sharex=True)
        # for j in range(3):
        #     axx[j, 0].plot(self.rightShoe.dataFrame.iloc[:, 3:6].values[:, j])
        #     axx[j, 0].plot(spB[:, j])
        #     axx[j, 1].plot(spAcc[:, j])
        RIMU = euler2Rot(goodShoe.dataFrame.iloc[0, 9],
                         goodShoe.dataFrame.iloc[0, 10],
                         goodShoe.dataFrame.iloc[0, 11], rotationOrder=[1, 2, 3])

        ank = np.matmul(ankle, RIMU.T)
        self.rAnkle = ank
        self.lAnkle = ank
        self.rAnkleD = np.zeros_like(ank)
        self.rAnkleD[1:] = np.diff(ank, axis=0)
        self.rAnkleD[0] = self.rAnkleD[1]
        self.lAnkleD = np.zeros_like(ank)
        self.lAnkleD[1:] = np.diff(ank, axis=0)
        self.lAnkleD[0] = self.lAnkleD[1]
        self.rSpaceAcceleration = body2spaceEuler(self.rightShoe.dataFrame.iloc[:, 3:6].values,
                                                  self.rightShoe.dataFrame.iloc[:, 9:].values, freeze=True)
        self.lSpaceAcceleration = body2spaceEuler(self.leftShoe.dataFrame.iloc[:, 3:6].values,
                                                  self.leftShoe.dataFrame.iloc[:, 9:].values, freeze=True)
        # ax = self.rSpaceAcceleration[:, 0]
        # ay = self.rSpaceAcceleration[:, 1]
        # az = self.rSpaceAcceleration[:, 2]
        # tAcc = np.arange(ax.shape[0]) / self.freq
        # ankleInt = posVelFromAccODE(self.rightShoe.dataFrame.iloc[:, 3].values,
        #                             self.rightShoe.dataFrame.iloc[:, 4].values+ 1.0,
        #                             self.rightShoe.dataFrame.iloc[:, 5].values, tAcc, tAcc[:-300], [0, 0, 0, 0, 0, 0])
        # ankleIntRot = posVelFromAccODE(ax, ay, az, tAcc, tAcc[:-300], [0, 0, 0, 0, 0, 0])
        # f, axx = plt.subplots(3,3, sharex=True)
        # for j in range(3):
        #     axx[j, 0].plot(tAcc, ankle[:-1, j])
        #     axx[j, 1].plot(tAcc[:-300], ankleInt[:, j])
        #     axx[j, 2].plot(tAcc[:-300], ankleIntRot[:, j])
        # rYspace = posVelFromAccODE(self.rSpaceAcceleration[:, 0], self.rSpaceAcceleration[:, 1], self.rSpaceAcceleration[:, 2],
        #                            self.rightShoe.dataFrame.index.values, self.rightShoe.dataFrame.index.values[:-1],
        #                            [*self.rAnkle[0, :], 0, 0, 0])
        # lYspace = posVelFromAccODE(self.lSpaceAcceleration[:, 0], self.lSpaceAcceleration[:, 1],
        #                            self.lSpaceAcceleration[:, 2],
        #                            self.leftShoe.dataFrame.index.values, self.leftShoe.dataFrame.index.values[:-1],
        #                            [*self.lAnkle[0, :], 0, 0, 0])
        # self.rAnkleInt = rYspace[:, :3]
        # self.lAnkleInt = lYspace[:, :3]
        self.stairTime = np.arange(ankle.shape[0]) / self.freq
        self.rHS = None
        self.rTO = None
        self.lHS = None
        self.lTO = None


def createNsamples(x, y, n, timeSize, aslist=False):
    if n > x.shape[0] - timeSize:
        warnings.warn('Not enough samples in input, creating only %d samples' % x.shape[0], UserWarning)
        n = x.shape[0] - timeSize

    actualSamples = randomOrder(x.shape[0] - timeSize)[:n] + timeSize
    xx = [x[j - timeSize:j, :] for j in actualSamples]
    yy = [y[j - timeSize:j, :] for j in actualSamples]
    if not aslist:
        xx = np.stack(xx, axis=0)
        yy = np.stack(yy, axis=0)
    return xx, yy


def createbatch(x, timeSize, aslist=False):
    actualSamples = np.arange(x.shape[0] - timeSize) + timeSize
    # actualSamples = randomOrder(x.shape[0] - timeSize)[:n] + timeSize
    x0 = np.zeros((timeSize, x.shape[1]), dtype=x.dtype)
    xi = x0.copy()
    xi[-1] = x[0]
    xii = [xi.copy()]
    for i in range(1, timeSize):
        xi = x0.copy()
        xi[-i:] = x[:i]
        xii.append(xi.copy())
    xx = xii + [x[j - timeSize:j, :] for j in actualSamples]
    if not aslist:
        xx = np.stack(xx, axis=0)
    return xx


def movingwindowSum(vec, windowSize):
    vecSum = [np.sum(vec[:j + 1], axis=0) for j in range(windowSize)] + \
             [np.sum(vec[j - windowSize + 1:j + 1], axis=0) for j in range(windowSize, vec.shape[0] - 1)] + \
             [np.sum(vec[-windowSize:], axis=0)]
    vecSum = np.stack(vecSum)
    return vecSum


def encodeVector2LogitMatrix(vec, minV, maxV, nClass):
    vClass = np.zeros((vec.shape[0], nClass))
    increments = np.linspace(minV, maxV, nClass + 1)
    for i, (st, en) in enumerate(zip(increments[:-1], increments[1:])):
        m = np.logical_and(np.greater_equal(vec, st), np.less(vec, en))
        vClass[m, i] = 1

    return vClass


def encodeVector2LogitVector(vec, minV, maxV, nClass):
    vClass = np.zeros_like(vec)
    increments = np.linspace(minV, maxV, nClass + 1)
    for i, (st, en) in enumerate(zip(increments[:-1], increments[1:])):
        m = np.logical_and(np.greater_equal(vec, st), np.less(vec, en))
        vClass[m] = i

    return vClass


def decodeClassVector2VectorValues(encodedVector, minV, maxV, nClass):
    newVec = np.zeros_like(encodedVector)
    increments = np.linspace(minV, maxV, nClass + 1)
    for i, (st, en) in enumerate(zip(increments[:-1], increments[1:])):
        m = np.equal(encodedVector, i)
        newVec[m] = (en + st) / 2

    return newVec


def logitsMatrix2ClassVector(encodedMatrix):
    return np.argmax(encodedMatrix, axis=1)


def classVector2logitsMatrix(cVec, nClass):
    newMat = np.zeros((cVec.size, nClass), dtype=cVec.dtype)
    for j in range(nClass):
        newMat[np.equal(cVec, j), j] = 1
    return newMat


class StairmillSpatialDataset(object):

    def __init__(self, pickleFile, trainsize, testSize, timeSize, channels2use, useSpatial, y2use, useOnlyRight):

        self.useOnlyRight = useOnlyRight
        self.useSpatial = useSpatial
        self.channels2use = channels2use
        self.pickleFile = pickleFile
        self.trainsize = trainsize
        self.testSize = testSize
        self.timeSize = timeSize
        self.y2use = y2use
        self.minValue = -16
        self.maxValue = 16
        self.classes = 30
        self.__generateDataset(pickleFile, trainsize, testSize, timeSize)

    def __generateDataset(self, pickleFile, trainsize, testSize, timeSize):
        with open(pickleFile, 'rb') as f:
            allSubs = pickle.load(f)
        self.allSubs = allSubs
        x = []
        y = []
        xT = []
        yT = []
        useSubTestMode = type(testSize) is int
        # if testSize is int, it means  you want testSize randomly selected from each subject
        if useSubTestMode:
            # This means that you want to split
            totalSamsPerSub = np.ceil((testSize + trainsize) / len(allSubs))
        else:
            totalSamsPerSub = np.ceil(trainsize / len(allSubs))
        for subName in allSubs:
            sub = allSubs[subName]
            samsPerSession = np.ceil(totalSamsPerSub / len(sub.sessions)).astype(np.int32)
            for sessName in sub.sessions:
                sess = sub.sessions[sessName]
                if self.useOnlyRight:
                    samsR = samsPerSession
                    samsL = 0
                else:
                    sAux = np.linspace(0, samsPerSession, 3)  # the middle number will be the middle
                    if np.random.rand() > 0.5:
                        samsR = np.floor(sAux[1]).astype(np.int)
                        samsL = np.ceil(sAux[1]).astype(np.int)
                    else:
                        samsL = np.floor(sAux[1]).astype(np.int)
                        samsR = np.ceil(sAux[1]).astype(np.int)

                # time for both might be different
                stairStartTime = int(sess.stairTime[0] * sess.freq)
                for shoe, ankleD, ankle, spAcc, nSams in zip([sess.rightShoe, sess.leftShoe],
                                                             [sess.rAnkleD, sess.lAnkleD],
                                                             [sess.rAnkle, sess.lAnkle],
                                                             [sess.rSpaceAcceleration, sess.lSpaceAcceleration],
                                                             [samsR, samsL]):
                    if nSams is 0:
                        continue
                    ankleD1 = np.zeros_like(ankleD)
                    ankleD1[1:] = np.diff(ankleD, axis=0)
                    ankleD1[0] = ankleD1[1]
                    if self.useSpatial:
                        xIn = np.concatenate(
                            (shoe.dataFrame.values[stairStartTime:, self.channels2use], spAcc[stairStartTime:]), axis=1)
                    else:
                        xIn = shoe.dataFrame.values[stairStartTime:, self.channels2use]
                    classPreVecT = movingwindowSum(ankle, timeSize)
                    classPreVec = ankleD

                    yIn = np.concatenate((ankleD, ankle, classPreVecT,
                                          np.expand_dims(encodeVector2LogitVector(classPreVec[:, 0], self.minValue,
                                                                                  self.maxValue, self.classes), axis=1),
                                          np.expand_dims(encodeVector2LogitVector(classPreVec[:, 1], self.minValue,
                                                                                  self.maxValue, self.classes), axis=1),
                                          np.expand_dims(encodeVector2LogitVector(classPreVec[:, 2], self.minValue,
                                                                                  self.maxValue, self.classes), axis=1),
                                          ankleD1),
                                         axis=1)
                    # check that both are the same size
                    if xIn.shape[0] > yIn.shape[0]:
                        # cut xIn
                        xIn = xIn[:yIn.shape[0]]
                    if yIn.shape[0] > xIn.shape[0]:
                        # cut yIn
                        yIn = yIn[:xIn.shape[0]]
                    xx, yy = createNsamples(xIn.astype(np.float32), yIn.astype(np.float32), nSams, timeSize,
                                            aslist=True)

                    if not useSubTestMode and sub.id in testSize:
                        # if not useSubTestMode and sessName in testSize:
                        xT += xx
                        yT += yy
                    else:
                        x += xx
                        y += yy

        # create the numpy arrays
        if not useSubTestMode:
            shu = randomOrder(len(x))
            x = np.stack(x, axis=0)[shu]
            y = np.stack(y, axis=0)[shu]
            shu = randomOrder(len(xT))
            xT = np.stack(xT, axis=0)[shu]
            yT = np.stack(yT, axis=0)[shu]
        else:
            x = np.stack(x, axis=0)
            y = np.stack(y, axis=0)
            xT = x[:testSize]
            yT = y[:testSize]
            x = x[testSize:]
            y = y[testSize:]
            shu = randomOrder(len(x))
            x = x[shu]
            y = y[shu]

        self.x = x
        self.y = y[:, :, :3]
        self.y2 = y[:, :, 3:6]
        self.yTimeSum = y[:, :, 6:9]
        self.y3 = y[:, :, 9:12].astype(np.int32)
        self.y4 = y[:, :, 12:]
        self.xT = xT
        self.yT = yT[:, :, :3]
        self.y2T = yT[:, :, 3:6]
        self.yTTimeSum = yT[:, :, 6:9]
        self.y3T = yT[:, :, 9:12].astype(np.int32)
        self.y4T = yT[:, :, 12:]
        self.yMag = np.linalg.norm(self.y2, axis=1)
        self.yTMag = np.linalg.norm(self.y2T, axis=1)

        self.allYs = {'Diff': {'y': self.y, 'yT': self.yT, 'outSize': 3},
                      'Acc': {'y': self.y4, 'yT': self.y4T, 'outSize': 3},
                      'Dist': {'y': self.y2, 'yT': self.y2T, 'outSize': 3},
                      'Mag': {'y': self.yMag, 'yT': self.yTMag, 'outSize': 1},
                      'TimeSum': {'y': self.yTimeSum, 'yT': self.yTTimeSum, 'outSize': 3},
                      # 'Discrete': {'y': self.y3, 'yT': self.y3T, 'outSize': 3}}
                      'Discrete': {'y': self.y3, 'yT': self.y3T, 'outSize': self.classes}}
        self.dataShape = xx[0].shape

    def giveExample(self, returnHS=False):
        a = 0
        while a <= 0:
            subName = list(self.allSubs.keys())[np.random.randint(len(self.allSubs))]
            sub = self.allSubs[subName]
            a = len(sub.sessions)
            if a is 0:
                continue
            sessName = list(sub.sessions.keys())[np.random.randint(len(sub.sessions))]
            sess = sub.sessions[sessName]

        fout = self.giveSessionInfo(subName, sessName, returnHS=returnHS)
        title = '%s %s' % (sub.id, sess.title)
        return fout + (title,)

    def giveSessionInfo(self, subName, sessName, filterMat=False, returnHS=False):
        sub = self.allSubs[subName]
        sess = sub.sessions[sessName]
        stairStartTime = int(sess.stairTime[0] * sess.freq)
        if self.useSpatial:
            matR = np.concatenate((sess.rightShoe.dataFrame.values[stairStartTime:, self.channels2use],
                                   sess.rSpaceAcceleration[stairStartTime:]), axis=1)
            matL = np.concatenate((sess.leftShoe.dataFrame.values[stairStartTime:, self.channels2use],
                                   sess.lSpaceAcceleration[stairStartTime:]), axis=1)
        else:
            matR = sess.rightShoe.dataFrame.values[stairStartTime:, self.channels2use]
            matL = sess.leftShoe.dataFrame.values[stairStartTime:, self.channels2use]

        yR = sess.rAnkleD
        yL = sess.lAnkleD
        y4R = np.zeros_like(sess.rAnkleD)
        y4R[1:] = np.diff(yR, axis=0)
        y4R[0] = y4R[1]
        y4L = np.zeros_like(sess.lAnkleD)
        y4L[1:] = np.diff(yL, axis=0)
        y4L[0] = y4L[1]
        y2R = sess.rAnkle
        y2L = sess.lAnkle

        s = np.array([matR.shape[0], matL.shape[0], yR.shape[0], yL.shape[0]]).min()
        matR = matR[:s]
        matL = matL[:s]
        yR = yR[:s]
        yL = yL[:s]
        y2R = y2R[:s]
        y2L = y2L[:s]
        y4R = y4R[:s]
        y4L = y4L[:s]
        allYs = {'Diff': {'yR': yR, 'yL': yL},
                 'Acc': {'yR': y4R, 'yL': y4L},
                 'Dist': {'yR': y2R, 'yL': y2L}}
        timeSize = self.timeSize
        # classPreVecR = movingwindowSum(yR, timeSize)
        classPreVecR = yR
        y3R = np.stack((encodeVector2LogitVector(classPreVecR[:, 0], self.minValue, self.maxValue,
                                                 self.classes),
                        encodeVector2LogitVector(classPreVecR[:, 1], self.minValue, self.maxValue,
                                                 self.classes),
                        encodeVector2LogitVector(classPreVecR[:, 2], self.minValue, self.maxValue,
                                                 self.classes)), axis=1)
        # classPreVecL = movingwindowSum(yL, timeSize)
        classPreVecL = yL
        y3L = np.stack((encodeVector2LogitVector(classPreVecL[:, 0], self.minValue, self.maxValue,
                                                 self.classes),
                        encodeVector2LogitVector(classPreVecL[:, 1], self.minValue, self.maxValue,
                                                 self.classes),
                        encodeVector2LogitVector(classPreVecL[:, 2], self.minValue, self.maxValue,
                                                 self.classes)), axis=1)
        allYs['Discrete'] = {'yR': y3R, 'yL': y3L}
        allYs['TimeSum'] = {'yR': classPreVecR, 'yL': classPreVecL}
        t = np.linspace(0, s / sess.freq, s)
        if filterMat:
            matR = butter_lowpass_filter(matR, 5, sess.freq)
            matL = butter_lowpass_filter(matL, 5, sess.freq)
        # title = '%s %s' % (sub.id, sess.title)
        if returnHS:
            return t, matR, matL, allYs, sess.rHS, sess.lHS
        else:
            return t, matR, matL, allYs

    def plotExampleWithFn(self, fn, savefilePath=None, returnHS=False, **kwargs):
        # fn must be a function that takes x and returns y
        fout = self.giveExample(returnHS=returnHS)
        if len(fout) is 5:
            t, matR, matL, allYs, title = fout
            rHS = None
            lHS = None
        else:
            t, matR, matL, allYs, rHS, lHS, title = fout
        # allPred = fn(np.concatenate([matR, matL], axis=0), **kwargs)
        yR = allYs[self.y2use]['yR']
        yL = allYs[self.y2use]['yL']
        ypredR = fn(matR, **kwargs)
        ypredL = fn(matL, **kwargs)
        self.makeSessionPlot(t, yR, yL, ypredR, ypredL, title, savefilePath=savefilePath, HSr=rHS, Hsl=lHS)

    def makeSessionPlot(self, t, yR, yL, ypredR, ypredL, title, savefilePath=None, HSr=None, Hsl=None):
        freq = 1.0 / (t[1] - t[0])
        ypredRfilter = butter_lowpass_filter(ypredR, 10, freq, order=5, axis=0)
        ypredLfilter = butter_lowpass_filter(ypredL, 10, freq, order=5, axis=0)
        t2 = t[-ypredR.shape[0]:]
        f, ax = plt.subplots(3, 2, sharex=True)
        f.set_size_inches(16, 10)
        f.suptitle(title)
        labels = ['x', 'y', 'z']
        for i, l in enumerate(labels):
            for j, (s, y, yP, yPf, hs) in enumerate(
                    zip(['R', 'L'], [yR, yL], [ypredR, ypredL], [ypredRfilter, ypredLfilter], [HSr, Hsl])):
                # plot each signal
                ax[i][j].plot(t, y[:, i], color='blue', label='True')
                ax[i][j].plot(t2, yP[:, i], '--r', label='Pred')
                ax[i][j].plot(t2, yPf[:, i], '-.g', label='Filtered')
                ax[i][j].set_title(s + l)
                ax[i][j].legend()
                if hs is not None:
                    for hsTi in hs:
                        ax[i][j].axvline(x=hsTi, color='red')

        if savefilePath is None:
            plt.show()
        else:
            plt.savefig(savefilePath, dpi=300)
            plt.close(f)


class StairDataset(object):
    """docs for StepsDataset"""

    def __init__(self, batchSize, pickleFile, trainsize, testSize, timeSize, channels2use, useSpatial,
                 y2use='Diff', useOnlyRight=False):
        self.stairData = StairmillSpatialDataset(pickleFile, trainsize, testSize, timeSize, channels2use, useSpatial,
                                                 y2use, useOnlyRight=useOnlyRight)
        x = self.stairData.x
        y = self.stairData.allYs[y2use]['y']
        xT = self.stairData.xT
        yT = self.stairData.allYs[y2use]['yT']
        dShape = self.stairData.dataShape
        y = np.squeeze(y[:])
        yT = np.squeeze(yT[:])
        if len(y.shape) is 1:
            y = np.expand_dims(y, axis=1)
            yT = np.expand_dims(yT, axis=1)
        self.batchsize = batchSize
        self.train = self.DatasetSubclass(x=x, y=y, n=batchSize)
        self.test = self.DatasetSubclass(x=xT, y=yT, n=batchSize)
        self.DATA_POINTS = dShape[0] * dShape[1]
        self.DATA_SHAPE = dShape
        self.CLASSES_NUM = self.stairData.allYs[y2use]['outSize']

    class DatasetSubclass(object):
        """docstring for DatasetSubclass"""

        def __init__(self, x, y, n, shuffleAfterEpoch=True):
            """
            Creates dataset for training
            :param x: Inputs to the network
            :param y: Outputs of the network
            :param n: batchsize
            """
            self.shuffleAfterEpoch = shuffleAfterEpoch
            self.x = x
            self.y = y
            self.index = 0
            self.n = n
            self.subX = []
            self.subY = []
            self.sameBatch = True
            self.batchStart = 0
            self.batchEnd = 0

        def nextBatch(self, makeZero=0):
            self.batchStart = self.index
            self.batchEnd = self.index + self.n
            if self.batchEnd > self.x.shape[0]:
                self.batchEnd = self.x.shape[0]
            self.subX = self.x[self.batchStart:self.batchEnd].copy()
            self.subY = self.y[self.batchStart:self.batchEnd].copy()
            if makeZero > 0:
                shu = randomOrder(self.subX.shape[0])[:makeZero]
                for i in shu:
                    nSams = np.random.randint(self.subX.shape[1] - 1)
                    self.subX[i, :nSams, :] = 0
            self.index += self.n
            if self.batchEnd >= self.x.shape[0]:
                # print("Reset index")
                self.index = 0
                self.sameBatch = False
                if self.shuffleAfterEpoch:
                    self.shuffleVars()

        def shuffleVars(self):
            # print('Shuffle dataset')
            shu = randomOrder(self.x.shape[0])
            self.x = self.x[shu, :]
            self.y = self.y[shu, :]
            # print(self.x.shape[0])


class SyncedShoes(object):
    def __init__(self, rightShoe: smartShoe, leftShoe: smartShoe, freq, useSync: bool, filter: bool):
        self.rightShoe = rightShoe
        self.leftShoe = leftShoe

        self.rightShoe.createPandasDataFrame(useSync=useSync, freq=freq, filter=filter)
        self.leftShoe.createPandasDataFrame(useSync=useSync, freq=freq, filter=filter)
        self.syncFound = self.rightShoe.syncFound and self.leftShoe.syncFound

        # Check if sync worked for both
        if useSync and not self.syncFound:
            if self.rightShoe.syncFound:
                s = np.where(np.array(self.rightShoe.sync) == 1)[0]
                tS = int(self.rightShoe.timestamp[s[0]] / 1000.0 * freq)
                self.leftShoe.dataFrame = self.leftShoe.dataFrame.iloc[tS:, :]
                self.leftShoe.dataFrame.index -= self.leftShoe.dataFrame.index[0]
                self.syncFound = True
            elif self.leftShoe.syncFound:
                s = np.where(np.array(self.leftShoe.sync) == 1)[0]
                tS = int(self.leftShoe.timestamp[s[0]] / 1000.0 * freq)
                self.rightShoe.dataFrame = self.rightShoe.dataFrame.iloc[tS:, :]
                self.rightShoe.dataFrame.index -= self.rightShoe.dataFrame.index[0]
                self.syncFound = True

        # Cut dataframe such that they are same length
        le = np.array([self.rightShoe.dataFrame.shape[0], self.leftShoe.dataFrame.shape[0]]).min()
        self.rightShoe.dataFrame = self.rightShoe.dataFrame.iloc[:le, :]
        self.leftShoe.dataFrame = self.leftShoe.dataFrame.iloc[:le, :]


def createDirIfNotExist(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)
