import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from SpatialStair.models import *
from SpatialStair.VRUtils import *

# tf variables and placeholders
kernels1D = [30, 10, 5]
kernels1Dpost = [30, 10, 5]
num_gru_units = [20 for _ in range(1)]
batchSize = 500
# validSubs = [3, 5, 6, 7, 8, 10, 11, 13]
trainsize = 40000
testSize = 10000
modelName = 'StrideLength'
parentPath = 'E:/Drive/JAP2254/Lab/SmartShoes/SPARK/CAT/VRData/'
allSubsPath = parentPath + 'DB2D.pickle'
with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)

channels2use = (3, 4, 5, 6, 7, 8, 9, 10, 11)
dataset = Dataset2D(batchSize, trainsize, testSize, allSubs, channels2use)
loss2use = tf.losses.absolute_difference
useScale = True
print('%d classes found' % dataset.CLASSES_NUM)
model = FilterRNNmodel2D(dataset, kernels1D, kernels1Dpost, num_gru_units, modelName, activationFn=tf.nn.tanh,
                         useScale=useScale, loss2use=loss2use, num_class=dataset.CLASSES_NUM)

maxEpochs = 5000
isRestore = False
restoreCP = False
resultsPath = "E:/VRDL/" + modelName + '/'

createDirIfNotExist(resultsPath)
with open(resultsPath + modelName + 'DBoptions.pickle', 'wb') as f:
    dbOptions = {'channels2use': channels2use, 'useScale': useScale,
                 'num_gru_units': num_gru_units, 'kernels1D': kernels1D, 'kernels1Dpost': kernels1Dpost,
                 'trainSize': trainsize, 'testSize': testSize, 'loss2use': 'tf.nn.l2_loss'}
    pickle.dump(dbOptions, f)
# restoreName = modelName
restoreName = 'chkp-%d' % 999
learn_rate = model.learn_rate
earlyStop = True
pltTestN = False
model.trainNetwork(maxEpochs, dataset, modelName, isRestore, restoreCP, resultsPath, restoreName, learn_rate, earlyStop,
                   pltTestN, initialAccuracy=50.0, saveChk=50)
model.freezeModel(resultsPath, modelName)
