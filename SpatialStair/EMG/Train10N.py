from SpatialStair.EMG.ReadFiles import createSessionDataset
import tensorflow as tf
from tensorflow.python.framework.ops import disable_eager_execution

disable_eager_execution()


def createModel(inputSize, kernels1D, num_gru_units, fullyCon, kernels1Dpost, outputSize, outputFunction,
                learningRate, dropout, loss, metrics):
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=inputSize))
    for k in kernels1D:
        model.add(tf.keras.layers.Conv1D(inputSize[1], k, activation=tf.nn.relu, padding='same'))
        model.add(tf.keras.layers.Dropout(dropout))

    model.add(tf.keras.layers.RNN([tf.keras.layers.GRUCell(n, dropout=dropout) for n in num_gru_units],
                                  return_sequences=True))
    for k in fullyCon:
        model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))
        model.add(tf.keras.layers.Dropout(dropout))

    for k in kernels1Dpost:
        model.add(tf.keras.layers.Conv1D(inputSize[1], k, activation=tf.nn.relu, padding='same'))
        model.add(tf.keras.layers.Dropout(dropout))

    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(outputSize, activation=outputFunction))

    opt = tf.keras.optimizers.Adam(lr=learningRate)

    model.compile(optimizer=opt, loss=loss, metrics=metrics)
    print(model.summary())
    return model


# create dataset
useFiltered = False
outType = 'Phase'
xnames = ['TFL', 'GMed', 'GMax', 'SA', 'RF', 'VL', 'VM', 'BF', 'ES', 'SEM', 'FL', 'TA', 'GasM', 'GasL', 'SOL']
if useFiltered:
    xnames = [xx + '_filt' for xx in xnames]
if 'Phase' in outType:
    ynames = ['Phase']
    loss = 'sparse_categorical_crossentropy'
    metrics = ['accuracy']
    outputFunction = tf.nn.softmax
    outSize = 4
elif 'kin' in outType:
    ynames = ['AnkleRsag', 'AnkleRfro', 'AnkleRhor', 'AnkleLsag', 'AnkleLfro', 'AnkleLhor', 'AnkleRGsag', 'AnkleRGfro',
              'AnkleRGhor', 'AnkleLGsag', 'AnkleLGfro', 'AnkleLGhor', 'KneeRsag', 'KneeRfro', 'KneeRhor', 'KneeLsag',
              'KneeLfro', 'KneeLhor', 'HipRsag', 'HipRfro', 'HipRhor', 'HipLsag', 'HipLfro', 'HipLhor', 'PelvisGsag',
              'PelvisGfro', 'PelvisGhor']
    loss = 'mean_absolute_error'
    metrics = ['mean_squared_error', 'mean_absolute_error']
    outputFunction = None
    outsize = len(ynames)

timeSize = 25

dataset = createSessionDataset(xnames, ynames, timeSize)

# create model
inputSize = [timeSize, len(xnames)]
kernels1D = [15, 10, 5]
num_gru_units = [15 for _ in range(1)]
fullyCon = [30, 64]
kernels1Dpost = [15, 10, 5]
learningRate = 1e-2
dropout = 0.2

model = createModel(inputSize, kernels1D, num_gru_units, fullyCon, kernels1Dpost, outSize, outputFunction, learningRate,
                    dropout, loss, metrics)

batchSize = 10000
maxEpochs = 50

hist = model.fit(dataset['x'], dataset['y'], batch_size=batchSize, epochs=maxEpochs,
                 validation_data=(dataset['xT'], dataset['yT']))

model.save('./SpatialStair/EMG/model.h5')

