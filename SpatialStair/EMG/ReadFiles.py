import pandas
import numpy as np
import pickle
import Tools.NNUtils as ntu


def ReadFiles(dataPath: str = "G:/My Drive/JAP2254/Lab/Stairmill/StairmillData_EMG",
              saveName: str = 'allData') -> pandas.DataFrame:
    """
    This function reads all the csv file in the dataPath and returns a panda dataframe with all the subjects combined
    :param dataPath: path to the folder
    :param saveName: Name of the pickle file to save
    :return: pandas dataframe with subject data
    """

    subjNumber = range(1, 11)
    sessions = ['baseline', 'downward', 'forward', 'incline', 'left', 'right', 'upward']
    levels = [0, 5, 10, 15]
    allData = []
    freq = 100.0
    for s in subjNumber:
        print("Sub %d" % s)
        for sess in sessions:
            for lev in levels:
                if 'baseline' in sess and lev > 0:
                    continue
                fileName = "%s/subject%03d%s%d.csv" % (dataPath, s, sess, lev)
                # read the corresponding csv file
                aux = pandas.read_csv(fileName)
                # add the subjects, session, level, and time columns
                aux['Subject'] = s
                aux['Sess'] = sess
                aux['Level'] = lev
                aux['t'] = np.arange(aux.shape[0]) / freq
                allData.append(aux)
    # concat all the files into a single huge file
    allData2 = pandas.concat(allData)
    # save the data, this is similar to a *.mat file
    with open('%s/%s.pickle' % (dataPath, saveName), 'wb') as f:
        pickle.dump(allData2, f)
    return allData2


def createDataset(allData: pandas.DataFrame, windowSize: int, xNames: iter, yNames: iter, subjects: iter,
                  sessions: iter, levels: iter) -> (np.array, np.array):
    """
    Creates a input (x), output (y) mapping using a subsets of subjects, sessions, and levels
    :param allData: The dataframe created by ReadFiles function
    :param windowSize: Size window for time history
    :param xNames: a list of names of the columns used as input
    :param yNames: a list of names of the columns used for output
    :param subjects: a list of the subject numbers to use
    :param sessions: a list of the sessions to use
    :param levels:  a list of the levels to use
    :return: x, y dataset
    """
    # init variables
    x = []
    y = []
    for s in subjects:
        for se in sessions:
            for lev in levels:
                # create a logic mask of the subject, session and level to be used
                m = np.logical_and(allData['Subject'] == s, allData['Sess'] == se)
                m = np.logical_and(m, allData['Level'] == lev)
                # createbatch function returns a tensor of shape (t, windowSize, features) consisting of a moving window
                x += ntu.createbatch(allData.loc[m, xNames].values, windowSize, True)
                y += ntu.createbatch(allData.loc[m, yNames].values, windowSize, True)
    x = np.stack(x, axis=0)
    y = np.stack(y, axis=0)[:, -1]
    return x, y


def createSessionDataset(xNames: iter, yNames: iter, timeSize: int = 25, saveFileName: str = None) -> dict:
    """
    Creates a dataset using all subjects and all sessions, but uses 10N for testing
    :param xNames: a list of names of the columns used as input
    :param yNames: a list of names of the columns used for output
    :param timeSize: Size window for time history
    :param saveFileName: if a string, it will save the dataset
    :return: a dictionary with the train and test datasets
    """
    dataPath = "G:/My Drive/JAP2254/Lab/Stairmill/StairmillData_EMG"
    saveName = 'allData'
    subjNumber = range(1, 11)
    sessions = ['downward', 'forward', 'incline', 'left', 'right', 'upward']
    levels = [0, 5, 15]

    with open('%s/%s.pickle' % (dataPath, saveName), 'rb') as f:
        alldata = pickle.load(f)

    x, y = createDataset(alldata, timeSize, xNames, yNames, subjNumber, sessions, levels)
    xT, yT = createDataset(alldata, timeSize, xNames, yNames, subjNumber, sessions, [10])

    db = {'x': x, 'y': y, 'xT': xT, 'yT': yT}
    if saveFileName is not None:
        with open(saveFileName, 'wb') as f:
            pickle.dump(db, f)

    return db
