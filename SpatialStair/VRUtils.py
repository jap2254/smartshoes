import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from Tools.Utils import *


class VRsession(ShoeSubject):
    def __init__(self, leftShoe: smartShoe, rightShoe: smartShoe, subID: int, testType: str = 'Sub',
                 matInfo: MatObject = None, cutSync=False, VRdataset=None):
        ShoeSubject.__init__(self, leftShoe=leftShoe, rightShoe=rightShoe, subID=subID, testType=testType,
                             matInfo=matInfo, cutSync=cutSync)
        self.VRdataset = VRdataset


class VRsubject(object):
    def __init__(self, id):
        self.sessions = {}
        self.x = None
        self.y = None
        self.id = id

    def addSession(self, session: VRsession, name: str):
        self.sessions[name] = session

    def combineSessionsXY(self):
        x = []
        y = []
        for sN in self.sessions:
            x.append(self.sessions[sN].db['x'])
            y.append(self.sessions[sN].db['y'])

        self.x = np.concatenate(x, axis=0)
        self.y = np.concatenate(y, axis=0)


class Dataset2D(DatasetProto):
    def __init__(self, batchSize, trainSize, testSize, allSubs, channels2use):
        useSubTestMode = type(testSize) is not int
        x = []
        y = []
        xT = []
        yT = []
        if useSubTestMode:
            # This means that you want to split
            totalSamsPerSub = np.ceil((testSize + trainSize) / len(allSubs)).astype(np.int)
        else:
            totalSamsPerSub = np.ceil(trainSize / len(allSubs)).astype(np.int)

        for subName in allSubs:
            sub = allSubs[subName]
            if sub.x is None:
                print('%s is empty' % subName)
                continue
            shu = randomOrder(sub.x.shape[0])
            if useSubTestMode and sub.id in testSize:
                # if not useSubTestMode and sessName in testSize:
                xT.append(sub.x[shu[:totalSamsPerSub]])
                yT.append(sub.y[shu[:totalSamsPerSub]])
            else:
                x.append(sub.x[shu[:totalSamsPerSub]])
                y.append(sub.y[shu[:totalSamsPerSub]])

        xx = np.concatenate(x, axis=0)
        shu = randomOrder(xx.shape[0])
        xx = xx[shu][:, :, channels2use]
        yy = np.concatenate(y, axis=0)[shu]

        if useSubTestMode:
            xxT = np.concatenate(xT, axis=0)[:, :, channels2use]
            yyT = np.concatenate(yT, axis=0)
        else:
            xxT = xx[-testSize:].copy()
            yyT = yy[-testSize:].copy()
            xx = xx[:testSize]
            yy = yy[:testSize]

        DatasetProto.__init__(self, xx, yy, xxT, yyT, batchSize)
