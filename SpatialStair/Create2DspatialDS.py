import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from SpatialStair.VRUtils import *
from Tools.Utils import *

parentPath = 'E:/Drive/JAP2254/Lab/SmartShoes/SPARK/CAT/VRData/'
allSubsPath = parentPath + 'DB2D.pickle'
st = 2
en = 15
freq = 50.0
maxStrideTime = 2.0
subName = 'CAT'
session = ['M', 'W']
allSubs = {}
# allSubs['dataFrame'] = pandas.DataFrame()
redoSubs = True
if redoSubs:
    for j in range(st, en + 1):
        try:
            subID = '%s%03d' % (subName, j)
            allSubs[subID] = VRsubject(subID)
            for s in session:
                print('Doing %s, %s' % (subID, s))
                fileName = parentPath + subID + '/' + s
                [r, _] = binaryFile2python(fileName + '_R.bin', V=2)
                [_, l] = binaryFile2python(fileName + '_L.bin', V=2)
                matO = MatObject(fileName + '.xlsx')
                allSubs[subID].addSession(VRsession(l, r, j, subName, matO, cutSync=True, VRdataset=None), s)
                allSubs[subID].sessions[s].make2Ddatabase(freq=freq, maxStrideTime=maxStrideTime,
                                                          maxTime=int(maxStrideTime * freq))
            allSubs[subID].combineSessionsXY()

        except:
            print('Error in %s, %s' % (subID, s))
    with open(allSubsPath, 'wb') as f:
        pickle.dump(allSubs, f)
else:
    with open(allSubsPath, 'rb') as f:
        allSubs = pickle.load(f)
