import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from SpatialStair.StairUtils import *

resPath = 'C:\\Users\\Antonio\\Desktop\\ViconTest\\Sess3'
viconPath = 'E:\\ViconDS\\SpatialIMU\\Sess1\\NewRotationOrder\\'
# viconPath = 'E:/ViconDS/NeckBrace/'
files = ('%s\\%s' % (dirName, fname) for dirName, subdirList, fileList in os.walk(resPath) for fname in fileList)
shoeFilesNames = [fname for dirName, subdirList, fileList in os.walk(resPath) for fname in fileList]
shoeDirNames = [dirName for dirName, subdirList, fileList in os.walk(resPath) for fname in fileList]
viconFiles = (fname for dirName, subdirList, fileList in os.walk(viconPath) for fname in fileList)

########################################## Part 1 ########################################################
#
# viconFiles = ('%s\\%s' % (dirName, fname) for dirName, subdirList, fileList in os.walk(viconPath) for fname in fileList)
# for dirName, subdirList, fileList in os.walk(resPath):
#     print('Found directory: %s' % dirName)
#     for fname in fileList:
#         print('\t%s' % fname)
# for fname in files:
#     fStats = os.stat(fname)
#     if fStats.st_size is 0:
#         os.remove(fname)
#     else:
#         fname2 = fname
#         fname2 = fname2.replace('_R', '')
#         fname2 = fname2.replace('_L', '')
#         os.rename(fname, fname2)

########################################## Part 2 ########################################################

# names = [p + su for p in ['BL', 'BR', 'TR', 'TM', 'TL', 'OP'] for su in ['x', 'y', 'z']]
# for f in viconFiles:
#     if 'csv' in f:
#         print(f)
#         vAux = pandas.read_csv(viconPath + f, names=names, skiprows=5)
#         for ax in ['x', 'y', 'z']:
#             meanNames = [co + ax for co in ['TR', 'BL', 'TL']]
#             vAux['C' + ax] = vAux[meanNames].mean(1)
#
#         # save new dataFrame
#         # newDir = shoeDirNames[np.where([f[:-3] + 'bin' in ff for ff in shoeFilesNames])[0][0]]
#         vAux.to_csv(resPath + '\\' + f)


########################################## Part 3 ########################################################
dataPath = 'E:\\Drive\\JAP2254\\Lab\\Stairmill\\ViconTest3\\'
picklePath = dataPath + 'allSubsIMUframe.pickle'
subjectNames = ['T%d' % (i + 1) for i in range(7)]
# subjectNames = ['Free', 'Front', 'Side', 'Up']

# subjectNames = ['Front', 'Side', 'Up']

allSubs = {}
sessErrors = 0

for i in subjectNames:
    sessNames, ind = np.unique([fname[:-4:] for dirName, subdirList, fileList in os.walk(dataPath + i)
                                for fname in fileList], return_index=True)
    sessDirNames = np.array([dirName + '\\' for dirName, subdirList, fileList in os.walk(dataPath + i)
                             for fname in fileList])[ind]
    allSubs[i] = SpaceSubject(sessNames, i, sessDirNames)
    sessErrors += allSubs[i].sessionsErrors

    print('Could not do %d sessions' % sessErrors)
    with open(picklePath, 'wb') as f:
        pickle.dump(allSubs, f)

else:
    with open(picklePath, 'rb') as f:
        allSubs = pickle.load(f)
