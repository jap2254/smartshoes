from scipy.io import loadmat
import numpy as np
import pandas
import pickle

matFile = "E:/Drive/JAP2254/Lab/Stairmill/Proposal/allsubjects_ml.mat"
pickleFile = "E:/Drive/JAP2254/Lab/Stairmill/Proposal/allsubjects_ml.pickle"

data = loadmat(matFile)

angle = data['angle']
force = data['force']

sub2use = ['subject%03d' % j for j in [1, 2, 4, 5, 6, 8, 9, 10, 11, 13]]

anglePD = []
forcePD = []

angleColumns = ['AnkleRcyc', 'AnkleRcycF', 'AnkleRcycH', 'AnkleRcycG', 'AnkleRcycGF', 'AnkleRcycGH', 'AnkleLcyc',
                'AnkleLcycF', 'AnkleLcycH', 'AnkleLcycG', 'AnkleLcycGF', 'AnkleLcycGH', 'KneeRcyc', 'KneeRcycF',
                'KneeRcycH', 'KneeLcyc', 'KneeLcycF', 'KneeLcycH', 'PelvisRcyc', 'PelvisRcycF', 'PelvisRcycH',
                'PelvisGcyc', 'PelvisGcycF', 'PelvisGcycH', 'PelvisLcyc', 'PelvisLcycF', 'PelvisLcycH']

forceColumns = ['Fx', 'Fy', 'Fz', 'Mx', 'My', 'Mz']
sessions = ['baseline', 'upward', 'downward']

for sub in sub2use:
    print('Doing %s' % sub)
    for sess in sessions:
        d = angle[sub][0][0][sess][0][0]
        auxAng = pandas.DataFrame(columns=angleColumns, data=d)
        if 'baseline' in sess:
            for j, n in enumerate(forceColumns):
                auxAng[n] = 0
        else:
            if '005' in sub and 'upw' in sess:
                for j, n in enumerate(forceColumns):
                    auxAng[n] = np.nan
            else:
                d = force[sub][0][0][sess][0][0]
                for j, n in enumerate(forceColumns):
                    auxAng[n] = d[:, j]

        auxAng['Session'] = sess
        auxAng['Sub'] = sub
        anglePD.append(auxAng.copy())

anglePD = pandas.concat(anglePD)
anglePD.reset_index(inplace=True)
anglePD.dropna(how='any', inplace=True)

with open(pickleFile, 'wb') as f:
    pickle.dump(anglePD, f)
