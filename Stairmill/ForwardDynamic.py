import numpy as np
import pandas
import pickle
import Stairmill.models as models
import tensorflow as tf
import Tools.Utils as tu

from tensorflow.python.framework.ops import disable_eager_execution

# tensorboard --logdir=E:/Stairmill/fwk/scalars


disable_eager_execution()

pickleFile = "E:\\Drive\\JAP2254\\Lab\\Stairmill\\Proposal\\allsubjects_ml.pickle"
pickleFileData = "E:\\Drive\\JAP2254\\Lab\\Stairmill\\Proposal\\fwk.pickle"

with open(pickleFile, 'rb') as f:
    anglePD = pickle.load(f)

windowSize = 20

angleColumns = ['AnkleRcyc', 'AnkleRcycF', 'AnkleRcycH',
                # 'AnkleRcycG', 'AnkleRcycGF', 'AnkleRcycGH',
                'AnkleLcyc', 'AnkleLcycF', 'AnkleLcycH',
                # 'AnkleLcycG', 'AnkleLcycGF', 'AnkleLcycGH',
                'KneeRcyc', 'KneeRcycF', 'KneeRcycH',
                'KneeLcyc', 'KneeLcycF', 'KneeLcycH',
                'PelvisRcyc', 'PelvisRcycF', 'PelvisRcycH',
                # 'PelvisGcyc', 'PelvisGcycF', 'PelvisGcycH',
                'PelvisLcyc', 'PelvisLcycF', 'PelvisLcycH']

forceColumns = ['Fx', 'Fy', 'Fz', 'Mx', 'My', 'Mz']

sub2use = ['subject%03d' % j for j in [1, 2, 4, 5, 6, 8, 9, 10, 11, 13]]

sessions = ['baseline', 'upward', 'downward']

resultsParent = "E:\\Stairmill\\"

modelName = 'fwk'
maxEpochs = 500
resultsPath = resultsParent + modelName + '\\'
tu.createDirIfNotExist(resultsPath)
redoDataset = False
if redoDataset:
    x, y = models.CreateDataset(anglePD, angleColumns, forceColumns, windowSize, sub2use, sessions)
    with open(pickleFileData, 'wb') as f:
        pickle.dump({'x': x, 'y': y}, f)
else:
    with open(pickleFileData, 'rb') as f:
        d = pickle.load(f)
    x = d['x']
    y = d['y']

shu = tu.randomOrder(x.shape[0])
x = x[shu]
y = y[shu]

kernels1D = [20, 10, 5]
kernels1Dpost = [20, 10, 5]
fullyCon = [32, 64]
# num_gru_units = None
num_gru_units = [5 for _ in range(1)]

model = models.EncoderDecoderRecurrentModel(kernels1D, kernels1Dpost, num_gru_units, fullyCon, x.shape[1:], y.shape[1],
                                            learningRate=1e-2)

tu.createDirIfNotExist(resultsParent + modelName + '\\scalars\\')
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=resultsParent + modelName + '\\scalars\\')

model.fit(x, y, batch_size=1000, epochs=maxEpochs, validation_split=0.3, callbacks=[tensorboard_callback])

model.save(resultsPath + modelName + '.h5')

