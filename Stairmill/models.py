import numpy as np
import NNUtils as tun
import tensorflow as tf


def CreateDataset(data, inputColums, outputColumns, windowSize, sub2use, sessions):
    x = []
    y = []

    select = lambda sSub, sSess: data.where(np.logical_and(data['Sub'] == sSub, data['Session'] == sSess)).dropna()

    for sub in sub2use:
        print('Doing %s' % sub)
        for sess in sessions:
            auxD = select(sub, sess)
            if auxD.shape[0] < windowSize:
                continue
            x.append(tun.createbatch(auxD[inputColums].values, windowSize))
            y.append(tun.createbatch(auxD[outputColumns].values, windowSize))

    x = np.concatenate(x, axis=0)
    y = np.squeeze(np.concatenate(y, axis=0)[:, -1, :])
    return x, y


class EncoderDecoderRecurrentModel(tf.keras.Sequential):
    def __init__(self, kernels1D, kernels1Dpost, num_gru_units, fullyCon, dataShape, outputShape,
                 learningRate=1e-3, loss='mean_squared_error', metrics=['mean_squared_error', 'mean_absolute_error'],
                 **kwargs):
        super(EncoderDecoderRecurrentModel, self).__init__(**kwargs)
        self.learningRate = learningRate
        self.loss = loss
        self.metrics2use = metrics
        # Encoder
        self.add(tf.keras.layers.Conv1D(dataShape[1], kernels1D[0], activation=tf.nn.relu, padding='same',
                                        input_shape=dataShape))
        for k in kernels1D[1:]:
            self.add(tf.keras.layers.Conv1D(dataShape[1], k, activation=tf.nn.relu, padding='same'))
        # RNN
        self.add(tf.keras.layers.RNN([tf.keras.layers.GRUCell(n, dropout=0.5) for n in num_gru_units],
                                     return_sequences=True))

        # dense layers
        for k in fullyCon:
            self.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        self.add(tf.keras.layers.Dense(outputShape, activation=tf.nn.relu))

        # Decoder
        for k in kernels1Dpost:
            self.add(tf.keras.layers.Conv1D(dataShape[1], k, activation=tf.nn.relu, padding='same'))

        # Flatten
        self.add(tf.keras.layers.Flatten())

        # sigmoid
        self.add(tf.keras.layers.Dense(outputShape, activation=tf.nn.relu))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        self.compile(optimizer=opt, loss=self.loss, metrics=metrics)
