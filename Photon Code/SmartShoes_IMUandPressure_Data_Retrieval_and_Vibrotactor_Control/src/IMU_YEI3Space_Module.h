/*   -
/*
* Project: PD_Shoes_slave_side
* File: IMU_YEI3Space_Module.h
* Description: This module communicates with the YEI Yost 3-space
               IMU. It retrieves IMU accelerometer and gyroscope data.
               This data is available through getter functions
* Author: Robert Carrera
* Date: August 4, 2017


* To-Do:
        -

* History:
Date              Who?              Why?
*******************************************************************************
August 4, 2017      RMC           Created

Sept. 18, 2017      RMC           IMU code separated into its own module, taken out from the project .ino

Sept. 21, 2017      RMC           Code successfully retrieving "accurate"/reasonable accelerometer and gyrometer data in modular code format
*/

#ifndef IMU_YEI3Space_Module
#define IMU_YEI3Space_Module

//variables

//A struct that holds the response data from the IMU, if we've just requested
// a time stamp and data length.
struct structResponseDataWithTimestampAndDataLength {
  uint8_t commandSuccess; //a flag marking if the previous command was successfully interpreted and carried out by the IMU
  uint32_t timeStamp; // the time stamp of this command, returned from the IMU as 4 bytes
  uint8_t dataLength; // the data length of the IMU response following the response header, returned from the IMU as 1 byte
};

// A struct that holds the IMU linear acceleration components
struct structLinearAcceleration {
 //Big Endian
 float ax;
 float ay;
 float az;
};

// A struct that holds the IMU linear acceleration components as a uint16_t
struct structLinearAcceleration_as_int16 {
 //Big Endian
 int16_t ax;
 int16_t ay;
 int16_t az;
};

// A struct that holds the IMU gyrometer angular velocity components
struct structAngularVelocity {
 //Big Endian
 float wx;
 float wy;
 float wz;
};

// A struct that holds the IMU gyrometer angular velocity components as a uint16_t
struct structAngularVelocity_as_int16 {
 //Big Endian
 int16_t wx;
 int16_t wy;
 int16_t wz;
};


//A struct that holds the IMU orientation data in the form of a quaternion/Euler parameters
struct structQuaternion {
 //Big Endian
 float qx;
 float qy;
 float qz;
 float qw;
};

// A struct that holds the IMU orientation data in the form of Euler angles
struct structEulerAngles {
 //Big Endian
 float pitch;
 float yaw;
 float roll;
};

// A struct that holds the IMU orientation data in the form of Euler angles as a uint16_t
struct structEulerAngles_as_int16 {
 //Big Endian
 int16_t roll;
 int16_t pitch;
 int16_t yaw;
};




//functions
void initialize_IMU(); //initialize and configure the IMU, by sending all the configuration commands
//retrieve new data from the IMU via SPI
void retrieve_IMU_Data_Ready_Interrupt_Status(); //get the interrupt data ready interrupt status from the IMU
void retrieve_Updated_Accelerometer_Data_From_IMU(); //obtain new accelerometer data from the IMU via SPI
void retrieve_Updated_Orientation_Data_From_IMU(); //obtain new orientation data from the IMU via SPI
void retrieve_Updated_Gyrometer_Data_From_IMU(); //obtain new gyrometer data from the IMU via SPI
void retrieve_IMU_SerialNumber_From_IMU(); //obtains the IMU serial number via SPI
void tare_IMU(); //tares the IMU at its current orientation...actually, I'm pretty sure we don't need this. So, hold off on implementation
//getter functions
bool get_IMU_Data_Ready_Interrupt_Status(); // returns a bool indicating if the IMU has new data available (true) or not (false)
structEulerAngles* get_IMU_Orientation_Data_EulerAngles(); // returns a pointer to a struct containing the IMU gyrometer data as Euler angles
structLinearAcceleration* get_IMU_Accelerometer_Data(); // returns a pointer to a struct containing the IMU accelerometer data
structAngularVelocity* get_IMU_Gyrometer_Data(); // returns a pointer to a struct containing the IMU accelerometer data
uint8_t* get_IMU_Serial_Number(); //get the IMU serial number, a good way to test if the data coming from the IMU is valid. Should start with 'T', 'S', 'S'



#endif //IMU_YEI3Space_Module
