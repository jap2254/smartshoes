/*
/*
* Project: PD_Shoes_slave_side
* File: shared_Definitions.h
* Description: A shared definitions file

* Author: Robert Carrera
* Date: August 4, 2017

* History:
Date              Who?              Why?
*******************************************************************************
August 4, 2017      RMC           Created

*/

#ifndef shared_Definitions
#define shared_Definitions

//general*********************************************************************************************************
 #define NUMBER_OF_BITS_IN_A_BYTE 8


//****************************************************************************************************************




//Main INO file (UDP comm., etc.)*********************************************************************************
 #define UDP_Buffer_Size 250 //the size of the UDP buffer, which buffers both incoming and
                             //outgoing UDP data
 #define UDP_Tx_Size  150 //the size of the UDP Tx array that we'll that we load with outgoing data,
                            //enough to store two data points from gyro+accelero
 #define UDP_Rx_Size 512 //the size of the receive (Rx) UDP array that we
                               //use to store incoming data.
//****************************************************************************************************************


//IMU_YEI3Space_Module.h******************************************************************************************
 //SPI communication
 #define SPI_CLOCK_SPEED_TO_IMU 2500000 //the clock frequency for SPI communication with the IMU [Hz]
 #define MAX_YEI_DATA_PACKET 255 //the maximum allowed size for a data packet sent to the IMU

 //Configuration Settings
 //IMPORTANT - specify how many response header bytes we'll be requesting***
 #define CURRENT_NUMBER_RESPONSE_HEADER_BYTES 0 //indicate how many response data bytes are currently being requested (e.g. how many bytes appended before each response).
                                                //Fill flag fields and add data lengths to get this number, see immediately below.
 //Response Headers
 #define REQUEST_RESPONSE_HEADER true
 #define NO_RESPONSE_HEADER false
 //Specify which parts of the response header we'll be requesting
 #define RESPONSE_HEADER_SUCCESS_FLAG_REQUESTED true // a flag indicating whether or not we will request a command success flag with the response header (1 byte)
 #define RESPONSE_HEADER_TIME_STAMP_REQUESTED true // a flag indicating whether or not we will request timestamp data with the response header (4 bytes)
 #define RESPONSE_HEADER_COMMAND_ECHO_REQUESTED false // a flag indicating whether or not we will request a command echo with the response header (1 byte)
 #define RESPONSE_HEADER_CHECKSUM_REQUESTED false // a flag indicating whether or not we will request an additive checksum with the response header (1 byte)
 #define RESPONSE_HEADER_LOGICAL_ID_REQUESTED false // a flag indicating whether or not we will request a logical ID with the response header (1 byte)
 #define RESPONSE_HEADER_SERIAL_NUMBER_REQUESTED  false // a flag indicating whether or not we will request the IMU serial number with the response header (4 bytes)
 #define RESPONSE_HEADER_DATA_LENGTH_REQUESTED true // a flag indicating whether or not we will request the response data length with the response header (1 byte)
 //Specify the data lengths of each part of the response header
 #define RH_SUCCESS_FLAG_BYTE_LENGTH 1
 #define RH_TIME_STAMP_BYTE_LENGTH 4
 #define RH_COMMAND_ECHO_BYTE_LENGTH 1
 #define RH_CHECKSUM_BYTE_LENGTH 1
 #define RH_LOGICAL_ID_BYTE_LENGTH 1
 #define RH_SERIAL_NUMBER_BYTE_LENGTH 4
 #define RH_DATA_LENGTH_BYTE_LENGTH 1
 //END configuration settings

 //Index positions in an IMU command bytes
 #define COMMAND_BYTE_INDEX 1 //the byte specifying the specific command is always in the first index of the array, after the start byte
 //Commands to the IMU
 //configuration commands
 #define START_CMD 0xF6 //the first byte of any command sent to the IMU, indicating the start of a command
 #define START_CMD_WITH_REQUESTED_RESPONSE_HEADER 0xF9
 #define CMD_RESPONSE_HEADER_BITFIELD 221 //the command header that specifies that we'll be accessing and setting data bits in the response header bitfield
 #define CMD_SET_ACCELEROMETER_RANGE 121 //
 #define CMD_SET_GYROSCOPE_RANGE 125 //
 #define CMD_SET_COMPASS_RANGE 126
 #define CMD_SET_CALIBRATION_MODE 169
 #define CMD_SET_REFERENCE_VECTOR_MODE 105
 #define CMD_SET_COMPASS_ENABLE 109
 #define CMD_SET_GYROSCOPE_ENABLE 107
 #define CMD_SET_ROTATION_ORDER 0x10
 #define CMD_BEGIN_GYROSCOPE_AUTOCALIBRATION 165
 #define CMD_RESET_FILTER 120
 #define CMD_SET_FILTER_MODE 123 //set the filter mode used for the orientation data - no filter (0), Kalman (1, default mode), Q-COMP (2), Q-GRAD (3)
 #define CMD_SET_OVERSAMPLING_RATE 106 //
#define CMD_TARE_WITH_CURRENT_ORIENTATION 96
 //get data commands
  #define READ_IMU_DATA_READY_INTERRUPT_STATUS 31 //the command header that specifies that the IMU return the status flag indicating if the filter has updated (new data is ready, flag = 1; no new data, flag = 0)
 #define READ_TARED_ORIENTATION_AS_QUATERNION 0 //the command header that specifies that the IMU return the tared orientation as a quaternion
 #define READ_TARED_ORIENTATION_AS_EULER 0x1 //the command header that specifies that the IMU return the tared orientation as a euler angles
 #define READ_CORRECTED_ACCELEROMETER_VECTOR   39 // the command header that specifies that the IMU return the corrected accelerometer vector
 #define READ_CORRECTED_GYROMETER_VECTOR   38 // the command header that specifies that the IMU return the corrected gyrometer vector
 #define GET_IMU_ID_CMD 230 //a command requesting the IMU ID
 //Commands to the IMU - command parameters
 #define ROT_XYZ 0
 #define ROT_YZX 1
 #define ROT_ZXY 2
 #define ROT_ZYX 3
 #define ROT_XZY 4
 #define ROT_YXZ 5
 #define ACCELEROMETER_RANGE_2G 0
 #define ACCELEROMETER_RANGE_4G 1
 #define ACCELEROMETER_RANGE_8G 2
 #define GYROSCOPE_RANGE_250 0
 #define GYROSCOPE_RANGE_500 1
 #define GYROSCOPE_RANGE_2000 2
 #define COMPASS_RANGE_0_8 0
 #define COMPASS_RANGE_1_3 1
 #define COMPASS_RANGE_1_9 2
 #define COMPASS_RANGE_2_5 3
 #define COMPASS_RANGE_4_0 4
 #define COMPASS_RANGE_4_7 5
 #define COMPASS_RANGE_5_6 6
 #define COMPASS_RANGE_8_1 7
 #define CALIBRATION_MODE_BIAS 0
 #define CALIBRATION_MODE_BIAS_SCALE 1
 #define CALIBRATION_MODE_ORTHO 2
 #define REFERENCE_VECTOR_SINGLE_STATIC_MODE 0
 #define REFERENCE_VECTOR_SINGLE_AUTO_MODE 1
 #define REFERENCE_VECTOR_SINGLE_AUTO_CONTINUOUS_MODE 2
 #define REFERENCE_VECTOR_MULTI_REFERENCE_MODE 3
 #define ORIENTATION_FILTER_MODE_NO_FILTER 0
 #define ORIENTATION_FILTER_MODE_KALMAN_FILTER 1
 #define ORIENTATION_FILTER_MODE_QCOMP_FILTER 2
 #define ORIENTATION_FILTER_MODE_QGRAD_FILTER 3
 //Command packet sizes
 #define SIZE_CONFIGURE_RESPONSE_PACKET 6
 #define SIZE_COMMAND_PACKET_NO_DATA_PARAMS 2
 #define SIZE_COMMAND_ONE_DATA_PARAM_PACKET 3
 //Response data
 #define MAX_NUMBER_IMU_RESPONSE_BYTES 48
 #define YEI_RESPONSE_DATA_READY_FLAG 0x01
 //Response data lengths
 #define READ_IMU_DATA_READY_INTERRUPT_STATUS_RESPONSE_LENGTH 1
 #define READ_TARED_ORIENTATION_AS_QUATERNION_RESPONSE_LENGTH 16
 #define READ_CORRECTED_ACCELEROMETER_VECTOR_RESPONSE_LENGTH 12
 #define READ_ANGULAR_VELOCITY_RESPONSE_LENGTH 12
 #define IMU_ID_RESPONSE_LENGTH 32
 //Response Header data
 #define RETURN_SUCCESS_FLAG_IN_HEADER_MASK 0x01 // use to mask the zeroth byte of the response header configuration command. Requests a success/fail to command response with each response header
 #define RETURN_TIMESTAMP_IN_HEADER_MASK 0x02 // use to mask the first byte of the response header configuration command. Requests a timestamp with each response header
 #define RETURN_COMMAND_ECHO_IN_HEADER_MASK 0x04 // use to mask the second byte of the response header configuration command. Requests a command echo with each response header
 #define RETURN_CHECKSUM_IN_HEADER_MASK 0x08 // use to mask the third byte of the response header configuration command. Requests an additive checksum with each response header
 #define RETURN_LOGICALID_IN_HEADER_MASK 0x10 // use to mask the fourth byte of the response header configuration command. Requests a logical ID with each response header
 #define RETURN_SERIAL_NUMBER_IN_HEADER_MASK 0x20 // use to mask the fifth byte of the response header configuration command. Requests the IMU serial number with each response header
 #define RETURN_REQUESTED_DATA_LENGTH_IN_HEADER_MASK 0x40 // use to mask the sixth byte of the response header configuration command. Requests the length of the requested data with each response header

//****************************************************************************************************************

#endif //shared_Definitions
