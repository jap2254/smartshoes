/*   -
/*
* Project: PD_Shoes_slave_side
* File: IMU_YEI3Space_Module.cpp
* Description: This module communicates with the YEI Yost 3-space
               IMU. It retrieves IMU accelerometer and gyroscope data.
               This data is available through getter functions.
* Author: Robert Carrera
* Date: August 4, 2017


* To-Do:
        -

* History:
Date              Who?              Why?
*******************************************************************************
August 4, 2017      RMC           Created

Sept. 18, 2017      RMC           IMU code separated into its own module, taken out from the project .ino

Sept. 21, 2017      RMC           Code successfully retrieving "accurate"/reasonable accelerometer and gyrometer data in modular code format
*/


//Includes**********************************************************************

//include generic libraries
#include "stdlib.h"
#include "Particle.h"
#include "math.h"
#include <algorithm>
#include <string>

//Include needed project header files
#include "IMU_YEI3Space_Module.h" //this module's header file
#include "shared_Definitions.h" //a file containing definitions, easy to reference from many files

//******************************************************************************




 //Definitions******************************************************************



 //******************************************************************************




 //Module-level variables********************************************************


 //structures to hold requested IMU data
 /*Note: using the struct and union data types here allows us to read in data from the IMU and organize it
         into meaningful groups/variables (e.g. acceleration along x-axis, rotation about y-axis)
         using the fewest number of clock cycles/operations.

         Streaming data is read into the xCommand_dataBuffer array of a specific command's corresponding union.
         The union also contains a parent struct xCommandDataStruct, which itself contains a response header struct and a
         command-specific data struct. As all elements in a union share the same memory addresses, the
         data in xCommand_dataBuffer is pointed to by the elements inside xCommandDataStruct, which means
         we've already bucketed the data into meaningful groups/variables.

         For example, the "read tared orientation" command will require its own union to store data.
         Inside the union, we have taredOrientation_dataBuffer (the array that IMU data is written directly into) and
         taredOrientation_DataStruct (the organized structure that shares the same memory as the data in taredOrientation_dataBuffer).

         Notice also that we have a structure for a specific response header. Make sure that the response header struct used matches
         the type of response data requested from the IMU
 */
 //write the IMU corrected acceleration vector data into this union
 union unionIMUAccelerometerData {
  structLinearAcceleration IMUAccelerometerData;
  uint8_t accelerometerVector_responseDataBuffer[sizeof(IMUAccelerometerData)];

 } unionForIMUAccelerometerData;

 //write the IMU tared orientation vector data into this union
 union unionIMUOrientationData {
  structQuaternion IMUOrientationData;
  uint8_t taredOrientation_responseDataBuffer[sizeof(IMUOrientationData)];

 } unionForIMUOrientationData;

 //write the IMU tared orientation vector data into this union
 union unionIMUOrientationData2 {
  structEulerAngles IMUOrientationData;
  uint8_t taredOrientation_responseDataBuffer[sizeof(IMUOrientationData)];

} unionForIMUOrientationData2;


 //write the IMU gyrometer's angular velocity data into this union
 union unionIMUGyrometerData {
   structAngularVelocity IMUGyrometerData;
   uint8_t gyrometerVector_responseDataBuffer[sizeof(IMUGyrometerData)];

 } unionForIMUGyrometerData;


 //IMU and SPI communication
 // I/O
 static int IMUSlaveSelectPin = D5; //the slave select line used for SPI communication with the IMU
 //communication settings
 static uint8_t currentResponseHeaderBitFieldValue; //the current value of the response header bitfield, which specifies the data we request in the response header
 static uint8_t YEIdataPacket[MAX_YEI_DATA_PACKET]; // holds command packet as we build commands. Sent to the IMU to configure IMU and request data
 static uint8_t IMUDummyDataByte = 0xFF; //a dummy data byte to send to the IMU when we're reading in response data
 static uint8_t IMUDummyDataBuffer[MAX_NUMBER_IMU_RESPONSE_BYTES]; //a vector of dummy bytes (0xff) to send to the IMU when we're reading in response data
 static uint8_t IMURxBuffer[MAX_NUMBER_IMU_RESPONSE_BYTES]; //a buffer to hold the response bytes sent from the IMU. Sized to the maximum number of response bytes the 3-Space IMU will send
 static uint8_t IMUIDNumber[IMU_ID_RESPONSE_LENGTH]; //a place to store the IMU ID. A good check to validate SPI communication and IMU data. Should start with "TSS"
 static uint8_t rawTaredOrientationData[READ_TARED_ORIENTATION_AS_QUATERNION_RESPONSE_LENGTH + CURRENT_NUMBER_RESPONSE_HEADER_BYTES];
 static uint8_t rawCorrectedAccelerometerData[READ_CORRECTED_ACCELEROMETER_VECTOR_RESPONSE_LENGTH + CURRENT_NUMBER_RESPONSE_HEADER_BYTES];
 static uint8_t currentTaredOrientation[READ_TARED_ORIENTATION_AS_QUATERNION_RESPONSE_LENGTH + CURRENT_NUMBER_RESPONSE_HEADER_BYTES]; //stores the  most recent tared orientation
 static uint8_t currentCorrectedAccelerometerVector[READ_CORRECTED_ACCELEROMETER_VECTOR_RESPONSE_LENGTH + CURRENT_NUMBER_RESPONSE_HEADER_BYTES]; //stores the most recent corrected accelerometer vector
 static bool SPITransferComplete; //a flag to indicate if the current call to the SPI module's transfer() function has been completed
 //based on the requested response header data, these variables specify where specific data will lie in the IMU response header
 static uint8_t commandSuccessFlagStartIndex; //which index the command success/fail data begins in the returned response header data
 static uint8_t timeStampStartIndex; //which index the time stamp data begins in the returned response header data
 static uint8_t commandEchoStartIndex; //which index the command echo data begins in the returned response header data
 static uint8_t checksumStartIndex; //which index the checksum data begins in the returned response header data
 static uint8_t logicalIDStartIndex; //which index the logical ID data begins in the returned response header data
 static uint8_t IMUSerialNumberStartIndex; //which index the IMU serial number data begins in the returned response header data
 static uint8_t responseDataLengthStartIndex; //which index the response data length data begins in the returned response header data
//data
 structEulerAngles orientationDataAsEulerAngles; // hold the orientation data in Euler angle format
 static uint8_t newIMUDataReady; //a flag to indicate if the the IMU has new data available (= 1) or not ( = 0 or anything else)
 //END module-level variables******************************************************************************



 //Helper functions ********************************************************
 void initializeConstsAndVars();
 void configurePhotonSPI();
 void YEIconfigureIMU();
 void YEIConfigureResponseHeader();
 void setCurrentResponseHeaderBitFieldValue();
 void YEIwriteCommand(uint8_t cmd, bool responseHeaderRequested);
 void YEIwriteCommandWithSingleParameter(uint8_t cmd, bool responseHeaderRequested, uint8_t value);
 void YEISendCommandAndReadResponse(uint8_t* commandDataPacket, uint8_t commandPacketLength);
 void SPIOnTransferCompletion();
 uint8_t YEIgetCommandResponseDataLength(uint8_t currentCommand, bool responseHeaderRequested);
 void copyIMURxBufferToTarget(uint8_t *target, uint8_t numElementsToCopy);
 void quaternion2euler(float q0,float q1, float q2, float q3, float& phi, float& theta, float& psi);
 void quaternion2euler2(float qx,float qy, float qz, float qw, float& heading, float& attitude, float& bank);
 void reverseFloatElements(uint8_t *arrayWithFloatsToFlip, uint8_t numFloatsToFlip);

 //******************************************************************************


void initialize_IMU(){

  //initialize constant values and variables
  initializeConstsAndVars();

  //  configure this Photon's hardware for SPI communication with the IMU
  configurePhotonSPI();

  // reset and configure the IMU
  YEIconfigureIMU();

}



void retrieve_IMU_Data_Ready_Interrupt_Status(){ //get the interrupt data ready interrupt status from the IMU
  YEIwriteCommand(READ_IMU_DATA_READY_INTERRUPT_STATUS, NO_RESPONSE_HEADER); //get the interrupt flag status from the IMU
  copyIMURxBufferToTarget(&newIMUDataReady, (READ_IMU_DATA_READY_INTERRUPT_STATUS_RESPONSE_LENGTH + CURRENT_NUMBER_RESPONSE_HEADER_BYTES)); //store the new orientation data
}


void retrieve_Updated_Orientation_Data_From_IMU2(){
  YEIwriteCommand(READ_TARED_ORIENTATION_AS_QUATERNION, NO_RESPONSE_HEADER); //get new orientation data from the IMU
  reverseFloatElements(IMURxBuffer, 4);
  copyIMURxBufferToTarget(unionForIMUOrientationData.taredOrientation_responseDataBuffer, (READ_TARED_ORIENTATION_AS_QUATERNION_RESPONSE_LENGTH + CURRENT_NUMBER_RESPONSE_HEADER_BYTES)); //store the new orientation data
  //quaternion2euler(unionForIMUOrientationData.IMUOrientationData.qw, unionForIMUOrientationData.IMUOrientationData.qy, unionForIMUOrientationData.IMUOrientationData.qx, unionForIMUOrientationData.IMUOrientationData.qz, orientationDataAsEulerAngles.yaw, orientationDataAsEulerAngles.pitch, orientationDataAsEulerAngles.roll);
  quaternion2euler2(unionForIMUOrientationData.IMUOrientationData.qx, unionForIMUOrientationData.IMUOrientationData.qy, unionForIMUOrientationData.IMUOrientationData.qz, unionForIMUOrientationData.IMUOrientationData.qw, orientationDataAsEulerAngles.yaw, orientationDataAsEulerAngles.pitch, orientationDataAsEulerAngles.roll);
}

void retrieve_Updated_Orientation_Data_From_IMU(){
  YEIwriteCommand(READ_TARED_ORIENTATION_AS_EULER, NO_RESPONSE_HEADER); //get new orientation data from the IMU
  Serial.print("IMU Rx for orientation   ");
  for (int i = 1; i<13; i++)
  {
    Serial.print(IMURxBuffer[i], HEX);
    Serial.print("   ");
  }
  reverseFloatElements(IMURxBuffer, 3);
  Serial.println("   ");
  uint8_t* pData = (uint8_t*)&orientationDataAsEulerAngles;
  // float myAx[] = {1.0,2.0,3.0};
  // pData = (uint8_t*)myAx;
  memcpy(pData, &IMURxBuffer[1], 12);
  // copyIMURxBufferToTarget(unionForIMUOrientationData2.taredOrientation_responseDataBuffer, (12 + CURRENT_NUMBER_RESPONSE_HEADER_BYTES)); //store the new orientation data
  // orientationDataAsEulerAngles = unionForIMUOrientationData2.IMUOrientationData;
  Serial.println(orientationDataAsEulerAngles.pitch);
  Serial.println(orientationDataAsEulerAngles.yaw);
  Serial.println(orientationDataAsEulerAngles.roll);
  Serial.println();
  // for (int i = 0; i<3; i++)
  // {
  //   Serial.println(myAx[i]);
  // }


}

void retrieve_Updated_Gyrometer_Data_From_IMU(){ //obtain new gyrometer data from the IMU via SPI
  // get accelerometer data
  YEIwriteCommand(READ_CORRECTED_GYROMETER_VECTOR, NO_RESPONSE_HEADER); //get new accelerometer data from the IMU
  // This is for testing only, DANGER make sure it is commented out when not testing
  //YEIwriteCommand(READ_TARED_ORIENTATION_AS_EULER, NO_RESPONSE_HEADER);

  reverseFloatElements(IMURxBuffer, 3);

  copyIMURxBufferToTarget(unionForIMUGyrometerData.gyrometerVector_responseDataBuffer, (READ_ANGULAR_VELOCITY_RESPONSE_LENGTH + CURRENT_NUMBER_RESPONSE_HEADER_BYTES)); //store the new accelerometer data

  /*Serial.print("IMU Rx for orientation");
  Serial.print(unionForIMUGyrometerData.IMUGyrometerData.wx);
  Serial.print("   ");
  Serial.print(unionForIMUGyrometerData.IMUGyrometerData.wy);
  Serial.print("   ");
  Serial.println(unionForIMUGyrometerData.IMUGyrometerData.wz);*/

  /*Serial.print("IMU Rx for orientation   ");
  Serial.print(IMURxBuffer[1]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[2]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[3]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[4]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[5]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[6]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[7]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[8]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[9]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[10]);
  Serial.print("   ");
  Serial.print(IMURxBuffer[11]);
  Serial.print("   ");
  Serial.println(IMURxBuffer[12]);*/
}


void retrieve_Updated_Accelerometer_Data_From_IMU(){
  // get accelerometer data
  YEIwriteCommand(READ_CORRECTED_ACCELEROMETER_VECTOR, NO_RESPONSE_HEADER); //get new accelerometer data from the IMU

  reverseFloatElements(IMURxBuffer, 3);

  copyIMURxBufferToTarget(unionForIMUAccelerometerData.accelerometerVector_responseDataBuffer, (READ_CORRECTED_ACCELEROMETER_VECTOR_RESPONSE_LENGTH + CURRENT_NUMBER_RESPONSE_HEADER_BYTES)); //store the new accelerometer data


}

void retrieve_IMU_SerialNumber_From_IMU(){
  YEIwriteCommand(GET_IMU_ID_CMD, NO_RESPONSE_HEADER); //get the IMU ID number
  copyIMURxBufferToTarget(IMUIDNumber, IMU_ID_RESPONSE_LENGTH); //store IMU ID number in the ID array
}

bool get_IMU_Data_Ready_Interrupt_Status(){
  if(newIMUDataReady == 1){ //if the IMU returned a 1 when we asked if new data is ready, then new data is ready
    return true; //return true, indicating that data is ready
  } else{ //if the IMU returned a 0 when we asked if new data is ready, then new data is not ready. Also, if it returned a value not equal to 1 (but not zero, an abnormality), we assume data is not ready
    return false; //return false, indicating that data is not ready
  }

}

structEulerAngles* get_IMU_Orientation_Data_EulerAngles(){ // returns a pointer to a struct containing the IMU gyrometer data as Euler angles
  return (&orientationDataAsEulerAngles); //return a pointer to the struct containing the most recent IMU gyrometer data
  //return (&unionForIMUOrientationData2.IMUOrientationData);
}


structLinearAcceleration* get_IMU_Accelerometer_Data() {// returns a pointer to a struct containing the IMU accelerometer data
  return(&unionForIMUAccelerometerData.IMUAccelerometerData); //return a pointer to the struct containing the most recent IMU accelerometer data

}

structAngularVelocity* get_IMU_Gyrometer_Data() {// returns a pointer to a struct containing the IMU accelerometer data
  return(&unionForIMUGyrometerData.IMUGyrometerData); //return a pointer to the struct containing the most recent IMU accelerometer data

}

uint8_t* get_IMU_Serial_Number(){
  return IMUIDNumber; //return a pointer to the string containing the data returned from the last request of the IMU's serial sumber (should be the serial number)
}
/*

Function: initializeConstsAndVars()

Parameters: none

Returns: none

Description: Initializes constants and variables that cannot be initialized
             before setup() is called

Notes: Call only once, a setup function

*/
void initializeConstsAndVars(){
 //initialize the IMU dummy data buffer
 for(int i = 0; i < sizeof(IMUDummyDataBuffer); i = i + 1){
   IMUDummyDataBuffer[i] = IMUDummyDataByte; // the dummy data buffer for the IMU should be filled with 0xff
 }

}





/*

Function: configurePhotonSPI()

Parameters: none

Returns: none

Description: Configures the Photon's second SPI module ("SPI1") for use with the YEI 3-Space IMU

Notes: Call only once, a setup function

*/
void configurePhotonSPI(){

  SPI1.setBitOrder(MSBFIRST); //the SPI communication protocol of the IMU requires MSB first
  SPI1.setClockSpeed(SPI_CLOCK_SPEED_TO_IMU); // set the SPI clock speed to the IMU
  SPI1.setDataMode(SPI_MODE0); // set the mode of the SPI communication, i.e. the phase and polarity. YEI 3-space: phase = 0, polarity = 0
  SPI1.begin(); //initializes the Photon hardware to appropriate I/O states. Clock, Tx, SS set to outputs; RX set to input

}


/*

Function: YEIconfigureIMU()()

Parameters: none

Returns: none

Description: Resets the IMU in case it is already running, and
             configures the type of data the IMU collects and the instrument
             parameters and calibration.

Notes: Call only once, a setup function

*/
void YEIconfigureIMU(){

 // configure the extra response headers the IMU will send back with each packet
 YEIConfigureResponseHeader();

 // set instrument measurement ranges, modes, etc.
 //SHOULD all pass argument "NO_RESPONSE_HEADER"
 YEIwriteCommandWithSingleParameter(CMD_SET_ACCELEROMETER_RANGE, NO_RESPONSE_HEADER, ACCELEROMETER_RANGE_4G); //set the accelerometer to read +- 4g's of acceleration
 YEIwriteCommandWithSingleParameter(CMD_SET_GYROSCOPE_RANGE, NO_RESPONSE_HEADER, GYROSCOPE_RANGE_2000);  // set a gyro resolution of 0.070 deg/second with a range of +- 2000 deg/second

 YEIwriteCommandWithSingleParameter(CMD_SET_COMPASS_RANGE, NO_RESPONSE_HEADER, COMPASS_RANGE_1_3); // set the compass to +-1.3 Gauss mode
 YEIwriteCommandWithSingleParameter(CMD_SET_CALIBRATION_MODE, NO_RESPONSE_HEADER, CALIBRATION_MODE_BIAS_SCALE); // set calibration mode to account for both the bias offset and scaling errors in the IMU readings
 YEIwriteCommandWithSingleParameter(CMD_SET_REFERENCE_VECTOR_MODE, NO_RESPONSE_HEADER, REFERENCE_VECTOR_MULTI_REFERENCE_MODE); // set the IMU's vector reference for orientation to multi-reference mode, which is supposed to reduce errors
 YEIwriteCommandWithSingleParameter(CMD_SET_COMPASS_ENABLE, NO_RESPONSE_HEADER, FALSE); // should this be TRUE or FALSE???!!!!! (really, a value of 0 indicates disabled, 1 indicates enabled)
 // I changed the fileter, check if this affects network performance
 //YEIwriteCommandWithSingleParameter(CMD_SET_FILTER_MODE, NO_RESPONSE_HEADER, ORIENTATION_FILTER_MODE_NO_FILTER); //set the filter mode for the IMU
 YEIwriteCommandWithSingleParameter(CMD_SET_FILTER_MODE, NO_RESPONSE_HEADER, ORIENTATION_FILTER_MODE_QGRAD_FILTER); //set the filter mode for the IMU
 YEIwriteCommandWithSingleParameter(CMD_SET_OVERSAMPLING_RATE,NO_RESPONSE_HEADER,1); //set the oversampling rate to sample the sensors only once per filter iteration
 YEIwriteCommandWithSingleParameter(CMD_SET_GYROSCOPE_ENABLE, NO_RESPONSE_HEADER,1);

 YEIwriteCommandWithSingleParameter(CMD_SET_ROTATION_ORDER, NO_RESPONSE_HEADER,ROT_XYZ);
 // re-calibrate gyro
 YEIwriteCommand(CMD_BEGIN_GYROSCOPE_AUTOCALIBRATION, NO_RESPONSE_HEADER); //collect a few frames of gyro data to update the IMU's internal measurement of its offset bias
 YEIwriteCommand(CMD_TARE_WITH_CURRENT_ORIENTATION, NO_RESPONSE_HEADER); //offset orientation
 delay(3000);

 // reset the IMU's Kalman filter
 YEIwriteCommand(CMD_RESET_FILTER, NO_RESPONSE_HEADER); //reset the Kalman filter's covariance and state matrices
 delay(1000);

}







/*

Function: YEIConfigureResponseHeader()

Parameters: none

Returns: none

Description: Builds and sends a command to the IMU which specifies what kinds of
             extra response bytes we want the IMU to send with every timestamp
             (e.g. timestamp, data length, etc.)

Notes: Call only once, a setup function

*/
void YEIConfigureResponseHeader()
{
  //select the response header bitfield values
  setCurrentResponseHeaderBitFieldValue();




	// Settings Header
	YEIdataPacket[0]= START_CMD;
	YEIdataPacket[1]= CMD_RESPONSE_HEADER_BITFIELD;

  //specify the 4 bytes used to control the 32-bit response header bitfield
	YEIdataPacket[2]= currentResponseHeaderBitFieldValue; //can OR request header bit masks here to request response header data
	YEIdataPacket[3]= 0x00; //leave as 0x00, no options
	YEIdataPacket[4]= 0x00; //leave as 0x00, no options
	YEIdataPacket[5]= 0x00; //leave as 0x00, no options
	YEISendCommandAndReadResponse(YEIdataPacket, SIZE_CONFIGURE_RESPONSE_PACKET); //send the data packet to the IMU
}







/*
Function: setCurrentResponseHeaderBitFieldValue()

Parameters: none

Returns: none

Description: Configure the response header bitfield value to reflect the response header data selected
             via the "defines" at the top of this program. This bitfield value is then sent to the IMU
             to configure the response header. This function also calculates the indices at which response
             data will start in the response header.

Notes: Call only once, a setup function

*/
void setCurrentResponseHeaderBitFieldValue(){
  //reset the bit field value to zero
  currentResponseHeaderBitFieldValue = 0;
  //the indices at which response data will start in the response header
// uint8_t currentDataIndexTracker = 0; // we'll increment this as we go

  //mask in the appropriate values, depending on whether or not we requested certain response header data
  if(RESPONSE_HEADER_SUCCESS_FLAG_REQUESTED){
    currentResponseHeaderBitFieldValue |= RETURN_SUCCESS_FLAG_IN_HEADER_MASK; //note that we want a success/failure flag for the previous command
  //  commandSuccessFlagStartIndex = currentDataIndexTracker;
  //  currentDataIndexTracker = currentDataIndexTracker + RH_SUCCESS_FLAG_BYTE_LENGTH;
  }
  if(RESPONSE_HEADER_TIME_STAMP_REQUESTED){
    currentResponseHeaderBitFieldValue |= RETURN_TIMESTAMP_IN_HEADER_MASK; //note that we want a timestamp of when the IMU retrieved the requested data
  //  timeStampStartIndex = currentDataIndexTracker;
  //  currentDataIndexTracker = currentDataIndexTracker + RH_TIME_STAMP_BYTE_LENGTH;
  }
  if(RESPONSE_HEADER_COMMAND_ECHO_REQUESTED){
    currentResponseHeaderBitFieldValue |= RETURN_COMMAND_ECHO_IN_HEADER_MASK; //note that we want a command echo of the previous command
    //commandEchoStartIndex = currentDataIndexTracker;
  //  currentDataIndexTracker = currentDataIndexTracker + RH_COMMAND_ECHO_BYTE_LENGTH;
  }
  if(RESPONSE_HEADER_CHECKSUM_REQUESTED){
    currentResponseHeaderBitFieldValue |= RETURN_CHECKSUM_IN_HEADER_MASK; //note that we want an additive checksum for the current response, not including the response header itself
    //checksumStartIndex = currentDataIndexTracker;
    //currentDataIndexTracker = currentDataIndexTracker + RH_CHECKSUM_BYTE_LENGTH;
  }
  if(RESPONSE_HEADER_LOGICAL_ID_REQUESTED){
    currentResponseHeaderBitFieldValue |= RETURN_LOGICALID_IN_HEADER_MASK; // note that we want a logical ID. For wired comm., always 0xFE
    //logicalIDStartIndex = currentDataIndexTracker;
    //currentDataIndexTracker = currentDataIndexTracker + RH_LOGICAL_ID_BYTE_LENGTH;
  }
  if(RESPONSE_HEADER_SERIAL_NUMBER_REQUESTED){
    currentResponseHeaderBitFieldValue |= RETURN_SERIAL_NUMBER_IN_HEADER_MASK; //note that we want the IMU serial number
    //IMUSerialNumberStartIndex = currentDataIndexTracker;
    //currentDataIndexTracker = currentDataIndexTracker + RH_SERIAL_NUMBER_BYTE_LENGTH;
  }
  if(RESPONSE_HEADER_DATA_LENGTH_REQUESTED){
    currentResponseHeaderBitFieldValue |= RETURN_REQUESTED_DATA_LENGTH_IN_HEADER_MASK; //note that we want the data length of the response data following the response header
    //responseDataLengthStartIndex = currentDataIndexTracker;
  }
}








/*

Function: YEIwriteCommand()

Parameters: uint8_t cmd - the command to be sent to the YEI 3-Space IMU

Returns: none

Description: Writes a command without command data to the YEI 3-Space IMU, without
             enforcing a program delay after writing the command

Notes:

*/
void YEIwriteCommand(uint8_t cmd, bool responseHeaderRequested) {
  if(responseHeaderRequested){ //if a response header is requested
	   YEIdataPacket[0]= START_CMD_WITH_REQUESTED_RESPONSE_HEADER; //then choose the command start byte that requests a response header
  } else { // if we do not want a response header
     YEIdataPacket[0]= START_CMD; //choose the command start byte that does not request a response header
  }
	YEIdataPacket[1]= cmd; // the command byte is the passed-in command
	YEISendCommandAndReadResponse(YEIdataPacket, SIZE_COMMAND_PACKET_NO_DATA_PARAMS);
}




/*

Function: YEIwriteCommand()

Parameters: uint8_t cmd - the command to be sent to the YEI 3-Space IMU
            uint8_t value - a command data field of the given command

Returns: none

Description: Writes a command with a single byte of command data (i.e. value) to
             the YEI 3-Space IMU. Enforces a program delay after writing the command.

Notes:

*/
void YEIwriteCommandWithSingleParameter(uint8_t cmd, bool responseHeaderRequested, uint8_t value) {
  if(responseHeaderRequested){ //if a response header is requested
     YEIdataPacket[0]= START_CMD_WITH_REQUESTED_RESPONSE_HEADER; //then choose the command start byte that requests a response header
  } else { // if we do not want a response header
     YEIdataPacket[0]= START_CMD; //choose the command start byte that does not request a response header
  }
  YEIdataPacket[1]= cmd; // the command byte is the passed-in command
	YEIdataPacket[2]= value;
	YEISendCommandAndReadResponse(YEIdataPacket, SIZE_COMMAND_ONE_DATA_PARAM_PACKET);
}



/*

Function: YEISendCommandAndReadResponse()

Parameters: uint8_t* commandDataPacket: the command portion of the data packet to be sent to the IMU
            uint8_t commandPacketLength: the length of the command portion of the data packet
Returns: none

Description: Carries out the SPI protocol required by the YEI 3-Space IMU to send commands
             and receive the response data. First, send the command. Then send dummy bytes
             until the IMU responds with the data ready flag. Then send the number of dummy
             equal to the number of response data bytes to read in the response.

Notes:

*/
void YEISendCommandAndReadResponse(uint8_t* commandDataPacket, uint8_t commandPacketLength){

//  Serial.print("Inside sendCommandAndReadResponse with command of size: ");
//  Serial.println(sizeof(commandDataPacket));

  //Initialize SPI transfer variables
  uint8_t singleByteRxBuffer; //a one-byte Rx buffer for looking at IMU response bytes, to check for the data ready flag (flag = 0x01)
  bool dataReady = false; // a flag indicating if the data ready flag has been received
  uint8_t commandSpecificResponseDataLength;
  bool responseHeaderRequested = false; //a flag indicating if the current command is configured to request response header bytes. Initialize as false.
  // check to see if a response header was requested
  if(commandDataPacket[0] == START_CMD_WITH_REQUESTED_RESPONSE_HEADER){ // if the first byte of the command indicates that a response header should be sent back
    responseHeaderRequested = true; //then indicate that a response header has been requested
  }
  //get the length of the response for this command, accounting for a response header request
  commandSpecificResponseDataLength = YEIgetCommandResponseDataLength(commandDataPacket[COMMAND_BYTE_INDEX], responseHeaderRequested); // get the length of the IMU response [bytes] for the given command

  //Carry out a data transfer with the IMU ***************************************
  // select the IMU as slave
  digitalWrite(IMUSlaveSelectPin, LOW); //the YEI 3-Space IMU slave select is active low
  //send command packet to IMU
  SPITransferComplete = false; //ensure that the transfer complete flag indicates that the transfer is incomplete before starting transfer
  SPI1.transfer((void*) commandDataPacket, NULL, commandPacketLength, SPIOnTransferCompletion); //maximum 48 response data bytes
  while(SPITransferComplete == false){Particle.connected();} //block until the SPI transfer has finished. Some function must be located inside of the while loop, or the Photon crashes (unclear why)
  //if the command has a response, then read and store it
  uint8_t responseHeaderByteCounter = 0; //a counter to count the number of response header bytes we've received
  if(commandSpecificResponseDataLength > 0){ //if the command has a response
    //Serial.println("IMUu should respond for this command");
    //Send dummy bytes while we read response header bytes and wait for the data ready flag
    while(!dataReady){ //while we are still waiting for the data ready flag from the IMU
      //Serial.println("waiting for IMU to signal that data is ready");
      //Serial.print("expected response length is");
      //Serial.println(commandSpecificResponseDataLength);
      SPITransferComplete = false; //ensure that the transfer complete flag indicates that the transfer is incomplete before starting transfer
      SPI1.transfer((void*) IMUDummyDataBuffer, (void*) &singleByteRxBuffer, 1, SPIOnTransferCompletion); //send a single dummy byte to the IMU, storing response byte in singleByteRxBuffer.    transfer(*IMUDummyDataBuffer)
      while(SPITransferComplete == false){Particle.connected();} //block until the SPI transfer has finished. Some function must be located inside of the while loop, or the Photon crashes (unclear why)
      //Serial.print("byte returned from IMU");
      //Serial.println(singleByteRxBuffer);
      if(singleByteRxBuffer == YEI_RESPONSE_DATA_READY_FLAG || responseHeaderByteCounter >= MAX_YEI_DATA_PACKET-1){ //if the response byte from the IMU is the flag indicating that it will now begin sending the requested data
        dataReady = true; //set the data ready flag
      }
      responseHeaderByteCounter++;
    }
    // read in all of the response data
    SPI1.transfer(IMUDummyDataBuffer, IMURxBuffer, commandSpecificResponseDataLength, NULL); // send the proper number of dummy bytes to read in and store the response data from the IMU
  }
  //deselect the IMU as slave to indicate the end of an SPI transfer
  digitalWrite(IMUSlaveSelectPin, HIGH); //the YEI 3-Space IMU slave select is inactive high
  //End data transfer with IMU***************************************************

}





/*

Function: SPIOnTransferCompletion()

Parameters: none

Returns: none

Description: Sets a flag indicating that an SPI transfer is complete. The Particle
             Photon indicates that we should be able to call SPI.transfer without
             a callback, so that the function does not return until the transfer is
             complete. But this isn't true. We have to have a callback and manually block
             after each call to transfer() until the transfer is complete.

Notes:

*/
void SPIOnTransferCompletion(){

  SPITransferComplete = true; //set the flag indicating the SPI transfer is complete

}



/*

Function: YEIgetCommandResponseDataLength()

Parameters: uint8_t currentCommand: the current command for which we will return the response data length
            bool responseHeaderRequested:a flag indicating if the current command requests a response header (true) or not (false)

Returns: uint8_t lengthOfResponse: the length of the response data (in bytes) for the current command

Description: Returns the length of the response data (in bytes) for the current command. Takes into
             account if a response header was requested with this command.

Notes:

*/
uint8_t YEIgetCommandResponseDataLength(uint8_t currentCommand, bool responseHeaderRequested){

    uint8_t lengthOfResponse= 0; //the length of the respone data (in bytes) for the current command
    //add the command-specific reponse length to the length of the response
    if(currentCommand == READ_TARED_ORIENTATION_AS_QUATERNION){
      lengthOfResponse = lengthOfResponse + READ_TARED_ORIENTATION_AS_QUATERNION_RESPONSE_LENGTH;
    } else if(currentCommand == READ_CORRECTED_ACCELEROMETER_VECTOR){
      lengthOfResponse = lengthOfResponse + READ_CORRECTED_ACCELEROMETER_VECTOR_RESPONSE_LENGTH;
    } else if(currentCommand == READ_TARED_ORIENTATION_AS_EULER){
      lengthOfResponse = lengthOfResponse + READ_CORRECTED_ACCELEROMETER_VECTOR_RESPONSE_LENGTH;
    } else if(currentCommand == READ_CORRECTED_GYROMETER_VECTOR){
      lengthOfResponse = lengthOfResponse + READ_ANGULAR_VELOCITY_RESPONSE_LENGTH;
    } else if(currentCommand == GET_IMU_ID_CMD){ //if we're requesting the IMU's ID number
      lengthOfResponse = lengthOfResponse + IMU_ID_RESPONSE_LENGTH;
    } else if(currentCommand == READ_IMU_DATA_READY_INTERRUPT_STATUS){ //if we're checking if the IMU has new data available
        lengthOfResponse = lengthOfResponse + READ_IMU_DATA_READY_INTERRUPT_STATUS_RESPONSE_LENGTH;
    } else{
      //leave the length of the response as 0
    }

    //account for the response header
    if(responseHeaderRequested){ //if a response header has been requested with this command
        lengthOfResponse = lengthOfResponse + CURRENT_NUMBER_RESPONSE_HEADER_BYTES; //add the number of response header bytes that the IMU currently sends back with each response
    }

    //account for the extra byte that the IMU is in error prepending to each of its responses. As of 9/18/2017, the IMU
    //duplicates the first byte of every response, beginning immediately after the '1' signaling that data
    //is ready to read. So, here, we add an extra byte to the response length IF the respone length
    //is non-zero. We'll read in this extra byte and dispose of it in the program.
    if(lengthOfResponse != 0) { //if the response length is greater than zero, e.g. we want to read in anything at all
      lengthOfResponse = lengthOfResponse + 1; //then add 1 to the length to account for the IMU's erroneous duplication of the first response byte
    }

    return lengthOfResponse; //return the length of the response for the current command

}





/*

Function: copyIMURxBufferToTarget()

Parameters:
            std::vector<uint8_t> &target - the vector we want to copy elements of the
                                           IMU Rx buffer to. Passed by reference.

             uint8_t numElementsToCopy - the number of elements to copy from the IMU
                                         Rx buffer to the target vector


Returns: none

Description: Copies the number of elements specified in "numElementstoCopy" from the IMU
             Rx buffer to the target vector of type uint8_t specified by "target".

Notes:
        - we add a 1 to the Rx buffer index to throw out the duplicate first byte erroneously added by the IMU.
          See YEIgetCommandResponseDataLength() for a further explanation of why this is necessary

*/
void copyIMURxBufferToTarget(uint8_t *target, uint8_t numElementsToCopy){
  memcpy(&target[0],&IMURxBuffer[1],numElementsToCopy);
  //if(sizeof(target) >= numElementsToCopy){
    /*for(int i = 0; i < numElementsToCopy; i = i + 1){
      target[i] = IMURxBuffer[i + 1]; //copy the IMU Rx buffer to the passed-in target array. Note that we add a 1 to the Rx buffer index to throw out the duplicate first byte erroneously added by the IMU
    }*/
  //}

}


/*

Function: quaternion2euler()

Parameters:
            float& q0, q1, q2, q3 - the "euler parameters", or four values of the quaternion

            float& phi, theta, psi - the "euler angles", rotations about three orthogonal axes


Returns: none

Description: Converts the orientation data in the form of the euler parameters/quaternion that the IMU returns into
             Euler angles. Parameters are passed by reference, hence no return values.

Notes:       This function borrowed from the IMU library, so certain operations like flipping
             the signs of the angles will go unexplained.

             phi - rotation about
             theta - rotation about
             psi - rotation about

*/
void quaternion2euler(float q0,float q1, float q2, float q3, float& phi, float& theta, float& psi) {
	//quaternConj(q0,q1,q2,q3);
//DO I NEED TO TAKE THE NEGATIVE AS SEEN BELOW?
	q1=-q1;
  q2=-q2;
	q3=-q3;

  // construct the rotation matrix elements we need to obtain the Euler angles
	float R11 = 2*q0*q0-1+2*q1*q1;
  float R21 = 2*(q1*q2-q0*q3);
	float R31 = 2*(q1*q3+q0*q2);
	float R32 = 2*(q2*q3-q0*q1);
	float R33 = 2*q0*q0-1+2*q3*q3;

  // obtain the Euler angles
	phi = atan2(R32, R33);
	theta = -atan2(R31, sqrt(1-R31*R31));
	psi = atan2(R21, R11);
}

void quaternion2euler2(float qx,float qy, float qz, float qw, float& heading, float& attitude, float& bank) {
	//quaternConj(q0,q1,q2,q3);
  heading = atan2(2*qy*qw-2*qx*qz , 1 - 2*qy*qy - 2*qz*qz);
  attitude = asin(2*qx*qy + 2*qz*qw);
  bank = atan2(2*qx*qw-2*qy*qz , 1 - 2*qx*qx - 2*qz*qz);
}

/*

Function: reverseFloatElements()

Parameters: uint8_t *arrayWithFloatsToFlip - a pointer to the start of the elements of interest that we want to flip. Can be start of array, but
                                             does not have to be the start.

            uint8_t numFloatsToFlip - the number of floats to flip

Returns: none

Description: Flips the order of 4 sequential bytes in order to flip a float from Big-endian to Little-endian, or vice versa.
             Flips the number of floats specified by numFloatsToFlip, starting with the float beginning at the element pointed to
             by arrayWithFloatsToFlip.

Notes:
       - we increment the pointer by 1 to ignore the duplicate first byte erroneously added by the IMU.
         See YEIgetCommandResponseDataLength() for a further explanation of why this is necessary
*/
void reverseFloatElements(uint8_t *arrayWithFloatsToFlip, uint8_t numFloatsToFlip){
  uint8_t* pointerToFirstByteOfFloat;
  for(uint8_t i = 0; i < numFloatsToFlip; i = i + 1){ //for the number of consecutive 4-byte floats we want to flip the order of
    pointerToFirstByteOfFloat = arrayWithFloatsToFlip + 4*i + 1;  // get a pointer to the first byte of the float. Add 1 to ignore the duplicated first byte that the IMU erroneously returns in all responses
    std::reverse(pointerToFirstByteOfFloat, (pointerToFirstByteOfFloat + 4)); //flip the order of the float using std::reverse()
  }

}
