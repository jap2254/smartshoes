/*
* Project: SmartShoes_IMUandPressure_Data_Retrieval_and_Vibrotactor_Control
* Description: Read IMU data and pressure sensor data. Send sensor data from this
Particle Photon to the data-logging microprocessor (currently, the Intel Edison).
This (the Particle Photon) will also control the vibrotactors within its shoe.
* Authors: Robert Carrera, Antonio Prado
* Date: August 4, 2017


* To-Do:
- Troubleshoot so that we can use SPI for both IMU and for data logging
- first, start by ensuring that with SD configuration commented out, we can retrieve IMU data (use logic analyzer)
- then, observe that configuring the SPI for the SD card ruins the SPI configuration for the IMU
- edit the code (likely the SD card code) to make it work in both cases

* Change log:
Date              Who?              Why?
*******************************************************************************
August 4, 2017      RMC           Created

Sept. 18, 2017      RMC           IMU code separated into its own module, taken out from the project .ino

Sept. 21, 2017      RMC           Code successfully retrieving "accurate"/reasonable accelerometer and gyrometer data in modular code format

Sept. 21, 2017      RMC           Merged IMU code with Antonio's high-level code governing sensor data gathering and UDP transmissions. Still not tested

Sept. 26, 2017      RMC           Verified that IMU data is being packed, sent and received via UDP, received data is reasonable



*******************************************************************************



Notes to user:

- if the IMU YEI-3Space module is not properly hooked up and communicating via SPI, the program will hang up waiting for the properly formatted IMU response
- if the microSD card is being used, and if communication with the microSD card cannot be established successfully, the program will hang up


*/
// TODO check recording modes
//Includes**********************************************************************


// This #include statement was automatically added by the Particle IDE.
#include <ntp-time.h>

//using namespace std;

//Include needed project header files
#include "IMU_YEI3Space_Module.h" //we will be communicating with a YEI 3-Space IMU
#include "shared_Definitions.h" //a file containing definitions, easy to reference from many files
//#include "Record_To_Local_MicroSD_Module.h"

//include generic libraries
#include "stdlib.h"
#include "math.h"

#include <stddef.h>
#include <stdint.h>
#include <stdio.h> //print
#include <stdlib.h> //exit(0);

#include "SdFat.h"
//******************************************************************************




//Definitions******************************************************************
#define LOOPMODE 1
/*
LOOPMODE = 0 --> writes to sdcard
LOOPMODE = 1 --> no sd card
*/
#define REWRITE_ALL 0
#define FIRMWARE_VERSION 6
#define TARGET_IS_DATA_LOGGER 0
#define TARGET_IS_PC 1
#define IAMLEFT 1
#define FREQ 50 //the maximum desired frequency of data collection (assuming the IMU update rate is not a limiting factor) [Hz]
#define MAX_DATE 50
#define MOTOR_MBALL_PIN WKP
#define MOTOR_LBALL_PIN RX
#define MOTOR_HEEL_PIN TX
#define PRESSURE_TOE_PIN A0
#define PRESSURE_BALL_PIN A1
#define PRESSURE_HEEL_PIN A6
#define BOARD_LED_PIN D7
#define UDP_PORT 12354   //The port on which to listen for incoming data
#define PORT_SEND 12345 //The port to send data to, e.g. the port the PC is using to receive data
#define BYTE_PACKET 37
#define BYTE_FULL_PACKET 64
#define SEC_TO_MILLISEC 1000
#define SCALING_FLOAT_TO_UINT16 8000 //a scaling that we multiply by the accelerometer and gyrometer data (returned as a float) to convert to uint16_t for sending over UDP
#define CYCLES_TO_WAIT_BETWEEN_CHECKING_FOR_INCOMING_UDP_DATA 20 //the number of program cycles (loop() calls) to wait between checking for incoming UDP data packets
#define MOTOR_NOISE_FREQ  100
#define LEFT_SHOE 108
#define RIGHT_SHOE 114
#define ID 75 // use values between 65-90 and 97-122 to get letters
//
// ------------------ SD stuff -----------------------------------------------
// Size of read/write transfers.
const size_t BUF_SIZE = BYTE_PACKET*512;
// Write pass count.
const uint8_t WRITE_COUNT = 1;

// Read pass count.
const uint8_t READ_COUNT = 1;

// Pick an SPI configuration.
// See SPI configuration section below (comments are for photon).
#define SPI_CONFIGURATION 0
//==============================================================================
// End of configuration constants.
//------------------------------------------------------------------------------
// Setup SPI configuration.
#if SPI_CONFIGURATION == 0
// Primary SPI with DMA
// SCK => A3, MISO => A4, MOSI => A5, SS => A2 (default)
SdFat sd;
const uint8_t chipSelect = SS;
#elif SPI_CONFIGURATION == 1
// Secondary SPI with DMA
// SCK => D4, MISO => D3, MOSI => D2, SS => D1
SdFat sd(1);
const uint8_t chipSelect = D1;
#elif SPI_CONFIGURATION == 2
// Primary SPI with Arduino SPI library style byte I/O.
// SCK => A3, MISO => A4, MOSI => A5, SS => A2 (default)
SdFatLibSpi sd;
const uint8_t chipSelect = SS;
#elif SPI_CONFIGURATION == 3
// Software SPI.  Use any digital pins.
// MISO => D5, MOSI => D6, SCK => D7, SS => D0
SdFatSoftSpi<D5, D6, D7> sd;
const uint8_t chipSelect = D0;
#endif  // SPI_CONFIGURATION

//******************************************************************************

uint8_t buf[BUF_SIZE];
// test file
SdFile file;
// Store error Strings in flash to save RAM.
#define error(s) sd.errorHalt(F(s))
// Serial output stream
ArduinoOutStream cout(Serial);

//Module-level variables********************************************************
//execution flow
static uint8_t cyclesSinceLastCheckForIncomingUDPData; //the number of cycles (calls to loop()) that have occurred since we last checked for incoming UDP data.
static bool waitingForIMUDataReady; //a flag indicating that we're currently waiting for IMU data to be ready, and should handle other processes before checking the IMU data ready flag again
static bool haveCheckedForUDPData; //a flag to ensure that we check for a waiting UDP data packet only once if
static bool isVibrating;
//timing
static unsigned long startTime; //stores the start time of the program [ms]
static unsigned long currentTime; //stores the current time of the program
static unsigned long lastDataRecordTime;
static unsigned long recentVibratorTime; //stores the most recent vibrating start time
static unsigned long controlVibratorTime; //stores the time vibrating should last
//Onboard LED
static unsigned long lastLEDblinkTime;
static bool LEDstate;
//UDP
//Antonio's UDP vars
static char dataCommunicationPacket[BYTE_PACKET];
static char PCIPaddress[] = "192.168.0.101";
static char logIPaddress[] = "192.168.0.101";
static uint8_t PCipInt[] = {192, 168, 0, 255};//send to "slave" photon. { 192, 168, 1, 252}; antonio's PC IP address
static IPAddress PCipObj(PCipInt);
static uint8_t logIpInt[] = {192, 168, 0, 255}; //send to "slave" photon. { 192, 168, 1, 252}; antonio's PC IP address
// static IPAddress logIpObj(logIpInt);
// IPAddress logIpObj;
static IPAddress logIpObj(255, 255, 255, 255);

static NtpTime* ntpTime;
static int16_t gSync;
//my UDP vars
static unsigned int localPort = 12354; // UDP Port used for two way communication
static unsigned int masterUDPPort = 8888; // UDP port used on the master side
static UDP Udp;  // An UDP instance to let us send and receive packets over UDP
static uint8_t masterSideIPAsBytes[] = {192, 168, 0, 255}; //the IP address of the master-side microcontroller
static IPAddress masterSideIPAddress(masterSideIPAsBytes); //the IP address of the master microcontroller
static int masterPort = 8888; //the port we'll be sending the
static IPAddress senderIPAddress; //the IP address of the sender, from the last received data packet
static int senderPort; //the port of the sender, from the last received data packet
static uint8_t UDP_buffer[UDP_Buffer_Size]; //the size of the UDP buffer
static uint8_t UDP_Tx[UDP_Tx_Size]; //the UDP transmit array that we load with outgoing data
static uint8_t UDP_Rx[UDP_Rx_Size]; //the UDP buffer for incoming data
static uint8_t currentTxBufferSize;
//microSD data logging
static bool loggingDataToMicroSD; //indicate if we're currently logging data to microSD (true) or not (false)
static bool loggingFileOpen; //indicate if we're currently logging data to microSD (true) or not (false)
static bool beginNewLoggingFile; //indicate if we want to start writing to a new microSD file when we start logging data since we don't already have one open (true), or we already have a file open (false)
// WHITE NOISE vars
unsigned long lastMtrBlinkTimeArr[3];
float noiseT[3];
float mVals[3];
float mBallVal;
float lBallVal;
float heelVal;
float mtrfreq[3];
int freqMin;
int freqMax;
int mtrPin[3];
bool useNoise;

// structs
struct pressStruct {
  int16_t pToe,pBall,pHeel;
};

struct structDataPacket
{
  uint32_t timestamp;

  pressStruct pressureData;

  structLinearAcceleration_as_int16 accelerationData;

  structAngularVelocity_as_int16 angularVelocityData;

  structEulerAngles_as_int16 orientationDataAsEulerAngles;

  int16_t sync;

  int8_t whichShoeAmI; //which shoe, left or right

  //int16_t d_ax,d_ay, d_az,d_gx,d_gy,d_gz,d_mx,d_my,d_mz,sync;

};

struct configSettingStruct{
  bool collectData, startIMU, streamData;
  float freq;
  float T; //the period for sensor data collection, in units of seconds
  //uint8_t iAmLeft; // a flag indicating if this is the left shoe (1) or not (anything else)
  int8_t myName; //a String specifying the name of this microprocessor
  //char* PCIPaddress; //the PC's (or, generally, the data logger's) IP address
  String fileName;
};

struct savedConfig {
  float freq;
  int8_t id;
};


//high-level settings
static configSettingStruct configSettings; //the current configuration settings for the program behavior

//data storage
static structDataPacket currentSensorData;

//testing
static uint8_t testCounter = 0; //a  test counter

savedConfig savedParams;

//******************************************************************************


//choose the Particle Photon's system mode. ******************************************************************
SYSTEM_MODE(MANUAL); //manual mode means the Photon does NOT automatically connect to the cloud, and we have to establish connnectivity and maintain it with calls to Particle.process(). We
//SYSTEM_THREAD(ENABLED);//don't need cloud connectivity at all though, so we'll avoid all of this.
//******************************************************************************

//SD functions
//------------------------------------------------------------------------------
void cidDmp() {
  cid_t cid;
  if (!sd.card()->readCID(&cid)) {
    error("readCID failed");
  }
  cout << F("\nManufacturer ID: ");
  cout << hex << int(cid.mid) << dec << endl;
  cout << F("OEM ID: ") << cid.oid[0] << cid.oid[1] << endl;
  cout << F("Product: ");
  for (uint8_t i = 0; i < 5; i++) {
    cout << cid.pnm[i];
  }
  cout << F("\nVersion: ");
  cout << int(cid.prv_n) << '.' << int(cid.prv_m) << endl;
  cout << F("Serial number: ") << hex << cid.psn << dec << endl;
  cout << F("Manufacturing date: ");
  cout << int(cid.mdt_month) << '/';
  cout << (2000 + cid.mdt_year_low + 10 * cid.mdt_year_high) << endl;
  cout << endl;
}

void SDstart(){
  // initialize the SD card at SPI_FULL_SPEED for best performance.
  // try SPI_HALF_SPEED if bus errors occur.
  Serial.println("Entered SDstart()");
  if (!sd.begin(chipSelect, SPI_HALF_SPEED)) {
    sd.initErrorHalt();
    Serial.println("SD not started");
  }
  Serial.println("SD started");
  /*
  cout << F("Type is FAT") << int(sd.vol()->fatType()) << endl;
  cout << F("Card size: ") << sd.card()->cardSize()*512E-9;
  cout << F(" GB (GB = 1E9 bytes)") << endl;
  */
  cidDmp();
}

// WifiSetup
void WifiSetup(/* arguments */) {
  int lasDir;
  // if(IAMLEFT==1){
  //   lasDir = 102;
  // } else {
  //   lasDir = 103;
  // }
  // IPAddress myAddress(192,168,0,lasDir);
  // IPAddress netmask(255,255,255,0);
  // IPAddress gateway(192,168,0,1);
  // IPAddress dns(192,168,0,1);
  // WiFi.setStaticIP(myAddress, netmask, gateway, dns);
  //
  // // now let's use the configured IP
  // WiFi.useStaticIP();
  WiFi.useDynamicIP();
  WiFi.connect(); //use stored credentials (loaded onto the Particle Photon) to try to connect to WiFi
  while(!WiFi.ready()){;} //block until this Photon has been assigned an IP address. We may need to do this on a Local Area Network.
}

// setup() runs once, when the device is first turned on.
void setup() {
  Serial.begin(9600);

  //initialize variables
  initializeVariables();

  //startSD

  // SDstart();

  // Choose the settings for this program (data retrieval settings, shoe specifier, etc.)
  chooseProgramSettings();

  // ntp
  ntpTime = new NtpTime(15, PCIPaddress);  // Do an ntp update every 15 minutes;
  ntpTime->start();

  //initialize the hardware needed for SPI comm. with IMU, configure IMU
  initialize_IMU();

  //initialize other hardware
  initializeHardware();
  //connect to WiFi
  WifiSetup();
  //UDP
  Udp.begin(UDP_PORT);

  //initialize microSD module
  //initializeMicroSDModule();
  loggingDataToMicroSD = false; //indicate that we'll start our program logging to a microSD file (true)
  loggingFileOpen = false;
  beginNewLoggingFile = true; //indicate that we want to start writing to a new microSD file when we start logging data, since we don't already have one open (true)

  #if REWRITE_ALL == 1 // write to SD card
    saveParamsEEPROM();
  #else
    loadParamsEEPROM();
  #endif
  //timekeeping
  startTime = millis(); //mark the start of the program so that we can keep track of the time

  Serial.println(WiFi.localIP());
  IPAddress localIP = WiFi.localIP();
  // localIP[3] = 255;
  // logIpObj = IPAddress(logIpInt);
  // static IPAddress logIpObj(logIpInt);

  sayHello();
}

void sayHello(){
  IPAddress localIP = WiFi.localIP();
  uint8_t myLastAddrByte = localIP[3];
  char charArrayToSendViaUDP[100]; //a String to send back to the "master" over UDP
  sprintf(charArrayToSendViaUDP,"Hello from %d, %c in mode %d, V %d, @ %.1f Hz, time is %u",
  myLastAddrByte, (char)configSettings.myName, LOOPMODE, FIRMWARE_VERSION, configSettings.freq,
   millis() - startTime);
  write_udp(charArrayToSendViaUDP, TARGET_IS_PC);
}


// loop() runs over and over again, as quickly as it can execute.
void loop() {


//manage sensor data
manageSensorData(); //if enough time has elapsed since our last transmission of sensor data, read and send current sensor data
manageMotors();
checkForReceivedUDPData();
if(!WiFi.ready()){
  Udp.stop();
  while (!WiFi.ready()) {
    WiFi.connect();}
  }
  Udp.begin(UDP_PORT);

/*
testCounter = testCounter + 1;
//Serial.println("loop");
if(testCounter == 1000) {
Serial.println("Stop");
configSettings.collectData = false;
}
Serial.print("IMU acceleration (x,y,z): ");
Serial.print(currentSensorData.accelerationData.ax);
Serial.print("   ");
Serial.print(currentSensorData.accelerationData.ay);
Serial.print("   ");
Serial.println(currentSensorData.accelerationData.az);

/*Serial.print("IMU angular velocity (wx, wy, wz): ");
Serial.print(currentSensorData.angularVelocityData.wx);
Serial.print("   ");
Serial.print(currentSensorData.angularVelocityData.wy);
Serial.print("   ");
Serial.println(currentSensorData.angularVelocityData.wz);*/

/*Serial.print("IMU orientation (roll, yaw, pitch): ");
Serial.print(currentSensorData.orientationDataAsEulerAngles.roll);
Serial.print("   ");
Serial.print(currentSensorData.orientationDataAsEulerAngles.yaw);
Serial.print("   ");
Serial.println(currentSensorData.orientationDataAsEulerAngles.pitch);

Serial.print("Pressure readings (toe, ball, heel): ");
Serial.print(currentSensorData.pressureData.pToe);
Serial.print("   ");
Serial.print(currentSensorData.pressureData.pBall);
Serial.print("   ");
Serial.println(currentSensorData.pressureData.pToe);*/
}

/*

Function: initializeVariables()

Parameters: none

Returns: none

Description: initialize variables to the values they should have at the start of the program

Notes: An initialization function, should only be called once

*/
void initializeVariables(){
  mtrPin[0] = MOTOR_MBALL_PIN;
  mtrPin[1] = MOTOR_LBALL_PIN;
  mtrPin[3] = MOTOR_HEEL_PIN;
  gSync = -1;
  lastLEDblinkTime = 0;
  recentVibratorTime = 0;
  controlVibratorTime = 0;
  isVibrating = false;
  LEDstate = false;
  lastDataRecordTime = 0;
  waitingForIMUDataReady = false; //should be false here!
  cyclesSinceLastCheckForIncomingUDPData = 0;
  // if(IAMLEFT==1){
  //   currentSensorData.whichShoeAmI = LEFT_SHOE;
  // } else {
  //   currentSensorData.whichShoeAmI = RIGHT_SHOE;
  // }
  #if REWRITE_ALL == 1 // write to SD card
      currentSensorData.whichShoeAmI = ID;
  #endif

  // lastMtrBlinkTime = 0;
  useNoise = false;
  configSettings.fileName = 'F';
  configSettings.streamData = false;
  // noiseT = 1.0 / MOTOR_NOISE_FREQ * 1000
}


/*

Function: chooseProgramSettings()

Parameters: none

Returns: none

Description: Sets the initial settings of the program. Settings include rate of UDP data transmission,
whether or not we should be collecting sensor data, etc. The user should edit this function as
desired to set the initial state of the program.

Notes: An initialization function, should only be called once

*/
void chooseProgramSettings(){
  configSettings.T = 1.0 / FREQ * SEC_TO_MILLISEC;
  configSettings.freq = FREQ;
  configSettings.collectData = false;
  // configSettings.startIMU = true;
  // if (IAMLEFT){
  //   configSettings.myName = LEFT_SHOE;
  // }else {
  //   configSettings.myName = RIGHT_SHOE;
  // }
  configSettings.myName = ID;
  // configSettings.PCIPaddress = PCIPaddress;

}


/*

Function: initializeHardware()

Parameters: none

Returns: none

Description: initializes the hardware needed for program functions handled by this file.
Currently, sets vibrotactor pin states and the LED pin state.

Notes: An initialization function, should only be called once

*/
void initializeHardware(){
  //Motors
  pinMode(MOTOR_MBALL_PIN, OUTPUT);
  pinMode(MOTOR_LBALL_PIN, OUTPUT);
  pinMode(MOTOR_HEEL_PIN, OUTPUT);
  // LED
  pinMode(BOARD_LED_PIN, OUTPUT);

  //Turn vibration motors off to start
  setVibrationAmplitude(0,0,0);
}

/*

Function: manageMotors()

Parameters: none

Returns: none

Description: manages all of the white noise of the motors

Notes:

*/
void manageMotors(){
  if (useNoise){
    currentTime = millis(); //get a timestamp for the start of this loop
    for (int j=0; j<3; j++){
      lastMtrBlinkTimeArr[j] = controlNoise(currentTime, j);
    }
  }
}

/*

Function: manageSensorData1()

Parameters: none

Returns: none

Description: manages all of the sensor data. If enough time has elapsed since the last time we read and sent sensor data,
then mark the time and read and send sensor data again. Also manages the LED that indicates if we're currently
in the "read sensor data" state. It only streams data, no sd save

Notes:

*/
void manageSensorData(){
  if (configSettings.collectData) { //if the flag indicating that we should collect sensor data is set
    currentTime = millis(); //get a timestamp for the start of this loop
    controlLED(1000, currentTime); //While the flag indicating that we should collect sensor data is set, ensure the LED blinks at 0.5Hz, 50% on-time

    //retrieve and send new sensor data, if enough time has elapsed since the last time we sent new data
    if ((currentTime - lastDataRecordTime) >= configSettings.T){ //if the length of time for one data collection period has passed, then we should collect new data
      lastDataRecordTime = currentTime; //update the last time of data collection with the current time
      retrieveCurrentSensorData(); // collect new sensor data, if enough time has elapsed since the last time we collected data
      createDataPacket(dataCommunicationPacket); // create a data packet
      if (configSettings.streamData){
        write_udp(dataCommunicationPacket, TARGET_IS_PC, BYTE_PACKET); // send data to data logging device over UDP
      }
      #if LOOPMODE == 0 // write to SD card
      logDataLocallyToMicroSD(); //log the data locally on a microSD card
      #elif LOOPMODE == 1
      //
      #endif  // loop mode
    }
  } else { //if we're not currently collecting data
  digitalWrite(BOARD_LED_PIN, LOW);  // turn the LED off

  #if LOOPMODE == 0 // write to SD card
  if (loggingFileOpen){
    file.close();
    loggingFileOpen = false;
  }
  #elif LOOPMODE == 1
  //
  #endif  // loop mode

  if (isVibrating){
    currentTime = millis(); //get a timestamp for the start of this loop
    controlVibrator(controlVibratorTime, currentTime);
  }
  // lastDataRecordTime = 0; //reset the data collect time. I THINK WE DONT NEED THIS!!!
}
}

/*

Function: manageSensorData1()

Parameters: none

Returns: none

Description: manages all of the sensor data. If enough time has elapsed since the last time we read and sent sensor data,
then mark the time and read and send sensor data again. Also manages the LED that indicates if we're currently
in the "read sensor data" state. It only streams data, no sd save

Notes:

*/
void manageSensorData_back(){
  if (configSettings.collectData) { //if the flag indicating that we should collect sensor data is set
    currentTime = millis(); //get a timestamp for the start of this loop
    controlLED(1000, currentTime); //While the flag indicating that we should collect sensor data is set, ensure the LED blinks at 0.5Hz, 50% on-time

    //retrieve and send new sensor data, if enough time has elapsed since the last time we sent new data
    //if (((currentTime - lastDataRecordTime)*SEC_TO_MILLISEC) >= configSettings.T){ //if the length of time for one data collection period has passed, then we should collect new data
    //Serial.println("checking for new IMU data");
    if(checkForNewIMUData()){ //block until new IMU data is ready. Then gather sensor data and send over UDP.
      //  Serial.println("IMU DATA READY");
      waitingForIMUDataReady = false; //note that data is ready and we're no longer waiting
      lastDataRecordTime = currentTime; //update the last time of data collection with the current time
      retrieveCurrentSensorData(); // collect new sensor data, if enough time has elapsed since the last time we collected data
      createDataPacket(dataCommunicationPacket); // create a data packet
      if (configSettings.streamData){
        write_udp(dataCommunicationPacket, TARGET_IS_PC, BYTE_PACKET); // send data to data logging device over UDP
      }
      #if LOOPMODE == 0 // write to SD card
      logDataLocallyToMicroSD(); //log the data locally on a microSD card
      #elif LOOPMODE == 1
      //
      #endif  // loop mode

    } else {
      if(waitingForIMUDataReady == false) { //if we have just started waiting for IMU data to be ready (i.e. have only gotten one response from the IMU so far telling us data is not yet ready)
        waitingForIMUDataReady = true; //then we have to wait for the IMU data to be ready, and we can perform other tasks
        haveCheckedForUDPData = false; //note that we now have not yet checked for incoming UDP data during the "waiting for IMU data to be ready" state
      }
    }
  } else { //if we're not currently collecting data
  digitalWrite(BOARD_LED_PIN, LOW);  // turn the LED off

  #if LOOPMODE == 0 // write to SD card
  if (loggingFileOpen){
    file.close();
    loggingFileOpen = false;
  }
  #elif LOOPMODE == 1
  //
  #endif  // loop mode

  if (isVibrating){
    currentTime = millis(); //get a timestamp for the start of this loop
    controlVibrator(controlVibratorTime, currentTime);
  }
  // lastDataRecordTime = 0; //reset the data collect time. I THINK WE DONT NEED THIS!!!
}
}

void openNewFilePrepareToLog(){
  //return;
  // open or create file - truncate existing file.
  String f = String(configSettings.fileName + millis() + configSettings.myName + ".bin");
  //Serial.println(f);
  //if (!file.open(f, O_CREAT | O_TRUNC | O_RDWR)) {
  if (!file.open(f, O_CREAT | O_TRUNC | O_WRITE)) {
    error("open failed");
  }
  loggingFileOpen = true;
}

void logDataToMicroSD(char *bufferToSend){
  int l = file.write(bufferToSend, BYTE_PACKET);
  file.sync();
  //Serial.println(l);

  if (l != BYTE_PACKET) {
    Serial.println('Warning write failed');
    sd.errorPrint("write failed");
    file.close();
    digitalWrite(BOARD_LED_PIN, LOW);
    while (1) {
      ;
    }
  }
}

void logDataLocallyToMicroSD(){
  //store FSLP data locally on a microSD card
  if(loggingDataToMicroSD)
  { //if we're currently logging data to microSD
  //open a new file if necessary
  if(beginNewLoggingFile)
  { //if we currently don't have a microSD file open to log data to
  SDstart();
  delay(1000);
  openNewFilePrepareToLog(); //then open a new file that we can log to
  beginNewLoggingFile = false; //we currently have a file open, so set the flag indicating we need to open a file to false
}
//log a single data point to microSD
logDataToMicroSD(dataCommunicationPacket);
/*
//debugging code!!!!
char c;
if(Serial.available()){
c = tolower(Serial.read());
}
if(c == 'q'){ //close file
closeSDFile();
Serial.println("closing SD file");
}
//end debugging code
*/
}
}

/*

Function: checkForNewIMUData()

Parameters: none

Returns: bool - whether the IMU has new data available (true) or not (false)

Description: first, requests the current status of the interrupt data ready flag from the IMU. Then, retrieves that value from
the IMU module as a boolean

Notes:

*/
bool checkForNewIMUData(){

  retrieve_IMU_Data_Ready_Interrupt_Status(); //request an updated data ready interrupt status flag from the IMU via SPI
  return get_IMU_Data_Ready_Interrupt_Status(); // get the value returned by the IMU. "true" if data is ready, "false" if not

}



/*

Function: retrieveCurrentSensorData()

Parameters: none

Returns: none

Description: retrieves all of the current sensor data (pressure sensor data, IMU data) and
stores it in the data packet struct

Notes:

*/
void retrieveCurrentSensorData(){
  currentSensorData.timestamp = (uint32_t)(currentTime - startTime);
  //pressure sensor data
  readPressureVals(); //read and store new pressure data

  //retrieve all IMU data
  //get IMU serial number
  /*retrieve_IMU_SerialNumber_From_IMU(); //tell the IMU module to request and retrieve the serial number from the IMU
  uint8_t* IMU_Serial_Number = get_IMU_Serial_Number(); //get a pointer to the serial number data, an array of uint8_t with 32 elements*/

  //get IMU gyrometer data
  retrieve_Updated_Gyrometer_Data_From_IMU(); //tell the IMU module to request and retrieve the gyrometer data from the IMU
  structAngularVelocity* currentIMUGyrometerData = get_IMU_Gyrometer_Data(); //get a pointer to the gyrometer data, stored in a struct which contains 3 floats


  //get IMU orientation data
  retrieve_Updated_Orientation_Data_From_IMU(); //tell the IMU module to request and retrieve the gyrometer data from the IMU
  structEulerAngles* currentIMUOrientationDataAsEulerAngles = get_IMU_Orientation_Data_EulerAngles(); //get a pointer to the gyrometer data, stored in a struct which contains 3 floats



  //get IMU accelerometer data
  retrieve_Updated_Accelerometer_Data_From_IMU(); //tell the IMU module to request and retrieve the acceleromater data from the IMU
  structLinearAcceleration* currentIMUAccelerationData = get_IMU_Accelerometer_Data(); //get a pointer to the accelerometer data, stored in a struct which contains 3 floats

  //format the data
  //convert acceleration data to uint16_t format
  currentSensorData.accelerationData.az = (int16_t)(SCALING_FLOAT_TO_UINT16*((*currentIMUAccelerationData).az));
  currentSensorData.accelerationData.ay = (int16_t)(SCALING_FLOAT_TO_UINT16*((*currentIMUAccelerationData).ay));
  currentSensorData.accelerationData.ax = (int16_t)(SCALING_FLOAT_TO_UINT16*((*currentIMUAccelerationData).ax));

  //convert gyrometer data to uint16_t format
  currentSensorData.angularVelocityData.wz = (int16_t)(SCALING_FLOAT_TO_UINT16*((*currentIMUGyrometerData).wz));
  currentSensorData.angularVelocityData.wy = (int16_t)(SCALING_FLOAT_TO_UINT16*((*currentIMUGyrometerData).wy));
  currentSensorData.angularVelocityData.wx = (int16_t)(SCALING_FLOAT_TO_UINT16*((*currentIMUGyrometerData).wx));

  //convert orientation data to uint16_t format
  currentSensorData.orientationDataAsEulerAngles.roll = (int16_t)(SCALING_FLOAT_TO_UINT16*((*currentIMUOrientationDataAsEulerAngles).roll));
  currentSensorData.orientationDataAsEulerAngles.yaw = (int16_t)(SCALING_FLOAT_TO_UINT16*((*currentIMUOrientationDataAsEulerAngles).yaw));
  currentSensorData.orientationDataAsEulerAngles.pitch = (int16_t)(SCALING_FLOAT_TO_UINT16*((*currentIMUOrientationDataAsEulerAngles).pitch));

  //sync???
  currentSensorData.sync = gSync; // assign sync...what is this?

}





/*

Function: readPressureVals()

Parameters: none

Returns: none

Description: reads the analog input values from pressure sensor pins PRESSURE_TOE_PIN,  PRESSURE_BALL_PIN,
and PRESSURE_HEEL_PIN and stores them in the data packet struct

Notes:

*/
void readPressureVals() {
  //read the analog signals from the three pressure sensors
  currentSensorData.pressureData.pToe = analogRead(PRESSURE_TOE_PIN);
  currentSensorData.pressureData.pBall = analogRead(PRESSURE_BALL_PIN);
  currentSensorData.pressureData.pHeel = analogRead(PRESSURE_HEEL_PIN);
}


/*

Function: setVibrationAmplitude()

Parameters: float mBallVal - specifies the duty cycle for the motor on the medial side of the ball of the foot

float lBallVall - specifies the duty cycle for the motor on the lateral side of the ball of the foot

float heelVal - specifies the duty cycle for the motor on the heel of the foot

Returns: none

Description: Sets new duty cycle values for the vibrotactors in the sole of the shoe

Notes:

*/
void setVibrationAmplitude(float mBallVal,float lBallVall, float heelVal){
  /*
  This function sets the dutty cycle for the vibration motors, the inputs are
  percentage values 0 - 100
  */
  int mB = (int)(mBallVal * 2.55);
  int lB = (int)(lBallVall * 2.55);
  int hM = (int)(heelVal * 2.55);
  analogWrite(MOTOR_MBALL_PIN, mB);
  analogWrite(MOTOR_LBALL_PIN, lB);
  analogWrite(MOTOR_HEEL_PIN, hM);
}

/*

Function: setVibrationAmplitudeOneChannel()

Parameters: float dCycle - specifies the duty cycle for the motor

int freq - specifies the duty cycle for the motor on the lateral side of the ball of the foot

int pin - specifies the duty cycle for the motor on the heel of the foot

Returns: none

Description: Sets new duty cycle values for the vibrotactor selected in the sole of the shoe

Notes:

*/
void setVibrationAmplitudeOneChannel(float dCycle,int freq, int pin){
  /*
  This function sets the dutty cycle for the vibration motors, the inputs are
  percentage values 0 - 100
  */
  int r = (int)(dCycle * 2.55);
  analogWrite(pin, r, freq);
}


/*

Function: startRecording()

Parameters: none

Returns: none

Description: called to change the program from a state of not collecting data to
a state of collecting data. Now, the program will read sensor data
and send it over UDP.

Notes:

*/
void startRecording(){
  if (~configSettings.collectData)
  {
    configSettings.collectData = true;
    startTime = millis();
    #if LOOPMODE == 0
    beginNewLoggingFile =  true;
    #elif LOOPMODE == 1
    beginNewLoggingFile =  false;
    #endif  // loop mode

  }

}





/*

Function: stopRecording()

Parameters: none

Returns: none

Description: called to change the program from a state of collecting data to
a state of not collecting data. In this state, sensor data is
no longer read or sent over UDP.

Notes:

*/
void stopRecording(){
  configSettings.collectData = false;

}


float Percentage2Log(float percent)
{
  float weighted;
  if (percent != 100)
  {
    percent = 100 - percent;
    //weighted = 1 - percent/100;
    weighted = 1-(log10(percent)/2);
  }
  else
  {
    weighted = 1;
  }
  return weighted*100;
}

/*

Function: changeVibratorCMD()

Parameters: none

Returns: none

Description: Called only when a UDP command packet has been received by the master,
specifying a new set of duty cycles for the vibrotactors in the shoe.
Changes the vibrotactor duty cycles to the new values.

Notes:

*/
void changeVibratorCMD() {
  bool useLog = (bool)UDP_Rx[10];
  if (useLog){
    mBallVal = Percentage2Log((float)UDP_Rx[4]);
    lBallVal = Percentage2Log((float)UDP_Rx[5]);
    heelVal = Percentage2Log((float)UDP_Rx[6]);
  }else
  {
    mBallVal = (float)UDP_Rx[4];
    lBallVal = (float)UDP_Rx[5];
    heelVal = (float)UDP_Rx[6];
  }
  mVals[0] = mBallVal;
  mVals[1] = lBallVal;
  mVals[2] = heelVal;
  useNoise = (bool)UDP_Rx[7];
  freqMin = (int)UDP_Rx[8];
  freqMax = (int)UDP_Rx[9];
  if (!useNoise)
  setVibrationAmplitude(mBallVal, lBallVal,  heelVal);

}


void controlVibratorCMD() {
  bool useLog = (bool)UDP_Rx[10];
  if (useLog){
    mBallVal = Percentage2Log((float)UDP_Rx[4]);
    lBallVal = Percentage2Log((float)UDP_Rx[5]);
    heelVal = Percentage2Log((float)UDP_Rx[6]);
  }else
  {
    mBallVal = (float)UDP_Rx[4];
    lBallVal = (float)UDP_Rx[5];
    heelVal = (float)UDP_Rx[6];
  }
  mVals[0] = mBallVal;
  mVals[1] = lBallVal;
  mVals[2] = heelVal;
  useNoise = (bool)UDP_Rx[7];
  freqMin = (int)UDP_Rx[8];
  freqMax = (int)UDP_Rx[9];
  if (!useNoise)
  setVibrationAmplitude(mBallVal, lBallVal,  heelVal);
  controlVibratorTime = (uint16_t)(((uint16_t)UDP_Rx[11] << 8) | UDP_Rx[12]);
  recentVibratorTime = millis();
  isVibrating = true;

}


void controlVibrator(unsigned long controlVibratorTime, unsigned long currentTime){
  if (currentTime - recentVibratorTime > controlVibratorTime){
    isVibrating = !isVibrating;
    setVibrationAmplitude(0, 0, 0);
  }

}



/*

Function: sendPressValues()

Parameters: IPAddress *ipAdd -

Returns: none

Description: Constructs a data packet containing only the pressure data

Notes:

*/
void sendPressValues(IPAddress *ipAdd){
  pressStruct p;
  readPressureVals();
  uint8_t dataPacket[13];
  dataPacket[0]=0x0F;
  dataPacket[1]=0x0F;
  dataPacket[2]=0x0A;

  uint8_t* pData = (uint8_t*)&p;
  memcpy(&dataPacket[3],&pData[0],sizeof(p));
  dataPacket[9]=configSettings.myName;

  dataPacket[10]=0x0A;
  dataPacket[11]=0x0B;
  dataPacket[12]=0x0B;

}



/*

Function: controlLED()

Parameters: unsigned long pulseTime - how long the LED should stay on or off for [ms]

unsigned long currentTime - roughly, the current time of the program, measured since the start of the program [ms]

Returns: none

Description: If the LED has not changed states for more than pulseTime, measured in ms, then change its state.

Notes:

*/
void controlLED(unsigned long pulseTime, unsigned long currentTime){
  if (currentTime - lastLEDblinkTime > pulseTime){
    LEDstate = !LEDstate;
    lastLEDblinkTime = currentTime;
    digitalWrite(BOARD_LED_PIN, LEDstate);
  }

}

/*

Function: controlNoise()

Parameters: unsigned long pulseTime - how long the motors should change value [ms]

unsigned long currentTime - roughly, the current time of the program, measured since the start of the program [ms]

Returns: none

Description: If the motors have not changed states for more than pulseTime, measured in ms, then change its state.

Notes:

*/

unsigned long controlNoise(unsigned long currentTime, int j){
  if (currentTime - lastMtrBlinkTimeArr[j] > noiseT[j]){
    // make motor vibrate
    setVibrationAmplitudeOneChannel(mVals[j], mtrfreq[j], mtrPin[j]);
    // calculate next freq and period
    //mtrfreq[j] = 1;
    mtrfreq[j] = random(freqMin,freqMax+1);
    noiseT[j] = 1.0 / mtrfreq[j] * SEC_TO_MILLISEC;
    return currentTime;
  } else{
    return lastMtrBlinkTimeArr[j];
  }

}




/*

Function: createDataPacket()

Parameters: none

Returns: none

Description: Creates a data packet from the current sensor that can be sent over UDP.

Notes:

*/
void createDataPacket(char *dataPacket)
{
  // START
  //why this padding? We could just use a single-byte start delimiter and a checksum (if we wanted error checking)
  dataPacket[0]=0x01;
  dataPacket[1]=0x02;
  dataPacket[2]=0x03;

  uint8_t* pData = (uint8_t*)&currentSensorData;
  memcpy(&dataPacket[3],&pData[0],sizeof(currentSensorData));

  // STOP
  //why this padding? We could just use a single-byte start delimiter and a checksum (if we wanted error checking)
  dataPacket[3 + sizeof(currentSensorData)]=0xA;
  dataPacket[4 + sizeof(currentSensorData)]=0xB;
  dataPacket[5 + sizeof(currentSensorData)]=0xC;
}


/*

Function: checkForReceivedUDPData()

Parameters: none

Returns: none

Description: Checks to see if the UDP hardware has received any UDP data that needs to be processed. If there is data to be processed,
reads and responds to the data

Notes:

*/
void checkForReceivedUDPData() {

  haveCheckedForUDPData = true; //note that we have checked for UDP data while we're waiting for new IMU data to be ready

  // Check if UDP data has been received
  uint16_t receivedPacketSize = Udp.parsePacket(); // prepares the packet to be read, and returns the size of the packet

  if (receivedPacketSize > 0) { //if we do have UDP data waiting to be read
    Serial.println("received a UDP packet");
    //respond to received UDP data
    readIncomingUDPData(receivedPacketSize); //read incoming UDP data from buffer
    interpretReceivedUDPData(receivedPacketSize); //take any necessary action from incoming UDP data
  }
}


/*

Function: readIncomingUDPData()

Parameters: none

Returns: none

Description: Reads UDP data sent from the master shoe Photon. Stores the data in
the UDP_Rx buffer.

Notes:

*/
void readIncomingUDPData(uint16_t receivedPacketSize){

  // Store sender ip and port
  senderIPAddress = Udp.remoteIP();
  senderPort = Udp.remotePort();

  // Read in all waiting data from the UDP buffer
  while(Udp.available()) { //if there is still data in the Rx UDP buffer
    Udp.read(UDP_Rx, receivedPacketSize); //read a character into the Rx buffer
  }

} //end readIncomingUDPData()





/*

Function: interpretReceivedUDPData()

Parameters: none

Returns: none

Description: Responds to incoming UDP data

Notes:

*/
void interpretReceivedUDPData(uint16_t receivedPacketSize){

  if((UDP_Rx[0]==0xA) & (UDP_Rx[1]==0xD) & (UDP_Rx[2]==0xA)){ //if this UDP data packet is from the "master" microcontroller or PC
  respondToMasterUDPCommand(receivedPacketSize);
} else { //if this UDP data packet is from the other shoe (we should explicitly do a check to ensure it's the other shoe)
//dataCommunicationPacket2 = buf;
//memcpy(dataCommunicationPacket2,buf,BYTE_PACKET);
}

}



/*

Function: respondToMasterUDPCommand()

Parameters: int lengthOfIncomingCommandPacket - the number of bytes that the incoming UDP packet
sent by the master contains

Returns: int - indicates success or failure?

Description: Responds to UDP commands sent by the master device, which control settings in this program

Notes:

*/
int respondToMasterUDPCommand(int lengthOfIncomingCommandPacket){

  uint8_t receivedCommand = UDP_Rx[3]; //the command received from the "master" microcontroller/PC. 3 is the position in the incoming UDP packet that specifies the type of command
  char charArrayToSendViaUDP[100]; //a String to send back to the "master" over UDP
  uint8_t newSide = 0;
  bool save = false;
  Serial.print("responding to command ");
  Serial.println(UDP_Rx[3]);
  //sprintf(charArrayToSendViaUDP,"Hello world!!");

  switch(receivedCommand){ //switch depending on the received command
    case 1://ping
    sayHello();
    // sprintf(charArrayToSendViaUDP,"Hello from %s",configSettings.myName.c_str());
    // write_udp(charArrayToSendViaUDP, TARGET_IS_PC);
    break;
    case 2://start recording
    //sprintf(charArrayToSendViaUDP,"Recording Started from %s",configSettings.myName.c_str());
    startRecording();
    sprintf(charArrayToSendViaUDP,"Recording Started from %c",(char)configSettings.myName);
    write_udp(charArrayToSendViaUDP, TARGET_IS_PC);
    break;
    case 3://stop recording
    stopRecording();
    sprintf(charArrayToSendViaUDP,"Recording Stopped from %c",(char)configSettings.myName);
    write_udp(charArrayToSendViaUDP, TARGET_IS_PC);
    break;
    case 4: //change name
    {
      char del[100];
      int loc = (int)UDP_Rx[4];
      sprintf(del, "%%%ds", loc);
      sprintf(charArrayToSendViaUDP,del, &UDP_Rx[5]);
      charArrayToSendViaUDP[loc] = 0;
      configSettings.fileName = charArrayToSendViaUDP;
      write_udp(charArrayToSendViaUDP, TARGET_IS_PC);

      break;
    }
    case 5://change shoe side
    newSide = UDP_Rx[4];
    save = UDP_Rx[5] == 1;
    changeSide(newSide, save, true);
    break;
    case 6://start streaming data
    //startRecording();
    configSettings.streamData = true;
    break;
    case 7://stop streaming data
    //stopRecording();
    configSettings.streamData = false;
    break;
    case 8://changeVibrator
    changeVibratorCMD();
    break;
    case 9://sendPressValues
    sendPressValues(&PCipObj);
    break;
    case 10://enter Safe
    sprintf(charArrayToSendViaUDP,"%c entering Safe mode for Flashing",(char)configSettings.myName);
    write_udp(charArrayToSendViaUDP, TARGET_IS_PC);
    System.enterSafeMode();
    break;
    case 11://reset program
    sprintf(charArrayToSendViaUDP,"%c is resetting",(char)configSettings.myName);
    write_udp(charArrayToSendViaUDP, TARGET_IS_PC);
    System.reset();
    break;
    case 12://sync
    gSync = (int16_t)UDP_Rx[4];
    sprintf(charArrayToSendViaUDP,"%s sync value is %c",(char)configSettings.myName, gSync);
    write_udp(charArrayToSendViaUDP, TARGET_IS_PC);
    break;
    case 13://control vibration
    controlVibratorCMD();
    break;
    case 14://chage freq
    {
    float newFreq = (float)UDP_Rx[4];
    save = UDP_Rx[5] == 1;
    updateFrequency(newFreq, save, true);
    break;
    }
    case 15: // signal LED for 5 seconds
    signal_LED();
    break;
    default: //the default case, if the command value did not match any known command
    sprintf(charArrayToSendViaUDP,"Hello from %c, wrong instruction",(char)configSettings.myName);
    write_udp(charArrayToSendViaUDP, TARGET_IS_PC);
    return 0;
  }

}

LEDStatus blinkRed(RGB_COLOR_RED, LED_PATTERN_BLINK, LED_SPEED_NORMAL, LED_PRIORITY_IMPORTANT);

void signal_LED(){
    blinkRed.setActive(true);
    delay(5000);
    blinkRed.setActive(false);

}


/*

Function: write_udp()

Parameters: char *bufferToSend - a pointer to the first element of the buffer (char array) we want to send over UDP

int targetDevice - a specifier for the target device (e.g., data logger, master PC, or other)

Returns: none

Description: Sends via UDP a single byte pointed to by *bufferToSend, to the target device specified by targetDevice.
Target device is used to choose the target's IP address and port.

Notes:       This function is overloaded

*/
void write_udp(char *bufferToSend, int targetDevice){
  //determine target and address the packet accordingly
  if (targetDevice == TARGET_IS_PC){ //if we're sending the packet to the PC
  Udp.beginPacket(PCipObj, PORT_SEND);
} else if (targetDevice == TARGET_IS_DATA_LOGGER){ //if we're sending the packet to the data logger
Udp.beginPacket(logIpObj, PORT_SEND);
}
//write the UDP packet out
Udp.write(bufferToSend);
//end this UDP transmission
Udp.endPacket();
}


/*

Function: write_udp()

Parameters: char *bufferToSend - a pointer to the first element of the buffer (char array) we want to send over UDP

int targetDevice - a specifier for the target device (e.g., data logger, master PC, or other)

uint8_t numBytesToSend - the number of bytes to send, starting with the one pointed to by *bufferToSend

Returns: none

Description: Sends via UDP the number of bytes specified by numBytesToSend that are stored consecutively in a char array pointed to by *bufferToSend, to
the target device specified by targetDevice. Target device is used to choose the target's IP address and port.

Notes:       This function is overloaded

*/
void write_udp(uint8_t *bufferToSend, int targetDevice, uint8_t numBytesToSend){
  //determine target and address the packet accordingly
  if (targetDevice == TARGET_IS_PC){ //if we're sending the packet to the PC
  Udp.beginPacket(PCipObj, PORT_SEND);
} else if (targetDevice == TARGET_IS_DATA_LOGGER){ //if we're sending the packet to the data logger
Udp.beginPacket(logIpObj, PORT_SEND);
}

//test code
/*Serial.print("data packet is: ");
for(int i = 0; i < (sizeof(currentSensorData) + 6); i = i + 1){
Serial.print(dataCommunicationPacket[i]);
}
Serial.println();*/
//end test code

//write the UDP packet out
Udp.write(bufferToSend, numBytesToSend);
//end this UDP transmission
Udp.endPacket();

}




/*

Function: write_udp()

Parameters: char *bufferToSend - a pointer to the first element of the buffer (char array) we want to send over UDP

int targetDevice - a specifier for the target device (e.g., data logger, master PC, or other)

uint8_t numBytesToSend - the number of bytes to send, starting with the one pointed to by *bufferToSend

Returns: none

Description: Sends via UDP the number of bytes specified by numBytesToSend that are stored consecutively in a char array pointed to by *bufferToSend, to
the target device specified by targetDevice. Target device is used to choose the target's IP address and port.

Notes:       This function is overloaded

*/
void write_udp(char *bufferToSend, int targetDevice, uint8_t numBytesToSend){
  //determine target and address the packet accordingly
  if (targetDevice == TARGET_IS_PC){ //if we're sending the packet to the PC
  Udp.beginPacket(PCipObj, PORT_SEND);
} else { //if we're sending the packet to the data logger //if (targetDevice == TARGET_IS_DATA_LOGGER)
Udp.beginPacket(logIpObj, PORT_SEND);
}

//write the UDP packet out
Udp.write((uint8_t*)dataCommunicationPacket, numBytesToSend);
//end this UDP transmission
Udp.endPacket();

}


/*
Function: changeSide()

Parameters:

int uint8_t - true if I want it to be left

Returns: none

Description: Changes all params so the shoe changes sides

Notes:       This function is overloaded
*/
void changeSide(uint8_t id, bool save, bool sendmsg){
  /*if (left == 1){
    configSettings.myName = 'L';
    currentSensorData.whichShoeAmI = LEFT_SHOE;
  }else{
    currentSensorData.whichShoeAmI = RIGHT_SHOE;
    currentSensorData.whichShoeAmI = 0;
    configSettings.myName = 'R';
  }*/
  configSettings.myName = id;
  currentSensorData.whichShoeAmI = id;
  if (save) {
    saveParamsEEPROM();
  }
  if (sendmsg){
    sayHello();
  }
}

/*
Function: updateFrequency()

Parameters:

float new desired frequency

Returns: none

Description: changes the collection frequency, this should even work on the fly, but it is untested

Notes:       This function is overloaded
*/
void updateFrequency(float newFreq, bool save, bool sendmsg){
  configSettings.T = 1.0 / newFreq * SEC_TO_MILLISEC;
  configSettings.freq = newFreq;
  if (save) {
    saveParamsEEPROM();
  }
  if (sendmsg){
    sayHello();
  }
}

void saveParamsEEPROM() {
  savedParams.freq = configSettings.freq;
  savedParams.id = configSettings.myName;
  EEPROM.put(0, savedParams);
}

void loadParamsEEPROM() {
  EEPROM.get(0, savedParams);
  changeSide(savedParams.id, false, false);
  updateFrequency(savedParams.freq, false, false);
}
