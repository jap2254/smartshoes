import matplotlib.pyplot as plt
import numpy as np
import pandas
import tensorflow as tf

import Tools.NNUtils as nnU
import Tools.Utils as tu


def sparse_categorical_crossentropy(labels, logits):
    return tf.keras.losses.sparse_categorical_crossentropy(labels, logits, from_logits=True)


class HealthyDataset(nnU.DatasetProto):
    def __init__(self, allSubs, trainsize, testSize, batchSize, db2use):
        self.allSubs = allSubs
        xUse = db2use[0]
        yUse = db2use[1]
        x = []
        y = []
        xT = []
        yT = []
        useSubTestMode = type(testSize) is not int
        if useSubTestMode:
            # This means that you want to split
            totalSamsPerSub = np.ceil(trainsize / len(allSubs)).astype(np.int)
        else:
            totalSamsPerSub = np.ceil((testSize + trainsize) / len(allSubs)).astype(np.int)

        for subName in allSubs:
            sub = allSubs[subName]
            if useSubTestMode and subName in testSize:
                xT.append(sub[xUse])
                yT.append(sub[yUse][:, -1])
            else:
                shu = tu.randomOrder(sub[xUse].shape[0])
                x.append(sub[xUse][shu[:totalSamsPerSub]])
                y.append(sub[yUse][shu[:totalSamsPerSub], -1])

        if useSubTestMode:
            xx = np.concatenate(x, axis=0)
            yy = np.concatenate(y, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            x = xx[shu].copy()
            y = yy[shu].copy()
            xx = np.concatenate(xT, axis=0)
            yy = np.concatenate(yT, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            xT = xx[shu]
            yT = yy[shu]
        else:
            xx = np.concatenate(x, axis=0)
            yy = np.concatenate(y, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            xT = xx[shu][trainsize:]
            yT = yy[shu][trainsize:]

            x = xx[shu][:trainsize]
            y = yy[shu][:trainsize]

        numClasses = 1

        nnU.DatasetProto.__init__(self, x, y, xT,
                                  yT,
                                  batchSize, numclases=numClasses)


class modelContainer(object):
    def __init__(self, dataset: nnU.DatasetProto, kernels1D, kernels1Dpost, num_gru_units, fullyCon, containerName):
        self.dataset = dataset
        self.learningRate = 1e-3
        self.loss = 'mean_absolute_error'
        self.metrics = ['mean_squared_error', 'mean_absolute_error']
        self.ED_RNN = self._buildCRNN(kernels1D, kernels1Dpost, num_gru_units, fullyCon, self.learningRate)
        self.ED_class = self._buildCRNNClass(kernels1D, kernels1Dpost, num_gru_units, fullyCon, self.learningRate)
        self.RNN = self._buildRNN(num_gru_units, fullyCon, self.learningRate)
        self.CNN = self._buildCNN(kernels1D, fullyCon, self.learningRate)
        self.Ln = self._buildLn(fullyCon, self.learningRate)
        self.ED_Ln = self._buildEDLn(kernels1D, kernels1Dpost, fullyCon, self.learningRate)
        self.AllModel = {containerName + '_ED_RNN': self.ED_RNN, containerName + '_RNN': self.RNN,
                         containerName + '_Ln': self.Ln, containerName + '_ED_Ln': self.ED_Ln,
                         containerName + '_class': self.ED_class}

    def _buildCRNN(self, kernels1D, kernels1Dpost, num_gru_units, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()
        # Encoder
        model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], kernels1D[0], activation=tf.nn.relu, padding='same',
                                         input_shape=dataset.DATA_SHAPE))
        for k in kernels1D[1:]:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))
        # RNN
        model.add(tf.keras.layers.RNN([tf.keras.layers.GRUCell(n, dropout=0.5) for n in num_gru_units],
                                      return_sequences=True))

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.relu))

        # Decoder
        for k in kernels1Dpost:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # sigmoid
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.sigmoid))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss=self.loss,
                      metrics=self.metrics)

        return model

    def _buildEDLn(self, kernels1D, kernels1Dpost, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()
        # Encoder
        model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], kernels1D[0], activation=tf.nn.relu, padding='same',
                                         input_shape=dataset.DATA_SHAPE))
        for k in kernels1D[1:]:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.relu))

        # Decoder
        for k in kernels1Dpost:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # sigmoid
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.sigmoid))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss=self.loss,
                      metrics=self.metrics)

        return model

    def _buildRNN(self, num_gru_units, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()

        # RNN
        model.add(tf.keras.layers.RNN([tf.keras.layers.GRUCell(n, dropout=0.5) for n in num_gru_units],
                                      return_sequences=True, input_shape=dataset.DATA_SHAPE))

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # sigmoid
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.sigmoid))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss=self.loss,
                      metrics=self.metrics)

        return model

    def _buildCNN(self, kernels1D, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()
        # Encoder
        model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], kernels1D[0], activation=tf.nn.relu, padding='same',
                                         input_shape=dataset.DATA_SHAPE))
        for k in kernels1D[1:]:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        # sigmoid
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.sigmoid))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss=self.loss,
                      metrics=self.metrics)

        return model

    def _buildLn(self, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()

        # Flatten
        model.add(tf.keras.layers.Flatten(input_shape=dataset.DATA_SHAPE))

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        # sigmoid
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.sigmoid))

        opt = tf.keras.optimizers.Adam(lr=learningRate)
        model.compile(optimizer=opt, loss=self.loss,
                      metrics=self.metrics)

        return model

    def _buildCRNNClass(self, kernels1D, kernels1Dpost, num_gru_units, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()
        # Encoder
        model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], kernels1D[0], activation=tf.nn.relu, padding='same',
                                         input_shape=dataset.DATA_SHAPE))
        for k in kernels1D[1:]:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))
        # RNN
        # for n in num_gru_units:
        #     model.add(tf.keras.layers.LSTM(n, recurrent_dropout=0.5))#, return_sequences=True))
        model.add(tf.keras.layers.RNN([tf.keras.layers.GRUCell(n, dropout=0.5) for n in num_gru_units],
                                      return_sequences=True))

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        model.add(tf.keras.layers.Dense(2, activation=tf.nn.relu))
        #
        # # Decoder
        for k in kernels1Dpost:
            model.add(tf.keras.layers.Conv1D(2, k, activation=tf.nn.relu, padding='same'))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # Softmax
        model.add(tf.keras.layers.Dense(2, activation=tf.nn.softmax))
        opt = tf.keras.optimizers.Adam(lr=learningRate)


        # model.compile(optimizer=opt, loss=sparse_categorical_crossentropy,
        #               metrics=['accuracy'])#, experimental_run_tf_function=False)
        model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        return model


def evaluateSubject(lPath, rPath, matPath, model, freq, batchSize, savePlot=None):
    [r, _] = tu.binaryFile2python(rPath)
    [_, l] = tu.binaryFile2python(lPath)
    r.createPandasDataFrame(useSync=True, freq=freq)
    l.createPandasDataFrame(useSync=True, freq=freq)
    matO = tu.MatObject(fName=matPath, freq=freq, newMat=False)
    matO.makeYpercent()
    rBatch = nnU.createbatch(r.dataFrame.values, 50)
    lBatch = nnU.createbatch(l.dataFrame.values, 50)

    # f, ax = plt.subplots(3, 3, sharex=True)
    # cols = [['pToe', 'pBall', 'pHeel'],
    #         ['ax', 'ay', 'az'],
    #         ['EUy', 'EUz', 'EUx']]
    # colors = ['orchid', 'orangered', 'royalblue']
    #
    # for axx, colu, c in zip(ax.T, cols, colors):
    #     for ax1, col in zip(axx, colu):
    #         r.dataFrame[col].plot(ax=ax1, color=c)
    # axx1 = [axpp for axp in ax for axpp in axp]
    # for axx in axx1:
    #     for item in ([axx.title, axx.xaxis.label, axx.yaxis.label] +
    #                  axx.get_xticklabels() + axx.get_yticklabels()):
    #         item.set_fontsize(20)

    # then = time.time()
    # for j in range(100):
    #     model.predict(rBatch[j:j + 2])
    # w = time.time() - then
    # w /= 100
    # f = 1/w
    #
    # print('Run took: %f s or %f Hz' % (w, f))
    if batchSize is None:
        yRp = model.predict(rBatch.reshape(-1, 12*50))
        yLp = model.predict(lBatch.reshape(-1, 12*50))
    else:
        yRp = model.predict(rBatch, batch_size=batchSize)
        yLp = model.predict(lBatch, batch_size=batchSize)
    mLen = min([yRp.shape[0], yLp.shape[0]])
    yR = np.zeros(mLen)
    yR[:mLen] = np.squeeze(yRp[:mLen])
    yL = np.zeros(mLen)
    yL[:mLen] = np.squeeze(yLp[:mLen])
    preds = {'R': yR, 'L': yL}
    t = np.arange(mLen) / freq
    db = {'gt': matO.binaryFunctions, 'pred': pandas.DataFrame(data=preds, index=t), 'laps': matO.lap}
    if savePlot is not None:
        f, ax = plt.subplots(2)
        st = mLen // 2
        en = st + int(10 * freq)
        matO.binaryFunctions['R'].iloc[st:en].plot(ax=ax[0])
        matO.binaryFunctions['L'].iloc[st:en].plot(ax=ax[1])
        db['pred']['R'].iloc[st:en].plot(ax=ax[0])
        db['pred']['L'].iloc[st:en].plot(ax=ax[1])
        plt.savefig(savePlot)
        plt.close()
    return db


def addLapsToEval(matPath, db, freq):
    matO = tu.MatObject(fName=matPath, freq=freq, newMat=False)
    db['laps'] = matO.lap
    return db


def correlateSignals(x, y, maxLags):
    co = np.correlate(x, y, "full")
    a = 1
