import os
import pickle

import tensorflow as tf

import HealthyTools.utils as HTu

data_path = 'Z:/HealthyControl/_Experiments/6MWT/'
# validSubs = [1, 3, 6, 7, 8, 9, 10, 11, 14, 15, 17, 19, 21, 25, 27, 28, 29, 31, 33, 34, 35, 36, 37, 39]
# validSubs = [1]
freq = 100.0
batchSize = 2000
modelParent = 'E:/HealthyP/LOO'
LOOevals = {}
# with open(modelParent + '/evalSubs_noLaps.pickle', 'rb') as f:
#     LOOevals = pickle.load(f)
redo = False
if redo:
    # for k in validSubs:
    for file in os.listdir(modelParent):
        if file.endswith(".h5"):
            subID = file[:6]
            print('Loading: %s' % file)
            lPath = data_path + subID + '/' + subID + 'L.bin'
            rPath = data_path + subID + '/' + subID + 'R.bin'

            matPath = data_path + subID + '/' + subID + '.xlsx'
            modelPath = os.path.join(modelParent, file)
            modelName = file[:-3]
            savePath = modelParent + '/' + modelName + '.png'
            # [r, _] = tu.binaryFile2python(rPath)
            # [_, l] = tu.binaryFile2python(lPath)
            # r.createPandasDataFrame(useSync=True, freq=freq)
            # l.createPandasDataFrame(useSync=True, freq=freq)
            # LOOevals[modelName]['r'] = r
            # LOOevals[modelName]['l'] = l
            model = tf.keras.models.load_model(modelPath)

            LOOevals[modelName] = HTu.evaluateSubject(lPath, rPath, matPath, model, freq, batchSize, savePath)
            del model
        # HSTO = matO.binaryFunctions.copy()

        #
        # f, ax = plt.subplots(2)
        # matO.binaryFunctions['R'].plot(ax=ax[0])
        # matO.binaryFunctions['L'].plot(ax=ax[1])
        #
        # ax[0].plot(r.dataFrame.index.values, yR)
        # ax[1].plot(l.dataFrame.index.values, yL)
    with open(modelParent + '/evalSubs.pickle', 'wb') as f:
        pickle.dump(LOOevals, f)

else:
    with open(modelParent + '/evalSubs_noLaps.pickle', 'rb') as f:
        LOOevals = pickle.load(f)

    for file in os.listdir(modelParent):
        if file.endswith(".h5"):
            subID = file[:6]
            print('Loading: %s' % file)
            matPath = data_path + subID + '/' + subID + '.xlsx'
            modelPath = os.path.join(modelParent, file)
            modelName = file[:-3]
            LOOevals[modelName] = HTu.addLapsToEval(matPath, LOOevals[modelName], freq)

    with open(modelParent + '/evalSubs.pickle', 'wb') as f:
        pickle.dump(LOOevals, f)
