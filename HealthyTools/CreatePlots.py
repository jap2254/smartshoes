import os
import pickle
import warnings

import matplotlib.pyplot as plt
import numpy as np
import pandas
import seaborn as sns

import HealthyTools.utils as HTu
from Tools.NNUtils import readTensorBoard
import Tools.Utils as tu
import Tools.PlotUtils as ptu


def doScalarsPlots(dataParent, regenerate=False):
    # Do training plots
    if regenerate:
        allData = []

        for folder in os.listdir(dataParent):
            subID = folder[:6]
            for file in os.listdir(dataParent + '/' + folder):
                a2 = readTensorBoard(dataParent + '/' + folder + '/' + file)
                a2['SubID'] = subID
                a2['Test'] = folder[7:]
                a2.reset_index(inplace=True)
                allData.append(a2.copy())

        allData = pandas.concat(allData)

        with open(dataParent + '/trainingScalars.pickle', 'wb') as f:
            pickle.dump(allData, f)
    else:
        with open(dataParent + '/trainingScalars.pickle', 'rb') as f:
            allData = pickle.load(f)

    # sns.lineplot(x='index', y='epoch_loss', hue='Test', data=allData)
    # plt.title('Training Mean Absolute Error')
    # plt.xlabel('Epoch')
    # plt.ylabel('MAE')
    # plt.tight_layout()

    allData['order'] = 0
    modelTypes = allData['Test'].unique()
    modelTypeNew = ['EDM', 'ERM', 'DM', 'RM']
    desOrder = [2, 3, 0, 1]

    for oldN, newN, orN in zip(modelTypes, modelTypeNew, desOrder):
        m = allData['Test'] == oldN
        allData.loc[m, 'Test'] = newN
        allData.loc[m, 'order'] = orN

    allData.sort_values(['order', 'SubID', 'index'], inplace=True)



    sns.lineplot(x='index', y='epoch_val_mean_absolute_error', hue='Test', data=allData)
    plt.title('Training LOO Mean Absolute Error')
    plt.xlabel('Epoch')
    plt.ylabel('MAE')
    plt.tight_layout()
    plt.show()

    sns.lineplot(x='index', y='epoch_mean_squared_error', hue='Test', data=allData)
    plt.title('Training  Mean Square Error')
    plt.xlabel('Epoch')
    plt.ylabel('RMS')
    plt.tight_layout()
    plt.show()

    sns.lineplot(x='index', y='epoch_val_mean_squared_error', hue='Test', data=allData)
    plt.title('Training LOO Mean Square Error')
    plt.xlabel('Epoch')
    plt.ylabel('RMS')
    plt.tight_layout()
    plt.show()


def useJustLaps(data):
    freq = 100.0
    gt = data['gt']
    pred = data['pred']
    laps = data['laps']
    # if gt and pred are different lengths, cut them
    mLen = min([gt.shape[0], pred.shape[0]])
    gt = gt.iloc[:mLen, :]
    pred = pred.iloc[:mLen, :]

    df = gt[['R', 'L']].diff()
    newData = []
    for lab in ['R', 'L']:
        # lab = 'R'
        hs = df.where(df == -1)[lab].dropna().index.values
        # hsL = df.where(df == -1)['L'].dropna().index.values
        # hs = hsR
        st = np.diff(hs)
        lapEnd = np.where(st > st.mean())[0]
        lapStart = np.concatenate((np.array([0]), lapEnd + 1))
        lapEnd = np.concatenate((lapEnd, np.array([hs.shape[0] - 1])))

        lapStartI = (hs[lapStart] * freq).astype(np.int)
        lapEndI = (hs[lapEnd] * freq).astype(np.int)

        tI = np.concatenate([np.arange(stt, ent) for stt, ent in zip(lapStartI, lapEndI)])
        pdAux = pandas.DataFrame()
        pdAux['Time'] = tI / freq
        pdAux['gt'] = gt[lab].values[tI]
        pdAux['pred'] = pred[lab].values[tI]
        pdAux['Side'] = lab
        newData.append(pdAux.copy())

    newData = pandas.concat(newData)
    newData.reset_index(inplace=True, drop=True)
    return newData


def calculatenStrides(gt):
    gt = justLaps['gt']
    df = gt.diff()
    df.iloc[0] = -1
    hsRef = df.where(df == -1).dropna().index.values
    return hsRef.shape[0]


def calculateErrors(contData):
    freq = 100.0
    n = 400
    gt = contData['gt']
    pred = contData['pred']
    df = gt.diff()
    df.iloc[0] = -1
    dfPred = pred.diff()
    # dfPred.iloc[0] = -1
    ddfPred = dfPred.diff()
    # ddfPred.iloc[0] = -1
    hsRef = df.where(df == -1).dropna().index.values
    m = np.logical_and(dfPred < -0.2, ddfPred < 0)
    m = m.astype(np.int).diff() > 0
    m.iloc[0] = True
    # m = np.logical_and(np.logical_and(pred < 0.2, dfPred < 0), ddfPred > 0)
    hsPred = df.where(m).dropna().index.values
    if hsPred.shape[0] is 0:
        warnings.warn('No HS detected on test')
        return np.nan, np.nan, hsRef.shape[0], np.nan, np.zeros(n) * np.nan
    falsePos = 0
    falseNeg = 0
    if hsPred.shape[0] > hsRef.shape[0]:
        falsePos += hsPred.shape[0] - hsRef.shape[0]

    match = [np.argmin(np.abs(hs - hsPred)) for hs in hsRef]
    lag = hsRef - hsPred[match]
    falsePos += (np.abs(lag) > 20).sum()
    hsRef = hsRef[np.abs(lag) < 20]
    hsPred = hsPred[match][np.abs(lag) < 20]
    falseNeg += lag.shape[0] - hsRef.shape[0]
    lag = lag[np.abs(lag) < 20]
    assert hsRef.shape[0] == hsPred.shape[0]
    errorDF = pandas.DataFrame(columns=['meanError', 'meanAbsError', 'rms'], index=np.arange(hsRef.shape[0] - 1),
                               dtype=np.float)

    preds = np.zeros(n)
    c = 0
    for hsRs, hsRe, hsPs, hsPe in zip(hsRef[:-1], hsRef[1:], hsPred[:-1], hsPred[1:]):
        refAux = gt[np.arange(hsRs, hsRe)]
        predAux = pred[np.arange(hsPs + 1, hsPe - 1)]
        preds += tu.reSampleMatrix(predAux.values, n)
        m = np.logical_and(refAux > 0.1, refAux < 0.9)
        errorAux = refAux - predAux
        errorAux = refAux[m] - predAux
        errorDF['meanError'][c] = errorAux.mean()
        errorDF['meanAbsError'][c] = np.abs(errorAux).mean()
        errorDF['rms'][c] = np.sqrt(np.power(errorAux, 2).mean())
        c += 1
    lagError = pandas.DataFrame(columns=['meanError', 'meanAbsError', 'rms'], index=[0])
    lagError['meanError'][0] = lag.mean() / freq
    lagError['meanAbsError'][0] = np.abs(lag).mean() / freq
    lagError['rms'][0] = np.sqrt(np.power(lag, 2).mean()) / freq
    predsMean = preds / c
    return errorDF, falsePos, falseNeg, lagError, predsMean


tb_Parent = 'E:/HealthyP/scalars'
# doScalarsPlots(dataParent=tb_Parent, regenerate=True)
evalPath = "E:/HealthyP/Linear/evalSubsLinear.pickle"
lapsPath = "E:/HealthyP/Linear/evalSubsLaps.pickle"
perModelSubPath = "E:/HealthyP/Linear/perModelSub_10_90_mean.pickle"
redoLapsPickle = False
if redoLapsPickle:
    with open(evalPath, 'rb') as f:
        allSubs = pickle.load(f)

    justLaps = []
    for test in allSubs:
        pdAux = useJustLaps(allSubs[test])
        subID = test[:6]
        modelType = test[7:]
        pdAux['SubID'] = subID
        pdAux['Model'] = modelType
        justLaps.append(pdAux.copy())

    justLaps = pandas.concat(justLaps)
    justLaps['Error'] = justLaps['gt'] - justLaps['pred']
    justLaps['Abs_Error'] = np.abs(justLaps['gt'] - justLaps['pred'])
    justLaps['Square_Error'] = np.power(justLaps['gt'] - justLaps['pred'], 2)
    justLaps.reset_index(inplace=True, drop=True)

    with open(lapsPath, 'wb') as f:
        pickle.dump(justLaps, f)
else:
    with open(lapsPath, 'rb') as f:
        justLaps = pickle.load(f)

# y = lambda x: np.arcsin(x-1) + np.arccos(x)
subs = justLaps['SubID'].unique()
modelTypes = justLaps['Model'].unique()
modelTypes = ['ED_RNN']
redoPerModelSub = True
if redoPerModelSub:
    perModelSubsData = []
    perModelSubsDataTraj = []
    # gt = justLaps['gt'].where(justLaps['Model'] == modelTypes[0]).dropna()
    # pred = justLaps['pred'].where(justLaps['Model'] == modelTypes[0]).dropna()
    # tu.plotErrorFigures(y(gt), y(pred))
    for subID in subs:
        for model in modelTypes:
            m = np.logical_and(justLaps['Model'] == model, justLaps['SubID'] == subID)
            # m = np.logical_and(np.logical_and(m, justLaps['gt'] > 0.1), justLaps['gt'] < 0.9)
            s1 = justLaps.where(m).dropna()
            rets = calculateErrors(s1)
            pdAux = pandas.DataFrame(columns=['meanError', 'meanAbsError', 'rms',
                                              'lags_meanError', 'lags_meanAbsError', 'lags_rms',
                                              'false_positive', 'false_negative'], index=[model + subID])
            pdAuxTraj = pandas.DataFrame()
            if rets[0] is not np.nan:
                # save the data
                errorDF, falsePos, falseNeg, lagError, predsMean = rets
                pdAux[['meanError', 'meanAbsError', 'rms']] = errorDF.mean().values
                pdAux[['lags_meanError', 'lags_meanAbsError', 'lags_rms']] = lagError.mean().values
                pdAux['false_positive'] = falsePos
                pdAux['false_negative'] = falseNeg
                pdAuxTraj['Values'] = predsMean
                pdAuxTraj['Time'] = np.arange(predsMean.shape[0])
                pdAuxTraj['SubID'] = subID
                pdAuxTraj['Model'] = model

            else:
                errorDF, falsePos, falseNeg, lagError, predsMean = rets
                pdAux[['meanError', 'meanAbsError', 'rms']] = 1
                pdAux[['lags_meanError', 'lags_meanAbsError', 'lags_rms']] = 1
                pdAux['false_positive'] = falsePos
                pdAux['false_negative'] = falseNeg

            pdAux['SubID'] = subID
            pdAux['Model'] = model

            perModelSubsData.append(pdAux.copy())
            perModelSubsDataTraj.append(pdAuxTraj.copy())

    perModelSubsData = pandas.concat(perModelSubsData)
    perModelSubsDataTraj = pandas.concat(perModelSubsDataTraj)
    perModelSubsDataDict = {'scalars': perModelSubsData, 'Traj': perModelSubsDataTraj}

    with open(perModelSubPath, 'wb') as f:
        pickle.dump(perModelSubsDataDict, f)

else:
    with open(perModelSubPath, 'rb') as f:
        perModelSubsDataDict = pickle.load(f)

    perModelSubsData = perModelSubsDataDict['scalars']
    perModelSubsDataTraj = perModelSubsDataDict['Traj']

# perModelSubsDataTraj2 = perModelSubsDataTraj.copy()
# perModelSubsDataTraj2['Time'] /= 400
# perModelSubsDataTraj2['order'] = 0
# modelTypeNew = ['EDM', 'ERM', 'DM', 'RM', 'Linear']
# desOrder = [2, 3, 0, 1, 4]
#
# for oldN, newN, orN in zip(modelTypes, modelTypeNew, desOrder):
#     m = perModelSubsDataTraj2['Model'] == oldN
#     perModelSubsDataTraj2.loc[m, 'Model'] = newN
#     perModelSubsDataTraj2.loc[m, 'order'] = orN
#
# perModelSubsDataTraj2.sort_values(['order', 'SubID', 'Time'], inplace=True)
#
# sns.lineplot(x='Time', y='Values', hue='Model', data=perModelSubsDataTraj2)
# plt.title('Mean cycle per Model')
# plt.xlabel('Gait Cycle Percentage')
# plt.ylabel('Gait Cycle Prediction')

modelTypeNew = ['EDM', 'ERM', 'DM', 'RM', 'Linear']
desOrder = [2, 3, 0, 1, 4]
perModelSubsData['order'] = 0


for oldN, newN, orN in zip(modelTypes, modelTypeNew, desOrder):
    m = perModelSubsData['Model'] == oldN
    perModelSubsData.loc[m, 'Model'] = newN
    perModelSubsData.loc[m, 'order'] = orN

perModelSubsData.sort_values(['order', 'SubID'], inplace=True)


falsePredPD = []
for fType in ['false_positive', 'false_negative']:
    falsePredPDaux = pandas.DataFrame()
    falsePredPDaux[['Error', 'SubID', 'Model']] = perModelSubsData[[fType, 'SubID', 'Model']]
    falsePredPDaux['Type'] = fType
    falsePredPD.append(falsePredPDaux.copy())

falsePredPD = pandas.concat(falsePredPD)

# for fType in ['false_positive', 'false_negative']:
#     print(fType)
#     for model in modelTypes:
#         s = falsePredPD['Error'].where(falsePredPD['Model'] == model).where(falsePredPD['Type'] == fType).dropna().astype(np.float).mean()/785*100
#         p = falsePredPD['Error'].where(falsePredPD['Model'] == model).where(falsePredPD['Type'] == fType).dropna().astype(np.float).std()/785*100
#         print(falsePredPD['Error'].where(falsePredPD['Model'] == model).where(falsePredPD['Type'] == fType).dropna().astype(np.float).shape)
#         print('%s: %f \pm %f' % (model, s, p))

# falsePredPD['Error'] /= 785/100
# data=falsePredPD.where(falsePredPD['Type']=='false_positive')
# ax = sns.boxplot(x='Model', y='Error', data=data)
# plt.title('False Positive Heel Strike Identification')
# plt.ylabel('Frequency [%]')
# plt.tight_layout()
#
# significance = [[0, 1],
#                 [0, 2],
#                 [0, 3],
#                 [0, 4]]
# maxHeight = 0.1
# minHeight = 0.03
# for j, ((c1, c2), h) in enumerate(zip(significance, np.linspace(minHeight, maxHeight, len(significance)))):
#     ptu.addSignificanceLevel(ax, c1, c2, data, '*', 'Error', offsetPercentage=h)
#
# plt.tight_layout()


# data = falsePredPD.where(falsePredPD['Type']=='false_negative')
# ax = sns.boxplot(x='Model', y='Error', data=data)
# plt.title('False Negative Heel Strike Identification')
# plt.ylabel('Frequency [%]')
# plt.tight_layout()
#
# significance = [[3, 4],
#                 [2, 3],
#                 [1, 2],
#                 [1, 3]]
# maxHeight = 0.1
# minHeight = 0.03
# for j, ((c1, c2), h) in enumerate(zip(significance, np.linspace(minHeight, maxHeight, len(significance)))):
#     ptu.addSignificanceLevel(ax, c1, c2, data, '*', 'Error', offsetPercentage=h)
#
# plt.tight_layout()

lagsPD = []
for fType in ['meanError', 'meanAbsError', 'rms']:
    falsePredPDaux = pandas.DataFrame()
    falsePredPDaux[['Error', 'SubID', 'Model']] = perModelSubsData[['lags_' + fType, 'SubID', 'Model']]
    falsePredPDaux['Type'] = fType
    lagsPD.append(falsePredPDaux.copy())

lagsPD = pandas.concat(lagsPD)

# sns.barplot(x='Model', y='Error', hue='Type', data=lagsPD)
# plt.title('Event ID lag')
# plt.ylabel('Delay [s]')
# plt.tight_layout()
#
# perModelSubsData['lags_rms'] = perModelSubsData['lags_rms'].astype(np.float) * 1000
# ax = sns.boxplot(x='Model', y='lags_rms', data=perModelSubsData)
# plt.title('Event ID lag')
# plt.ylabel('Delay [ms]')
#
# significance = [[1, 3],
#                 [1, 4]]
# maxHeight = 0.1
# minHeight = 0.03
# for j, ((c1, c2), h) in enumerate(zip(significance, np.linspace(minHeight, maxHeight, len(significance)))):
#     ptu.addSignificanceLevel(ax, c1, c2, perModelSubsData, '*', 'lags_rms', offsetPercentage=h)
# plt.tight_layout()


errsPD = []
for fType in ['meanError', 'meanAbsError', 'rms']:
    falsePredPDaux = pandas.DataFrame()
    falsePredPDaux[['Error', 'SubID', 'Model']] = perModelSubsData[[fType, 'SubID', 'Model']]
    falsePredPDaux['Type'] = fType
    errsPD.append(falsePredPDaux.copy())

errsPD = pandas.concat(errsPD)

# sns.barplot(x='Model', y='Error', hue='Type', data=errsPD)
# plt.title('Event ID lag')
# plt.title('% Error')
# plt.tight_layout()

# perModelSubsData['rms'] = perModelSubsData['rms'].astype(np.float) * 100
# ax = sns.boxplot(x='Model', y='rms', data=perModelSubsData, fliersize=0, whis=0.99)
# plt.title('Root Mean Square Error')
# plt.ylabel('% Error')
#
# significance = [[3, 4],
#                 [1, 2],
#                 [1, 3],
#                 [1, 4],
#                 [0, 2],
#                 [0, 3],
#                 [0, 4]]
# maxHeight = 0.15
# minHeight = 0.03
# for j, ((c1, c2), h) in enumerate(zip(significance, np.linspace(minHeight, maxHeight, len(significance)))):
#     ptu.addSignificanceLevel(ax, c1, c2, perModelSubsData, '*', 'rms', offsetPercentage=h)
# plt.tight_layout()

# for model in modelTypes:
#     s, p = diagn.lilliefors(perModelSubsData['rms'].where(perModelSubsData['Model'] == model).dropna().astype(np.float))
#     print('%s p: %f' % (model, p))

perModelSubsData['precision'] = 785 / (perModelSubsData['false_positive'] + 785)
perModelSubsData['recall'] = 785 / (perModelSubsData['false_negative'] + 785)
perModelSubsData['f1'] = 2 * (perModelSubsData['precision'] * perModelSubsData['recall'] / (perModelSubsData['precision'] + perModelSubsData['recall']))

# for model in modelTypes:
#     s = lagsPD['Error'].where(lagsPD['Model'] == model).where(lagsPD['Type'] == 'rms').dropna().astype(np.float).mean()
#     p = lagsPD['Error'].where(lagsPD['Model'] == model).where(lagsPD['Type'] == 'rms').dropna().astype(np.float).std()
#     print(lagsPD['Error'].where(lagsPD['Model'] == model).where(lagsPD['Type'] == 'rms').dropna().astype(np.float).shape)
#     print('%s: %f \pm %f' % (model, s, p))

