import pickle

import tensorflow as tf

import HealthyTools.utils as HTu
import Tools.Utils as tu
from tensorflow.python.framework.ops import disable_eager_execution

disable_eager_execution()

# tensorboard --logdir=E:/HealthyP/scalars
data_path = 'Z:/HealthyControl/_Experiments/6MWT/'
allSubsPath = data_path + 'allSubs_50Hz_10.pickle'

with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)

kernels1D = [15, 5, 2]
kernels1Dpost = [15, 5, 2]
fullyCon = [16, 32]
# num_gru_units = None
num_gru_units = [5 for _ in range(1)]
batchSize = 1000
trainSize = len(allSubs) * 15000
resultsParent = "E:/HealthyP/Final50Hz_10"
freq = 50.0
maxEpochs = 100

modelName = 'GaitPhase'
resultsPath = resultsParent + modelName + '/'

testSize = 5000
dataset = HTu.HealthyDataset(allSubs, trainSize, testSize, batchSize, db2use=['x', 'y'])
contAux = HTu.modelContainer(dataset, kernels1D, kernels1Dpost, num_gru_units, fullyCon, modelName)
# for each model
for modelName in [modelName]:
    print('Doing %s' % modelName)
    model = contAux.AllModel[modelName + '_class']
    model.summary()
    # tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=resultsParent + 'scalars/' + modelName)
    #
    # model.fit(dataset.train.x, dataset.train.y, batch_size=batchSize, epochs=maxEpochs,
    #           validation_data=(dataset.test.x, dataset.test.y), callbacks=[tensorboard_callback])
    model.fit(dataset.train.x, dataset.train.y, batch_size=batchSize, epochs=maxEpochs,
              validation_split=0.2)#, callbacks=[tensorboard_callback])

    tu.createDirIfNotExist(resultsPath)
    model.save(resultsPath + modelName + '.h5')

