import pickle

from sklearn import linear_model

import HealthyTools.utils as HTu
import Tools.Utils as tu

# tensorboard --logdir=E:/HealthyP/scalars
data_path = 'Z:/HealthyControl/_Experiments/6MWT/'
allSubsPath = data_path + 'allSubs.pickle'

with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)

batchSize = 5000
trainSize = len(allSubs) * 5000
resultsParent = "E:/HealthyP/"
freq = 100.0

# for each subject of the LOO
modelName = 'Linear'
resultsPath = resultsParent + modelName + '/'
evalPath = "E:/HealthyP/LOO/evalSubs_ReDo.pickle"
with open(evalPath, 'rb') as f:
    LOOevals = pickle.load(f)
redoAll = True
if redoAll:
    for sub in allSubs:
        testSize = [sub]
        dataset = HTu.HealthyDataset(allSubs, trainSize, testSize, batchSize, db2use=['xP', 'yP'])
        dataset.train.x = dataset.train.x.reshape((-1, dataset.DATA_SHAPE[0]*dataset.DATA_SHAPE[1]))
        print('Doing %s' % sub)
        model = linear_model.LinearRegression()
        model.fit(dataset.train.x, dataset.train.y)

        tu.createDirIfNotExist(resultsPath)


        lPath = data_path + sub + '/' + sub + 'L.bin'
        rPath = data_path + sub + '/' + sub + 'R.bin'

        matPath = data_path + sub + '/' + sub + '.xlsx'
        savePath = resultsPath + sub + '.png'
        LOOevals[sub + '_' + modelName] = HTu.evaluateSubject(lPath, rPath, matPath, model, freq, None, savePath)

    with open(resultsPath + 'evalSubsLinear.pickle', 'wb') as f:
        pickle.dump(LOOevals, f)

else:
    with open(resultsPath + 'evalSubsLinear.pickle', 'rb') as f:
        LOOevals = pickle.load(f)
