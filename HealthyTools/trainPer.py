import pickle

import tensorflow as tf

import HealthyTools.utils as HTu
import Tools.Utils as tu


# tensorboard --logdir=E:/HealthyP/scalars
data_path = 'Z:/HealthyControl/_Experiments/6MWT/'
allSubsPath = data_path + 'allSubs.pickle'

with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)

kernels1D = [20, 10, 5]
kernels1Dpost = [20, 10, 5]
fullyCon = [32, 64, 512]
# num_gru_units = None
num_gru_units = [5 for _ in range(1)]
batchSize = 5000
trainSize = len(allSubs) * 5000
resultsParent = "E:/HealthyP/"
freq = 100.0
maxEpochs = 500
# 'Sbj009_Ln', 'Sbj011_ED_Ln'
reDoModels = ['Sbj025_Ln', 'Sbj027_RNN', 'Sbj031_ED_Ln', 'Sbj035_RNN', 'Sbj039_Ln', 'Sbj039_RNN']
# for each subject of the LOO
modelName = 'LOO'
resultsPath = resultsParent + modelName + '/'
LOOmodels = {}
with open(resultsPath + 'evalSubs_ReDo.pickle', 'rb') as f:
    LOOevals = pickle.load(f)
for sub in allSubs:
    testSize = [sub]
    dataset = HTu.HealthyDataset(allSubs, trainSize, testSize, batchSize, db2use=['xP', 'yP'])
    contAux = HTu.modelContainer(dataset, kernels1D, kernels1Dpost, num_gru_units, fullyCon, sub)
    # for each model
    for modelName in contAux.AllModel:
        if modelName in reDoModels:
            print('Doing %s' % modelName)
            model = contAux.AllModel[modelName]
            tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=resultsParent + 'scalars/' + modelName)

            model.fit(dataset.train.x, dataset.train.y, batch_size=batchSize, epochs=maxEpochs,
                      validation_data=(dataset.test.x, dataset.test.y), callbacks=[tensorboard_callback])

            tu.createDirIfNotExist(resultsPath)
            model.save(resultsPath + modelName + '.h5')

            lPath = data_path + sub + '/' + sub + 'L.bin'
            rPath = data_path + sub + '/' + sub + 'R.bin'

            matPath = data_path + sub + '/' + sub + '.xlsx'
            savePath = resultsPath + modelName + '.png'
            LOOevals[modelName] = HTu.evaluateSubject(lPath, rPath, matPath, model, freq, batchSize, savePath)

with open(resultsPath + 'evalSubs_ReDo.pickle', 'wb') as f:
    pickle.dump(LOOevals, f)
