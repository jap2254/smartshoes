import pickle
import seaborn as sns
import pandas
import numpy as np
import matplotlib.pyplot as plt
import plotly.express as px


perModelSubPath = "E:/HealthyP/Linear/perModelSub_10_90.pickle"
lapsPath = "E:/HealthyP/Linear/evalSubsLaps.pickle"

with open(perModelSubPath, 'rb') as f:
    perModelSubsDataDict = pickle.load(f)

with open(lapsPath, 'rb') as f:
    justLaps = pickle.load(f)

modelTypes = justLaps['Model'].unique()
perModelSubsData = perModelSubsDataDict['scalars']

modelTypeNew = ['EDM', 'ERM', 'DM', 'RM', 'Linear']
desOrder = [2, 3, 0, 1, 4]
perModelSubsData['order'] = 0

for oldN, newN, orN in zip(modelTypes, modelTypeNew, desOrder):
    m = perModelSubsData['Model'] == oldN
    perModelSubsData.loc[m, 'Model'] = newN
    perModelSubsData.loc[m, 'order'] = orN

perModelSubsData.sort_values(['order', 'SubID'], inplace=True)
doERMSummary = False
if doERMSummary:
    perModelSubsData = perModelSubsData.where(perModelSubsData['Model'] == 'ERM').dropna(how='all')
    # falsePredPD = []
    # for fType in ['false_positive', 'false_negative']:
    #     falsePredPDaux = pandas.DataFrame()
    #     falsePredPDaux[['Error', 'SubID', 'Model']] = perModelSubsData[[fType, 'SubID', 'Model']]
    #     falsePredPDaux['Type'] = fType
    #     falsePredPD.append(falsePredPDaux.copy())
    #
    # falsePredPD = pandas.concat(falsePredPD)
    # falsePredPD['Error'] /= 785/100
    #
    # lagsPD = []
    # for fType in ['meanError', 'meanAbsError', 'rms']:
    #     falsePredPDaux = pandas.DataFrame()
    #     falsePredPDaux[['Error', 'SubID', 'Model']] = perModelSubsData[['lags_' + fType, 'SubID', 'Model']]
    #     falsePredPDaux['Type'] = fType
    #     lagsPD.append(falsePredPDaux.copy())
    #
    # lagsPD = pandas.concat(lagsPD)

    perModelSubsData['lags_rms'] = perModelSubsData['lags_rms'].astype(np.float) * 1000
    perModelSubsData['rms'] = perModelSubsData['rms'].astype(np.float) * 100
    perModelSubsData[['false_positive', 'false_negative']] = perModelSubsData[['false_positive', 'false_negative']].astype(
        np.float) / 785 * 100
    metrics = ['rms',  'false_positive', 'false_negative', 'lags_rms']
    colors = ['blue', 'orange', 'green', 'red']
    units = ['% Error', 'Frequency [%]', 'Frequency [%]', 'Delay [ms]']
    titles = ['Root Mean Square', 'False Positives', 'False Negative', 'Event ID lag']
    f, ax = plt.subplots(1, 4)
    fontSize = 25
    for aa, t, c, u, ti in zip(ax, metrics, colors, units, titles):
        sns.boxplot(x='Model', y=t, data=perModelSubsData, ax=aa, color=c)
        aa.set_title(ti, size=fontSize)
        aa.set_ylabel(u, fontsize=fontSize)
        aa.set_xlabel('')
        aa.set_xticklabels([''])
        aa.set_ylim(0, 25)
        plt.setp(aa.get_yticklabels(), fontsize=fontSize)

    aa.set_ylim(0, 100)
    plt.tight_layout()

doSpiderPlots = True
if doSpiderPlots:
    metrics = ['rms',  'false_positive', 'false_negative', 'lags_rms']
    colors = ['blue', 'orange', 'green', 'red']
    units = ['% Error', 'Frequency [%]', 'Frequency [%]', 'Delay [ms]']
    titles = ['Root Mean Square', 'False Positives', 'False Negative', 'Event ID lag']
    # perModelSubsData = perModelSubsData[metrics]
    perModelSubsData['lags_rms'] = perModelSubsData['lags_rms'].astype(np.float) * 100
    perModelSubsData['rms'] = perModelSubsData['rms'].astype(np.float) * 100
    perModelSubsData[['false_positive', 'false_negative']] = perModelSubsData[['false_positive', 'false_negative']].astype(
        np.float) / 785 * 100
    perModelSubsData.rename(columns={j: k for j, k in zip(metrics, titles)}, inplace=True)
    sumMetrics = []
    for mo in perModelSubsData['Model'].unique():
        dataAux = perModelSubsData.where(perModelSubsData['Model'] == mo).dropna(how='all')
        sumMetrics.append(dataAux[titles].mean().to_frame())
        sumMetrics[-1].reset_index(inplace=True)
        sumMetrics[-1]['Model'] = mo
    sumMetrics = pandas.concat(sumMetrics)
    f = px.line_polar(sumMetrics, r=0, theta='index', color='Model', line_close=True)
    # f.update_traces(fill='toself')
    f.write_image('./spider.svg')
