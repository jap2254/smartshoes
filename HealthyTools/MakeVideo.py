import pickle
import time
import tkinter as tk

import numpy as np

import Tools.GUItools as guiT


class VideoApp(object):
    def __init__(self):
        self.root = tk.Tk()
        self.root.title('DeepSole System, Real Time')
        self.root.iconbitmap(r'./GUI/deepsole.ico')
        self.refreshTime = 10  # ms

        rightFrame = tk.Frame(self.root)
        rightFrame.pack(fill=tk.BOTH, expand=tk.YES, side=tk.TOP)

        self.bigPlot = guiT.PlotPanel(rightFrame, 'Rigth', [['Gait Cycle [%]']], [[['Ground Truth'], ['Prediction']]],
                                      [['red', 'blue']], [[]], [[[]], [[]]], number2Plot=0,
                                      showTime=False, useScale=False, color='blue')
        self.bigPlot.pack(fill=tk.BOTH, expand=tk.YES)
        self.bigPlot2 = guiT.PlotPanel(rightFrame, 'Left', [['Gait Cycle [%]']], [[['Ground Truth'], ['Prediction']]],
                                       [['red', 'blue']], [[]], [[[]], [[]]], number2Plot=0,
                                       showTime=False, useScale=False, color='red')
        self.bigPlot2.pack(fill=tk.BOTH, expand=tk.YES)
        botFrame = tk.Frame(self.root)
        botFrame.pack(fill=tk.BOTH, expand=tk.YES, side=tk.BOTTOM)
        self.bt = guiT.ButtonPanel(botFrame, [['Start Prediction']], [[self.btCMD]])

        self.bt.pack(side=tk.RIGHT)
        self.pressDisp = guiT.ShoePressDisp(botFrame)
        self.pressDisp.pack(fill=tk.BOTH, expand=tk.YES)

        modelParent = 'E:/HealthyP/LOO'

        with open(modelParent + '/evalSubs.pickle', 'rb') as f:
            LOOevals = pickle.load(f)

        modelName = 'Sbj001_ED_RNN'

        lapsPath = "E:/HealthyP/LOO/evalSubsLaps.pickle"
        with open(lapsPath, 'rb') as f:
            justLaps = pickle.load(f)
        m = np.logical_and(justLaps['Model'] == 'ED_Ln', justLaps['SubID'] == 'Sbj001')
        self.sb = justLaps.where(m).dropna()
        # self.gt = LOOevals[modelName]['gt']
        # self.pred = LOOevals[modelName]['pred']
        r = LOOevals[modelName]['r']
        l = LOOevals[modelName]['l']
        f = lambda x: (x - x.min()) / (x.max() - x.min())
        self.r = r.dataFrame.values
        self.r[:, 0] = f(self.r[:, 0])
        self.r[:, 1] = f(self.r[:, 1])
        self.r[:, 2] = f(self.r[:, 2])
        self.l = r.dataFrame.values
        self.l[:, 0] = f(self.r[:, 0])
        self.l[:, 1] = f(self.r[:, 1])
        self.l[:, 2] = f(self.r[:, 2])
        # self.rBatch = createbatch(r.dataFrame.values, 50)
        # self.lBatch = createbatch(l.dataFrame.values, 50)
        # self.gtBatch = createbatch(np.expand_dims(self.gt['R'].values, 1), 50)
        # self.predBatch = createbatch(np.expand_dims(self.pred['R'].values, 1), 50)
        self.index = 0
        self.moveTime = False
        self.now = 0

    def startApp(self):
        # while True:
        #   self.updatePlots()
        #  self.root.update_idletasks()
        # self.root.update()
        # self.loopFunctions()
        self.root.protocol("WM_DELETE_WINDOW", self.stopApp)
        self.now = time.time()
        self.loopFunctions()
        self.root.mainloop()

    def stopApp(self):
        # self.theRec.stopThread()
        self.root.destroy()

    def loopFunctions(self):
        self.updatePlots()
        self.root.after(self.refreshTime, self.loopFunctions)

    def updatePlots(self):
        if self.moveTime:
            s = time.time() - self.now
            s = int(s * 100)
            shoe = self.r
            duration = 1000
            m = self.sb['Side'] == 'R'
            gt = self.sb['gt'][m].values[s:s + duration]
            pred = self.sb['pred'][m].values[s:s + duration]
            t = self.sb['Time'].values[s:s + duration]
            self.bigPlot.all = [[p for p in gt], [p for p in pred]]
            self.bigPlot.timestamp = np.arange(duration, dtype=np.float) + s
            self.bigPlot.plotControlFromChecks()
            m = self.sb['Side'] == 'L'
            gt = self.sb['gt'][m].values[s + 50:s + duration + 50]
            pred = self.sb['pred'][m].values[s + 50:s + duration + 50]
            self.bigPlot2.all = [[p for p in gt], [p for p in pred]]
            self.bigPlot2.timestamp = np.arange(duration, dtype=np.float) + s + 50
            self.bigPlot2.plotControlFromChecks()

            sS = (t * 100).astype(np.int)
            if len(sS) is 0:
                return
            self.pressDisp.updatePlot(toe=shoe[sS[-1], 0], ball=shoe[sS[-1], 1], heel=shoe[sS[-1], 2], axLeft=False)

            shoe = self.l
            self.pressDisp.updatePlot(toe=shoe[sS[-1] + 50, 0], ball=shoe[sS[-1] + 50, 1], heel=shoe[sS[-1] + 50, 2],
                                      axLeft=True)

    def btCMD(self):
        self.moveTime = not self.moveTime


class VideoApp2(object):
    def __init__(self):
        self.root = tk.Tk()
        self.root.title('DeepSole System, Real Time')
        self.root.iconbitmap(r'./GUI/deepsole.ico')
        self.refreshTime = 30  # ms
        leftFrame = tk.Frame(self.root)
        leftFrame.pack(fill=tk.BOTH, expand=tk.YES, side=tk.LEFT)
        rightFrame = tk.Frame(self.root)
        rightFrame.pack(fill=tk.BOTH, expand=tk.YES, side=tk.RIGHT)
        title = ['Press', 'Acc', 'Euler']
        names = [[['Toe', 'Ball', 'Heel']],
                 [['ax', 'ay', 'az']],
                 [['roll', 'pitch', 'yaw']]]
        plotColor = [[['fuchsia', 'orange', 'purple']],
                     [['Chartreuse', 'darkgreen', 'chocolate']],
                     [['blue', 'teal', 'cyan']]]
        self.smallPlots = {}
        for ti, n, c in zip(title, names, plotColor):
            # self.smallPlots[n] = guiT.PlotPanel(leftFrame, ti, n, [[]], c, [[]], [[]], color=c[0], number2Plot=0,
            #                                     showTime=True, useScale=True, figsize=(2,1))
            self.smallPlots[ti] = guiT.PlotPanel(leftFrame, ti, [['']], n, c, [[]], [[[]], [[]], [[]]], number2Plot=0,
                                                 showTime=False, useScale=True, figsize=(3, 1))

            self.smallPlots[ti].pack(fill=tk.BOTH, expand=tk.YES)

        self.bigPlot = guiT.PlotPanel(rightFrame, '', [['Gait Cycle [%]']], [[['Ground Truth'], ['Prediction']]],
                                      [['red', 'blue']], [[]], [[[]], [[]]], number2Plot=0,
                                      showTime=False, useScale=False)
        self.bigPlot.pack(fill=tk.BOTH, expand=tk.YES)
        self.bt = guiT.ButtonPanel(self.root, [['Start Prediction']], [[self.btCMD]])

        self.bt.pack(side=tk.BOTTOM)

        modelParent = 'E:/HealthyP/LOO'

        with open(modelParent + '/evalSubs.pickle', 'rb') as f:
            LOOevals = pickle.load(f)

        modelName = 'Sbj001_ED_RNN'

        lapsPath = "E:/HealthyP/LOO/evalSubsLaps.pickle"
        with open(lapsPath, 'rb') as f:
            justLaps = pickle.load(f)
        m = np.logical_and(np.logical_and(justLaps['Model'] == 'ED_Ln', justLaps['SubID'] == 'Sbj001'),
                           justLaps['Side'] == 'R')
        self.sb = justLaps.where(m).dropna()
        # self.gt = LOOevals[modelName]['gt']
        # self.pred = LOOevals[modelName]['pred']
        r = LOOevals[modelName]['r']
        l = LOOevals[modelName]['l']
        self.r = r.dataFrame.values
        # self.rBatch = createbatch(r.dataFrame.values, 50)
        # self.lBatch = createbatch(l.dataFrame.values, 50)
        # self.gtBatch = createbatch(np.expand_dims(self.gt['R'].values, 1), 50)
        # self.predBatch = createbatch(np.expand_dims(self.pred['R'].values, 1), 50)
        self.index = 0
        self.moveTime = False
        self.now = 0

    def startApp(self):
        # while True:
        #   self.updatePlots()
        #  self.root.update_idletasks()
        # self.root.update()
        # self.loopFunctions()
        self.root.protocol("WM_DELETE_WINDOW", self.stopApp)
        self.now = time.time()
        self.loopFunctions()
        self.root.mainloop()

    def stopApp(self):
        # self.theRec.stopThread()
        self.root.destroy()

    def loopFunctions(self):
        self.updatePlots()
        self.root.after(self.refreshTime, self.loopFunctions)

    def updatePlots(self):
        s = self.now - time.time()
        s = int(s * 100)
        shoe = self.r
        gt = self.sb['gt'].values[s:s + 50]
        pred = self.sb['pred'].values[s:s + 50]
        t = self.sb['Time'].values[s:s + 50]
        self.bigPlot.all = [[p for p in gt], [p for p in pred]]
        self.bigPlot.timestamp = np.arange(50, dtype=np.float) + s
        self.bigPlot.plotControlFromChecks()
        sS = (t * 100).astype(np.int)

        self.smallPlots['Press'].all = [[p for p in shoe[sS, 0]], [p for p in shoe[sS, 1]], [p for p in shoe[sS, 2]]]
        self.smallPlots['Press'].timestamp = np.arange(50, dtype=np.float) + s
        self.smallPlots['Press'].plotControlFromChecks()

        self.smallPlots['Acc'].all = [[p for p in shoe[sS, 3]], [p for p in shoe[sS, 4]], [p for p in shoe[sS, 5]]]
        self.smallPlots['Acc'].timestamp = np.arange(50, dtype=np.float) + s
        self.smallPlots['Acc'].plotControlFromChecks()

        self.smallPlots['Euler'].all = [[p for p in shoe[sS, 9]], [p for p in shoe[sS, 10]], [p for p in shoe[sS, 11]]]
        self.smallPlots['Euler'].timestamp = np.arange(50, dtype=np.float) + s
        self.smallPlots['Euler'].plotControlFromChecks()

    def btCMD(self):
        self.moveTime = not self.moveTime


app = VideoApp()
app.startApp()
