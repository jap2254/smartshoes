import pickle

import Tools.Utils as tu
import Tools.NNUtils as ntu


data_path = 'Z:/HealthyControl/_Experiments/6MWT/'
allSubsPath = data_path + 'allSubs_50Hz_10.pickle'
validSubs = [1, 3, 6, 7, 8, 9, 10, 11, 14, 15, 17, 19, 21, 25, 27, 28, 29, 31, 33, 34, 35, 36, 37, 39]
freq = 50.0
samplesPerSubject = 12000
samplesUse = 10
allSubs = {}
for k in validSubs:
    subID = 'Sbj%03d' % k
    print('Loading: %s' % subID)
    lPath = data_path + subID + '/' + subID + 'L.bin'
    rPath = data_path + subID + '/' + subID + 'R.bin'
    [r, _] = tu.binaryFile2python(rPath)
    [_, l] = tu.binaryFile2python(lPath)
    r.createPandasDataFrame(useSync=True, freq=freq)
    l.createPandasDataFrame(useSync=True, freq=freq)
    matPath = data_path + subID + '/' + subID + '.xlsx'
    matO = tu.MatObject(fName=matPath, freq=freq, newMat=False)
    yR = matO.binaryFunctions['R'].values
    yL = matO.binaryFunctions['L'].values
    xR = r.dataFrame.values
    xL = l.dataFrame.values
    laps = matO.lap.copy()
    tMax = min([yR.shape[0], yL.shape[0], xR.shape[0], xL.shape[0]])
    yR = yR[:tMax]
    yL = yL[:tMax]
    xR = xR[:tMax]
    xL = xL[:tMax]

    x, y, _ = ntu.createTemporalSamples(yR, yL, xR, xL, laps,
                                       freq, freq, samplesPerSubject, samplesUse, samplesUse)
    xP, yP, _ = ntu.createTemporalSamples(yR, yL, xR, xL, laps,
                                         freq, freq, samplesPerSubject, samplesUse, samplesUse, useP=True)
    allSubs[subID] = {'x': x.copy(), 'y': y.copy(),
                      'xP': xP.copy(), 'yP': yP.copy()}

with open(allSubsPath, 'wb') as f:
    pickle.dump(allSubs, f)
