import pandas
from matplotlib import pyplot as plt
from statsmodels.stats.anova import AnovaRM
from statsmodels.stats.diagnostic import kstest_normal as normTest
from statsmodels.sandbox.stats.multicomp import MultiComparison
import scipy.stats as stats
from scipy.stats import kruskal, friedmanchisquare
import numpy as np
import seaborn as sns

def normalTestCols(x):
    # return x.apply(normTest).explode(x.columns).iloc[1::2]
    return x.apply(normTest).iloc[1, :]


# def rmANOVA_rmFriedman(data: pandas.DataFrame, sub, within, cols4stats, aggregatFn='mean', **kwargs):
#     if type(aggregatFn) is str:
#         if 'mean' in aggregatFn:
#             summary = data.groupby([sub, within])[cols4stats].mean()
#         elif 'std' in aggregatFn:
#             summary = data.groupby([sub, within])[cols4stats].std()
#     else:
#         summary = data.groupby([sub, within])[cols4stats].apply(aggregatFn, **kwargs)
#     summary.reset_index(inplace=True)
#     normalT = summary.groupby(within)[cols4stats].apply(normalTestCols) > 0.05
#     all_stats = []
#     for col2use in cols4stats:
#         # print(col2use)
#         allStatsDS = pandas.DataFrame(columns=['F_val', 'DF', 'DDF', 'p_val', 'Data Type', 'Normal'])
#         # aux = summary[col2use]
#         if normalT[col2use].all():
#             model = AnovaRM(data, col2use, sub, within=[within], aggregate_func='mean').fit()
#             if model.anova_table.iloc[0, 3] <= 0.05:
#                 print('%s is significant' % col2use)
#             results = model.anova_table.copy()
#             results['Data Type'] = col2use
#             results['Normal'] = True
#             allStatsDS.loc[col2use, :] = results.values
#         else:
#             # print('%s %s is not normal' % (side, muscle))
#             pars = [summary[col2use].loc[summary[within] == xx].values for xx in summary[within].unique()]
#             if len(pars) > 2:
#                 non_par = friedmanchisquare(*pars)
#             else:
#                 non_par = kruskal(*pars)
#             allStatsDS.loc[col2use, :] = np.array([non_par[0], 0, 0, non_par[1], col2use, False])
#             if non_par[1] <= 0.05:
#                 print('%s %s is significant' % (col2use))
#         all_stats.append(allStatsDS)
#
#     return pandas.concat(all_stats)

def addSignificanceLevel(ax, colname, col1, col2, data, annotation, plotY, yLevel=None, offsetPercentage=0.1,
                         color='k'):
    '''

    :param ax: axis handle
    :type ax: matplotlib.axes._subplots.AxesSubplot
    :param col1: index of column 1, see plt.xticks()
    :param col2: index of column 2
    :param data: pandas dataframe
    :type data: pandas.DataFrame
    :param annotation: string with the annotation
    :param color: line and text color
    '''
    x1, x2 = col1, col2  # columns
    for i in np.arange(0, len(ax.get_xticklabels())):
        if str(x1) == ax.get_xticklabels()[i]._text:
            x1_loc = i
        elif str(x2) == ax.get_xticklabels()[i]._text:
            x2_loc = i
    if yLevel is None:
        q3 = data.groupby([colname]).quantile(q=.75)[plotY]
        q1 = data.groupby([colname]).quantile(q=.25)[plotY]
        y = q3.max() + 1.5 * (q3 - q1).max() * (1. + offsetPercentage)
    else:
        y = yLevel
    h, col = y * 0.1, color
    ax.plot([x1, x1, x2, x2], [y, y + h, y + h, y], lw=1.5, c=col)
    ax.text((x1_loc + x2_loc) * .5, y + (y * 0.075), annotation, ha='center', va='bottom', color=col,
            fontsize='xx-large')


def rmANOVA_rmFriedman(data: pandas.DataFrame, sub, within, cols4stats, aggregatFn='mean', plot=False,
                       parentFolder=None, testNormality=True, **kwargs):
    def plotFn(sig, pairwise: pandas.DataFrame):
        data2 = data.copy()
        f, ax = plt.subplots(1)
        sns.barplot(x=within, y=col2use, data=data2, palette='colorblind', ci='sd')
        if sig:
            num_sig = 0
            for i in pairwise.index:
                if pairwise['reject'].iloc[i]:
                    num_sig += 1
                    g1 = pairwise['group1'].iloc[i]
                    g2 = pairwise['group2'].iloc[i]
                    p_val = pairwise['pval_corr'].iloc[i]
                    if (p_val <= 0.05) & (p_val > 0.01):
                        sig_stars = '*'
                    elif (p_val <= 0.01) & (p_val > 0.005):
                        sig_stars = '**'
                    elif p_val < 0.005:
                        sig_stars = '***'
                    addSignificanceLevel(ax, within, g1, g2, data2, sig_stars, col2use,
                                         offsetPercentage=0.15 * num_sig)
            plt.title('Normalized %s' % (col2use))
            plt.tight_layout()
            if parentFolder is not None:
                fileLoc = parentFolder + '_' + col2use.replace('/', '_') + '_notsig.svg'
                plt.savefig(fileLoc, format='svg')
                plt.close()

    if type(aggregatFn) is str:
        if 'mean' in aggregatFn:
            summary = data.groupby([sub, within])[cols4stats].mean()

        elif 'std' in aggregatFn:
            summary = data.groupby([sub, within])[cols4stats].std()
        elif 'cov' in aggregatFn:
            summary = data.groupby([sub, within])[cols4stats].std() / data.groupby([sub, within])[cols4stats].std()

    else:
        summary = data.groupby([sub, within])[cols4stats].apply(aggregatFn, **kwargs)
    if type(summary) is pandas.Series:
        summary = summary.to_frame()
    summary.reset_index(inplace=True)
    if testNormality:
        normalT = summary.groupby(within)[cols4stats].apply(normalTestCols) > 0.05
    else:
        normalT = pandas.DataFrame(columns=cols4stats, index=[0])
        normalT.loc[:] = True
    all_stats = []
    pairwise_stats = []

    for col2use in cols4stats:
        # print(col2use)
        allStatsDS = pandas.DataFrame(columns=['F Value', 'Num DF', 'Den DF', 'Pr > F', 'Data Type', 'Normal', 'group1',
                                               'group2', 'pval', 'pval_corr', 'reject', 'stat'])

        if normalT[col2use].all():
            model = AnovaRM(data, col2use, sub, within=[within], aggregate_func='mean').fit()
            if model.anova_table.iloc[0, 3] <= 0.05:
                print('%s is significant' % col2use)

                pairwise = MultiComparison(summary[col2use], summary[within])
                comp = pairwise.allpairtest(stats.ttest_rel, method='Holm')
                compDF = pandas.read_html(comp[0].as_html())[0]
                compDF['Data Type'] = col2use
                compDF['Normal'] = True
                for col in model.anova_table.columns:
                    compDF[col] = model.anova_table[col][0]
                pairwise_stats.append(compDF)
                if plot:
                    plotFn(True, compDF)

            else:
                results = model.anova_table.copy()
                results['Data Type'] = col2use
                results['Normal'] = True
                allStatsDS.loc[col2use, results.columns] = results.values
                all_stats.append(allStatsDS)
                if plot:
                    plotFn(False, None)
        else:
            pars = [summary[col2use].loc[summary[within] == xx].values for xx in summary[within].unique()]
            if len(pars) != 2:
                non_par = friedmanchisquare(*pars)
            else:
                non_par = kruskal(*pars)
            allStatsDS.loc[col2use, :] = np.array([non_par[0], 0, 0, non_par[1], col2use, False,
                                                   np.nan, np.nan, np.nan, np.nan, np.nan, np.nan])
            if non_par[1] <= 0.05:
                print('%s is significant' % col2use)

                pairwise = MultiComparison(summary[col2use], summary[within])
                comp = pairwise.allpairtest(kruskal, method='Holm')
                compDF = pandas.read_html(comp[0].as_html())[0]
                compDF['Data Type'] = col2use
                compDF['Normal'] = False
                for col in allStatsDS.columns[:4]:
                    compDF[col] = allStatsDS[col][0]
                pairwise_stats.append(compDF)

                if plot:
                    plotFn(True, compDF)

            else:
                all_stats.append(allStatsDS)
                if plot:
                    plotFn(False, None)

    if len(pairwise_stats) == 0:
        pairwiseDF = None
    else:
        pairwiseDF = pandas.concat(pairwise_stats)
    if len(all_stats) == 0:
        all_statsDF = None
    else:
        all_statsDF = pandas.concat(all_stats)
    stats_and_pairs = pandas.concat([all_statsDF, pairwiseDF], sort=True, ignore_index=True)
    stats_and_pairs.insert(loc=6, column='F Significance', value=stats_and_pairs['Pr > F'].astype(np.float) <= 0.05)
    # return all_statsDF, pairwiseDF
    return stats_and_pairs