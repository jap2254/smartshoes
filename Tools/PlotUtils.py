import os

import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm

import Tools.Utils as tu


def plotErrorFigures(ref, pred, removeOut=False, savePlot=None, name='img', format='png'):
    vals = [ref.copy(), pred.copy()]
    diff = pred - ref
    refN = ref.copy()
    predN = pred.copy()
    if removeOut:
        m = tu.is_not_outlier(diff, thresh=10)
        tot = len(m)
        outli = np.sum(~m)
        ref = ref[m]
        pred = pred[m]
        refN[~m] = np.nan
        predN[~m] = np.nan
        diff = pred - ref
        if (100.0 * outli / (1.0 * tot)) > 10:
            print("%f is more than 10%% are outlier, using all" % (100.0 * outli / (1.0 * tot)))
            ref, pred = vals
            refN = ref.copy()
            predN = pred.copy()
            diff = pred - ref
    titleSize = 20
    labelSize = 16
    tickSize = 14
    f1, axarr1 = plt.subplots()
    plotCompare(ref, pred, axarr1, ref=False, title=name, xlabel='Reference System [s]', ylabel='Machine Learning [s]',
                titleFontSize=titleSize, labelFontSize=labelSize, tickFontSize=tickSize)
    plt.tight_layout()
    if savePlot is None:
        plt.show()
    else:
        if not os.path.exists(savePlot):
            os.makedirs(savePlot)
        plt.savefig(savePlot + name + 'Corr' + '.' + format, dpi=300, format=format)
        plt.close(f1)
    f2, axarr2 = plt.subplots()
    plotHistogram(diff, axarr2, title=name, xlabel='Error [s]', ylabel='Frequency',
                  titleFontSize=titleSize, labelFontSize=labelSize, tickFontSize=tickSize)
    plt.tight_layout()
    if savePlot is None:
        plt.show()
    else:
        plt.savefig(savePlot + name + 'Hist' + '.' + format, dpi=300, format=format)
        plt.close(f2)
    f3, axarr3 = plt.subplots()
    bland_altman_plot(ref, pred, axarr3, title=name, xlabel='Reference System [s]', ylabel='Difference [s]',
                      titleFontSize=titleSize, labelFontSize=labelSize, tickFontSize=tickSize)
    plt.tight_layout()
    if savePlot is None:
        plt.show()
    else:
        plt.savefig(savePlot + name + 'BA' + '.' + format, dpi=300, format=format)
        plt.close(f3)
    return refN, predN


def plotCompare(reference, pred, ax, ref=False, title='', xlabel='', ylabel='',
                titleFontSize=20, labelFontSize=18, tickFontSize=8, *args, **kwargs):
    ax.scatter(reference, pred, s=1, *args, **kwargs)
    ax.plot([reference.min() * 0.95, reference.max() * 1.05], [reference.min() * 0.95, reference.max() * 1.05],
            color='k', linestyle='--')
    if ref:
        z = np.polyfit(reference, pred, 1)
        p = np.poly1d(z)
        ax.plot(reference, p(reference), "r--")
    ax.set_aspect('equal', 'box')
    ax.set_title(title, fontsize=titleFontSize)
    ax.set_xlabel(xlabel, fontsize=labelFontSize)
    ax.set_ylabel(ylabel, fontsize=labelFontSize)
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontsize(tickFontSize)


def plotDifferences(reference, dff, ax, hLine=0.1, hLineScale=3, title='', xlabel='', ylabel='',
                    titleFontSize=20, labelFontSize=18, tickFontSize=8, *args, **kwargs):
    ax.scatter(reference, dff, *args, **kwargs)
    ax.plot([reference.min(), reference.max()], [hLine, hLine], color='k', linestyle='--')
    ax.plot([reference.min(), reference.max()], [-hLine, -hLine], color='k', linestyle='--')
    ax.set_ylim(-hLineScale * hLine, hLineScale * hLine)
    ax.set_title(title, fontsize=titleFontSize)
    ax.set_xlabel(xlabel, fontsize=labelFontSize)
    ax.set_ylabel(ylabel, fontsize=labelFontSize)
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontsize(tickFontSize)


def bland_altman_plot(data1, data2, ax, title='', xlabel='', ylabel='',
                      titleFontSize=20, labelFontSize=18, tickFontSize=8, showTextValues=False, *args, **kwargs):
    data1 = np.asarray(data1)
    data2 = np.asarray(data2)
    mean = np.mean([data1, data2], axis=0)
    diff = data1 - data2  # Difference between data1 and data2
    md = np.mean(diff)  # Mean of the difference
    sd = np.std(diff, axis=0)  # Standard deviation of the difference

    ax.scatter(mean, diff, s=1, *args, **kwargs)
    ax.axhline(md, color='green', linestyle='--')
    ax.axhline(md + 1.96 * sd, color='r', linestyle='--')
    ax.axhline(md - 1.96 * sd, color='r', linestyle='--')
    ax.set_title(title, fontsize=titleFontSize)
    ax.set_xlabel(xlabel, fontsize=labelFontSize)
    ax.set_ylabel(ylabel, fontsize=labelFontSize)
    if showTextValues:
        ax.text((data1[0] + data1[-1]) / 2, md, '%0.2f' % md)
        ax.text((data1[0] + data1[-1]) / 2, md + 1.96 * sd, '%0.2f' % (md + 1.96 * sd))
        ax.text((data1[0] + data1[-1]) / 2, md - 1.96 * sd, '%0.2f' % (md - 1.96 * sd))
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontsize(tickFontSize)


def plotHistogram(x, ax, title='', xlabel='', ylabel='',
                  titleFontSize=20, labelFontSize=18, tickFontSize=8, *args, **kwargs):
    (mu, sig) = norm.fit(x)
    n, bins = np.histogram(x, density=True)
    n2, bins2, patches = ax.hist(x, *args, **kwargs)
    xx = np.linspace(bins[0], bins[-1])
    fac = n2[0] / n[0]
    y = mlab.normpdf(xx, mu, sig) * fac
    ax.plot(xx, y, color='r', linestyle='--')
    ax.set_title(title, fontsize=titleFontSize)
    ax.set_xlabel(xlabel, fontsize=labelFontSize)
    ax.set_ylabel(ylabel, fontsize=labelFontSize)
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontsize(tickFontSize)


def plotCurveWithError(x, y, err, plot_ax=None, *args, **kwargs):
    if plot_ax is None:
        f, ax = plt.subplots(1)
    else:
        ax = plot_ax
    if 'color' in kwargs:
        col = kwargs['color']
    else:
        col = 'b'
        kwargs['color'] = 'b'
    ax.plot(x, y, *args, **kwargs)
    ax.fill_between(x, y - err, y + err, color=col, alpha=0.5)
    if plot_ax is None:
        return f, ax


def plotErrorSummary(SWtAll, SWpAll, STtAll, STpAll, name, savePlot=None):
    vals = [SWtAll.copy(), SWpAll.copy(), STtAll.copy(), STpAll.copy()]
    swD = SWpAll - SWtAll
    m = tu.is_not_outlier(swD)
    tot = len(m)
    outli = np.sum(~m)
    SWpAll = SWpAll[m]
    SWtAll = SWtAll[m]
    STpAll = STpAll[m]
    STtAll = STtAll[m]
    stD = STpAll - STtAll
    m = tu.is_not_outlier(stD)
    STpAll = STpAll[m]
    STtAll = STtAll[m]
    SWpAll = SWpAll[m]
    SWtAll = SWtAll[m]
    outli += np.sum(~m)
    print("Removed %d outliers of %d samples, %f%%" % (outli, tot, 100.0 * outli / (1.0 * tot)))
    swD = SWpAll - SWtAll
    stD = STpAll - STtAll
    if 100.0 * outli / (1.0 * tot) > 80:
        print("More than 80% are outlier, using all")
        SWtAll, SWpAll, STtAll, STpAll = vals
        swD = SWpAll - SWtAll
        stD = STpAll - STtAll
    print(
        'error for %s swing is %s %%, %f s, std %f' % (name, np.mean(swD / SWtAll) * 100.0, np.mean(swD), np.std(swD)))
    print('error for %s stance is %s %%, %f s, std %f' % (
        name, np.mean(stD / STtAll * 100.0), np.mean(stD), np.std(stD)))

    f1, axarr1 = plt.subplots(3, 2)
    plotCompare(SWtAll, SWpAll, axarr1[0][0], ref=False, title='Swing Phase', xlabel='Reference System[s]',
                ylabel='Machine Learning [s]')
    plotCompare(STtAll, STpAll, axarr1[0][1], ref=False, title='Stance Phase', xlabel='Reference System[s]')
    plotHistogram(swD, axarr1[1][0], xlabel='Error[s]')
    plotHistogram(stD, axarr1[1][1], xlabel='Error[s]')
    bland_altman_plot(SWtAll, SWpAll, axarr1[2][0])
    bland_altman_plot(STtAll, STpAll, axarr1[2][1])
    # plotDifferences(SWtAll, swD, axarr1[2][0], hLine=0.1, title='', xlabel='Duration [s]', ylabel='Difference')
    # plotDifferences(STtAll, stD, axarr1[2][1], hLine=0.1, title='', xlabel='Duration [s]', ylabel='Difference')
    if savePlot is None:
        plt.show()
    else:
        plt.savefig(savePlot + name, dpi=300)


def addSignificanceLevel(ax, colname, col1, col2, data, annotation, plotY, yLevel=None, offsetPercentage=0.1,
                         color='k'):
    '''

    :param ax: axis handle
    :type ax: matplotlib.axes._subplots.AxesSubplot
    :param col1: index of column 1, see plt.xticks()
    :param col2: index of column 2
    :param data: pandas dataframe
    :type data: pandas.DataFrame
    :param annotation: string with the annotation
    :param color: line and text color
    '''
    x1, x2 = col1, col2  # columns
    x1_loc, x2_loc = col1, col2
    for i in np.arange(0, len(ax.get_xticklabels())):
        if str(x1) == ax.get_xticklabels()[i]._text:
            x1_loc = i
        elif str(x2) == ax.get_xticklabels()[i]._text:
            x2_loc = i
    if yLevel is None:
        q3 = data.groupby([colname])[plotY].quantile(q=.75)
        q1 = data.groupby([colname])[plotY].quantile(q=.25)
        y = q3.max() + 1.5*(q3-q1).max() * (1. + offsetPercentage)
    else:
        y = yLevel
    y2 = (q3 - 1.5 * (q3 - q1)).min()
    h, col = (y-y2) * 0.05, color
    ax.plot([x1, x1, x2, x2], [y, y + h, y + h, y], lw=1.5, c=col)
    ax.text((x1_loc + x2_loc) * .5, y + (h * 1.05), annotation, ha='center', va='bottom', color=col,
            fontsize='xx-large')


def normalizeByBaseline(data, groups, groupToNormalizeby, col2Normalize):

    means = data.groupby(groups).mean()
    # means[col2Normalize].loc[groupToNormalizeby]
    data[col2Normalize] = data[col2Normalize] / (means[col2Normalize].loc[groupToNormalizeby])

    return data