import os

import numpy as np
import pandas
import tensorflow as tf
from tensorboard.backend.event_processing import event_accumulator
from tensorflow.python.framework import graph_io
from tensorflow.python.tools import freeze_graph, optimize_for_inference_lib

import Tools.Utils as tu


class DatasetProto(object):
    def __init__(self, x, y, xT, yT, batchSize, numclases=None):
        self.batchsize = batchSize
        self.train = self.DatasetSubclass(x=x, y=y, n=batchSize)
        self.test = self.DatasetSubclass(x=xT, y=yT, n=batchSize)
        dShape = x.shape[1:]
        self.DATA_POINTS = dShape[0]
        self.DATA_SHAPE = dShape
        if numclases is None:
            self.CLASSES_NUM = y.shape[1]
        else:
            self.CLASSES_NUM = numclases

    class DatasetSubclass(object):
        """docstring for DatasetSubclass"""

        def __init__(self, x, y, n, shuffleAfterEpoch=True):
            """
            Creates dataset for training
            :param x: Inputs to the network
            :param y: Outputs of the network
            :param n: batchsize
            """
            self.shuffleAfterEpoch = shuffleAfterEpoch
            self.x = x
            self.y = y
            self.index = 0
            self.n = n
            self.subX = []
            self.subY = []
            self.sameBatch = True
            self.batchStart = 0
            self.batchEnd = 0

        def nextBatch(self, makeZero=0):
            self.batchStart = self.index
            self.batchEnd = self.index + self.n
            if self.batchEnd > self.x.shape[0]:
                self.batchEnd = self.x.shape[0]
            self.subX = self.x[self.batchStart:self.batchEnd].copy()
            self.subY = self.y[self.batchStart:self.batchEnd].copy()
            if makeZero > 0:
                shu = tu.randomOrder(self.subX.shape[0])[:makeZero]
                for i in shu:
                    nSams = np.random.randint(self.subX.shape[1] - 1)
                    self.subX[i, :nSams] = 0
            self.index += self.n
            if self.batchEnd >= self.x.shape[0]:
                # print("Reset index")
                self.index = 0
                self.sameBatch = False
                if self.shuffleAfterEpoch:
                    self.shuffleVars()

        def shuffleVars(self):
            # print('Shuffle dataset')
            shu = tu.randomOrder(self.x.shape[0])
            self.x = self.x[shu]
            self.y = self.y[shu]
            # print(self.x.shape[0])


class NeuralNetworkModel(object):
    def __init__(self, input_feed, output_truth, keep_prob, keep_prob_bool, learn_rate, num_class, accThr, builder=None,
                 input_name='x', output_name='output_node', input_size=(-1, 1, 1)):
        self.input_size = input_size
        self.output_name = output_name
        self.input_name = input_name
        self.accThr = accThr
        self.num_class = num_class
        self.learn_rate = learn_rate
        self.input_feed = input_feed
        self.output_truth = output_truth
        self.keep_prob = keep_prob
        self.keep_prob_bool = keep_prob_bool
        if builder is None:
            self.__buildModel()
        else:
            builder()
        self.saver = tf.train.Saver()

    def __buildModel(self):
        # this is just a placeholder function, it assumes that input_feed is mapped to
        y_outputs = tf.contrib.layers.fully_connected(inputs=self.input_feed, num_outputs=self.num_class)
        self.output = tf.squeeze(y_outputs, name=self.output_name)
        # self.correct = tf.equal(self.output - self.output_truth, name='AllPred')
        self.correct = tf.less(tf.abs(self.output - self.output_truth), self.accThr, name='AllPred')
        self.loss = tf.nn.l2_loss(self.output_truth - self.output)
        self.accuracy = tf.reduce_mean(tf.cast(self.correct, tf.float32), name='accuracy')
        self.train_step = tf.train.AdamOptimizer(self.learn_rate).minimize(self.loss)
        self.s = tf.shape(self.output_truth, name='shapeOf')

    def saveModel(self, sess, filePath):
        log = self.saver.save(sess, filePath)
        return log

    def restoreModel(self, sess, filePath):
        print("Restoring model")
        self.saver.restore(sess, filePath)

    def __optimizeModel(self, inputGraph):
        # inputGraph = tf.GraphDef()
        # with tf.gfile.Open(graphPath, "r") as f:
        #     data2read = f.read()
        #     inputGraph.ParseFromString(data2read)

        outputGraph = optimize_for_inference_lib.optimize_for_inference(
            inputGraph,
            [self.input_name],  # an array of the input node(s)
            [self.output_name],  # an array of output nodes
            tf.int32.as_datatype_enum)

    def freezeModel(self, resultsPath, modelName, optimize=False):

        print('Freezing model')
        with tf.Session() as sess:
            self.restoreModel(sess, resultsPath + modelName)
            input_graph_path = resultsPath + modelName + '_graph.pb'
            if optimize:
                print('Optimizing model')
                # input_graph_path = self.optimizeModel(input_graph_path, tf.get_default_graph())

            # xx = np.zeros((2, *self.input_size), np.float32)
            names = [n.name for n in tf.get_default_graph().as_graph_def().node]
            input_saver_def_path = ""
            input_binary = False
            output_node_names = self.output_name
            restore_op_name = "save/restore_all"
            filename_tensor_name = "save/Const:0"
            output_graph_path = resultsPath + modelName + '_graph_final.pb'
            clear_devices = False
            checkpoint_path = resultsPath + modelName
            try:
                freeze_graph.freeze_graph(input_graph_path, input_saver_def_path,
                                          input_binary, checkpoint_path, output_node_names,
                                          restore_op_name, filename_tensor_name,
                                          output_graph_path, clear_devices, "")
            except:
                freeze_graph.freeze_graph(input_graph_path, input_saver_def_path,
                                          input_binary, checkpoint_path, output_node_names,
                                          restore_op_name, filename_tensor_name,
                                          output_graph_path, clear_devices, "")
            print('Model saved to %s' % output_graph_path)

    def trainNetwork(self, maxEpochs, dataset, modelName, isRestore, restoreCP, resultsPath, restoreName, learn_rate,
                     earlyStop, pltTestN, initialAccuracy=1.0, saveChk=50):
        print('Training for %d epochs' % maxEpochs)
        if not os.path.exists(resultsPath):
            os.makedirs(resultsPath)
        # t, matR, matL, yyR, yyL = loadTestSubject(pltTestN)
        thrVal = self.accThr
        logInfo = ""
        stopVal = 5
        thr = initialAccuracy
        loss1 = False
        with tf.Session() as sess:
            if restoreCP:
                self.restoreModel(sess, resultsPath + 'chkp')
            elif isRestore:
                self.restoreModel(sess, resultsPath + restoreName)
            else:
                sess.run(tf.global_variables_initializer())
            print('variables init')
            c = -1

            for i in range(maxEpochs):
                tTraainVal = 0.01
                dataset.train.sameBatch = True
                train_accuracy = 0.0
                train_cost = 0.0
                thisC = c
                loss1 = not loss1
                while dataset.train.sameBatch:
                    dataset.train.nextBatch(np.random.randint(dataset.batchsize * 0.1))
                    batch = [dataset.train.subX, dataset.train.subY]
                    # print(dataset.train.subY.shape)
                    dictInput = {self.input_feed: batch[0], self.output_truth: batch[1], self.keep_prob: 1.0,
                                 thrVal: thr, learn_rate: tTraainVal, self.keep_prob_bool: False}
                    train_accuracy += self.accuracy.eval(feed_dict=dictInput)
                    # print(self.s.eval(feed_dict=dictInput))

                    # sp = self.spAcc.eval(feed_dict=dictInput)
                    # print(sp.shape)
                    # pred = self.output.eval(feed_dict=dictInput)
                    # gT = batch[1].copy()
                    # m = np.abs(pred-gT) > 0.9
                    # gT[m] = 1-gT[m]
                    # dictInput[self.output_truth] = gT
                    train_cost += self.loss.eval(feed_dict=dictInput)
                    c += 1
                    dictInput[self.keep_prob] = 0.5
                    dictInput[self.keep_prob_bool] = True
                    # if loss1:
                    self.train_step.run(feed_dict=dictInput)
                    # else:
                    # self.train_step2.run(feed_dict=dictInput)
                train_accuracy = train_accuracy / (c - thisC)
                train_cost = train_cost / (c - thisC)
                dataset.test.nextBatch()
                batch = [dataset.test.subX, dataset.test.subY]
                dictInput = {self.input_feed: batch[0], self.output_truth: batch[1], self.keep_prob: 1.0,
                             thrVal: thr, learn_rate: tTraainVal, self.keep_prob_bool: False}
                test_accuracy = self.accuracy.eval(feed_dict=dictInput)
                nowLog = "Epoch %d, step %d, acc val is %.3g, training accuracy %.3g, test accuracy %.3g," \
                         " cost mean %.6g" % (
                             i, c, thr, train_accuracy, test_accuracy, train_cost)
                print(nowLog)
                logInfo = logInfo + nowLog + '\n'
                if i % saveChk == 0:
                    print(self.saver.save(sess, resultsPath + 'chkp', global_step=i))
                    if pltTestN:
                        dataset.stairData.plotExampleWithFn(self.runInput, savefilePath=resultsPath + 'Epoch%d.png' % i,
                                                            batchsize=dataset.batchsize,
                                                            timeSize=dataset.DATA_SHAPE[0])

                # if i % 10 == 0:
                # plotAndSaveTest(t, matR, matL, yyR, yyL, dataset.batchsize, dataset.DATA_SHAPE[0],
                #                 dataset.DATA_SHAPE[0], self.output, self.input_feed, self.keep_prob,
                #                 '%sTest%04d' % (resultsPath, i))
                if test_accuracy >= 0.8:
                    thr = thr * 0.8

                if earlyStop and os.path.isfile(resultsPath + 'STOP.txt'):
                    break
            print(self.saver.save(sess, resultsPath + 'chkp', global_step=i))
            if pltTestN:
                dataset.stairData.plotExampleWithFn(self.runInput, savefilePath=resultsPath + 'Epoch%d.png' % i,
                                                    batchsize=dataset.batchsize,
                                                    timeSize=dataset.DATA_SHAPE[0])
            batch = [dataset.test.x, dataset.test.y]
            ac = self.accuracy.eval(feed_dict={self.input_feed: batch[0], self.output_truth: batch[1],
                                               self.keep_prob: 1.0, thrVal: thr, self.keep_prob_bool: False})
            print("Final test accuracy %g with thr %.3g" % (ac, thr))
            logInfo = logInfo + "Final test accuracy %g with thr %.3g" % (ac, thr)
            # print("saving model in " + resultsPath + modelName + '.ckpt')
            print(self.saveModel(sess, resultsPath + modelName))
            # print(saver.save(sess, resultsPath + modelName))
            graph_io.write_graph(sess.graph, resultsPath, modelName + '_graph.pb')
            with open(resultsPath + modelName + '_Log.txt', 'w') as f:
                f.write(logInfo)

    def runInput(self, x, batchsize=-1, timeSize=100):
        if batchsize < 1:
            dictInput = {self.input_feed: x, self.keep_prob: 1.0, self.keep_prob_bool: False}
            y = self.output.eval(feed_dict=dictInput)
        else:
            xx = createbatch(x, timeSize, aslist=False)
            auxData = DatasetProto.DatasetSubclass(xx, xx, batchsize, shuffleAfterEpoch=False)
            y = []
            while auxData.sameBatch:
                auxData.nextBatch()
                # batch = [dataset.train.subX, dataset.train.subY]
                dictInput = {self.input_feed: auxData.subX, self.keep_prob: 1.0, self.keep_prob_bool: False}
                y.append(self.output.eval(feed_dict=dictInput))
                # print(y[-1].shape)
            y = np.squeeze(np.concatenate(y, axis=0))
        return y


def createbatch(x, timeSize, aslist=False):
    actualSamples = np.arange(x.shape[0] - timeSize) + timeSize
    # actualSamples = randomOrder(x.shape[0] - timeSize)[:n] + timeSize
    x0 = np.zeros((timeSize, x.shape[1]), dtype=x.dtype)
    xi = x0.copy()
    xi[-1] = x[0]
    xii = [xi.copy()]
    for i in range(1, timeSize):
        xi = x0.copy()
        xi[-i:] = x[:i]
        xii.append(xi.copy())
    xx = xii + [x[j - timeSize:j, :] for j in actualSamples]
    if not aslist:
        xx = np.stack(xx, axis=0)
    return xx


def classes2probailityVector(y: np.ndarray, numClasses=None) -> np.ndarray:
    if numClasses is None:
        numClasses = y.max()
    yP = np.zeros((y.shape[0], numClasses))
    for j in range(y.shape[0]):
        yP[j, y[j]] = 1

    return yP


def readTensorBoard(f):
    ea = event_accumulator.EventAccumulator(f,
                                            size_guidance={  # see below regarding this argument
                                                event_accumulator.COMPRESSED_HISTOGRAMS: 500,
                                                event_accumulator.IMAGES: 4,
                                                event_accumulator.AUDIO: 4,
                                                event_accumulator.SCALARS: 0,
                                                event_accumulator.HISTOGRAMS: 1,
                                            })
    ea.Reload()
    a = {}
    for tag in ea.Tags()['scalars']:
        a[tag] = pandas.DataFrame(ea.Scalars(tag))['value'].values

    aP = pandas.DataFrame(a)
    return aP


def createTemporalSamples(yR, yL, shoeR, shoeL, laps, shoeFreq, matFreq, n, nSams, samplesBack, useP=False):
    """
    Creates the input and output for deep learning in the following format:
    X --> nSams x Sensor data x n
    Y --> n
    :param shoeFreq: sampling frequency of the shoe
    :param yR: on/off ground for right shoe
    :param yL: on/off ground for right shoe
    :param shoeR: Matrix of values from right shoe sampled at shoeFreq
    :param shoeL: Matrix of values from left shoe sampled at shoeFreq
    :param laps: list of start and end of laps
    :param matFreq: sampling frequency of the mat
    :param n: number of training samples to obtain form yR & yL
    :param nSams: number of samples to take from present to samplesBack
    :param samplesBack: how far back to obtain sensor reading
    :return: X & Y of temporal parameters
    """
    if useP:
        yR = tu.binary2Percentage(yR)
        yL = tu.binary2Percentage(yL)
        xC = np.empty((n, nSams, shoeR.shape[1]))
    # check that the laps are in the same time as shoes
    tMax = shoeL.shape[0] / shoeFreq
    goodLaps = np.where(laps[:, 1] <= tMax)
    lapsC = np.squeeze(laps[goodLaps, :].copy())
    if len(lapsC) == 0:
        return -1, -1, -1
    if len(lapsC.shape) == 1:
        lapsC = [lapsC]
    perLap = n // len(lapsC)
    if len(lapsC) * perLap < n:
        perLap += 1
    x = np.zeros((n, nSams, shoeR.shape[1]))
    y = np.zeros((n, nSams), dtype=np.float32)
    c = 0

    np.random.shuffle(lapsC)
    for lap in lapsC:
        tToUse = tu.randBetween(lap[0], lap[1], perLap)
        posT = tu.time2ind(tToUse, matFreq)
        posTmat = tu.time2ind(tToUse, shoeFreq)
        # posT = randBetween(lap[0], lap[1], perLap)
        # create index of posT, posT
        flipY = True
        for tim, timMat in zip(posT, posTmat):
            if c >= n:
                break
            indAux = np.round(np.linspace(tim - samplesBack, tim, nSams)).astype(int)
            # indAux = np.round(np.linspace(tim, tim - samplesBack, nSams)).astype(int)
            indAuxMat = np.round(np.linspace(timMat - samplesBack, timMat, nSams)).astype(int)
            if flipY:
                xx = shoeR
                xxC = shoeL
                yy = yR
            else:
                xx = shoeL
                xxC = shoeR
                yy = yL

            x[c, :, :] = xx[indAux, :]
            if useP:
                xC[c, :, :] = xxC[indAux, :]
            y[c, :] = yy[indAuxMat]
            c += 1
            flipY = not flipY
    if c < n:
        # if more samples were requested than it exist, I send the number of existing samples
        n = c
    shu = tu.randomOrder(n)
    x = x[shu, :, :]
    y = y[shu, :]
    if useP:
        xC = xC[shu, :, :]
        return x, y, xC
    # check if there is an error in y
    # if not np.all((y == 0) | (y == 1)):
    #     print('error in ys')
    return x, y, 0


def createTemporalSamples2Sides(yR, yL, shoeR, shoeL, laps, shoeFreq, matFreq, n, nSams, samplesBack, useP=False):
    """
    Creates the input and output for deep learning in the following format:
    X --> nSams x Sensor data x n
    Y --> n
    :param shoeFreq: sampling frequency of the shoe
    :param yR: on/off ground for right shoe
    :param yL: on/off ground for right shoe
    :param shoeR: Matrix of values from right shoe sampled at shoeFreq
    :param shoeL: Matrix of values from left shoe sampled at shoeFreq
    :param laps: list of start and end of laps
    :param matFreq: sampling frequency of the mat
    :param n: number of training samples to obtain form yR & yL
    :param nSams: number of samples to take from present to samplesBack
    :param samplesBack: how far back to obtain sensor reading
    :return: X & Y of temporal parameters
    """
    if useP:
        yR = tu.binary2Percentage(yR)
        yL = tu.binary2Percentage(yL)
        # xC = np.empty((n, nSams, shoeR.shape[1]))
    # check that the laps are in the same time as shoes
    tMax = shoeL.shape[0] / shoeFreq
    goodLaps = np.where(laps[:, 1] <= tMax)
    lapsC = np.squeeze(laps[goodLaps, :].copy())
    if len(lapsC) == 0:
        return -1, -1, -1
    if len(lapsC.shape) == 1:
        lapsC = [lapsC]
    perLap = n // len(lapsC)
    if len(lapsC) * perLap < n:
        perLap += 1
    x = np.empty((n, nSams, shoeR.shape[1], 2))
    y = np.empty((n, nSams, 2), dtype=np.float32)
    c = 0

    np.random.shuffle(lapsC)
    for lap in lapsC:
        tToUse = tu.randBetween(lap[0], lap[1], perLap)
        posT = tu.time2ind(tToUse, matFreq)
        posTmat = tu.time2ind(tToUse, shoeFreq)
        # posT = randBetween(lap[0], lap[1], perLap)
        # create index of posT, posT
        # flipY = True
        # xx = shoeR
        # xxC = shoeL
        # yy = yR
        for tim, timMat in zip(posT, posTmat):
            if c >= n:
                break
            indAux = np.round(np.linspace(tim - samplesBack, tim, nSams)).astype(int)
            # indAux = np.round(np.linspace(tim, tim - samplesBack, nSams)).astype(int)
            indAuxMat = np.round(np.linspace(timMat - samplesBack, timMat, nSams)).astype(int)
            x[c, :, :, 0] = shoeR[indAux, :]
            x[c, :, :, 1] = shoeL[indAux, :]

            y[c, :, 0] = yR[indAuxMat]
            y[c, :, 1] = yL[indAuxMat]
            c += 1
            # flipY = not flipY
    shu = tu.randomOrder(n)
    x = x[shu, :, :]
    y = y[shu, :]
    # check if there is an error in y
    # if not np.all((y == 0) | (y == 1)):
    #     print('error in ys')
    return x, y, 0

