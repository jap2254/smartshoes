# import os
# import sys
#
# sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import struct
import tkinter as tk
import warnings
from tkinter import ttk

import matplotlib.pyplot as plt
import numpy as np
import pandas
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from mpl_toolkits.mplot3d import Axes3D  # <-- Note the capitalization!

import Tools.Utils as tu


def wrap(angles):
    return (angles + np.pi) % (2 * np.pi) - np.pi


# from tkinter import *

class CheckControl(tk.Frame):
    def __init__(self, parent, title, names, ax, cmd=None, color=['red'], defaultVal=None, vertical=True, offset=0):
        """
        creates n checkboxes cluster, where n=len(names)
        """
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.titleLabel = ttk.Label(self, text=title)
        j = 0
        if vertical:
            self.titleLabel.grid(row=j, column=offset)
        else:
            self.titleLabel.grid(row=offset, column=j)
            j += 1
        self.vals = []
        self.boxes = []
        self.names = names
        # self.ax = ax
        if len(color) != len(names):
            color = [color[0]] * len(names)
        if defaultVal is None:
            dV = [0] * len(names)
        else:
            if len(defaultVal) is not len(names):
                if len(defaultVal) is 1:
                    dV = defaultVal * len(names)
                else:
                    warnings.warn('Default values should be 1 or equal to the number of names')
                    dV = defaultVal + [''] * (len(names) - len(defaultVal))
            else:
                dV = defaultVal
        if len(color) is not len(names):
            color = color * (len(names) - len(color))
        for i, (n, co, d) in enumerate(zip(names, color, dV)):
            self.vals.append(tk.IntVar())
            self.vals[-1].set(d)
            self.boxes.append(tk.Checkbutton(self, text=n, variable=self.vals[-1], command=cmd, background=co,
                                             foreground='black'))
            if vertical:
                self.boxes[-1].grid(row=i+1, column=offset)
            else:
                self.boxes[-1].grid(row=offset, column=i+1)
                j += 1

    def getAllValues(self):
        """
        Returns the values of all boxes in cluster
        """
        v = [val.get() for val in self.vals]
        # for val in self.vals:
        #     v.append(val.get())
        return v


class ConsoleFrame(tk.Frame):
    def __init__(self, parent, nLines=5, parseCMD=None):
        tk.Frame.__init__(self, parent)
        self.parseCMD = parseCMD
        self.nLines = nLines
        self.consoleVar = tk.StringVar()
        self.consoleList = []
        tk.Label(self, text='Console').pack(fill=tk.BOTH)
        self.console = tk.Message(self, textvariable=self.consoleVar, relief=tk.SUNKEN)
        self.console.pack()
        tk.Button(self, text='Clear', command=self.clearConsole).pack()

    def __append2console(self, text):
        self.consoleList.append(text)
        if len(self.consoleList) >= self.nLines:
            self.consoleList.pop(0)
        console = ''
        for t in self.consoleList:
            console += t + '\n'
        self.consoleVar.set(console)

    def set(self, text):
        self.__append2console(text)
        if self.parseCMD is not None:
            self.parseCMD(text)

    def clearConsole(self):
        self.consoleVar.set('')
        self.consoleList = []


class PlotPanel(tk.Frame):
    def __init__(self, parent, title, titles, names, plotColor, timestamp, allValsList, color=None, number2Plot=500,
                 shareAxis=None, showTime=False, useScale=True, figsize=(5, 2), useCheckFn=False):
        # this widget has checkboxes with variables and a matplot figure
        # the label goes to the left
        tk.Frame.__init__(self, parent)
        self.title = title
        self.useScale = useScale
        self.showTime = showTime
        self.s = number2Plot
        self.count = 0
        self.parent = parent
        self.timestamp = timestamp
        self.all = allValsList
        self.names = names
        # titleLabelFrame = tk.Frame(self,background=color)
        # self.titleLabel = ttk.Label(self, text=title, background=color)
        self.consoleVar = tk.StringVar()
        self.titleLabel = tk.Message(self, textvariable=self.consoleVar, background=color, foreground='white')
        if self.showTime:
            self.consoleVar.set(title + '\n 0.0 s')
        else:
            self.consoleVar.set(title)
        # titleLabelFrame.grid()
        self.titleLabel.pack(side=tk.LEFT, fill=tk.Y)
        # self.titleLabel.grid(row=0,column=0)
        self.checkFrame = tk.Frame(self)
        self.checkFrame.pack(side=tk.LEFT)
        # self.checkFrame.grid(row=0,column=1)
        self.checks = []
        self.f = plt.figure(figsize=figsize, dpi=100)
        # self.f = plt.figure(figsize=(5, 5), dpi=100)
        self.axis = self.f.add_subplot(111, sharex=shareAxis, sharey=shareAxis)
        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.draw()
        # self.axis = self.f.gca(projection='3d')
        self.plotColor = plotColor
        for t, n, c in zip(titles, names, self.plotColor):
            if useCheckFn:
                self.checks.append(
                    CheckControl(self.checkFrame, t, n, self.axis, color=c, cmd=self.plotControlFromChecks))
            else:
                self.checks.append(CheckControl(self.checkFrame, t, n, self.axis, color=c))
            # self.checks[-1].grid(row=0,column=len(self.checks))
            self.checks[-1].pack(side=tk.LEFT)

        # self.axis.plot([1,2,3,4,5,6,7,8],[5,6,1,3,8,9,3,5])

        self.canvas.get_tk_widget().pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        # self.canvas.get_tk_widget().grid(row=0,column=2)
        self.toolbar = NavigationToolbar2Tk(self.canvas, self)
        self.toolbar.update()
        # self.canvas._tkcanvas.grid()
        self.canvas._tkcanvas.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

    def plotControlFromChecks(self):
        # this function will plot all the values from all
        self.axis.clear()
        useScale = self.useScale
        s = self.s
        # s = 500
        # I'll plot only the new 500 values
        i = 0
        if self.showTime:
            if type(self.timestamp[0]) is list:
                t = self.timestamp[0][-1] / 1000.0
                # print(t.shape)
            else:
                t = self.timestamp[-1] / 1000.0
            self.consoleVar.set(self.title + '\n %.1fs' % t)
        # t = np.array(t1) / 1000
        for ch, col in zip(self.checks, self.plotColor):
            vals = ch.getAllValues()
            for v, c1 in zip(vals, col):
                if v == 1:
                    # plot that var

                    if type(self.timestamp[0]) is list:

                        t = np.array(self.timestamp[i][-s:]) / 1000.0
                        # print(t.shape)
                    else:
                        t = np.array(self.timestamp[-s:]) / 1000.0
                    y = np.array(self.all[i])
                    if y.size > t.size:
                        y = y[:t.size]
                    if y.size < t.size:
                        t = t[:y.size]
                    # divAux = FACTOR
                    # ymin = SCALE_MIN
                    if useScale:
                        ymin = y.min()
                        divAux = y.max() - y.min()
                    else:
                        ymin = 0.0
                        divAux = 1.0
                    # divAux = 0
                    if divAux == 0:
                        divAux = 1
                    try:
                        self.axis.plot(t, (y[-s:] - ymin) / divAux, color=c1)
                    except ValueError:
                        a = 1
                i = i + 1
        if useScale:
            self.axis.set_ylim(-0.1, 1.1)
        # self.axis.set_ylim(-200, 2200)
        self.canvas.draw()

    def plotControlFromChecksTime(self, tV, extraT=2):
        # this function will plot all the values from all
        if len(self.all[0]) < 2:
            return
        useScale = self.useScale
        self.axis.clear()
        # s = self.s
        # s = 500
        # I'll plot only the new 500 values
        i = 0
        if self.showTime:
            if type(self.timestamp[0]) is list:
                t = self.timestamp[0][-1] / 1000.0
                # print(t.shape)
            else:
                t = self.timestamp[-1] / 1000.0
            self.consoleVar.set(self.title + '\n %.1fs' % t)
        # t = np.array(t1) / 1000
        for ch, col in zip(self.checks, self.plotColor):
            vals = ch.getAllValues()
            for v, c1 in zip(vals, col):
                if v == 1:
                    # plot that var
                    if len(self.all[i]) < 2:
                        continue
                    if type(self.timestamp[0]) is list:
                        dt = self.timestamp[i][-1] - self.timestamp[i][-2]
                        s = int(tV * 1000 // dt)
                        t = np.array(self.timestamp[i][-s:]) / 1000
                        # print(s)
                    else:
                        dt = self.timestamp[-1] - self.timestamp[-2]
                        s = int(tV * 1000 // dt)
                        t = np.array(self.timestamp[-s:]) / 1000
                    y = np.array(self.all[i])
                    if y.size > t.size:
                        y = y[:t.size]
                    if y.size < t.size:
                        t = t[:y.size]
                    # divAux = FACTOR
                    # ymin = SCALE_MIN
                    if useScale:
                        ymin = y.min()
                        divAux = y.max() - y.min()
                    else:
                        ymin = 0.0
                        divAux = 8000.0
                    # divAux = 0
                    if divAux == 0:
                        divAux = 1
                    try:
                        self.axis.plot(t, (y[-s:] - ymin) / divAux, color=c1)
                    except ValueError:
                        a = 1
                i = i + 1
        if useScale:
            self.axis.set_ylim(-0.1, 1.1)
        x_lim = self.axis.get_xlim()
        self.axis.set_xlim(x_lim[0], x_lim[1] + extraT)
        # self.axis.set_ylim(-200, 2200)
        self.canvas.draw()


class ConsoleFramePlus(ConsoleFrame):
    def __init__(self, parent, nLines=5):
        ConsoleFrame.__init__(self, parent, nLines=nLines, parseCMD=self.parseMSG)
        self.ipBase = '192.168.0.'
        self.ipAddress = {}
        self.recordNewAdress = False
        # self.ipAddress = {'IP_BASE': '192.168.0.', 'r': '192.168.0.103', 'l': '192.168.0.102'}

    def parseMSG(self, text):
        if self.recordNewAdress:
            self.__changeIP(text)

    def __changeIP(self, text):
        serchStr = 'Hello from'
        if serchStr in text:
            newIP = text.split(',')[0].split(' ')[-1]
            lab = text.split(',')[1].split(' ')[1]
            self.ipAddress[lab] = self.ipBase + newIP
            self.ipAddress = dict(sorted(self.ipAddress.items()))
        print('Found %d boards' % len(self.ipAddress))
        for k in self.ipAddress.keys():
            print('%s is %s' % (k, self.ipAddress[k]))


class PlotPanelTimer(tk.Frame):
    def __init__(self, parent, label, **kwargs):
        super(PlotPanelTimer, self).__init__(parent, **kwargs)
        self._consoleVar = tk.StringVar()
        self._titleLabel = tk.Message(self, textvariable=self._consoleVar)
        self._titleLabel.pack()
        self.label = label
        self.setNewTime(0.0)

    def setNewTime(self, newT):
        self._consoleVar.set('%s: %.1fs' % (self.label, newT))


class PlotPanelPandasContainer(tk.Frame):
    def __init__(self, parent, console: ConsoleFramePlus, getDictIDs, titles, names, plotColor, pingCMD, visualizeTime,
                 extraPlots=0, postPopulateFn=None, multiplier=10, **kwargs):
        super(PlotPanelPandasContainer, self).__init__(parent, **kwargs)
        self.titles = titles
        self.postPopulateFn = postPopulateFn
        self.visualizeTime = visualizeTime
        self.names = names
        self.plotColor = plotColor
        self.console = console
        self.getDictIDs = getDictIDs
        self.pindCMD = pingCMD
        topFrame = tk.Frame(self)
        topFrame.pack(fill=tk.BOTH, expand=tk.YES)
        self.plotChoice = tk.StringVar(self)
        self.choices = {'None'}
        self.plotChoice.set('None')
        # self.plotChoice.trace('w', self.dropDownChanged)
        self.popUp = ttk.OptionMenu(topFrame, self.plotChoice, None, *self.choices, command=self.dropDownChanged)
        self.pingAdd = tk.Button(topFrame, text='Populate objects', command=self.populatePlotPanel)
        self.pingAdd["state"] = "disabled"
        self.pingBorads = tk.Button(topFrame, text='Get Boards', command=self._GetBoards)
        # self.addPlotButton = tk.Button(topFrame, text='Add Plot', command=self.addExtraPlot)
        self.popUp.pack(side=tk.LEFT)
        # self.addPlotButton.pack(side=tk.RIGHT)
        self.pingBorads.pack(side=tk.RIGHT)
        self.pingAdd.pack(side=tk.RIGHT)
        self.middleFrame = tk.Frame(self)
        self.middleFrame.pack(fill=tk.BOTH, expand=tk.YES)
        self.bottomFrame = tk.Frame(self)
        self.bottomFrame.pack(fill=tk.BOTH, expand=tk.YES)
        self.currentFrame = tk.Frame(self.bottomFrame)
        self.currentFrame.pack()
        self.plots = {}
        self.populated = False
        self.multiplier = multiplier
        # self.extraPlotsFrames = tk.Frame(self)
        # self.extraPlotsFrames.pack(fill=tk.BOTH, expand=tk.YES)
        # self.extraPlots = {}
        # for j in range(extraPlots):
        #     # self.extraPlots[str(j)] = self.ContainerSubClass(extraPlotsFrames, self.plots, self.choices)
        #     # self.extraPlots[str(j)].pack()
        #     self.addExtraPlot()

    def _GetBoards(self):
        self.pingAdd["state"] = "normal"
        self.pindCMD()

    def populatePlotPanel(self, resetPlots=False):

        # self.getDictIDs()
        self.pingAdd["state"] = "disabled"
        if resetPlots:
            self.choices.clear()
            ke = [k for k in self.plots.keys()]
            for k in ke:
                self.plots.pop(k)
        self.populated = True
        self.console.recordNewAdress = False
        for k in self.console.ipAddress:
            self.choices.add(k)
            if k not in self.plots:
                self.plots[k] = {'shoe': tu.DeepSoleObject(k)}
                self.plots[k]['plot'] = PlotPanelPandas(self.bottomFrame, k, self.titles, self.names, self.plotColor,
                                                        [self.plots[k]['shoe'].timestamp for _ in
                                                         self.plots[k]['shoe'].all],
                                                        self.plots[k]['shoe'].dataFrame, showTime=True, color='black',
                                                        multiplier=self.multiplier)
                self.plots[k]['timer'] = PlotPanelTimer(self.middleFrame, k)
                self.plots[k]['timer'].pack(side=tk.LEFT)
        # self.popUp['menu'].delete(0, 'end')

        # self.choices.add('test')
        # self.choices.add('test2')
        self.popUp.set_menu(None, *self.choices)
        if self.postPopulateFn is not None:
            self.postPopulateFn()

        # for k in self.extraPlots:
        #     self.extraPlots[k].updatesChoices()
        # for c in self.choices:
        #     self.popUp['menu'].add_command(label=c, command=tk._setit(self.plotChoice, c))

    def dropDownChanged(self, *args):
        val = self.plotChoice.get()
        self.currentFrame.pack_forget()
        if 'None' in val:
            return
        self.currentFrame = self.plots[val]['plot']
        self.currentFrame.pack()

    def updatePlots(self):
        # fn = lambda x: (x/8000 + np.pi) % (2 * np.pi) - np.pi
        # for k in self.extraPlots:
        #     self.extraPlots[k].updatePlots()
        if not self.populated:
            return
        val = self.plotChoice.get()
        if 'None' not in val:
            self.currentFrame.plotControlFromChecksTime(self.visualizeTime, extraT=1.0)

        for k in self.plots:
            if self.plots[k]['plot'].all.shape[0] is 0:
                continue
            self.plots[k]['timer'].setNewTime(self.plots[k]['plot'].all.index[-1] / 1000.0)

    # def addExtraPlot(self):
    #     lab = str(len(self.extraPlots))
    #     self.extraPlots[lab] = self.ContainerSubClass(self.extraPlotsFrames, self.plots, self.choices)
    #     self.extraPlots[lab].pack()

    class ContainerSubClass(tk.Frame):
        def __init__(self, parent, plotsDict, choices, **kwargs):
            super(PlotPanelPandasContainer.ContainerSubClass, self).__init__(parent, **kwargs)
            topFrame = tk.Frame(self)
            topFrame.pack(fill=tk.BOTH, expand=tk.YES)
            self.plots = plotsDict
            self.plotChoice = tk.StringVar(self)
            self.choices = choices
            # self.choices = {'None'}
            self.plotChoice.set('None')
            # self.plotChoice.trace('w', self.dropDownChanged)
            self.popUp = ttk.OptionMenu(topFrame, self.plotChoice, None, *self.choices, command=self.dropDownChanged)
            self.popUp.pack()
            self.currentFrame = tk.Frame(topFrame)
            self.currentFrame.pack()

        def updatesChoices(self):
            self.popUp.set_menu(None, *self.choices)

        def dropDownChanged(self, *args):
            val = self.plotChoice.get()
            self.currentFrame.pack_forget()
            if 'None' in val:
                return
            self.currentFrame = self.plots[val]['plot']
            self.currentFrame.pack()

        def updatePlots(self):
            # fn = lambda x: (x/8000 + np.pi) % (2 * np.pi) - np.pi
            val = self.plotChoice.get()
            if 'None' in val or not self.populated:
                return
            self.currentFrame.plotControlFromChecksTime(self.visualizeTime, extraT=1.0)


class PlotPanelPandas(PlotPanel):
    def __init__(self, parent, title, titles, names, plotColor, timestamp, allValsPandas: pandas.DataFrame, color=None,
                 number2Plot=500, shareAxis=None, showTime=False, useScale=True, multiplier=10,
                 timerFrame: PlotPanelTimer = None):

        PlotPanel.__init__(self, parent, title, titles, names, plotColor, timestamp, allValsPandas, color=color,
                           number2Plot=number2Plot, shareAxis=shareAxis, showTime=showTime, useScale=useScale)
        self.button = ButtonPanel(self, [['Reset Scale']], [[self.resetScale]])
        self.button.pack()
        self.maxValsAux = None
        self.minValsAux = None
        self.dt = 0
        self.multiplier = multiplier
        self.counter = multiplier
        self.timerFrame = timerFrame

    def plotControlFromChecks(self):
        warnings.warn('Please do not use this funciton with plotPanelPandas')

    def resetScale(self):
        self.maxValsAux = None

    def plotControlFromChecksTime(self, tV, extraT=2, preprocess=None, justTime=False):
        '''
        :param tV: time to show in seconds
        :param extraT: extra time (empty) to show for visualization
        :return: None
        '''
        # this function will plot all the values from all
        if self.all.shape[0] is 0:
            return
        if self.counter != 0:
            self.counter -= 1
            return
        # print(self.counter)
        self.counter = self.multiplier
        # i = 0

    # def _plotThread(self, tV, extraT, preprocess):
        dt = 1000.0 / np.diff(self.all.index.values).mean()
        if np.isnan(dt):
            return
        self.dt = dt
        if self.showTime:
            t = self.all.index[-1] / 1000.0
            # print(self.all.columns)
            s_val = self.all['sync'].values[-1] == 1
            self.consoleVar.set(self.title + '\n %.1fs\n%.1fHz\n Sync: %d' % (t, dt, s_val))
            if self.timerFrame is not None:
                self.timerFrame.setNewTime(t)
        vals = np.array([ch.getAllValues() for ch in self.checks]).flatten()
        if np.all(vals == 0):
            return
        self.axis.cla()

        # I'll plot only the new 500 values

        # t = np.array(t1) / 1000
        # timeS = int((self.all.index[-1] - tV * 1000 ) // self.all.index.get_values().mean())
        # # print(tV * 1000)
        # windowVals = self.all.iloc[-timeS:, :].copy() / 4096

        windowVals = self.all.iloc[-int(tV * dt):, :].copy()
        if preprocess is not None:
            for k, fn in zip(*preprocess):
                windowVals[k] = fn(windowVals[k])
        if self.useScale:
            if self.maxValsAux is None:
                self.maxValsAux = pandas.Series(np.zeros_like(windowVals.max()), index=windowVals.max().index)
                self.minValsAux = self.maxValsAux.copy() + 4096
            else:
                self.maxValsAux.loc[windowVals.max() > self.maxValsAux] = windowVals.max().loc[
                    windowVals.max() > self.maxValsAux]
                self.minValsAux.loc[windowVals.min() < self.minValsAux] = windowVals.min().loc[
                    windowVals.min() < self.minValsAux]
            divAux = self.maxValsAux - self.minValsAux
            divAux[divAux == 0] = 1
            windowVals = (windowVals - self.minValsAux) / (divAux)
        # print(dt)
        # print(windowVals.shape[0])
        windowVals.index /= 1000.0
        for ch, col, colNames in zip(self.checks, self.plotColor, self.names):
            vals = ch.getAllValues()
            for v, c1, na in zip(vals, col, colNames):
                if na not in windowVals.columns:
                    continue
                if v == 1:
                    # print(na)
                    # try:
                    windowVals[na].plot(ax=self.axis, color=c1)
                        # self.axis.plot(t, (y[-s:] - ymin) / divAux, color=c1)
                    # except TypeError:
                    #     a = 1
                # i = i + 1
        self.axis.set_ylim(-0.1, 1.1)
        x_lim = self.axis.get_xlim()
        self.axis.set_xlim(x_lim[0], x_lim[1] + extraT)
        # self.axis.set_ylim(-200, 2200)
        self.canvas.draw()


class PlotPanel3D(tk.Frame):
    def __init__(self, parent, title, titles, names, plotColor, angleValsList, figsize=(5, 5), color=None):
        # this widget has checkboxes with variables and a matplot figure
        # the label goes to the left
        tk.Frame.__init__(self, parent)
        self.rotationOrder = [1, 2, 3]
        self.all = angleValsList
        self.parent = parent
        self.titleLabel = ttk.Label(self, text=title, background=color)
        self.titleLabel.pack(side=tk.LEFT, fill=tk.Y)
        self.checkFrame = tk.Frame(self)
        self.checkFrame.pack(side=tk.LEFT)
        self.checks = []
        self.f = plt.figure(figsize=figsize)
        # self.axis = self.f.gca(projection='3d')
        self.axis = Axes3D(self.f)
        self.plotColor = plotColor
        for t, n, c in zip(titles, names, self.plotColor):
            self.checks.append(CheckControl(self.checkFrame, t, n, self.axis, color=c, cmd=self.plotControlFromChecks))
            # self.checks[-1].grid(row=0,column=len(self.checks))
            self.checks[-1].pack(side=tk.LEFT)

        self.canvas = FigureCanvasTkAgg(self.f, self)
        # self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        # self.toolbar = NavigationToolbar2Tk(self.canvas, self)
        # self.toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.Hs = []
        self.systs = []
        for j in range(len(names)):
            self.Hs.append(np.identity(4))
            # self.Hs[-1][0, 3] = j
            self.systs.append(AxesSystemPlotter(self.axis))
            self.systs[-1].rotateAndPlotSystem(H=self.Hs[-1])
        self.canvas.draw()

    def plotControlFromChecks(self):
        # this function will plot all the values from all
        self.axis.clear()

        i = 0
        # t = np.array(t1) / 1000
        for ch, col in zip(self.checks, self.plotColor):
            vals = ch.getAllValues()
            for v, c1 in zip(vals, col):
                if v == 1:
                    # Get the values of rotation
                    if len(self.all[i][0]) is 0:
                        self.systs[i].rotateAndPlotSystem(H=self.Hs[i], clearAx=False)
                        continue
                    # self.all is a list of list with [rx, ry, rz]
                    eu = [x[-1] / 8000.0 for x in self.all[i]]
                    self.Hs[i][:3, :3] = tu.euler2Rot(*eu, rotationOrder=self.rotationOrder).T
                    self.systs[i].rotateAndPlotSystem(H=self.Hs[i], clearAx=False)

                i = i + 1

        self.axis.set_xlim(-2, 2)
        self.axis.set_ylim(-2, 2)
        self.axis.set_zlim(-2, 2)
        self.canvas.draw()


class PlotPanel3DPandas(tk.Frame):
    def __init__(self, parent, title, titles, names, plotColor, allValsPandas: pandas.DataFrame,
                 figsize=(5, 5), color=None):
        # this widget has checkboxes with variables and a matplot figure
        # the label goes to the left
        tk.Frame.__init__(self, parent)
        self.rotationOrder = [1, 2, 3]
        self.all = allValsPandas
        self.parent = parent
        self.titleLabel = ttk.Label(self, text=title, background=color)
        self.titleLabel.pack(side=tk.LEFT, fill=tk.Y)
        self.checkFrame = tk.Frame(self)
        self.checkFrame.pack(side=tk.LEFT)
        self.checks = []
        self.f = plt.figure(figsize=figsize)
        # self.axis = self.f.gca(projection='3d')
        self.axis = Axes3D(self.f)
        self.plotColor = plotColor
        for t, n, c in zip(titles, names, self.plotColor):
            self.checks.append(CheckControl(self.checkFrame, t, n, self.axis, color=c, cmd=self.plotControlFromChecks))
            # self.checks[-1].grid(row=0,column=len(self.checks))
            self.checks[-1].pack(side=tk.LEFT)

        self.canvas = FigureCanvasTkAgg(self.f, self)
        # self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        # self.toolbar = NavigationToolbar2Tk(self.canvas, self)
        # self.toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.Hs = []
        self.systs = []
        for j in range(len(names)):
            self.Hs.append(np.identity(4))
            # self.Hs[-1][0, 3] = j
            self.systs.append(AxesSystemPlotter(self.axis))
            self.systs[-1].rotateAndPlotSystem(H=self.Hs[-1])
        self.canvas.draw()

    def plotControlFromChecks(self):
        # this function will plot all the values from all
        self.axis.clear()

        i = 0
        # t = np.array(t1) / 1000
        for ch, col in zip(self.checks, self.plotColor):
            vals = ch.getAllValues()
            for v, c1 in zip(vals, col):
                if v == 1:
                    # Get the values of rotation
                    if self.all[i].shape[0] is 0:
                        self.systs[i].rotateAndPlotSystem(H=self.Hs[i], clearAx=False)
                        continue
                    # self.all is a list of list with [rx, ry, rz]
                    # eu = [x[-1] / 8000.0 for x in self.all[i]]
                    eu = [x / 8000.0 for x in self.all[i][['EUy', 'EUz', 'EUx']].iloc[-1].values]
                    self.Hs[i][:3, :3] = tu.euler2Rot(*eu, rotationOrder=self.rotationOrder).T
                    self.systs[i].rotateAndPlotSystem(H=self.Hs[i], clearAx=False)

                i = i + 1

        self.axis.set_xlim(-2, 2)
        self.axis.set_ylim(-2, 2)
        self.axis.set_zlim(-2, 2)
        self.canvas.draw()


class InputWithButton(tk.Frame):
    def __init__(self, parent, label, buttonLabel, buttonCmd, browseTitle, browseFiletypes, defaultVal='',
                 dialogType=0):
        tk.Frame.__init__(self, parent)
        self.dialogType = dialogType
        tk.Label(self, text=label, width=10).grid(row=0, column=0)
        self.browseTitle = browseTitle
        self.browseFiletypes = browseFiletypes
        self.val = tk.StringVar()
        tk.Entry(self, textvariable=self.val, width=80).grid(row=0, column=1)
        self.val.set(defaultVal)
        # frBot = tk.Frame(self)
        self.cmd = buttonCmd
        self.exButton = tk.Button(self, text=buttonLabel, command=self.exeCMD, bg='yellow')
        self.exButton.grid(row=0, column=3)
        tk.Button(self, text='Browse', command=self._browseCMD).grid(row=0, column=2)
        # self.statusIm = tk.Label(self, background='yellow', width=3)
        # self.statusIm.grid(row=0, column=4)
        self.getValue = self.val.get

    def exeCMD(self):
        self.cmd(self.getValue())
        # self.exButton.config(bg='green')
        self.changeButtonColor('g')

    def _browseCMD(self):
        root = tk.Tk()
        if self.dialogType == 0:
            fileN = tk.filedialog.askopenfilename(parent=root, title=self.browseTitle, filetypes=self.browseFiletypes)
        elif self.dialogType == 1:
            fileN = tk.filedialog.asksaveasfilename(parent=root, title=self.browseTitle, filetypes=self.browseFiletypes,
                                                    defaultextension=self.browseFiletypes[0][1][1:])
        else:
            fileN = tk.filedialog.askdirectory(parent=root, title=self.browseTitle)
        root.withdraw()
        if fileN is None:
            return 0
        self.val.set(fileN)
        # self.exButton.config(bg='yellow')
        self.exeCMD()

    def changeButtonColor(self, typeE):
        if typeE is 'Error':
            c = 'red'
        elif typeE is 'Warn':
            c = 'yellow'
        else:
            c = 'green'
        self.exButton.config(bg=c)


class ButtonPanel(tk.Frame):
    def __init__(self, parent, labels, cmds, width=20):
        tk.Frame.__init__(self, parent)
        self.buttons = {}
        for row, (ls, cs) in enumerate(zip(labels, cmds)):
            for col, (l, c) in enumerate(zip(ls, cs)):
                self.buttons[l] = tk.Button(self, text=l, command=c, width=width)
                self.buttons[l].grid(row=row, column=col)

    def enableDisableButtons(self, labels, states):
        if type(labels) is not list:
            self.buttons[labels]["state"] = states
        else:
            if type(states) is not list:
                states = [states] * len(labels)

            for l, s in zip(labels, states):
                self.buttons[l]["state"] = s


class StringInputs(tk.Frame):
    def __init__(self, parent, names, vertical=True, defaultVals=None, dtype=np.int, useInt=True):
        tk.Frame.__init__(self, parent)
        # self.root = tk.Toplevel(self)
        self.vals = None
        self.valR = []
        self.dtype = dtype
        # names = ['Freq']
        self.allNames = names
        self.useInt = useInt
        if defaultVals is None:
            dV = [''] * len(names)
        else:
            if len(defaultVals) is not len(names):
                if len(defaultVals) is 1:
                    dV = defaultVals * len(names)
                else:
                    warnings.warn('Default values should be 1 or equal to the number of names')
                    dV = defaultVals + [''] * (len(names) - len(defaultVals))
            else:
                dV = defaultVals
        for i, (n, d) in enumerate(zip(names, dV)):
            if vertical:
                # label
                tk.Label(self, text=n).grid(row=0, column=i + 1)
                # box 1
                self.valR.append(tk.StringVar())
                tk.Entry(self, textvariable=self.valR[-1], validate='focusout').grid(row=1, column=i + 1)
            else:
                # label
                tk.Label(self, text=n).grid(row=i + 1, column=0)
                # box 1
                self.valR.append(tk.StringVar())
                tk.Entry(self, textvariable=self.valR[-1], validate='focusout').grid(row=i + 1, column=0)
            self.valR[-1].set(d)
        # root.quit()

    def readValues(self):
        # self.vals = np.zeros((len(self.allNames)), dtype=self.dtype)
        # if not self.useInt:
        self.vals = [[] for _ in self.allNames]
        for i, r in enumerate(self.valR):
            if self.useInt:
                self.vals[i] = int(r.get())
            else:
                self.vals[i] = r.get()
        return np.array(self.vals)


def createCMD(inst, te=None):
    if inst == 4:  # name
        c = struct.pack('5c ' + str(len(te)) + 's 3c', bytes([0xA]), bytes([0xD]), bytes([0xA]), bytes([inst]),
                        bytes([len(te)]), te.encode('utf-8'), bytes([0xC]), bytes([0xD]), bytes([0xF]))
    elif inst == 8:  # motor cmd
        c = struct.pack('14c', bytes([0xA]), bytes([0xD]), bytes([0xA]),
                        bytes([inst]), bytes([te[0]]), bytes([te[1]]), bytes([te[2]]), bytes([te[3]]), bytes([te[4]]),
                        bytes([te[5]]), bytes([te[6]]), bytes([0xC]), bytes([0xD]), bytes([0xF]))
    elif inst == 5 or inst == 14:  # change side, change frequency
        c = struct.pack('9c', bytes([0xA]), bytes([0xD]), bytes([0xA]), bytes([inst]), bytes([te[0]]), bytes([te[1]]),
                        bytes([0xC]), bytes([0xD]), bytes([0xF]))
    elif inst == 0xC:  # Sync
        c = struct.pack('8c', bytes([0xA]), bytes([0xD]), bytes([0xA]), bytes([inst]), bytes([te]), bytes([0xC]),
                        bytes([0xD]), bytes([0xF]))
    else:
        c = struct.pack('7c', bytes([0xA]), bytes([0xD]), bytes([0xA]), bytes([inst]), bytes([0xC]), bytes([0xD]),
                        bytes([0xF]))

    return c


class AxesSystemPlotter(object):
    def __init__(self, ax, H0=None):
        self.axis = ax
        self.xi = np.array([1.0, 0.0, 0.0, 1.0])
        self.yi = np.array([0.0, 1.0, 0.0, 1.0])
        self.zi = np.array([0.0, 0.0, 1.0, 1.0])

    def rotateAndPlotSystem(self, H=None, clearAx=False):
        if H is None:
            H = np.identity(4)
        xSpace = np.matmul(H, self.xi)[:-1]
        ySpace = np.matmul(H, self.yi)[:-1]
        zSpace = np.matmul(H, self.zi)[:-1]
        o = H[:-1, -1]
        if clearAx:
            self.axis.clear()
        for v, c in zip([xSpace, ySpace, zSpace], ['red', 'green', 'blue']):
            self.axis.quiver(*o, *v, length=1.0, color=c)
        # plt.show()


class InputsMatrix(tk.Frame):
    def __init__(self, parent, defaultVals: pandas.DataFrame, inputWidth, parseFN=int, **kwargs):
        # tk.Frame.__init__(parent, **kwargs)
        super(InputsMatrix, self).__init__(parent, **kwargs)
        self.parseFN = parseFN
        self.values = defaultVals.copy()
        rowNames = defaultVals.index.values
        colNames = defaultVals.columns.values
        self.strVals = []
        for i, rN in enumerate(rowNames):
            tk.Label(self, text=rN).grid(row=i + 1, column=0)
            for j, cN in enumerate(colNames):
                tk.Label(self, text=cN).grid(row=0, column=j + 1)
                self.strVals.append(tk.StringVar(self))
                tk.Entry(self, textvariable=self.strVals[-1], width=inputWidth).grid(row=i + 1, column=j + 1)
                self.strVals[-1].set(defaultVals.loc[rN, cN])

    def getValues(self):
        c = 0
        for j in self.values.index.values:
            for i in self.values.columns.values:
                try:
                    self.values.loc[j, i] = self.parseFN(self.strVals[c].get())
                except ValueError:
                    self.values.loc[j, i] = self.parseFN(0)
                c += 1
        return self.values
