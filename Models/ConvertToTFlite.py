import tensorflow as tf
import numpy as np

modelPath = 'C:/Users/Antonio/workspace_iot/SmartShoe/Models/GaitPercentage.h5'
liteModelPath = 'C:/Users/Antonio/workspace_iot/SmartShoe/Models/GaitPercentage.tflite'

model = tf.keras.models.load_model(modelPath)

converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()

# Save the model.
with open(liteModelPath, 'wb') as f:
    f.write(tflite_model)
