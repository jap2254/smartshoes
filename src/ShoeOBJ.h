/*
 * ShoeOBJ.h
 *
 *  Created on: May 26, 2017
 *      Author: Antonio
 */

#ifndef SHOEOBJ_H_
#define SHOEOBJ_H_
#include <stdint.h>
#include "mraa.hpp"

#define M_MBALL 0
#define M_LBALL 1
#define M_HEEL 2
#define P_TOE 2
#define P_BALL 1
#define P_HEEL 0
class SHOE{
public:
	LSM9DS0 *imu;
	ads1015 *adc;
	pca9685 *pwm;
	//SHOE(uint8_t gAddr, uint8_t xmAddr,uint8_t adcI2CAddress,uint8_t pwmI2CAddress);
	SHOE(uint8_t gAddr, uint8_t xmAddr,uint8_t adcI2CAddress,uint8_t pwmI2CAddress,mraa::I2c* adc_i2c);
	void begin();
	void SetVibrationValue(float mBallVal,float lBallVall, float heelVal);
	void readIMU();
private:
	//mraa::I2c* adc_i2c;
	//mraa::I2c* pwm_i2c;

};



#endif /* SHOEOBJ_H_ */
