/*
 * ShoeOBJ.cpp
 *
 *  Created on: May 26, 2017
 *      Author: Antonio Prado
 *
 *  This contains all the parts of the shoe:
 */
#include <iostream>
#include "mraa.hpp"
#include <unistd.h>
#include "SFE_LSM9DS0.h"
#include "SparkFun_pca9685_Edison.h"
#include "SparkFunADS1015.h"
#include "ShoeOBJ.h"

#include<stdio.h> //printf
#include<string.h> //memset
#include<stdlib.h> //exit(0);
#include<arpa/inet.h>
#include<sys/socket.h>
#include<errno.h>
#include <sys/time.h>
#include <time.h>

using namespace std;
SHOE::SHOE(uint8_t gAddr, uint8_t xmAddr,uint8_t adcI2CAddress,uint8_t pwmI2CAddress,mraa::I2c* adc_i2c):
				imu( new LSM9DS0(gAddr, xmAddr)),
				adc(new ads1015 (adc_i2c, adcI2CAddress)),
				pwm(new pca9685(adc_i2c, pwmI2CAddress))

{

}

void SHOE::begin(){
	//IMU:

	//  The begin() function sets up some basic parameters and turns the device
	//  on; you may not need to do more than call it. It also returns the "whoami"
	//  registers from the chip. If all is good, the return value here should be
	//  0x49d4. Here are the initial settings from this function:
	//  Gyro scale:        245 deg/sec max
	//  Xl scale:          4g max
	//  Mag scale:         2 Gauss max
	//  Gyro sample rate:  95Hz
	//  Xl sample rate:    100Hz
	//  Mag sample rate:   100Hz
	// These can be changed either by calling appropriate functions or by
	//  pasing parameters to the begin() function. There are named constants in
	//  the .h file for all scales and data rates; I won't reproduce them here.
	//  Here's the list of fuctions to set the rates/scale:
	//  setMagScale(mag_scale mScl)      setMagODR(mag_odr mRate)
	//  setGyroScale(gyro_scale gScl)    setGyroODR(gyro_odr gRate)
	//  setAccelScale(accel_scale aScl)  setGyroODR(accel_odr aRate)
	// If you want to make these changes at the point of calling begin, here's
	//  the prototype for that function showing the order to pass things:
	//  begin(gyro_scale gScl, accel_scale aScl, mag_scale mScl,
	//				gyro_odr gODR, accel_odr aODR, mag_odr mODR)
	//imu = new LSM9DS0(gAddr, xmAddr);
	// ADC
	// The ADC is (by default) connected to I2C channel 1. Here, we create
	//  a device to pass to the ads1015 object constructor.
	//adc_i2c = new mraa::I2c(1);

	// Now, pass that I2C object and the address of the ADC block in your
	//  system to the ads1015 object constructor. Note that there are up to
	//  four different addresses available here, settable by jumper on the
	//  board. You'll need to create an ads1015 object for each one.
	//ads1015 adc1(adc_i2c, adcI2CAddress);
	//adc = adc1;
	// PWM(actuators)
	//pwm_i2c = new mraa::I2c(2); // Tell the I2c object which bus it's on.
	//pca9685 pwm1(adc_i2c, pwmI2CAddress); // 0x40 is the default address for the PCA9685.
	//pwm = pwm1;

	uint16_t imuResult = imu->begin();
	cout<<hex<<"Chip ID: 0x"<<imuResult<<dec<<" (should be 0x49d4)"<<endl;
	// ADC
	// The ADC is (by default) connected to I2C channel 1. Here, we create
	//  a device to pass to the ads1015 object constructor.
	//adc_i2c = new mraa::I2c(1);

	// Now, pass that I2C object and the address of the ADC block in your
	//  system to the ads1015 object constructor. Note that there are up to
	//  four different addresses available here, settable by jumper on the
	//  board. You'll need to create an ads1015 object for each one.
	//ads1015 adc(adc_i2c, 0x48);

	// There are 6 settable ranges:
	//  _0_256V - Range is -0.256V to 0.255875V, and step size is 125uV.
	//  _0_512V - Range is -0.512V to 0.51175V, and step size is 250uV.
	//  _1_024V - Range is -1.024V to 1.0235V, and step size is 500uV.
	//  _2_048V - Range is -2.048V to 2.047V, and step size is 1mV.
	//  _4_096V - Range is -4.096V to 4.094V, and step size is 2mV.
	//  _6_144V - Range is -6.144V to 6.141V, and step size is 3mV.
	// The default setting is _2_048V.
	// NB!!! Just because FS reading is > 3.3V doesn't mean you can take an
	//  input above 3.3V! Keep your input voltages below 3.3V to avoid damage!
	adc->setRange(_4_096V);
	// I use LED mode because it allows to set 100% duty cycle, it is inverted so the motor goes from sig to Vin
	pwm->enableLEDMode();
	SetVibrationValue(0,0,0);
}

void SHOE::SetVibrationValue(float mBallVal,float lBallVall, float heelVal){
	/*
	 * This function controls the vibration intensity of the motor
	 */
	pwm->setChlLEDPercent(M_MBALL, mBallVal);

	pwm->setChlLEDPercent(M_LBALL, lBallVall);

	pwm->setChlLEDPercent(M_HEEL, heelVal);

}

void SHOE::readIMU(){
	imu->readAccel();
	imu->readMag();
	imu->readGyro();
}



