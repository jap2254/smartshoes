/*
 * Copyright (c) 2016 Intel Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <arpa/inet.h>
#include <errno.h>
#include <mraa.hpp>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h> //printf
#include <stdlib.h> //exit(0);
#include <string.h> //memset
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

#include "SFE_LSM9DS0.h"
#include "SparkFun_pca9685_Edison.h"
#include "SparkFunADS1015.h"

using namespace std;


#define MAX_DATE 50
#define M_MBALL 0
#define M_LBALL 1
#define M_HEEL 2
#define P_TOE 2
#define P_BALL 1
#define P_HEEL 0
#define BUFLEN 512  //Max length of buffer
#define PORT_SEND 12345   //The port on which to send data
#define PORT_RECEIVE 12354   //The port on which to listen for incoming data
#define BYTE_PACKET 36
#define BYTE_FULL_PACKET 64


// structures
struct structDataPacket
{
	uint32_t timestamp;

	int16_t pToe,pBall,pHeel,d_ax,d_ay, d_az,d_gx,d_gy,d_gz,d_mx,d_my,d_mz,sync;

};

struct configParameters{
	bool runLoop,collectData,streamData,startIMU;
	string outputName, myName,fileSubName;
	char* PCIPaddress;
	ofstream outputFile;
	struct timeval tNow;


};

// Global variables
bool iAmLeft =  true; // change this to choose target shoe
mraa::I2c* adc_i2c;
mraa::I2c* pwm_i2c;
volatile int sock;
uint8_t dataCommunicationPacket[BYTE_FULL_PACKET];
uint8_t dataCommunicationPacket2[BYTE_PACKET];
configParameters con;
ads1015* adcPointer;
int16_t gSync = 0;
//const char* PCIPaddress ="192.168.42.20";

string getFileNameWithDate(void)
{
	/*
	 * Create file name, i don't know where to save it!!
	 */
	time_t now;
	char the_date[MAX_DATE],the_out[MAX_DATE];

	the_date[0] = '\0';

	now = time(NULL);

	if (now != -1)
	{
		//strftime(the_date, MAX_DATE, "~/data/%H_%M_%S_%d_%m_%Y.bin", gmtime(&now));
		strftime(the_date, MAX_DATE, "%H_%M_%S_%d_%m_%Y.bin", gmtime(&now));
		sprintf(the_out,"./data/%s_%s",con.fileSubName.c_str(),the_date);
		//cout<<the_date<<endl;
	}
	return string(the_out);
}

void sleepMilliseconds(int millisec){
	//int millisec = 100; // length of time to sleep, in milliseconds
	struct timespec req = {0};
	req.tv_sec = 0;
	req.tv_nsec = millisec * 1000000L;
	nanosleep(&req, (struct timespec *)NULL);
}
void SetVibrationValue(pca9685 pwm,float mBallVal,float lBallVall, float heelVal){
	/*
	 * This function controls the vibration intensity of the motor
	 */

	pwm.setChlLEDPercent(M_MBALL, mBallVal);

	pwm.setChlLEDPercent(M_LBALL, lBallVall);

	pwm.setChlLEDPercent(M_HEEL, heelVal);



}

void createDataPacket(uint8_t *dataPacket,structDataPacket data,size_t offset)
{


	// START
	dataPacket[0]=0x01;
	dataPacket[1]=0x02;
	dataPacket[2]=0x03;

	uint8_t* pData = (uint8_t*)&data;
	memcpy(&dataPacket[3],&pData[0],sizeof(data)-offset);

	// STOP
	dataPacket[33-offset]=0xA;
	dataPacket[34-offset]=0xB;
	dataPacket[35-offset]=0xC;
}
void create2DataPackets(uint8_t *dataPacket,structDataPacket dataL,uint8_t *dataPacketR)
{
	/*
	 * this function is different from the createDataPacket because it combines 2 datas
	 */
	// creates first packet, the offset removes the sync variable
	createDataPacket(dataPacket, dataL,0);
	// overwrite from byte 31 and write dataPacketR from 3 until end
	memcpy(&dataPacket[33],&dataPacketR[3],BYTE_PACKET-5);

}
void init_udp(){
	sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0){
		printf("ER UDP Socket error\n");
	}
	else printf("UP Socket %d OK\n",sock);
	int on = 1;
	int ret = setsockopt( sock, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on) );
	printf("return value is %d\n",ret);
}
void write_udp(uint8_t *buf,int len,struct sockaddr_in address){
	// nmea data is in buff
	//char buf2[100];
	//sprintf(buf2,"Hello world!!");
	int errno;
	if (sock >= 0){
		int le = sendto( sock, buf, len, 0, (struct sockaddr*)&address, sizeof(address) );
		//int le = sendto( sock, buf2, strlen(buf2), 0, (struct sockaddr*)&address, sizeof(address) );
		if ( le  < 0) {
			printf("ER UDP send error %d\n",errno);
		}/*
		else {
			printf("UP %s,%d\n",buf,le);
		}*/
	}
}
void write_udpChar(char *buf,int len,struct sockaddr_in address){
	// nmea data is in buff
	//char buf2[100];
	//sprintf(buf2,"Hello world!!");
	int errno;
	if (sock >= 0){
		int le = sendto( sock, buf, len, 0, (struct sockaddr*)&address, sizeof(address) );
		//int le = sendto( sock, buf2, strlen(buf2), 0, (struct sockaddr*)&address, sizeof(address) );
		if ( le  < 0) {
			printf("ER UDP send error %d\n",errno);
		}/*
		else {
			printf("UP %s,%d\n",buf,le);
		}*/
	}
}
uint32_t diff_ms(timeval t1, timeval t2)
{
	uint32_t t = (uint32_t) (((t1.tv_sec - t2.tv_sec) * 1000000) +
			(t1.tv_usec - t2.tv_usec))/1000;
	return t;
}
uint64_t getMicrosTimeStamp()
{
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return tv.tv_sec*(uint64_t)1000000+tv.tv_usec;
}
uint32_t getMilliTimeStamp()
{
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return diff_ms(tv, con.tNow);
}

struct sockaddr_in createAddressOBjectUDP(const char*PCIPaddress,short int portSend){

	struct sockaddr_in address;// = {0};
	bzero(&address,sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr( PCIPaddress ); // Edison IP
	//address.sin_addr.s_addr = htonl(INADDR_ANY);// inet_addr( "127.0.0.1" ); // Edison IP
	//address.sin_addr.s_addr = inet_addr( "224.0.0.251" );
	address.sin_port = htons( portSend );
	return address;
}
void startRecording(){
	if(con.collectData){
		cout<<"already recording"<<endl;
	}else{
		gettimeofday(&con.tNow,NULL);
		int errno;
		con.outputFile.open(getFileNameWithDate(), ios::out| ios::app | ios::binary);
		if (con.outputFile.is_open())
		{
			cout<<"file open"<<endl;
		}else{
			cout<<"file not open"<<endl;
			printf("ER ofstream send error %d\n",errno);
		}
		sleepMilliseconds(100);
		con.collectData = true;
		con.startIMU = true;
	}
}
void stopRecording(){
	if(con.collectData){
		con.collectData = false;
		sleepMilliseconds(100);
		con.outputFile.close();
	}else{
		cout<<"not recording"<<endl;
	}
}
void stopExecution(bool shut){
	if(con.collectData)
		stopRecording();
	con.runLoop = false;
	if(shut){
		sleepMilliseconds(100);
		system("shutdown -P now");
	}
}
void changeLogName(uint8_t *dataPacket){
	int s = dataPacket[4];
	//cout<<s<<endl;
	//the problem is init this array!!!
	char buf2[20];
	//memcpy(buf2,dataPacket+5,s);
	char val;
	int c;
	for (int j=0;j<s;j++){
		val = dataPacket[j+5];
		//cout<<val<<endl;
		/*if((val<48)|(val>122)){
			//this is the stop char
			break;
		}*/
		buf2[j]=val;
		c=j;
	}
	buf2[c+1] = '\0';
	con.fileSubName = buf2;
	cout<<con.fileSubName<<endl;
	cout<<getFileNameWithDate()<<endl;
}
void changeVibratorCMD(pca9685 pwm,uint8_t *dataPacket){
	float mBallVal = (float)dataPacket[4];
	float lBallVall = (float)dataPacket[5];
	float heelVal = (float)dataPacket[6];
	SetVibrationValue( pwm,mBallVal, lBallVall,  heelVal);
	cout<<"Setting vibration to: "<<mBallVal<<", "<<lBallVall<<", "<<heelVal<<endl;
}
void sendPressValues(struct sockaddr_in PCAddress){
	uint8_t dataPacket[13];
	uint16_t pToe = adcPointer->getRawResult(P_TOE);
	uint16_t pBall = adcPointer->getRawResult(P_BALL);
	uint16_t pHeel = adcPointer->getRawResult(P_HEEL);
	dataPacket[0]=0x0F;
	dataPacket[1]=0x0F;
	dataPacket[2]=0x0A;

	//pToe
	uint16_t val=pToe;
	uint8_t* pointer=(uint8_t*)&val;
	dataPacket[3]=pointer[1];
	dataPacket[4]=pointer[0];

	//pBall
	val=pBall;
	pointer=(uint8_t*)&val;
	dataPacket[5]=pointer[1];
	dataPacket[6]=pointer[0];

	//pHeel
	val=pHeel;
	pointer=(uint8_t*)&val;
	dataPacket[7]=pointer[1];
	dataPacket[8]=pointer[0];

	val = (uint8_t)iAmLeft;
	dataPacket[9]=val;

	dataPacket[10]=0x0A;
	dataPacket[11]=0x0B;
	dataPacket[12]=0x0B;

	write_udp(dataPacket,13,PCAddress);
}
int controlCMD(uint8_t *dataPacket,struct sockaddr_in PCAddress,pca9685 pwm){
	//if((dataPacket[0]==0xA) & (dataPacket[1]==0xD) & (dataPacket[2]==0xA)){
	uint8_t val = dataPacket[3];
	char buf2[100];
	//sprintf(buf2,"Hello world!!");
	switch(val){
	case 1://ping
		sprintf(buf2,"Hello from %s",con.myName.c_str());
		write_udpChar(buf2,(unsigned)strlen(buf2),PCAddress);
		cout<<buf2<<endl;
		break;
	case 2://start recording
		sprintf(buf2,"Recording Started");
		write_udpChar(buf2,(unsigned)strlen(buf2),PCAddress);
		cout<<"Recording started"<<endl;
		startRecording();
		break;
	case 3://stop recording
		cout<<"Recording stopped"<<endl;
		stopRecording();
		sprintf(buf2,"Recording Stopped");
		write_udpChar(buf2,(unsigned)strlen(buf2),PCAddress);
		break;
	case 4://send time
		cout<<"Changing name"<<endl;
		changeLogName(dataPacket);
		break;
	case 5://send time
		cout<<"Sending time"<<endl;
		system("rdate 192.168.42.21");
		sprintf(buf2,"Hello from %s, time is %u",con.myName.c_str(),getMilliTimeStamp());
		write_udpChar(buf2,(unsigned)strlen(buf2),PCAddress);
		break;
	case 6://start streaming data
		cout<<"Starting stream"<<endl;
		con.streamData = true;
		break;
	case 7://stop streaming data
		cout<<"Stopping stream"<<endl;
		con.streamData = false;
		break;
	case 8://stop program
		cout<<"setting vibrators"<<endl;
		changeVibratorCMD(pwm,dataPacket);
		break;
	case 9://stop program
		cout<<"Sending Pressure"<<endl;
		sendPressValues(PCAddress);
		break;
	case 10://stop program
		cout<<"Stopping program"<<endl;
		stopExecution(false);
		break;
	case 11://stop program
		cout<<"Bye bye"<<endl;
		stopExecution(true);
		break;
	case 12://stop program
		gSync = htons((int16_t)dataPacket[4]);
		cout<<"Sync is "<< gSync<<endl;
		break;
	default:
		cout<<"Wrong instruction"<<endl;
		sprintf(buf2,"Hello from %s, wrong instruction",con.myName.c_str());
		write_udpChar(buf2,(unsigned)strlen(buf2),PCAddress);
		return 0;
	}
	/*}else{
		return 0;
	}*/
	return 1;
}
void threadUDPreceive(struct sockaddr_in PCAddress,pca9685 pwm){
	struct sockaddr_in addressReceive;
	uint8_t buf[BUFLEN];
	int blen=0;
	socklen_t slen = sizeof(addressReceive);
	while(1)
	{
		blen= recvfrom(sock, buf, sizeof(buf), 0, (struct sockaddr*) &addressReceive, &slen);
		if(blen>0){
			if((buf[0]==0xA) & (buf[1]==0xD) & (buf[2]==0xA)){
				controlCMD(buf,PCAddress, pwm);
			}else{
				// it is from other shoe, parse to other shoe
				//dataCommunicationPacket2 = buf;
				memcpy(dataCommunicationPacket2,buf,BYTE_PACKET);
			}
		}
	}
}
void leftMaster()
{
	/*
	 * Main function for collecting data and sending it
	 */
	// init all variables
	thread networkReceiver;
	con.runLoop = true;
	con.collectData = false;
	con.outputName = getFileNameWithDate();
	con.PCIPaddress = "192.168.42.21";
	con.myName = "LeftShoe";
	con.streamData = false;
	con.fileSubName = "f";
	// UDP connection
	cout<<getFileNameWithDate()<<endl;
	init_udp();
	struct sockaddr_in addressSend = createAddressOBjectUDP(con.PCIPaddress,PORT_SEND);
	struct sockaddr_in addressMe = createAddressOBjectUDP("192.168.42.1",PORT_RECEIVE);
	if (bind(sock, (struct sockaddr*) &addressMe, sizeof(addressMe))==-1)
		cout<<"Bind failed"<<endl;

	usleep(1000);
	//IMU:
	LSM9DS0 *imu;
	imu = new LSM9DS0(0x6B, 0x1D);
	//  The begin() function sets up some basic parameters and turns the device
	//  on; you may not need to do more than call it. It also returns the "whoami"
	//  registers from the chip. If all is good, the return value here should be
	//  0x49d4. Here are the initial settings from this function:
	//  Gyro scale:        245 deg/sec max
	//  Xl scale:          4g max
	//  Mag scale:         2 Gauss max
	//  Gyro sample rate:  190Hz
	//  Xl sample rate:    100Hz
	//  Mag sample rate:   100Hz
	// These can be changed either by calling appropriate functions or by
	//  pasing parameters to the begin() function. There are named constants in
	//  the .h file for all scales and data rates; I won't reproduce them here.
	//  Here's the list of fuctions to set the rates/scale:
	//  setMagScale(mag_scale mScl)      setMagODR(mag_odr mRate)
	//  setGyroScale(gyro_scale gScl)    setGyroODR(gyro_odr gRate)
	//  setAccelScale(accel_scale aScl)  setGyroODR(accel_odr aRate)
	// If you want to make these changes at the point of calling begin, here's
	//  the prototype for that function showing the order to pass things:
	//  begin(gyro_scale gScl, accel_scale aScl, mag_scale mScl,
	//				gyro_odr gODR, accel_odr aODR, mag_odr mODR)
	uint16_t imuResult = imu->begin();
	cout<<hex<<"Chip ID: 0x"<<imuResult<<dec<<" (should be 0x49d4)"<<endl;
	bool newAccelData = false;
	bool newMagData = false;
	bool newGyroData = false;
	bool overflow = false;

	// ADC
	// The ADC is (by default) connected to I2C channel 1. Here, we create
	//  a device to pass to the ads1015 object constructor.
	adc_i2c = new mraa::I2c(1);

	// Now, pass that I2C object and the address of the ADC block in your
	//  system to the ads1015 object constructor. Note that there are up to
	//  four different addresses available here, settable by jumper on the
	//  board. You'll need to create an ads1015 object for each one.
	ads1015 adc(adc_i2c, 0x48);
	adcPointer = &adc;
	// There are 6 settable ranges:
	//  _0_256V - Range is -0.256V to 0.255875V, and step size is 125uV.
	//  _0_512V - Range is -0.512V to 0.51175V, and step size is 250uV.
	//  _1_024V - Range is -1.024V to 1.0235V, and step size is 500uV.
	//  _2_048V - Range is -2.048V to 2.047V, and step size is 1mV.
	//  _4_096V - Range is -4.096V to 4.094V, and step size is 2mV.
	//  _6_144V - Range is -6.144V to 6.141V, and step size is 3mV.
	// The default setting is _2_048V.
	// NB!!! Just because FS reading is > 3.3V doesn't mean you can take an
	//  input above 3.3V! Keep your input voltages below 3.3V to avoid damage!
	adc.setRange(_4_096V);
	//int16_t pToe,pBall,pHeel,d_gx,d_gy,d_gz,d_ax,d_ay, d_az,d_mx,d_my,d_mz;
	structDataPacket data;
	//cout<<sizeof(data)<<endl;
	// PWM (motors)
	pwm_i2c = new mraa::I2c(1); // Tell the I2c object which bus it's on.
	pca9685 pwm(pwm_i2c, 0x40); // 0x40 is the default address for the PCA9685.
	// I use LED mode because it allows to set 100% duty cycle, it is inverted so the motor goes from sig to Vin
	pwm.enableLEDMode();
	SetVibrationValue(pwm,0,0,0);
	networkReceiver=thread(threadUDPreceive,addressSend,pwm);

	networkReceiver.detach();
	// start main loop
	int c = 0;
	int mOn = 0;
	uint32_t preD = 0;
	while(con.runLoop){
		if(con.collectData){
			if (con.startIMU){
				imuResult = imu->begin();
				cout<<hex<<"Chip ID: 0x"<<imuResult<<dec<<" (should be 0x49d4)"<<endl;
				con.startIMU = false;
			}
			// First, let's make sure we're collecting up-to-date information. The
			//  sensors are sampling at 100Hz (for the accelerometer, magnetometer, and
			//  temp) and 95Hz (for the gyro), and we could easily do a bunch of
			//  stuff within that ~10ms sampling period.
			while ((/*newGyroData & */newAccelData & newMagData) != true)
			{
				if (newAccelData != true)
				{
					newAccelData = imu->newXData();
				}
				/*
				if (newGyroData != true)
				{
					newGyroData = imu->newGData();
				}*/
				if (newMagData != true)
				{
					newMagData = imu->newMData(); // Temp data is collected at the same
					//  rate as magnetometer data.
				}
				// place here any extra stuff you want to do, like udp??
			}
			// if read here the ADC, then the system will be running at ~ 95Hz, read the raw data to have a int value
			data.timestamp =  htonl(getMilliTimeStamp());
			data.pToe = htons(adc.getRawResult(P_TOE));
			data.pBall = htons(adc.getRawResult(P_BALL));
			data.pHeel = htons(adc.getRawResult(P_HEEL));
			// Calling these functions causes the data to be read from the IMU into
			//  10 16-bit signed integer public variables, as seen below. There is no
			//  automated check on whether the data is new; you need to do that
			//  manually as above. Also, there's no check on overflow, so you may miss
			//  a sample and not know it.
			imu->readAccel();
			imu->readMag();
			imu->readGyro();
			newAccelData = false;
			newMagData = false;
			newGyroData = false;
			/*c++;
			if (c>= 100){
				// 1 second has passed, toggle motors this is just testing!
				switch(mOn){
				case 0:
					SetVibrationValue(pwm,80,0,0);
					//cout<<"Turning m ball on"<<endl;
					break;
				case 1:
					SetVibrationValue(pwm,0,80,0);
					//cout<<"Turning l ball on"<<endl;
					break;
				case 2:
					SetVibrationValue(pwm,0,0,80);
					//cout<<"Turning heel on"<<endl;
					break;
				default:
					mOn = -1;
					SetVibrationValue(pwm,0,0,0);
					//cout<<"Turning all off"<<endl;
				}
				mOn++;
				c = 0;
				//createDataPacket(dataCommunicationPacket,data);
				//write_udp(dataCommunicationPacket,address);
			}*/
			// Of course, we may care if an overflow occurred; we can check that
			//  easily enough from an internal register on the part. There are functions
			//  to check for overflow per device.
			overflow = // imu->xDataOverflow() |
					// imu->gDataOverflow() |
					imu->mDataOverflow();

			if ((overflow) & ((data.timestamp - preD) >= 20 ))
			{
				cout<<"WARNING: DATA OVERFLOW!!!"<<endl;
			}
			preD = data.timestamp;


			data.d_gx = htons(imu->gx);
			data.d_gy = htons(imu->gy);
			data.d_gz = htons(imu->gz);
			data.d_ax = htons(imu->ax);
			data.d_ay = htons(imu->ay);
			data.d_az = htons(imu->az);
			data.d_mx = htons(imu->mx);
			data.d_my = htons(imu->my);
			data.d_mz = htons(imu->mz);
			data.sync = gSync;
			// cout<< data.sync << endl;
			//create packet
			create2DataPackets(dataCommunicationPacket,data,dataCommunicationPacket2);
			//write to file
			con.outputFile.write(reinterpret_cast<char*>(dataCommunicationPacket),BYTE_FULL_PACKET);
			//createDataPacket(dataCommunicationPacket,data);
			if(con.streamData)
				write_udp(dataCommunicationPacket,BYTE_FULL_PACKET,addressSend);
			/*
		// For testing I'll print twice per second
		if(c%50){
			cout<<"-------------------------------------"<<endl;
			cout<<" x: "<<data.d_ax<<" y: "<<data.d_ay<<" z: "<<data.d_az<<endl;
			cout<<"Press Toe: "<<data.pToe<<" Ball: "<<data.pBall<<" Heel: "<<data.pHeel<<endl;
		}
			 */
		}
	}
	cout<<"Program ended"<<endl;
}
void rightSlave()
{
	/*
	 * Main function for collecting data and sending it
	 */
	// init all variables
	thread networkReceiver;
	con.runLoop = true;
	con.collectData = false;
	con.outputName = getFileNameWithDate();
	con.PCIPaddress = "192.168.42.21";
	con.myName = "RightShoe";
	con.streamData = false;
	con.fileSubName = "f";
	// UDP connection
	cout<<getFileNameWithDate()<<endl;
	init_udp();
	struct sockaddr_in addressPC = createAddressOBjectUDP(con.PCIPaddress,PORT_SEND);
	struct sockaddr_in addressLeft = createAddressOBjectUDP("192.168.42.1",PORT_RECEIVE);
	struct sockaddr_in addressMe = createAddressOBjectUDP("192.168.42.20",PORT_RECEIVE);
	if (bind(sock, (struct sockaddr*) &addressMe, sizeof(addressMe))==-1)
		cout<<"Bind failed"<<endl;

	usleep(1000);
	//IMU:
	LSM9DS0 *imu;
	imu = new LSM9DS0(0x6B, 0x1D);
	//  The begin() function sets up some basic parameters and turns the device
	//  on; you may not need to do more than call it. It also returns the "whoami"
	//  registers from the chip. If all is good, the return value here should be
	//  0x49d4. Here are the initial settings from this function:
	//  Gyro scale:        245 deg/sec max
	//  Xl scale:          4g max
	//  Mag scale:         2 Gauss max
	//  Gyro sample rate:  95Hz
	//  Xl sample rate:    100Hz
	//  Mag sample rate:   100Hz
	// These can be changed either by calling appropriate functions or by
	//  pasing parameters to the begin() function. There are named constants in
	//  the .h file for all scales and data rates; I won't reproduce them here.
	//  Here's the list of fuctions to set the rates/scale:
	//  setMagScale(mag_scale mScl)      setMagODR(mag_odr mRate)
	//  setGyroScale(gyro_scale gScl)    setGyroODR(gyro_odr gRate)
	//  setAccelScale(accel_scale aScl)  setGyroODR(accel_odr aRate)
	// If you want to make these changes at the point of calling begin, here's
	//  the prototype for that function showing the order to pass things:
	//  begin(gyro_scale gScl, accel_scale aScl, mag_scale mScl,
	//				gyro_odr gODR, accel_odr aODR, mag_odr mODR)
	uint16_t imuResult = imu->begin();
	cout<<hex<<"Chip ID: 0x"<<imuResult<<dec<<" (should be 0x49d4)"<<endl;

	bool newAccelData = false;
	bool newMagData = false;
	bool newGyroData = false;
	bool overflow = false;

	// ADC
	// The ADC is (by default) connected to I2C channel 1. Here, we create
	//  a device to pass to the ads1015 object constructor.
	adc_i2c = new mraa::I2c(1);

	// Now, pass that I2C object and the address of the ADC block in your
	//  system to the ads1015 object constructor. Note that there are up to
	//  four different addresses available here, settable by jumper on the
	//  board. You'll need to create an ads1015 object for each one.
	ads1015 adc(adc_i2c, 0x48);
	adcPointer = &adc;
	// There are 6 settable ranges:
	//  _0_256V - Range is -0.256V to 0.255875V, and step size is 125uV.
	//  _0_512V - Range is -0.512V to 0.51175V, and step size is 250uV.
	//  _1_024V - Range is -1.024V to 1.0235V, and step size is 500uV.
	//  _2_048V - Range is -2.048V to 2.047V, and step size is 1mV.
	//  _4_096V - Range is -4.096V to 4.094V, and step size is 2mV.
	//  _6_144V - Range is -6.144V to 6.141V, and step size is 3mV.
	// The default setting is _2_048V.
	// NB!!! Just because FS reading is > 3.3V doesn't mean you can take an
	//  input above 3.3V! Keep your input voltages below 3.3V to avoid damage!
	adc.setRange(_4_096V);
	//int16_t pToe,pBall,pHeel,d_gx,d_gy,d_gz,d_ax,d_ay, d_az,d_mx,d_my,d_mz;
	structDataPacket data;
	// PWM (motors)
	pwm_i2c = new mraa::I2c(1); // Tell the I2c object which bus it's on.
	pca9685 pwm(pwm_i2c, 0x40); // 0x40 is the default address for the PCA9685.
	// I use LED mode because it allows to set 100% duty cycle, it is inverted so the motor goes from sig to Vin
	pwm.enableLEDMode();
	SetVibrationValue(pwm,0,0,0);
	networkReceiver=thread(threadUDPreceive,addressPC,pwm);

	networkReceiver.detach();
	// start main loop
	int c = 0;
	int mOn = 0;
	uint32_t preD = 0;
	while(con.runLoop){
		if(con.collectData){
			if (con.startIMU){
				imuResult = imu->begin();
				cout<<hex<<"Chip ID: 0x"<<imuResult<<dec<<" (should be 0x49d4)"<<endl;
				con.startIMU = false;
			}
			// First, let's make sure we're collecting up-to-date information. The
			//  sensors are sampling at 100Hz (for the accelerometer, magnetometer, and
			//  temp) and 95Hz (for the gyro), and we could easily do a bunch of
			//  stuff within that ~10ms sampling period.
			while ((/*newGyroData & */newAccelData & newMagData) != true)
			{
				if (newAccelData != true)
				{
					newAccelData = imu->newXData();
				}
				/*
				if (newGyroData != true)
				{
					newGyroData = imu->newGData();
				}*/
				if (newMagData != true)
				{
					newMagData = imu->newMData(); // Temp data is collected at the same
					//  rate as magnetometer data.
				}
				// place here any extra stuff you want to do, like udp??
			}
			// if read here the ADC, then the system will be running at ~ 100HZ, read the raw data to have a int value
			data.timestamp =  htonl(getMilliTimeStamp());
			data.pToe = htons(adc.getRawResult(P_TOE));
			data.pBall = htons(adc.getRawResult(P_BALL));
			data.pHeel = htons(adc.getRawResult(P_HEEL));
			// Calling these functions causes the data to be read from the IMU into
			//  10 16-bit signed integer public variables, as seen below. There is no
			//  automated check on whether the data is new; you need to do that
			//  manually as above. Also, there's no check on overflow, so you may miss
			//  a sample and not know it.
			imu->readAccel();
			imu->readMag();
			imu->readGyro();
			newAccelData = false;
			newMagData = false;
			newGyroData = false;
			/*c++;
			if (c>= 100){
				// 1 second has passed, toggle motors this is just testing!
				switch(mOn){
				case 0:
					SetVibrationValue(pwm,80,0,0);
					//cout<<"Turning m ball on"<<endl;
					break;
				case 1:
					SetVibrationValue(pwm,0,80,0);
					//cout<<"Turning l ball on"<<endl;
					break;
				case 2:
					SetVibrationValue(pwm,0,0,80);
					//cout<<"Turning heel on"<<endl;
					break;
				default:
					mOn = -1;
					SetVibrationValue(pwm,0,0,0);
					//cout<<"Turning all off"<<endl;
				}
				mOn++;
				c = 0;
				//createDataPacket(dataCommunicationPacket,data);
				//write_udp(dataCommunicationPacket,address);
			}*/
			// Of course, we may care if an overflow occurred; we can check that
			//  easily enough from an internal register on the part. There are functions
			//  to check for overflow per device.
			overflow = // imu->xDataOverflow() |
					// imu->gDataOverflow() |
					imu->mDataOverflow();

			if ((overflow) & ((data.timestamp - preD) >= 20 ))
			{
				cout<<"WARNING: DATA OVERFLOW!!!"<<endl;
			}
			preD = data.timestamp;


			data.d_gx = htons(imu->gx);
			data.d_gy = htons(imu->gy);
			data.d_gz = htons(imu->gz);
			data.d_ax = htons(imu->ax);
			data.d_ay = htons(imu->ay);
			data.d_az = htons(imu->az);
			data.d_mx = htons(imu->mx);
			data.d_my = htons(imu->my);
			data.d_mz = htons(imu->mz);
			data.sync = gSync;
			//create packet
			createDataPacket(dataCommunicationPacket2,data,0);
			create2DataPackets(dataCommunicationPacket,data,dataCommunicationPacket2);

			//write to file
			con.outputFile.write(reinterpret_cast<char*>(dataCommunicationPacket),BYTE_FULL_PACKET);
			//createDataPacket(dataCommunicationPacket,data);
			write_udp(dataCommunicationPacket2,BYTE_PACKET,addressLeft);
			/*
		// For testing I'll print twice per second
		if(c%50){
			cout<<"-------------------------------------"<<endl;
			cout<<" x: "<<data.d_ax<<" y: "<<data.d_ay<<" z: "<<data.d_az<<endl;
			cout<<"Press Toe: "<<data.pToe<<" Ball: "<<data.pBall<<" Heel: "<<data.pHeel<<endl;
		}
			 */
		}
	}
	cout<<"Program ended"<<endl;
}

int main(){
	if (iAmLeft){
		leftMaster();
	}else{
		rightSlave();
	}
	return 1;
}
