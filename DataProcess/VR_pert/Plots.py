import pandas
import numpy as np
import Tools.Utils as tu
import seaborn as sns
import scipy.io as spio
import matplotlib.pyplot as plt



def calculateH(x, xy, ori, space2body=True):
    """
    Returns a space to body homogenous transformation
    :param x: X axis of the system in space frame, it can be a matrix of shape nx3
    :type x: numpy.ndarray
    :param xy: vector in the xy plane axis of the system in space frame, it can be a matrix of shape nx3
    :type xy: numpy.ndarray
    :param ori: location of the origin in space
    :type ori: numpy.ndarray
    :param space2body: if true returns space2body, else body2space
    :type space2body: bool
    :return: homogenous transformation
    :rtype: np.ndarray
    """
    inpu = [x, xy, ori]
    for j, i in enumerate(inpu):
        if len(i.shape) == 1:
            inpu[j] = np.expand_dims(i, 0)
    x, xy, ori = inpu
    xA = np.multiply(x.T, 1 / np.linalg.norm(x, axis=1)).T
    zA = np.cross(xA, xy)
    zA = np.multiply(zA.T, 1 / np.linalg.norm(zA, axis=1)).T
    yA = np.cross(zA, xA)
    # R is space to body
    H = np.zeros((yA.shape[0], 4, 4))
    R = np.stack((xA, yA, zA), axis=1)
    H[:, :3, :3] = R
    # H[:, :3, -1] = np.squeeze(ori)
    H[:, :3, -1] = -np.squeeze(np.matmul(R, np.expand_dims(ori, 2)))
    H[:, -1, -1] = 1
    if not space2body:
        # R = np.transpose(np.stack((xA, yA, zA), axis=2), (0, 2, 1))
        # H[:, :3, :3] = R
        # H[:, :3, -1] = np.squeeze(-np.matmul(R, np.expand_dims(ori, 2)))
        H = inverseH(H)
    return H


def inverseH(H):
    if len(H.shape) is 2:
        Ht = np.expand_dims(H, 0)
    else:
        Ht = H.copy()
    R = np.transpose(Ht[:, :3, :3], (0, 2, 1))
    R1 = Ht[:, :3, :3]
    o = Ht[:, :3, -1]
    Ht[:, :3, :3] = R
    Ht[:, :3, -1] = -np.matmul(R1
                               , np.expand_dims(o, 2)).squeeze()
    return Ht



vrPath = '//roar2.me.columbia.edu/VR/NeckPerturbation/AA'
matPath = '//roar2.me.columbia.edu/VR/NeckPerturbation/AA/MAT_DATA'

gaitData = spio.loadmat('%s/perS.mat' % vrPath)

subs = [3]
# sessions = ['BF', 'NP', 'PF']
sessions = ['BF']
pelvisTracker = ['Tracker_01']
# pelvisTracker = ['Controller (left)', 'Tracker_01', 'Tracker_02']

addAxisNAmes = lambda name: ['%s [%s]' %(name, ax) for ax in ['x', 'y', 'z']]

nPoints = 200
hsData = []
for s in subs:
    subID = 'PER%03d' % s
    for sess in sessions:
        vrData = pandas.read_csv('%s/%s/%s.csv' % (vrPath, subID, sess))
        # cut sync
        vrData = vrData.where(vrData['Sync'] == 1).dropna(how='all')
        vrData['T'] -= vrData['T'].values[0]

        hs = np.squeeze(gaitData['%s_%s' % (subID, sess)])[:10]
        for j, (st, en) in enumerate(zip(hs[:-1], hs[1:])):
            for p in pelvisTracker:
                aux = vrData[addAxisNAmes(p)].where(vrData['T'].between(st, en)).dropna(how='all')

                xaux = aux.values[-1] - aux.values[0]
                xaux[1] = 0
                H = np.squeeze(calculateH(xaux, np.array([0., 1., 0.]), aux.values[0], True))

                aux['extra'] = 1
                aux2 = tu.reSampleMatrix(np.squeeze(np.dot(H, aux.T).T)[:, :3], nPoints)
                aux3 = pandas.DataFrame(data=aux2, columns=['x', 'y', 'z'])
                aux3['Percentage'] = np.linspace(0, 100, nPoints)
                aux3['Stride'] = j
                aux3['Session'] = sess
                aux3['Subject'] = subID
                aux3['Tracker'] = p
                hsData.append(aux3.copy())

hsData = pandas.concat(hsData)
f, ax = plt.subplots(3)
for d, axx in zip(['x', 'y', 'z'], ax):
    sns.lineplot(data=hsData, x='Percentage', y=d, hue='Tracker', ax=axx, ci=None)





