import seaborn as sns
import matplotlib.pyplot as plt
import pandas

fileName = "E:/Drive/JAP2254/Lab/Defense/SegmentationResults.xlsx"

data = pandas.read_excel(fileName)

f = sns.catplot(data=data, y='Type', x='Value', hue='Author', kind='bar')
