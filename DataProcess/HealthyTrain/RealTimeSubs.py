import pickle

import matplotlib.pyplot as plt

# picklePath = 'E:/Drive/JAP2254/Lab/SmartShoes/RealTime/Healthy/RT/Shoes/'
# modelType = 'RNN'
# pFile = picklePath + modelType + 'Subs.pickle'

pFile = 'Z:/PD/PD_val/_Experiments/AllSubs.pickle'

with open(pFile, 'rb') as f:
    allSubs = pickle.load(f)

# k = 2
for subID in allSubs:
    # subID = 'Sbj%03d' % k

    gt = allSubs[subID]['b6MWT']['gt']
    pred = allSubs[subID]['b6MWT']['pred']

    f, ax = plt.subplots(2, sharex=True, sharey=True)
    gt.plot(y='R', ax=ax[0])
    gt.plot(y='L', ax=ax[1])
    # if k < 6:
    #     pred.plot(y='R', ax=ax[1])
    #     pred.plot(y='L', ax=ax[0])
    # else:
    pred.plot(y='R', ax=ax[0])
    pred.plot(y='L', ax=ax[1])
    plt.title(subID)
