import Tools.Utils as tu
import pandas
import numpy as np


def splitTimeSeries(fpars, fSeries, nPoints=100, useLeft=True):
    mat = tu.matFile2Pandas(fpars)
    mat = mat.where(mat['isLeft'] == useLeft).dropna(how='all')
    tS = pandas.read_csv(fSeries)
    laps = mat['Laps'].unique()
    series = []
    for la in laps:
        hs = mat.where(mat['Laps'] == la).dropna(how='all')['First Contact (sec.)']
        for st, en in zip(hs[:-1], hs[1:]):
            aux = tu.reSampleMatrix(tS.where(tS['Time (sec.)'].between(st, en)).dropna(how='all').values[:,
                                    1:], nPoints)
            auxD = pandas.DataFrame(data=aux, columns=tS.columns[1:])
            auxD['Percentage'] = np.linspace(0, 100, nPoints)
            auxD['Lap'] = la
            series.append(auxD)
            # a = 1

    return pandas.concat(series)
#
# fpars = "C:/Users/Antonio/Desktop/test/paramsMats.xlsx"
# fSeries = "C:/Users/Antonio/Desktop/test/timeSeries.csv"
# a = splitTimeSeries(fpars, fSeries)





