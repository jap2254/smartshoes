import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from Tools.Utils import *

parentPath = 'E:/Drive/JAP2254/Lab/SmartShoes/SPARK/CAT/VRData/'
allSubsPath = parentPath + 'allSubs.pickle'
allDataPath = parentPath + 'allData.pickle'

st = 1
en = 15
freq = 50.0
subName = 'CAT'
session = ['W', 'M', 'S']
walkingParams = ['Stride Length (cm.)', 'Stride Width (cm.)',
                 'Stride Time (sec.)', 'Stride Velocity (cm./sec.)',
                 'Total D. Support (sec.)']  # , 'Stance COP Path Eff. %']
reReadCSVs = False
if reReadCSVs:
    allSubs = {}
    allSubs['Mean'] = []
    normInd = []
    for j in range(st, en + 1):
        subID = '%s%03d' % (subName, j)
        for s in session:

            fileName = parentPath + subID + '/' + s
            print('Doing %s, %s' % (subID, s))
            fN = fileName + '.csv'
            fNmat = fileName + '.xlsx'
            try:
                try:
                    allSubs[subID + s + '_mat'] = MatObject(fNmat, freq=freq)
                    allSubs[subID + s + '_mat'].makeYpercent()
                    if s is 'W':
                        allSubs['Mean'].append(allSubs[subID + s + '_mat'].raw[walkingParams].mean())
                        normInd.append(subID)
                except:
                    print('Mat file not found for %s' % fileName)
                allSubs[subID + s] = pandas.read_csv(fN)
                m = np.where(allSubs[subID + s]['Sync'] == 1)[0]
                if len(m) is 0:
                    print('%s sync not found' % fileName)
                    continue
                allSubs[subID + s] = allSubs[subID + s].iloc[m]
                allSubs[subID + s]['T'] = allSubs[subID + s]['T'] - allSubs[subID + s]['T'].iloc[0]
            except:
                print('Error in %s' % fileName)
    allSubs['Mean'] = pandas.concat(allSubs['Mean'], axis=1)
    allSubs['Mean'].columns = normInd
    allSubs['Mean'] = allSubs['Mean'].T
    with open(allSubsPath, 'wb') as f:
        pickle.dump(allSubs, f)

else:
    with open(allSubsPath, 'rb') as f:
        allSubs = pickle.load(f)

validSubs = ['%s%03d' % (subName, j) for j in [2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15]]

redoDatasets = True
if redoDatasets:
    subjectNames = ['%s%03d' % (subName, j) for j in range(st, en + 1)]
    catchScore = {}
    names = ['T', 'Attempt', 'Caught', 'ThrowDisI', 'BallState']
    # I will add to the list: [Shot, Catch, Throw, During Event, Outside Event] [R, L, both]
    columns = ['Catch', 'Score', 'SubID', 'Session',
               'Shot Phase', 'Shot GaitCycle',
               'Catch Phase', 'Catch GaitCycle',
               'Throw Phase', 'Throw GaitCycle'] + ['%s %s' % (j, k)
                                                    for j in walkingParams
                                                    for k in ['Shot', 'Catch', 'Throw', 'During Event', 'Outside Event']
                                                    ]  # for m in ['R', 'L', 'both']]
    allData = []
    for s in session:
        print('___________________________ %s ___________________________' % s)
        catchScore[s + 'Catch'] = pandas.DataFrame(index=range(50), columns=subjectNames)
        catchScore[s + 'Score'] = pandas.DataFrame(index=range(50), columns=subjectNames)
        for subID in validSubs:
            # subID = '%s%03d' % (subName, j)
            print('Doing %s, %s' % (subID, s))
            auxData = pandas.DataFrame(columns=columns)
            la = subID + s
            s1 = allSubs[la][names].copy()
            s1.drop_duplicates(inplace=True)
            m = np.diff(s1['Attempt']).astype(np.bool)
            s1 = s1.iloc[m]
            s1.iloc[1:, 2] = np.diff(s1['Caught']).astype(np.bool)
            s1.reset_index(drop=True, inplace=True)
            s1['Caught'].loc[s1['Caught'] < 0] = np.nan
            catchScore[s + 'Catch'][subID] = s1['Caught']
            catchScore[s + 'Score'][subID] = s1['ThrowDisI']
            auxData['Catch'] = s1['Caught']
            auxData['Score'] = s1['ThrowDisI']
            auxData['SubID'] = subID
            auxData['Session'] = s
            if s is 'S':
                auxData[['Shot Phase', 'Shot GaitCycle',
                         'Catch Phase', 'Catch GaitCycle',
                         'Throw Phase', 'Throw GaitCycle']] = 1
            if la + '_mat' in allSubs.keys():
                gaitChar = allSubs[la + '_mat'].raw[['Laps', 'isLeft', 'First Contact (sec.)', 'Last Contact (sec.)']
                                                    + walkingParams].copy()
                # gaitChar[walkingParams] /= allSubs['Mean'].loc[subID]
                s2 = allSubs[la][['T', 'Attempt', 'BallState']].copy()
                bs = s2['BallState'].diff()
                ch = bs.where(bs != 0).dropna()
                startFound = None
                stat = None
                enat = None
                for k, _ in ch.iteritems():
                    b = s2['BallState'][k]
                    att = s2['Attempt'][k]
                    t = s2['T'][k]
                    T = int(s2['T'][k] * freq)
                    try:
                        if b == 1:
                            # the moment when the ball was thrown
                            event = 'Shot'
                            stat = t
                            startFound = True
                        elif b == 2:
                            # the moment when the ball was caught
                            event = 'Catch'
                        elif b == 3:
                            # the moment when the sub threw the ball
                            event = 'Throw'
                        elif b == 0:
                            event = 'other'
                            enat = t
                        else:
                            event = 'other'

                        if event is not 'other':
                            auxData['%s Phase' % event][att] = allSubs[la + '_mat'].binaryFunctions['phase'].iloc[
                                T].copy()
                            auxData['%s GaitCycle' % event][att] = allSubs[la + '_mat'].binaryFunctions['R'].iloc[
                                T].copy()
                            # do gait param
                            # I will add to the list: [Shot, Catch, Throw, During Event, Outside Event][R, L, both]
                            # identify the steps of interest
                            a = gaitChar.where(np.logical_and(t > gaitChar['First Contact (sec.)'],
                                                              t < gaitChar['Last Contact (sec.)'])).dropna(how='all')
                            # record them to the correct event
                            auxData.loc[att, ['%s %s' % (jj, event)
                                              for jj in walkingParams]] = a[walkingParams].mean().values
                            # for _, kk in a.iterrows():
                            #     if kk['isLeft']:
                            #         ss = 'L'
                            #     else:
                            #         ss = 'R'
                            #     auxData.loc[att, ['%s %s %s' % (jj, event, ss)
                            #                       for jj in walkingParams]] = kk[walkingParams].values

                        # now do the outside events and during events
                        if startFound and enat is not None:
                            # get the values During event
                            mm = np.logical_and(gaitChar['First Contact (sec.)'] < enat,
                                                gaitChar['Last Contact (sec.)'] > stat)
                            a = gaitChar.where(mm).dropna(how='all')
                            auxData.loc[att, ['%s %s' % (jj, 'During Event')
                                              for jj in walkingParams]] = a[walkingParams].mean().values
                            # auxData.loc[att, ['%s %s %s' % (jj, 'During Event', 'R')
                            #                   for jj in walkingParams]] = a[walkingParams].where(
                            #     a['isLeft'] == 0).mean().values
                            # auxData.loc[att, ['%s %s %s' % (jj, 'During Event', 'L')
                            #                   for jj in walkingParams]] = a[walkingParams].where(
                            #     a['isLeft'] == 1).mean().values
                            # get the values not in event
                            mm = np.logical_and(np.logical_not(mm), gaitChar['Laps'] == att + 1.)
                            a = gaitChar.where(mm).dropna(how='all')
                            auxData.loc[att, ['%s %s' % (jj, 'Outside Event')
                                              for jj in walkingParams]] = a[walkingParams].mean().values
                            # auxData.loc[att, ['%s %s %s' % (jj, 'Outside Event', 'R')
                            #                   for jj in walkingParams]] = a[walkingParams].where(
                            #     a['isLeft'] == 0).mean().values
                            # auxData.loc[att, ['%s %s %s' % (jj, 'Outside Event', 'L')
                            #                   for jj in walkingParams]] = a[walkingParams].where(
                            #     a['isLeft'] == 1).mean().values
                            # then reset vars
                            startFound = False
                            enat = None
                            enat = None

                    except IndexError:
                        continue

            allData.append(auxData.copy())

    allData = pandas.concat(allData)
    allData.reset_index(inplace=True)
    with open(allDataPath, 'wb') as f:
        pickle.dump(allData, f)

else:
    with open(allDataPath, 'rb') as f:
        allData = pickle.load(f)

columns = ['type', 'Catch', 'Score', 'SubID', 'Session',
           'Shot Phase', 'Shot GaitCycle',
           'Catch Phase', 'Catch GaitCycle',
           'Throw Phase', 'Throw GaitCycle'] + \
          ['%s %s' % (j, k) for j in walkingParams
           for k in ['Shot', 'Catch', 'Throw', 'During Event', 'Outside Event']]
# for m in ['R', 'L', 'both']]

colsDoMean = ['Catch', 'Score',
              'Shot Phase', 'Shot GaitCycle',
              'Catch Phase', 'Catch GaitCycle',
              'Throw Phase', 'Throw GaitCycle'] + \
             ['%s %s' % (j, k) for j in walkingParams
              for k in ['Shot', 'Catch', 'Throw', 'During Event', 'Outside Event']]
# for m in ['R', 'L', 'both']]
meanData = []
# do the mean per subject per session
for subID in validSubs:
    # subID = '%s%03d' % (subName, j)
    auxData = pandas.DataFrame(columns=columns, data=np.zeros((len(session) * 2, len(columns))) * np.nan,
                               index=np.arange(len(session) * 2))
    auxData['SubID'] = subID
    for k, s in enumerate(session):
        print('Doing %s, %s' % (subID, s))
        auxData.loc[2 * k:2 * k + 1, 'Session'] = s
        m = np.logical_and(allData['index'] > 9, np.logical_and(allData['SubID'] == subID, allData['Session'] == s))
        auxData.loc[2 * k, 'type'] = 'mean'
        auxData.loc[2 * k, colsDoMean] = allData.loc[m, colsDoMean].mean()
        auxData.loc[2 * k + 1, 'type'] = 'sd'
        auxData.loc[2 * k + 1, colsDoMean] = allData.loc[m, colsDoMean].std()
    meanData.append(auxData.copy())

# meanData2 = []
#
# for j in range(st, en + 1):
#     subID = '%s%03d' % (subName, j)
#     auxData = pandas.DataFrame(columns=['timing', 'type', 'Catch', 'Score', 'SubID', 'Session'] + walkingParams,
#                                index=['Shot', 'Catch', 'Throw', 'During Event', 'Outside Event'])
#     for s in session:
#         for ti in ['Shot', 'Catch', 'Throw', 'During Event', 'Outside Event']:
#             intParams = ['%s %s %s' % (j, k, m) for j in walkingParams
#                          for k in [ti]
#                          for m in ['both']]
#             auxData.loc[ti, walkingParams] =


meanData = pandas.concat(meanData)
mMean = meanData.where(meanData['type'] == 'mean').dropna(how='all')
mStd = meanData.where(meanData['type'] == 'sd').dropna(how='all')

# m = allData.where(allData['Session'] == 'M')
# m.dropna(inplace=True, how='all')

# sns.barplot(x='SubID', y='Catch', hue='Session', data=allData)


# f, ax = plt.subplots(2, sharex=True, sharey=True)
# sns.barplot(hue='SubID', y='Catch', x='Session', data=allData, ax=ax[0])
# sns.barplot(y='Catch', x='Session', data=allData, ax=ax[1])
#
# f2, ax2 = plt.subplots(2, sharex=True, sharey=True)
# sns.barplot(hue='SubID', y='Score', x='Session', data=allData, ax=ax2[0])
# sns.barplot(y='Score', x='Session', data=allData, ax=ax2[1])
#
# # sns.barplot(hue='SubID', y='Score', x='Phase', data=allData, ax=ax3[0])
# # sns.barplot(y='Score', x='Phase', data=allData, ax=ax3[1])
#
#
# f3, ax3 = plt.subplots(2)
# sns.barplot(x='Catch', y='GaitCycle', hue='SubID', data=m, ax=ax3[0])
# sns.barplot(x='Catch', y='GaitCycle', data=m, ax=ax3[1])

# catchScore['MCatch'].loc[49,'CAT010']=0

# f, ax = plt.subplots(2, sharex=True)
# mCatch = catchScore['MCatch'].iloc[10:, :].sum(axis=0)
# sCatch = catchScore['SCatch'].iloc[10:, :].sum(axis=0)
# catchAll = pandas.DataFrame(data={'Standing': sCatch, 'Walking': mCatch})
#
# sns.barplot(data=catchAll, ax=ax[0], ci='sd')
#
# mScore = catchScore['MScore'].iloc[10:, :].sum(axis=0)
# sScore = catchScore['SScore'].iloc[10:, :].sum(axis=0)
# ScorehAll = pandas.DataFrame(data={'Standing': sScore, 'Walking': mScore})
#
# sns.barplot(data=ScorehAll, ax=ax[1], ci='sd')
#
# f2, ax2 = plt.subplots(2, sharex=True)
# sns.boxplot(data=catchAll, ax=ax2[0])
# sns.boxplot(data=ScorehAll, ax=ax2[1])


# for i, (s, c) in enumerate(zip(session, ['r', 'b'])):
#     for j, n in enumerate(['Catch', 'Score']):
#         sns.tsplot(catchScore[s + n].values.T, ci='sd', ax=ax[j], color=c)
#
# ax[0].legend(['Multi Task', 'Walking'])
# ax[0].set_ylim([-0.2, 2])
# ax[0].set_title('Catch Performance')
# ax[0].set_xlabel('Attempt')
# ax[0].set_yticks([0, 1])
#
# ax[1].legend(['Multi Task', 'Walking'])
# ax[1].set_title('Throw Performance')
# ax[1].set_xlabel('Attempt')
# ax[1].set_ylabel('Points Scored')
