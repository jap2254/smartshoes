import pandas
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.transform import Rotation as R

VRfile = "E:/Drive/JAP2254/Lab/Walker/OvergroundFrontalMoments/Unity/Sub06/BaselineWalker.csv"

def viveQuats2H(quatsDF, coordsDF):

    r = R.from_quat(quatsDF)
    rot = r.as_matrix()
    H = createTransformationMatrix(rot, coordsDF)

    return H
def createTransformationMatrix(rotation, translation):

    len = rotation.shape[0]

    translation = np.expand_dims(translation, axis=2)

    translation_local = np.matmul(rotation, translation)

    t = np.dstack((rotation, -translation_local))
    bottomRow = np.expand_dims((np.hstack((np.zeros((len,3)), np.ones((len,1))))), axis=1)
    tMat = np.hstack((t,bottomRow))

    return tMat


# movingTH = 1.0
#
# addAxisNames = lambda name: ['%s [%s]' % (name, ax) for ax in ['x', 'y', 'z']]
# addAxisNamesRot = lambda name: ['%s [%s]' % (name, ax) for ax in ['ox', 'oy', 'oz', 'ow']]

# Locate pelvis, trunk, walker right, walker left
def locateTrackers(VRfile, movingTH=1.0):
    # what is moving
    trackerInfo = {}
    rDatao = pandas.read_csv(VRfile)
    xCols = rDatao.filter(like='[x]')
    xCols = xCols[xCols.std().where(xCols.std() > movingTH).dropna().index]
    yCols = rDatao.filter(like='[y]')
    movingTrackers = [c[:-4] for c in xCols.columns]
    zCols = pandas.DataFrame(index=xCols.index)
    for c in movingTrackers:
        zCols = zCols.join(rDatao.filter(like=c))
    zCols = zCols.filter(like='[z]')
    if xCols.shape[1] >= 2:
        mea = zCols.mean()
        trackerInfo = {k: v[:-4] for k, v in zip(['Trunk', 'Pelvis'], mea.sort_values(ascending=True).index[:2])}
        if xCols.shape[1] > 2:
            # so had 4
            noUsedCols = [v[:-4] for v in mea.sort_values(ascending=True).index[2:]]
            trackerInfo['Walker 1'] = noUsedCols[0]
            trackerInfo['Walker 0'] = noUsedCols[1]

            # rotatedpv = []
            # # rots = R.from_quat()
            # H = viveQuats2H(rDatao[addAxisNamesRot(trackerInfo['Pelvis'])], rDatao[addAxisNames(trackerInfo['Pelvis'])])
            # pv = []
            # # f, ax = plt.subplots(3, sharey=True)
            # for nU in noUsedCols:
            #     aux = rDatao[addAxisNames(nU)].copy()
            #     aux['extra'] = 1
            #     aux = np.matmul(H, np.expand_dims(aux.values, 2))
            #     # ax[0].plot(aux[:, 0])
            #     # ax[1].plot(aux[:, 1])
            #     # ax[2].plot(aux[:, 2])
            #     pv.append(aux.copy()[:, 2])










        return trackerInfo



# locateTrackers(VRfile)
