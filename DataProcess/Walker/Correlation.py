import pickle
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import seaborn as sns


fileName = "E:/Drive/JAP2254/Lab/Walker/OvergroundFrontalMoments/SubR_allProcessedData.pickle"

with open(fileName, 'rb') as f:
    data = pickle.load(f)

parsCols = ['Subject', 'Trial', 'Lap', 'Right Stride', 'Percentage']
col2use = 'Calc Wrench My'


def myFun(aO, parsCols=['Subject', 'Trial', 'Lap', 'Right Stride'], col2use='Calc Wrench My'):
    b = aO.loc[aO.index[0], parsCols]
    a = aO.groupby('Percentage').mean()
    xx = np.sin(2 * np.pi * a.index.values)
    aa = signal.correlate(xx, a[col2use].values)
    lags = signal.correlation_lags(xx.size, a[col2use].size, mode="full")
    b['Corr'], b['Lags'] = (max(aa), lags[np.argmax(aa)])

    return b


dataV = data['Unstacked Data'].loc[data['Unstacked Data']['Trial'] == 'Moments']
a = dataV.groupby(['Subject', 'Trial', 'Lap'])
b = a.apply(myFun)
b.reset_index(inplace=True, drop=True)
b.dropna(how='any', inplace=True)
print(b.groupby('Subject')['Lags'].describe())

li = dataV.groupby('Percentage').mean()['Calc Wrench My']
xx = li.max() * np.sin(2 * np.pi * li.index.values)
aa = signal.correlate(xx, li.values)
lags = signal.correlation_lags(xx.size, li.size, mode="full")
corr, lag = (max(aa), lags[np.argmax(aa)])
print('Max correlation is %.2f with a delay of %f' % (corr, lag))
f, ax = plt.subplots(1)
xx = 8*np.sin(2 * np.pi * np.linspace(0, 100, 100))
plt.plot(xx)
sns.lineplot(data=dataV, x='Percentage', y=col2use, ci='sd', ax=ax, color='r')
ax.set_ylabel('Frontal Plane Moment')
ax.set_xlabel('Gait Cycle Percentage')
plt.legend(['Desired Moment', 'Applied Moment'])
plt.tight_layout()
