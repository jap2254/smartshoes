import pandas
import numpy as np
import Tools.Utils as tu
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.spatial.transform import Rotation as R


def locateTrackers(rDatao, movingTH=1.0):
    # what is moving
    trackerInfo = {}
    # rDatao = pandas.read_csv(VRfile)
    xCols = rDatao.filter(like='[x]')
    xCols = xCols[xCols.std().where(xCols.std() > movingTH).dropna().index]
    yCols = rDatao.filter(like='[y]')
    movingTrackers = [c[:-4] for c in xCols.columns]
    zCols = pandas.DataFrame(index=xCols.index)
    for c in movingTrackers:
        zCols = zCols.join(rDatao.filter(like=c))
    zCols = zCols.filter(like='[z]')
    if xCols.shape[1] >= 2:
        mea = zCols.mean()
        trackerInfo = {k: v[:-4] for k, v in zip(['Trunk', 'Pelvis'], mea.sort_values(ascending=False).index[:2])}
        if xCols.shape[1] > 2:
            # so had 4
            noUsedCols = [v[:-4] for v in mea.sort_values(ascending=True).index[2:]]
            trackerInfo['Walker 1'] = noUsedCols[0]
            trackerInfo['Walker 0'] = noUsedCols[1]

            # rotatedpv = []
            # # rots = R.from_quat()
            # H = viveQuats2H(rDatao[addAxisNamesRot(trackerInfo['Pelvis'])], rDatao[addAxisNames(trackerInfo['Pelvis'])])
            # pv = []
            # # f, ax = plt.subplots(3, sharey=True)
            # for nU in noUsedCols:
            #     aux = rDatao[addAxisNames(nU)].copy()
            #     aux['extra'] = 1
            #     aux = np.matmul(H, np.expand_dims(aux.values, 2))
            #     # ax[0].plot(aux[:, 0])
            #     # ax[1].plot(aux[:, 1])
            #     # ax[2].plot(aux[:, 2])
            #     pv.append(aux.copy()[:, 2])

        return trackerInfo

def angleBetween(x1, x2):
    x1A = np.multiply(x1, 1 / np.linalg.norm(x1))
    x2A = np.multiply(x1, 1 / np.linalg.norm(x2))
    return np.arccos(np.dot(x1A, x2A))


def calculateH(x, xy, ori, space2body=True):
    """
    Returns a space to body homogenous transformation
    :param x: X axis of the system in space frame, it can be a matrix of shape nx3
    :type x: numpy.ndarray
    :param xy: vector in the xy plane axis of the system in space frame, it can be a matrix of shape nx3
    :type xy: numpy.ndarray
    :param ori: location of the origin in space
    :type ori: numpy.ndarray
    :param space2body: if true returns space2body, else body2space
    :type space2body: bool
    :return: homogenous transformation
    :rtype: np.ndarray
    """
    inpu = [x, xy, ori]
    for j, i in enumerate(inpu):
        if len(i.shape) == 1:
            inpu[j] = np.expand_dims(i, 0)
    x, xy, ori = inpu
    xA = np.multiply(x.T, 1 / np.linalg.norm(x, axis=1)).T
    zA = np.cross(xA, xy)
    zA = np.multiply(zA.T, 1 / np.linalg.norm(zA, axis=1)).T
    yA = np.cross(zA, xA)
    # R is space to body
    H = np.zeros((yA.shape[0], 4, 4))
    R = np.stack((xA, yA, zA), axis=2)
    H[:, :3, :3] = R
    # H[:, :3, -1] = np.squeeze(ori)
    H[:, :3, -1] = -np.squeeze(np.matmul(R, np.expand_dims(ori, 2)))
    H[:, -1, -1] = 1
    if not space2body:
        # R = np.transpose(np.stack((xA, yA, zA), axis=2), (0, 2, 1))
        # H[:, :3, :3] = R
        # H[:, :3, -1] = np.squeeze(-np.matmul(R, np.expand_dims(ori, 2)))
        H = inverseH(H)
    return H

def calculateH2(x, xy, ori, space2body=True):
    """
    Returns a space to body homogenous transformation
    :param x: X axis of the system in space frame, it can be a matrix of shape nx3
    :type x: numpy.ndarray
    :param xy: vector in the xy plane axis of the system in space frame, it can be a matrix of shape nx3
    :type xy: numpy.ndarray
    :param ori: location of the origin in space
    :type ori: numpy.ndarray
    :param space2body: if true returns space2body, else body2space
    :type space2body: bool
    :return: homogenous transformation
    :rtype: np.ndarray
    """
    xA = np.multiply(x.T, 1 / np.linalg.norm(x))
    zA = np.cross(xA, xy)
    zA = np.multiply(zA.T, 1 / np.linalg.norm(zA))
    yA = np.cross(zA, xA)
    # R is space to body
    H = np.zeros((4, 4))
    R = np.stack((xA, yA, zA), axis=0)
    H[:3, :3] = R
    # H[:3, -1] = ori
    H[:3, -1] = -np.dot(R, ori)
    H[-1, -1] = 1
    if not space2body:
        # R = np.transpose(np.stack((xA, yA, zA), axis=2), (0, 2, 1))
        # H[:, :3, :3] = R
        # H[:, :3, -1] = np.squeeze(-np.matmul(R, np.expand_dims(ori, 2)))
        H = inverseH(H)
    return H

def inverseH2(H):
    HI = np.zeros_like(H)
    HI[:3, :3] = H[:3, :3].T
    HI[:3, -1] = -np.dot(H[:3, :3].T, H[:3, -1])
    HI[:3, :3] = H[:3, :3].T
    HI[-1, -1] = 1
    return HI

def inverseH(H):
    if len(H.shape) is 2:
        Ht = np.expand_dims(H, 0)
    else:
        Ht = H.copy()
    R = np.transpose(Ht[:, :3, :3], (0, 2, 1))
    R1 = Ht[:, :3, :3]
    o = Ht[:, :3, -1]
    Ht[:, :3, :3] = R
    Ht[:, :3, -1] = -np.matmul(R1
                               , np.expand_dims(o, 2)).squeeze()
    return Ht


def wrap(angles):
    return np.arcsin(np.sin(angles))
    # return (angles + np.pi) % (2 * np.pi) - np.pi


def transformVec(hMat,vec):

    fullVec = np.hstack((vec, np.ones((vec.shape[0],1))))
    fullVec = np.expand_dims(fullVec,axis=2)
    fulltVec = np.matmul(hMat,fullVec)
    tVec = fulltVec.squeeze()
    return tVec[:,:-1]


addAxisNames = lambda name: ['%s [%s]' % (name, ax) for ax in ['x', 'y', 'z']]
addAxisNamesRot = lambda name: ['%s [%s]' % (name, ax) for ax in ['ox', 'oy', 'oz', 'ow']]

parentPath = 'E:/Drive/JAP2254/Lab/Walker/OvergroundFrontalMoments/'
VRpath = parentPath + 'Unity'
matPAth = parentPath + 'Mat'
useLeft = False
subjects = [6]
sessions = ['Baseline', 'BaselineWalker', 'Moments', 'Post']
tCut = [[5000.] * 4] * len(subjects)

nPoints = 200
hsData = []
pelvisData = []
trunkData = []
freq = 100.

for sub, t1 in zip(subjects, tCut):
    subID = 'Sub%02d' % sub
    walkerTrackerR = None
    walkerTrackerL = None
    for sess, tt in zip(sessions, t1):
        vrDatao = pandas.read_csv('%s/%s/%s.csv' % (VRpath, subID, sess))
        trackerInfo = locateTrackers(vrDatao)
        pelvisTracker = trackerInfo['Pelvis']
        trunkTracker = trackerInfo['Trunk']
        # cut sync
        vrDatao = vrDatao.where(np.logical_and(vrDatao['Sync'] == 1, vrDatao['T'] < tt)).dropna(how='all')
        vrDatao['T'] -= vrDatao['T'].values[0]
        # resample data
        cols2use = addAxisNames(pelvisTracker) + addAxisNamesRot(pelvisTracker) + \
                   addAxisNames(trunkTracker) + \
                   addAxisNamesRot(trunkTracker)
        vrData = pandas.DataFrame(columns=['T'] + cols2use)
        vrData['T'] = np.arange(vrDatao['T'].values[-1], step=1. / freq)
        vrData[cols2use] = tu.butter_lowpass_filter(
            tu.reSampleMatrix(vrDatao[cols2use].values, 0,
                              vrDatao['T'], vrData['T']), 10., freq)
        mat = tu.matFile2Pandas('%s/%s/%s.xlsx' % (matPAth, subID, sess))
        mat = mat.where(mat['isLeft'] == useLeft).dropna(how='all')
        laps = mat['Laps'].unique()
        series = []
        j = 0
        for la in laps:
            hs = mat.where(mat['Laps'] == la).dropna(how='all')['First Contact (sec.)']
            for st, en in zip(hs[:-1], hs[1:]):
                aux = vrData[addAxisNames(pelvisTracker)].where(vrData['T'].between(st, en)).dropna(how='all')
                if aux.shape[0] == 0:
                    continue
                xaux = aux.values[-1] - aux.values[0]
                xaux[2] = 0
                # this is my walking direction
                H = calculateH2(xaux, np.array([0., 0., 1.]), aux.values[0], True)
                if walkerTrackerR is None and sess not in 'Baseline' and sess not in 'Post':
                    # id it!!!
                    auxW1 = vrDatao[addAxisNames(trackerInfo['Walker 1'])].where(vrDatao['T'].between(st, en)).dropna(how='all')
                    auxW2 = vrDatao[addAxisNames(trackerInfo['Walker 0'])].where(vrDatao['T'].between(st, en)).dropna(how='all')
                    auxW1 = transformVec(H, auxW1)
                    auxW2 = transformVec(H, auxW2)
                    xAx = np.array([1., 0])
                    an1 = angleBetween(xAx, auxW1[-1, [0, 2]])
                    an2 = angleBetween(xAx, auxW2[-1, [0, 2]])
                    if an1 > an2:
                        # an1 is left
                        trackerInfo['WalkerR'] = trackerInfo['Walker 0']
                        trackerInfo['WalkerL'] = trackerInfo['Walker 1']
                    else:
                        # an1 is left
                        trackerInfo['WalkerR'] = trackerInfo['Walker 1']
                        trackerInfo['WalkerL'] = trackerInfo['Walker 0']
                    a = 1
                # aux['extra'] = 1
                aux2 = tu.reSampleMatrix(transformVec(H, aux), nPoints)
                # aux2 = tu.reSampleMatrix(np.squeeze(np.dot(H, aux.T).T)[:, :3], nPoints)
                aux3 = pandas.DataFrame(data=aux2, columns=['x', 'y', 'z'])
                aux3['Percentage'] = np.linspace(0, 100, nPoints)
                aux3['Time'] = np.linspace(st, en, nPoints)
                aux3['Stride'] = j
                aux3['Session'] = sess
                aux3['Subject'] = subID
                aux3['Tracker'] = pelvisTracker
                hsData.append(aux3.copy())
                # this is rotation
                aux = vrData[addAxisNamesRot(pelvisTracker)].where(vrData['T'].between(st, en)).dropna(how='all')
                # R.from_quat(aux[addAxisNamesRot(pelvisTracker)]).as_euler('XYZ')
                aux2 = R.from_quat(aux[addAxisNamesRot(pelvisTracker)]).as_matrix()
                # aux22 = wrap(R.from_matrix(np.matmul(aux2[0], aux2.transpose(0, 2, 1))).as_euler('XYZ'))
                aux22 = wrap(R.from_matrix(np.matmul(np.expand_dims(H[:3, :3], 0), aux2.transpose(0, 2, 1))).as_euler('XYZ'))
                aux22 = tu.butter_lowpass_filter(aux22, 10.0, freq)
                aux22 -= aux22[0]
                aux3 = pandas.DataFrame(data=tu.reSampleMatrix(aux22, nPoints) * 180. / np.pi,
                                        columns=['x', 'y', 'z'])
                aux3['Percentage'] = np.linspace(0, 100, nPoints)
                aux3['Time'] = np.linspace(st, en, nPoints)
                aux3['Stride'] = j
                aux3['Session'] = sess
                aux3['Subject'] = subID
                aux3['Tracker'] = 'Pelvis'
                pelvisData.append(aux3.copy())
                # pelvis
                auxTrunkD = vrData[addAxisNamesRot(trunkTracker)].where(vrData['T'].between(st, en)).dropna(how='all')

                auxTrunk = R.from_quat(auxTrunkD[addAxisNamesRot(trunkTracker)]).as_matrix()

                aux22 = wrap(R.from_matrix(np.matmul(aux2, auxTrunk.transpose(0, 2, 1))).as_euler('XYZ'))
                aux22 = tu.butter_lowpass_filter(aux22, 10.0, freq)
                aux22 -= aux22[0]
                aux3 = pandas.DataFrame(data=tu.reSampleMatrix(aux22, nPoints) * 180. / np.pi,
                                        columns=['x', 'y', 'z'])
                aux3['Percentage'] = np.linspace(0, 100, nPoints)
                aux3['Time'] = np.linspace(st, en, nPoints)
                aux3['Stride'] = j
                aux3['Session'] = sess
                aux3['Subject'] = subID
                aux3['Tracker'] = 'Trunk'
                trunkData.append(aux3.copy())
                j += 1

hsData = pandas.concat(hsData)
pelvisData = pandas.concat(pelvisData)
trunkData = pandas.concat(trunkData)
# db = {'PelvisP': hsData, 'PelvisO': pelvisData, 'TrunkO': trunkData}
# with open(parentPath + 'movementData.pickle', 'wb') as f:
#     pickle.dump(db, f)
f, ax = plt.subplots(3, sharex=True)
f2, ax2 = plt.subplots(3, sharex=True)
f3, ax3 = plt.subplots(3, sharex=True)
for d, axx, axx2, axx3 in zip(['x', 'y', 'z'], ax, ax2, ax3):
    sns.lineplot(data=hsData, x='Percentage', y=d, hue='Session', ax=axx, ci=None)
    sns.lineplot(data=pelvisData, x='Percentage', y=d, hue='Session', ax=axx2, ci=None)
#     sns.lineplot(data=trunkData, x='Percentage', y=d, hue='Session', ax=axx3, ci=None)
