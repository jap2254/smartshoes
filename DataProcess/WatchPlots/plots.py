import numpy as np
import seaborn as sns
import pandas
import matplotlib.pyplot as plt


def createData(conditions, group, means, sds, varName):
    data = []
    for c, m, s, g in zip(conditions, means, sds, group):
        aux = pandas.DataFrame(data=np.random.normal(m, s, 80), columns=[varName])
        aux['Group'] = c
        aux['Sub'] = aux.index.values
        aux['Fit'] = g[0]
        aux['Activity'] = g[1]
        data.append(aux)

    return pandas.concat(data)


conditions = ['TL', 'TH', 'SL', 'SH', 'LL', 'LH']
group = [[x, xx] for x in ['Tight', 'Snug', 'Loose'] for xx in ['Low', 'High']]
comfortMeans = [50, 52, 80, 85, 88, 89]
comfortSD = [5, 4, 10, 8, 5, 15]
sensorMeans = [90, 88, 88, 89, 75, 65]
sensorSD = [5, 4, 10, 8, 5, 15]
comfort = createData(conditions, group, comfortMeans, comfortSD, 'Comfort Rating')
metrics = createData(conditions, group, sensorMeans, sensorSD, 'Accuracy')
allData = metrics.copy()
allData['Comfort Rating'] = comfort['Comfort Rating']


# g = sns.PairGrid(allData, y_vars=["Comfort Rating", 'Accuracy'],
#                  x_vars=['Activity'], hue='Fit')
#                  # height=5, aspect=.5)
#
# # Draw a seaborn pointplot onto each Axes
# g.map(sns.pointplot, scale=1.3, errwidth=4)

f, ax = plt.subplots(1, 2, sharex=True, sharey=True)
sns.pointplot(data=comfort, x='Activity', y='Comfort Rating', hue='Fit', ci='sd', ax=ax[0])
sns.pointplot(data=metrics, x='Activity', y='Accuracy', hue='Fit', ci='sd', ax=ax[1])

