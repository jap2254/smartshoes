import Tools.Utils as tu
import seaborn as sns
import pickle
import pandas
import numpy as np
import Tools.PlotUtils as ptu

# TODO: The prediction time is not synced!!!!!!!!

parentPath = 'E:/Drive/JAP2254/Lab/SmartShoes/RealTime/Haptic'
# parentPath = 'G:/My Drive/JAP2254/Lab/SmartShoes/RealTime/Haptic'
allSubsPath = parentPath + '/predictions.pickle'

with open(allSubsPath, 'rb') as f:
    pickledb = pickle.load(f)

subs = range(2, 11)

rightSideDominant = [True, True, False, True, False, True, True, False, True]

sessions = ['baseline', 'train', 'post']
sessionsN = ['Baseline', 'Train', 'Post']
fmt = '>3c 4L 3c'
footer = [(x).to_bytes(1, 'big') for x in [1, 2, 3]]
header = [(x).to_bytes(1, 'big') for x in [0xA, 0xB, 0xC]]
freq = 50.0
allSubs = pandas.DataFrame()
c = -1
for s, side in zip(subs, rightSideDominant):
    subID = 'Sub%03d' % s
    if side:
        actSide = 'L'
    else:
        actSide = 'R'

    for sess, sN in zip(sessions, sessionsN):
        c += 1
        tMat = pickledb[subID][sess]['gtT']
        gt = pickledb[subID][sess]['gt']
        tt = pickledb[subID][sess]['predT']
        pred = pickledb[subID][sess]['pred']
        minLen = min(gt.shape[0], pred.shape[0])
        allSubs.loc[c, 'acc'] = np.mean(gt[:minLen] == pred[:minLen])
        allSubs.loc[c, 'Sub'] = subID
        allSubs.loc[c, 'Session'] = sN
        allSubs.loc[c, 'Type'] = 'all'
        # compared with pred saved in rt
        # if 'train' in sess:
        #     c += 1
        #     predFile = parentPath + '/' + subID + '/' + sess + '_pred.bin'
        #     predRT = tu.readBinaryFile2Pandas(fmt, fileName=predFile, colNames=['tR', 'tL', 'pR', 'pL'], header=header,
        #                                       footer=footer) / 1000.0
        #     matFile = parentPath + '/' + subID + '/' + sess + '.xlsx'
        #     mat2 = tu.matFile2Pandas(matFile)
        #     mat2 = mat2.where(mat2['isLeft'] == float(side)).dropna(how='all')[['Laps', 'isLeft',
        #                                                                         'First Contact (sec.)',
        #                                                                         'Last Contact (sec.)']]
        #     laps = pandas.DataFrame()
        #     for j in mat2['Laps'].unique():
        #         laps.loc[j, 'Start'] = mat2.where(mat2['Laps'] == j).dropna().values[0, 2]
        #         laps.loc[j, 'End'] = mat2.where(mat2['Laps'] == j).dropna().values[-1, 3]
        #
        #     if side:
        #         tRT = predRT['tL'].values
        #         ppRT = predRT['pL'].values
        #     else:
        #         tRT = predRT['tR'].values
        #         ppRT = predRT['pR'].values
        #
        #     tRT = np.expand_dims(tRT, 1)
        #     m = np.logical_and(tRT > laps.values[:, 0], tRT < laps.values[:, 1]).any(axis=1)
        #     tRT = np.squeeze(tRT[m])
        #     ppRT = ppRT[m].astype(np.int)
        #
        #     allSubs.loc[c, 'acc'] = np.mean(pred[(tRT * freq).astype(np.int)] == ppRT)
        #     allSubs.loc[c, 'Sub'] = subID
        #     allSubs.loc[c, 'Session'] = sess
        #     allSubs.loc[c, 'Type'] = 'rt'

allSubs['acc'] *= 100
sns.catplot(data=allSubs, x='Session', y='acc', kind='bar')
ax = sns.catplot(data=allSubs, x='Session', y='acc', kind='box', fliersize=0)
significance = [[0, 1],
                [0, 2]]
maxHeight = 0.05
minHeight = 0.03
for j, ((c1, c2), h) in enumerate(zip(significance, np.linspace(minHeight, maxHeight, len(significance)))):
    ptu.addSignificanceLevel(ax.ax, c1, c2, allSubs, '*', 'acc', offsetPercentage=h)
