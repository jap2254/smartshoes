import Tools.Utils as tu
import seaborn as sns
import pandas
import numpy as np
import pickle
import Tools.PlotUtils as ptu


parentPath = 'Z:/Haptic/muscle/musclesVibration'
saveFile = parentPath + '/Not_normalized.pickle'
saveFileStats = parentPath + '/stats.csv'

subs = range(1, 8)

sessions = ['Baseline', 'Constant', 'Timed', 'Post_Constant', 'Post_Timed']

columnsDescription = ['Laps', 'isLeft', 'StepIndex', 'num', 'First Contact (sec.)', 'Last Contact (sec.)']
columnsPars = ['Stride Length (cm.)', 'Stride Width (cm.)', 'Stride Time (sec.)', 'Stance Time (sec.)',
               'Swing Time (sec.)', 'Step Length (cm.)', 'Stride Velocity (cm./sec.)', 'Step Time (sec.)']
normalize = False
REDO_READ = False
if REDO_READ:
    allSubs = []
    allSubsSummary = []
    nVal = 1
    for s in subs:
        subID = 'MS%03d' % s
        for sess in sessions:
            f = parentPath + '/' + subID + '/' + sess + '.xlsx'
            mat = tu.matFile2Pandas(f)
            # mat['Stance-TDS'] = (mat['Stance Time (sec.)'] - mat['Terminal D. Support (sec.)']).values
            mat = mat[columnsDescription + columnsPars]
            if normalize:
                if 'Baseline' in sess:
                    nVal = mat[columnsPars].mean()
                mat[columnsPars] /= nVal
            matAux = pandas.concat((mat[columnsDescription] for _ in range(len(columnsPars))))
            matAux['Sub'] = subID
            matAux['Session'] = sess
            matAux['Vals'] = mat[columnsPars].values.T.reshape(mat.shape[0] * len(columnsPars))
            matAux['Type'] = np.array([[s] * mat.shape[0] for s in columnsPars]).reshape((-1, 1))
            if 'Baseline' not in sess and 'Post' not in sess:
                matAux2 = []
                for j, (st, en) in enumerate(zip([0, 300, 600], [300, 600, 900])):
                    matAu3 = mat.where((mat['First Contact (sec.)'] > st) &
                                       (mat['Last Contact (sec.)'] < en))[columnsPars].mean().to_frame().reset_index()
                    matAu3.rename(columns={0: 'Val', 'index': 'Type'}, inplace=True)
                    matAu3['CV'] = (mat[columnsPars].std() / mat[columnsPars].mean()).values
                    # matAux2 = pandas.concat((matAuxb[columnsDescription] for _ in range(len(columnsPars))))
                    matAu3['Sub'] = subID
                    matAu3['Session'] = sess + str(j + 1)
                    matAux2.append(matAu3)
                matAux2 = pandas.concat(matAux2)
                allSubsSummary.append(matAux2)

            matAux2 = mat[columnsPars].mean().to_frame().reset_index()
            matAux2.rename(columns={0: 'Val', 'index': 'Type'}, inplace=True)
            matAux2['CV'] = (mat[columnsPars].std() / mat[columnsPars].mean()).values
            # matAux2 = pandas.concat((matAuxb[columnsDescription] for _ in range(len(columnsPars))))
            matAux2['Sub'] = subID
            matAux2['Session'] = sess
            # for pars in columnsPars:

            allSubs.append(matAux)
            allSubsSummary.append(matAux2)

    allSubs = pandas.concat(allSubs)
    allSubsSummary = pandas.concat(allSubsSummary)

    allSubsSummary.to_csv(saveFileStats)
    with open(saveFile, 'wb') as f:
        pickle.dump({'allSubs': allSubs, 'summary': allSubsSummary}, f)

    # sns.catplot(x='Type', y='Vals', hue='Session', data=allSubs, ci='sd', kind='bar', col='Sub', col_wrap=3)
else:
    with open(saveFile, 'rb') as f:
        d = pickle.load(f)
        allSubsSummary = d['summary']
        allSubs = d['allSubs']

# columnsPars = ['Stride Length (cm.)', 'Stride Width (cm.)', 'Stride Time (sec.)', 'Stance Time (sec.)',
#                'Swing Time (sec.)', 'Step Length (cm.)', 'Stride Velocity (cm./sec.)', 'Step Time (sec.)']
sigsC = [[[0, 2]],  # 'Stride Length (cm.)'
         [],  # 'Stride Width (cm.)'
         [],  # 'Stride Time (sec.)'
         [],  # 'Stance Time (sec.)'
         [],  # 'Swing Time (sec.)'
         [[0, 2]],  # 'Step Length (cm.)'
         [[0, 2], [1, 2]],  # 'Stride Velocity (cm./sec.)'
         [[1, 2]],  # 'Step Time (sec.)'
         ]
sigsT = [[],  # 'Stride Length (cm.)'
         [],  # 'Stride Width (cm.)'
         [[1, 2]],  # 'Stride Time (sec.)'
         [[0, 1]],  # 'Stance Time (sec.)'
         [],  # 'Swing Time (sec.)'
         [],  # 'Step Length (cm.)'
         [[1, 2]],  # 'Stride Velocity (cm./sec.)'
         [[1, 2]],  # 'Step Time (sec.)'
         ]
sessions1 = [['Baseline', 'Constant', 'Post_Constant'], ['Baseline', 'Timed', 'Post_Timed']]
maxHeight = 0.01
minHeight = 0.00
for par, sC, sT in zip(columnsPars, sigsC, sigsT):
    for sess, significance, co in zip(sessions1, [sC, sT], ['k', 'k']):
        aux = allSubs.where(allSubs['Type'] == par).dropna()
        ax = sns.catplot(x='Session', y='Vals', data=aux, ci='sd', kind='box', order=sess, fliersize=0.0)
        ax.ax.set_ylabel(par)
        ax.ax.set_xlabel('')
        # for significance, co in zip(signs[:2], ['k', 'k']):
        for j, ((c1, c2), h) in enumerate(zip(significance, np.linspace(minHeight, maxHeight, len(significance)))):
            ptu.addSignificanceLevel(ax.ax, c1, c2, aux, '*', 'Vals', offsetPercentage=h, color=co)
