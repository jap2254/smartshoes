import Tools.Utils as tu
import pandas
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


# trainCSV = "G:/My Drive/JAP2254/Lab/SmartShoes/RealTime/Haptic/Sub008/train2.csv"
# trainCSVnew = "G:/My Drive/JAP2254/Lab/SmartShoes/RealTime/Haptic/Sub008/train3.csv"
#
# file = open(trainCSV, 'r')
#
# file_read = file.read()
# file.close()
#
# firstLap = 17
# nLapsInFile = 24
#
# for j in range(nLapsInFile).__reversed__():
#     for s in ['Right', 'Left']:
#         orLap = ',%d: %s' % (j+True, s)
#         newLap = ',%d: %s' % (j+firstLap, s)
#         file_read = file_read.replace(orLap, newLap)
#
#
# with open(trainCSVnew, 'w') as f:
#     f.write(file_read)

parentPath = 'E:/Drive/JAP2254/Lab/SmartShoes/RealTime/Haptic'
# parentPath = 'G:/My Drive/JAP2254/Lab/SmartShoes/RealTime/Haptic'
subs = range(2, 11)

rightSideDominant = [True, True, False, True, False, True, True, False, True]

sessions = ['baseline', 'train', 'post']
sessionsName = ['Baseline', 'Train', 'Post']
plt.rcParams.update({'font.size': 22})

columnsDescription = ['Laps', 'isLeft', 'StepIndex', 'num', 'First Contact (sec.)', 'Last Contact (sec.)']
columnsPars = ['Stride Length (cm.)', 'Stride Width (cm.)', 'Stance Time (sec.)', 'Swing Time (sec.)', 'Step Length (cm.)']

# columnsPars = ['Stride Length (cm.)', 'Stride Width (cm.)', 'Stride Velocity (cm./sec.)', 'Stance Time (sec.)',
#                'Swing Time (sec.)', 'Single Support (sec.)', 'Terminal D. Support (sec.)', 'Initial D. Support (sec.)']
#, 'Stance-TDS']

allSubs = []

for s, side in zip(subs, rightSideDominant):
    subID = 'Sub%03d' % s
    pBaseline = pandas.DataFrame(columns=columnsPars, index=range(3))
    pBaseline.loc[:, :] = 1
    rxTi = 0
    for sess, sN in zip(sessions, sessionsName):
        pAux = pandas.DataFrame(columns=columnsPars)
        f = parentPath + '/' + subID + '/' + sess + '.xlsx'
        mat = tu.matFile2Pandas(f)
        # mat['Stance-TDS'] = (mat['Stance Time (sec.)'] - mat['Terminal D. Support (sec.)']).values
        mat = mat[columnsDescription + columnsPars]
        pAux.loc[0, :] = mat.where(mat['isLeft'] == 0).dropna()[columnsPars].mean()
        pAux.loc[1, :] = mat.where(mat['isLeft'] == 1).dropna()[columnsPars].mean()
        if side:
            # pAux.loc[2, :] = pAux.loc[1, :] / pAux.loc[0, :]
            pAux.loc[0, 'Side'] = 'Not-Trained'
            pAux.loc[1, 'Side'] = 'Trained'
            # pAux.loc[1, 'Stance Time (sec.)'] -= rxTi
        else:
            # pAux.loc[2, :] = pAux.loc[0, :] / pAux.loc[1, :]
            pAux.loc[0, 'Side'] = 'Trained'
            pAux.loc[1, 'Side'] = 'Not-Trained'
        pAux.loc[:, 'Stance Time (sec.)'] -= rxTi

        # pAux.loc[0, columnsPars] /= pBaseline.loc[0, columnsPars]
        # pAux.loc[1, columnsPars] /= pBaseline.loc[1, columnsPars]
        # pAux.loc[2, 'Side'] = 'Ratio'
        if 'baseline' in sess:
            pBaseline = pAux.copy()
            rxTi = 0.0
            # rxTi = pAux['Terminal D. Support (sec.)'][0] + pAux['Initial D. Support (sec.)'][0] + 0.12
            # pAux['Stance-TDS'] = pAux['Stance Time (sec.)'].values
            # continue
        pAux['SubID'] = subID
        pAux['Session'] = sN

        allSubs.append(pAux.copy())

allSubs = pandas.concat(allSubs)
a2 = tu.pandasPivot(allSubs, columnsPars, ['Side', 'SubID', 'Session'])
allSubs.reset_index(inplace=True, drop=True)

f, ax = plt.subplots(2, 2)
for j, c in enumerate(columnsPars):
    sns.catplot(data=allSubs, x='Session', y=c, hue='Side', ax=ax[j // 2, j % 2], ci='sd', kind='point')
    for s in sessions[1:]:
        for si in ['Trained', 'Not-Trained']:
            mm = allSubs[c].where(np.logical_and(allSubs['Session'] == s, allSubs['Side']==si)).mean()
            sd = allSubs[c].where(np.logical_and(allSubs['Session'] == s, allSubs['Side'] == si)).std()
            print('%s %s %s %f + %f' % (c, s, si, mm, sd))



sessions = ['train']

allSubs2 = []

for s, side in zip(subs, rightSideDominant):
    subID = 'Sub%03d' % s
    for sess in sessions:
        f = parentPath + '/' + subID + '/' + sess + '.xlsx'
        mat = tu.matFile2Pandas(f)
        # mat['Stance-TDS'] = (mat['Stance Time (sec.)'] - mat['Terminal D. Support (sec.)']).values
        mat = mat[columnsDescription + columnsPars]
        # pAux = pandas.DataFrame(columns=columnsPars)
        pAux = mat[columnsPars]
        # pAux.loc[mat['isLeft'] == 0, :] /= pBaseline.loc[0, 'Stance Time (sec.)']
        # pAux.loc[mat['isLeft'] == 1, :] /= pBaseline.loc[1, 'Stance Time (sec.)']
        pAux.loc[mat['isLeft'] == 0, :] /= pBaseline.loc[0, columnsPars]
        pAux.loc[mat['isLeft'] == 1, :] /= pBaseline.loc[1, columnsPars]
        pAux['Side'] = 'Not-Trained'
        if side:
            pAux.loc[mat['isLeft'] == 1, 'Side'] = 'Trained'
        else:
            pAux.loc[mat['isLeft'] == 0, 'Side'] = 'Trained'

        pAux['SubID'] = subID
        pAux['Session'] = sess
        pAux['Minute'] = (mat['First Contact (sec.)'].values // 60 + 1).astype(np.int)

        allSubs2.append(pAux.copy())

allSubs2 = pandas.concat(allSubs2)
allSubs2.reset_index(inplace=True, drop=True)

# sns.catplot(data=allSubs2, x='Minute', y='Single Support (sec.)', hue='Side', kind='bar')
forSPSS = pandas.DataFrame()
for par in columnsPars:
    for sess in sessionsName:
        for side in ['Not-Trained', 'Trained']:
            forSPSS["%s %s %s" % (par, side, sess)] = allSubs[par].where(np.logical_and(allSubs['Side'] == side,
                                                                                       allSubs['Session'] == sess)
                                                                         ).dropna().values
