import Tools.Utils as tu
import numpy as np
import matplotlib.pyplot as plt
import pandas

parentPath = 'Z:/Haptic/muscle/musclesVibration'
fmt = '>3c L 6B 3c'
subs = [2]
freq = 50.0
footer = [(x).to_bytes(1, 'big') for x in [1, 2, 3]]
header = [(x).to_bytes(1, 'big') for x in [0xA, 0xB, 0xC]]
muscleNames = [s + ' ' + m for m in ['Tibial', 'Rectus', 'Femoris']
               for s in ['Left', 'Right']]
ranges = [[.40, .60], [.60, .75], [.30, .40],
          [00, .10], [.10, .30], [.87, 1.00]]
sessionName = 'timed'
for s in subs:
    # f, ax = plt.subplots(1)
    subID = 'MS%03d' % s
    # ax.set_title(subID)
    f = parentPath + '/' + subID + '/%s_triggers.bin' % sessionName
    trig = tu.readBinaryFile2Pandas(fmt, f, colNames=['Time'] + muscleNames, header=header, footer=footer)
    # remove sync
    shoeFile = parentPath + '/' + subID + '/' + sessionName
    syncT = []
    for jj, ss in enumerate(['R', 'L']):
        shoes = tu.binaryFile2python(shoeFile + '_%s.bin' % ss)
        syncT.append(shoes[jj].timestamp[np.where(shoes[jj].sync)[0][0]] / 1000.0)
    # print('%s is L? %d' % (subID, (p['Side'] == 'L'.encode()).all()))
    matFile = parentPath + '/' + subID + '/train.xlsx'
    mat = tu.MatObject(fName=matFile, freq=freq)
    mat.binaryFunctions[trig['Side'].iloc[10].decode()].plot(ax=ax)
    isLeft = float('L' in trig['Side'].iloc[10].decode())
    mat2 = tu.matFile2Pandas(matFile)
    mat2 = mat2.where(mat2['isLeft'] == isLeft).dropna(how='all')[['Laps', 'isLeft',
                                                                   'First Contact (sec.)', 'Last Contact (sec.)']]
    laps = pandas.DataFrame()
    for j in mat2['Laps'].unique():
        laps.loc[j, 'Start'] = mat2.where(mat2['Laps'] == j).dropna().values[0, 2]
        laps.loc[j, 'End'] = mat2.where(mat2['Laps'] == j).dropna().values[-1, 3]
    tt, mTT = np.unique(trig['Time'].astype(np.float) / 1000.0, return_index=True)
    tt = np.expand_dims(tt, 1) - syncT[int(isLeft)]
    m = np.logical_and(tt > laps.values[:, 0], tt < laps.values[:, 1]).any(axis=1)
    tt = np.squeeze(tt[m])
    # [ax.axvline(x=xx, color='b') for xx in trig['Time'].astype(np.float).values / 1000.0]
    [ax.axvline(x=xx, color='r') for xx in tt]
    hs = mat2['First Contact (sec.)']
