import Tools.Utils as tu
import pandas
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pickle

# TODO: The prediction time is not synced!!!!!!!! also the trigger!!!


def compareJustLaps(mat: tu.MatObject, predDF: pandas.DataFrame, freq, matP, syncT):
    # acc = pandas.DataFrame(columns=['Performance', 'sensitivity', 'specificity', 'Precision', 'accuracy', 'delay'])
    acc = pandas.DataFrame(columns=['values', 'Metric', 'side'])
    c = 0
    # f, ax = plt.subplots(2, sharex=True, sharey=True)
    for j, si in enumerate(['R', 'L']):
        mat2 = matP.where(matP['isLeft'] == float(j)).dropna(how='all')[['Laps', 'isLeft',
                                                                         'First Contact (sec.)', 'Last Contact (sec.)']]
        laps = pandas.DataFrame()
        for k in mat2['Laps'].unique():
            laps.loc[k, 'Start'] = mat2.where(mat2['Laps'] == k).dropna().values[0, 2]
            laps.loc[k, 'End'] = mat2.where(mat2['Laps'] == k).dropna().values[-1, 3]
        tt, mTT = np.unique(predDF['t%s' % si], return_index=True)
        predSide = pred['p%s' % si].values[mTT]
        tt -= syncT[j]
        tIndex = (tt * freq).astype(np.int)
        tt = np.expand_dims(tt, 1)
        m = np.logical_and(tt > laps.values[:, 0], tt < laps.values[:, 1]).any(axis=1)
        tt = tt[m]
        pp = predSide[m]
        gt = mat.binaryFunctions.values[tIndex[m], j]
        # ax[j].plot(gt)
        # ax[j].plot(pp)
        acc.loc[c, ['values', 'Metric', 'side']] = [np.mean(pp.astype(np.int) == gt.astype(np.int)) * 100,
                                                    'Performance', si]
        c += 1
        windowSize = 4
        hsP = []
        for jj in range(windowSize, tt.shape[0]):
            if pp[jj - windowSize: jj].sum() == 0 and pp[jj - 2*windowSize:jj - windowSize].sum() == windowSize:
                hsP.append(tt[jj])
        hsP = np.array(hsP)
        hsGT = np.expand_dims(mat2['First Contact (sec.)'].values, 1)
        m = np.logical_and(np.logical_and(hsGT > laps.values[:, 0], hsGT < laps.values[:, 1]),
                           hsGT < tt[-1]).any(axis=1)
        hsGT = hsGT[m]
        for v, me in zip(getMetrics(hsGT, hsP, 0.3, gt.shape[0]), ['Sensitivity', 'Specificity', 'Precision',
                                                                   'Accuracy', 'Delay']):
            acc.loc[c, ['values', 'Metric', 'side']] = [v, me, si]
            c += 1
    return acc


def getMetrics(hsT, hsPo, maxDelay, recordingLength):
    hsP = hsPo.copy()
    falseNegative = 0
    dt = []
    for h in hsT:
        # find the closest hs
        delay = h - hsP
        if delay.shape[0] == 0:
            falseNegative += 1
            continue
        indAux = np.argmin(np.abs(delay))

        if np.abs(delay[indAux]) > maxDelay:
            falseNegative += 1
            continue
        dt.append(delay[indAux])
        # delete the hs from hsP
        hsP = np.delete(hsP, indAux)

    truePositive = len(dt)
    falsePositive = hsP.shape[0]
    trueNegative = recordingLength - truePositive - falseNegative - falsePositive

    sensitivity = truePositive / (truePositive + falseNegative) * 100
    specificity = trueNegative / (trueNegative + falsePositive) * 100
    precision = truePositive / (truePositive + falsePositive) * 100
    accuracy = (truePositive + trueNegative) / recordingLength * 100

    return sensitivity, specificity, precision, accuracy, np.array(dt).mean()[0]


parentPath = 'E:/Drive/JAP2254/Lab/SmartShoes/RealTime/Haptic'

accPath = parentPath + '/acc.csv'

subs = range(2, 11)

rightSideDominant = [True, True, False, True, False, True, True, False, True]

sessions = ['train']

allSubs = []

fmt = '>3c 4L 3c'
footer = [(x).to_bytes(1, 'big') for x in [1, 2, 3]]
header = [(x).to_bytes(1, 'big') for x in [0xA, 0xB, 0xC]]
freq = 120.0
acc = []
c = 0
redoAcc = False
if redoAcc:
    for s, side in zip(subs, rightSideDominant):
        subID = 'Sub%03d' % s
        print('Doing %s' % subID)
        for sess in sessions:
            matFile = parentPath + '/' + subID + '/' + sess + '.xlsx'
            predFile = parentPath + '/' + subID + '/' + sess + '_pred.bin'
            shoeFile = parentPath + '/' + subID + '/' + sess

            matO = tu.MatObject(fName=matFile, freq=freq)
            pred = tu.readBinaryFile2Pandas(fmt, fileName=predFile, colNames=['tR', 'tL', 'pR', 'pL'], header=header,
                                            footer=footer) / 1000.0
            mat2 = tu.matFile2Pandas(matFile)
            syncT = []
            for jj, ss in enumerate(['R', 'L']):
                shoes = tu.binaryFile2python(shoeFile + '_%s.bin' % ss)
                syncT.append(shoes[jj].timestamp[np.where(shoes[jj].sync)[0][0]]/1000.0)


            accAux = compareJustLaps(matO, pred, freq, mat2, syncT)
            accAux['SubID'] = subID
            accAux['Session'] = sess
            if side:
                accAux.loc[accAux['side'] == 'R', 'SideT'] = 'Not-Trained'
                accAux.loc[accAux['side'] == 'L', 'SideT'] = 'Trained'
                # acc.loc[subID, 'Not-Trained'] = accAux[0]
                # acc.loc[subID, 'Trained'] = accAux[1]
            else:
                accAux.loc[accAux['side'] == 'R', 'SideT'] = 'Trained'
                accAux.loc[accAux['side'] == 'L', 'SideT'] = 'Not-Trained'
                # acc.loc[subID, 'Trained'] = accAux[0]
                # acc.loc[subID, 'Not-Trained'] = accAux[1]
            acc.append(accAux.copy())

    acc = pandas.concat(acc, sort=False)
    acc.reset_index(drop=True, inplace=True)
    acc.to_csv(accPath)
else:
    acc = pandas.read_csv(accPath)

g = sns.catplot(kind='bar', x='Metric', y='values', hue='SideT', data=acc.where(acc['Metric'] != 'Delay').dropna(), ci='sd')
g._legend.remove()

g = sns.catplot(kind='bar', x='Metric', y='values', hue='SideT', ci='sd',
                data=acc.where(np.logical_and(acc['Metric'] != 'Delay', acc['Metric'] != 'Performance')).dropna())
g._legend.remove()


g = sns.catplot(kind='box', x='Metric', y='values', hue='SideT', data=acc.where(acc['Metric'] == 'Delay').dropna(), ci='sd')
g._legend.remove()

g = sns.catplot(kind='bar', x='Metric', y='values', hue='SideT', ci='sd', order=['Sensitivity', 'Precision'],
                data=acc)
g._legend.remove()

parentPath = 'E:/Drive/JAP2254/Lab/SmartShoes/RealTime/Haptic'

allSubsPath = parentPath + '/predictions.pickle'


with open(allSubsPath, 'rb') as f:
    pickledb = pickle.load(f)

allSubs = pickledb['All']
allSubs['acc'] *= 100
# f, ax = plt.subplots(1)

g = sns.catplot(data=allSubs, x='Session', y='acc', kind='bar')

summaryMean = pandas.DataFrame()
summarySd = pandas.DataFrame()
for me in ['Sensitivity', 'Specificity', 'Precision', 'Accuracy']:
    for si in ['Not-Trained', 'Trained']:
        aux = acc.where(acc['Metric']==me).where(acc['SideT']==si)['values']
        summaryMean.loc[si, me] = aux.mean()
        summarySd.loc[si, me] = aux.std()

summaryMean = pandas.DataFrame()
summarySd = pandas.DataFrame()
for si in ['Not-Trained', 'Trained']:
    aux = allSubs.where(allSubs['Session'] == si)['acc']
    summaryMean.loc[si, me] = aux.mean()
    summarySd.loc[si, me] = aux.std()
