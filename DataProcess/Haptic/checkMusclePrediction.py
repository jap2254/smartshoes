import Tools.Utils as tu
import seaborn as sns
import pickle
import pandas
import numpy as np


parentPath = 'Z:/Haptic/muscle/musclesVibration'

allSubsPath = parentPath + '/predictions.pickle'

# with open(allSubsPath, 'rb') as f:
#     pickledb = pickle.load(f)

subs = range(1, 8)

sessions = ['timed']
fmt = '>3c 3L 3c'
footer = [(x).to_bytes(1, 'big') for x in [1, 2, 3]]
header = [(x).to_bytes(1, 'big') for x in [0xA, 0xB, 0xC]]
freq = 50.0
allSubs = pandas.DataFrame(columns=['Error', 'AbsError', 'RMSE'])
c = -1
sessionName = 'Timed'
for s in subs:
    # f, ax = plt.subplots(1)
    subID = 'MS%03d' % s
    # ax.set_title(subID)
    f = parentPath + '/' + subID + '/%s_pred.bin' % sessionName
    pred = tu.readBinaryFile2Pandas(fmt, f, colNames=['TimeComputer', 'ShoeTime', 'Pred'],
                                    header=header, footer=footer)
    pred /= 1000
    # remove sync
    shoeFile = parentPath + '/' + subID + '/' + sessionName
    syncT = []
    for jj, ss in enumerate(['r', 'l']):
        shoes = tu.binaryFile2python(shoeFile + '_%s.bin' % ss)
        syncT.append(shoes[jj].timestamp[np.where(shoes[jj].sync)[0][0]] / 1000.0)
    # print('%s is L? %d' % (subID, (p['Side'] == 'L'.encode()).all()))
    pred[['TimeComputer', 'ShoeTime']] -= syncT[0]
    # remove before 0
    pred = pred.where(pred['ShoeTime'] >= 0).dropna()
    matFile = parentPath + '/' + subID + '/%s.xlsx' % sessionName
    mat = tu.MatObject(fName=matFile, freq=freq)
    mat.makeYpercent()
    m = np.zeros_like(pred['ShoeTime'].values, dtype=np.bool)
    for (st, en) in mat.lap:
        m = np.logical_or(m, np.logical_and(pred['ShoeTime'].values >= st,
                                            pred['ShoeTime'].values <= en))
    # m has the prediction times that should be compared
    pp = pred['Pred'].values[m]
    tp = (pred['ShoeTime'].values[m] * freq).astype(np.int)
    error = mat.binaryFunctions['R'].values[tp] - pp
    error[error > .30] -= .5
    error[error < -.30] += .5
    allSubs.loc[subID, 'Error'] = error.mean()
    allSubs.loc[subID, 'AbsError'] = np.abs(error).mean()
    allSubs.loc[subID, 'RMSE'] = np.sqrt((error**2).mean())
    # how to compare??


    c += 1

allSubs = allSubs.astype(np.float)
allSubs.reset_index(inplace=True)
allSubs.describe()
forPlot = []
for c in allSubs.columns:
    if 'index' in c or 'Abs' in c:
        continue
    aux = allSubs[[c, 'index']].copy()
    aux.rename(columns={c: 'Val'}, inplace=True)
    aux['Type'] = c
    forPlot.append(aux)

forPlot = pandas.concat(forPlot)
forPlot['Val'] *= 100
sns.catplot(data=forPlot, x='Type', y='Val', kind='bar', ci='sd')
