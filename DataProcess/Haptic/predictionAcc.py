import Utils as tu
import NNUtils as nTu
import tensorflow as tf
import pandas
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from tensorflow.python.framework.ops import disable_eager_execution

disable_eager_execution()

parentPath = 'E:/Drive/JAP2254/Lab/SmartShoes/RealTime/Haptic'

allSubsPath = parentPath + '/predictions.pickle'

subs = range(2, 11)

rightSideDominant = [True, True, False, True, False, True, True, False, True]

sessions = ['baseline', 'train', 'post']

freq = 50.0
allSubs = pandas.DataFrame()
pickledb = {}
c = -1
for s, side in zip(subs, rightSideDominant):
    subID = 'Sub%03d' % s
    if side:
        actSide = 'L'
    else:
        actSide = 'R'
    # load model
    modelPath = './Models/Subs/' + subID + '.h5'
    model = tf.keras.models.load_model(modelPath)
    pickledb[subID] = {}
    for sess in sessions:
        c += 1
        matFile = parentPath + '/' + subID + '/' + sess + '.xlsx'
        mat = tu.MatObject(fName=matFile, freq=freq)
        mat2 = tu.matFile2Pandas(matFile)
        mat2 = mat2.where(mat2['isLeft'] == float(side)).dropna(how='all')[['Laps', 'isLeft',
                                                                            'First Contact (sec.)',
                                                                            'Last Contact (sec.)']]
        laps = pandas.DataFrame()
        for j in mat2['Laps'].unique():
            laps.loc[j, 'Start'] = mat2.where(mat2['Laps'] == j).dropna().values[0, 2]
            laps.loc[j, 'End'] = mat2.where(mat2['Laps'] == j).dropna().values[-1, 3]
        shoeFile = parentPath + '/' + subID + '/' + sess + '_' + actSide + '.bin'
        if side:
            [_, shoe] = tu.binaryFile2python(shoeFile)
            gt = mat.yL
        else:
            [shoe, _] = tu.binaryFile2python(shoeFile)
            gt = mat.yR
        shoe.createPandasDataFrame(useSync=True, freq=freq)
        xx = nTu.createbatch(shoe.dataFrame.values, 10)
        tt = shoe.dataFrame.index.values
        tt = np.expand_dims(tt, 1)
        # laps = mat.lap
        m = np.logical_and(tt > laps.values[:, 0], tt < laps.values[:, 1]).any(axis=1)
        tt = np.squeeze(tt[m])
        xx = xx[m]
        pred = np.round(model.predict(xx, batch_size=2000)).astype(np.int)[:, 1]

        # now ground truth
        tMat = mat.t
        m = (tt * freq).astype(np.int)
        # m = np.logical_and(tMat > laps[:, 0], tMat < laps[:, 1]).any(axis=1)
        tMat = tMat[m]
        gt = gt[m]

        minLen = min(gt.shape[0], pred.shape[0])
        allSubs.loc[c, 'acc'] = np.mean(gt[:minLen] == pred[:minLen])
        allSubs.loc[c, 'Sub'] = subID
        allSubs.loc[c, 'Session'] = sess

        pickledb[subID][sess] = {'gtT': tMat.copy(), 'gt': gt.copy(), 'predT': tt.copy(), 'pred': pred.copy()}

sns.catplot(data=allSubs, x='Session', y='acc', kind='bar')

pickledb['All'] = allSubs

with open(allSubsPath, 'wb') as f:
    pickle.dump(pickledb, f)
