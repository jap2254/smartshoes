import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from Tools.Utils import *

parentPath = 'E:/Drive/JAP2254/Lab/VR/CAT/'
outputFolder = 'E:/Drive/JAP2254/Lab/VR/CAT/Matlab/'

st = 1
en = 15

subName = 'CAT'
session = ['M', 'S', 'W', 'W_2']

for j in range(st, en + 1):
    subID = '%s%03d' % (subName, j)
    for s in session:
        fileName = parentPath + subID + '/' + s
        print('Doing %s, %s' % (subID, s))
        try:
            [r, _] = binaryFile2python(fileName + '_R.bin', V=2)
            [_, l] = binaryFile2python(fileName + '_L.bin', V=2)
            sub = ShoeSubject(l, r, j, cutSync=True)
            createDirIfNotExist(outputFolder + subID + '/')
            sub.saveMatlab(outputFolder + subID + '/' + s + '.mat')
        except:
            print('Error in %s' % fileName)
