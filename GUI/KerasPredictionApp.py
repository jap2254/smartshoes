import pandas
import time
import numpy as np
from tkinter import messagebox, simpledialog
import tkinter as tk
import tensorflow as tf
import struct
import Tools.GUItools as guT
import Tools.Utils as tu
from tensorflow.python.framework.ops import disable_eager_execution
import GUI.DeepSoleModulesTensorflow as dsm_tf
import threading
import GUI.DeepSoleModules as dsm


def loss(labels, logits):
    return tf.keras.losses.sparse_categorical_crossentropy(labels, logits, from_logits=True)


class KerasPredictionModel(dsm_tf.KerasPredictionModelProto):
    def __init__(self, rightDataFrame, leftDataFrame, udpSend, **kwargs):
        super(KerasPredictionModel, self).__init__(udpSend, **kwargs)
        self.addDataframes({'R': rightDataFrame, 'L': leftDataFrame})


class FakeKerasModel(KerasPredictionModel):
    def __init__(self, rightShoe, leftShoe, theSend, **kwargs):
        super(FakeKerasModel, self).__init__(rightShoe, leftShoe, theSend, **kwargs)
        self.start = time.time()
        self.predict = False
        self.c = 0

    def __predict(self):
        # with tf.Session() as _:
        now = time.time()
        # print(pred.shape)

        # print(pred.shape)
        t = np.ones(2) * (now - self.start) * 1000
        pred = np.squeeze(self.model(t))
        t = t.astype(np.int)
        msg = struct.pack(self.fmt, t[0], t[1],
                          (pred[0] * 1000).astype(np.int), (pred[1] * 1000).astype(np.int))

        self.binaryFile += msg
        self.prediction.loc[t[0], :] = np.squeeze(pred)
        if self.prediction.shape[0] > self.sizeBuf:
            self.prediction.drop(self.prediction.index[0], inplace=True)
        # self.freqDF.loc[c, 'Time'] = 1 / (time.time() - now)
        self.c += 1
        if self.c > 50:
            self.c = 0

    def startPrediction(self):
        # self.rec1 = threading.Event()
        # self.rec1.set()
        # self.t1 = threading.Thread(target=self.__predict2, args=[self.rec1])
        # self.t1.daemon = True
        # self.t1.start()
        self.model = lambda x: np.round(0.5 * np.sin(np.pi * (x / 1000)) + 0.5)
        self.predict = True
        self.c = 0
        self.start = time.time()

    def updateFrame(self):
        if self.predict:
            self.__predict()


class ThresholdModel(dsm_tf.KerasPredictionModelProto):
    def __init__(self, rightShoe, leftShoe, theSend, tmax, **kwargs):
        super(ThresholdModel, self).__init__(theSend, **kwargs)
        self.addDataframes({'R': rightShoe, 'L': leftShoe})
        self.start = time.time()
        self.predict = True
        self.tmax = tmax
        self.c = 0
        self.model = lambda x: x.mean(axis=0)

    def __predict(self):
        # with tf.Session() as _:
        if self._predictionDataFrames['R'].shape[0] < self.tmax:
            return

        t = self._predictionDataFrames['R'].index.values.astype(np.int)[-1]
        if t == self._lastPredictionTime:
            return
        self._lastPredictionTime = t
        # print(t)
        pres = self._predictionDataFrames['R'][['pToe', 'pBall']].values[-self.tmax:]
        pred = np.squeeze(self.model(pres)).astype(np.int)
        # print(pred)
        # msg = struct.pack(self.fmt, t, t,
        #                   (pred[0] * 1000), (pred[1] * 1000))
        #
        # self.binaryFile += msg
        self.prediction.loc[t, ['L', 'R']] = np.squeeze(pred)
        # print(self.prediction.head(1))
        if self.prediction.shape[0] > self.sizeBuf:
            self.prediction.drop(self.prediction.index[0], inplace=True)
        # self.freqDF.loc[c, 'Time'] = 1 / (time.time() - now)
        self.c += 1
        if self.c > 50:
            self.c = 0

    def startPrediction(self):
        # self.rec1 = threading.Event()
        # self.rec1.set()
        # self.t1 = threading.Thread(target=self.__predict2, args=[self.rec1])
        # self.t1.daemon = True
        # self.t1.start()
        # self.model = lambda x: np.round(0.5 * np.sin(np.pi * (x / 1000)) + 0.5)
        self.predict = True
        self.c = 0
        self.start = time.time()

    # def model(self, x):
    #     # print(x.mean(axis=0))
    #     return x.mean(axis=0)

    def updateFrame(self):
        if self.predict:
            self.__predict()


class ControlFunctionsContainer(object):
    def __init__(self, labels, main):
        # self.RIGHT_IP_ADDRESS = '192.168.0.103'
        # self.LEFT_IP_ADDRESS = '192.168.0.102'
        self.STREAM_IP_ADDRESS = '192.168.0.255'
        self.buttons = None
        self.labels = labels
        self.main = main
        self.theSend = main.theSend
        self.isRecord = True
        self.isStream = True
        self.isPredict = True
        self.fileName = 'file%d' % (int(time.time()))

    class motorControl(tk.Toplevel):
        def __init__(self, parent):
            tk.Toplevel.__init__(self, parent)
            # self.root = tk.Toplevel(self)
            self.vals = None
            fr = tk.Frame(self)
            fr.pack()
            tk.Label(fr, text='R').grid(row=1)
            tk.Label(fr, text='L').grid(row=2)
            self.valR = []
            self.valL = []
            names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
            self.allNames = names
            okCMD = self.register(self.validateVal)
            vld = (okCMD, '%P')
            for i, n in enumerate(names):
                # label
                tk.Label(fr, text=n).grid(row=0, column=i + 1)
                # box 1
                self.valR.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                        column=i + 1)
                self.valR[-1].set('0')
                # box 2
                self.valL.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valL[-1], validate='focusout', validatecommand=vld).grid(row=2,
                                                                                                        column=i + 1)
                self.valL[-1].set('0')
            frBot = tk.Frame(self)
            tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
            tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
            frBot.pack()
            self.protocol("WM_DELETE_WINDOW", self.cancelB)
            self.transient(parent)
            self.grab_set()
            parent.wait_window(self)
            # root.quit()

        def validateVal(self, value):
            try:
                if value:
                    # print(type(value))
                    # print(value)
                    v = int(value)
                    return True
            except ValueError:
                return False

        def okB(self):
            self.vals = np.zeros((len(self.allNames), 2), dtype=np.uint8)
            for i, (r, l) in enumerate(zip(self.valR, self.valL)):
                self.vals[i, :] = np.array([int(r.get()), int(l.get())], dtype=np.uint8)

            self.destroy()

        def cancelB(self):
            self.vals = None
            self.destroy()

    class FreqChange(tk.Toplevel):
        def __init__(self, parent):
            tk.Toplevel.__init__(self, parent)
            # self.root = tk.Toplevel(self)
            self.vals = None
            fr = tk.Frame(self)
            fr.pack()
            self.valR = []
            names = ['Freq']
            self.allNames = names
            okCMD = self.register(self.validateVal)
            vld = (okCMD, '%P')
            for i, n in enumerate(names):
                # label
                tk.Label(fr, text=n).grid(row=0, column=i + 1)
                # box 1
                self.valR.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                        column=i + 1)
                self.valR[-1].set('100')
            self.che = guT.CheckControl(self, '', ['Save params'], ax=None, defaultVal=[1])
            self.che.pack()
            frBot = tk.Frame(self)
            tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
            tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
            frBot.pack()
            self.protocol("WM_DELETE_WINDOW", self.cancelB)
            self.transient(parent)
            self.grab_set()
            parent.wait_window(self)
            # root.quit()

        def validateVal(self, value):
            try:
                if value:
                    # print(type(value))
                    # print(value)
                    v = int(value)
                    return True
            except ValueError:
                return False

        def okB(self):
            self.vals = np.zeros((len(self.allNames) + 1), dtype=np.int8)
            for i, r in enumerate(self.valR):
                self.vals[i] = int(r.get())
            self.vals[-1] = self.che.getAllValues()[0]
            self.destroy()

        def cancelB(self):
            self.vals = None
            self.destroy()

    class assignControl(tk.Toplevel):
        def __init__(self, parent):
            tk.Toplevel.__init__(self, parent)
            # self.root = tk.Toplevel(self)
            self.vals = None
            fr = tk.Frame(self)
            fr.pack()
            tk.Label(fr, text='R').grid(row=1)
            tk.Label(fr, text='L').grid(row=2)
            self.valR = []
            self.valL = []
            names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
            self.allNames = names
            okCMD = self.register(self.validateVal)
            vld = (okCMD, '%P')
            for i, n in enumerate(names):
                # label
                tk.Label(fr, text=n).grid(row=0, column=i + 1)
                # box 1
                self.valR.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                        column=i + 1)
                self.valR[-1].set('0')
                # box 2
                self.valL.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valL[-1], validate='focusout', validatecommand=vld).grid(row=2,
                                                                                                        column=i + 1)
                self.valL[-1].set('0')
            frBot = tk.Frame(self)
            tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
            tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
            frBot.pack()
            self.protocol("WM_DELETE_WINDOW", self.cancelB)
            self.transient(parent)
            self.grab_set()
            parent.wait_window(self)
            # root.quit()

        def validateVal(self, value):
            try:
                if value:
                    # print(type(value))
                    # print(value)
                    v = int(value)
                    return True
            except ValueError:
                return False

        def okB(self):
            self.vals = np.zeros((len(self.allNames), 2), dtype=np.int8)
            for i, (r, l) in enumerate(zip(self.valR, self.valL)):
                self.vals[i, :] = np.array([int(r.get()), int(l.get())], dtype=np.int8)

            self.destroy()

        def cancelB(self):
            self.vals = None
            self.destroy()

    def sendPing(self):
        cmd = guT.createCMD(1)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def changeFreq(self):
        self.mW = self.FreqChange(self.main.root)
        if self.mW.vals is None:
            print('Cancel was pressed')
        else:
            # send motor cmd
            print(self.mW.vals)
            cmd = guT.createCMD(14, te=self.mW.vals)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def reassign(self):
        # TODO send only to 1
        cmd = guT.createCMD(5, [108, 1])
        # cmd = guT.createCMD(14, 10)
        # self.theSend.send(cmd, addr=(self.LEFT_IP_ADDRESS, 12354))
        self.theSend.send(cmd, addr=(self.RIGHT_IP_ADDRESS, 12354))

    def sendSendStart(self):
        n = 2
        self.buttons['Start Recording']["text"] = "Stop recording"
        self.main.leftShoe.resetShoe()
        self.main.rightShoe.resetShoe()
        self.main.leftShoe.binaryFile = bytes(0)
        self.main.rightShoe.binaryFile = bytes(0)
        self.main.startTime[0] = time.time()
        self.buttons['Start Recording'].config(bg='green')
        cmd = guT.createCMD(n)
        # self.main.theRec.startStreamRec()
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def sendSendStop(self):
        n = 3
        cmd = guT.createCMD(n)
        # self.main.theRec.startStreamRec()
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def recordCmd(self):
        if self.isRecord:
            n = 2
            self.buttons['Start Recording']["text"] = "Stop recording"
            self.main.leftShoe.resetShoe()
            self.main.rightShoe.resetShoe()
            self.main.leftShoe.binaryFile = bytes(0)
            self.main.rightShoe.binaryFile = bytes(0)
            self.main.startTime[0] = time.time()
            self.buttons['Start Recording'].config(bg='green')
            cmd = guT.createCMD(n)
            # self.main.theRec.startStreamRec()
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
        else:
            n = 3
            self.buttons['Start Recording']["text"] = "Start recording"
            self.buttons['Start Recording'].config(bg='yellow')
            cmd = guT.createCMD(n)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
            # self.main.theRec.stopStreamRec()
            self.saveDataCmd()
        self.isRecord = not self.isRecord

        self.streamCmd()

    def streamCmd(self):
        if self.isStream:
            n = 6
            # self.streamB["text"] = "Stop streaming"
        else:
            n = 7
            # self.streamB["text"] = "Start streaming"
        self.isStream = not self.isStream
        cmd = guT.createCMD(n)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def reNameCmd(self):
        name = simpledialog.askstring('Name for log file', 'Name')
        if name is None:
            return
        print(name)
        self.fileName = '%s%d' % (name, int(time.time()))
        cmd = guT.createCMD(4, te=name)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def sendMotorCmd(self):
        self.mW = self.motorControl(self.main.root)
        if self.mW.vals is None:
            print('Cancel was pressed')
        else:
            # send motor cmd
            print(self.mW.vals)
            cmd = guT.createCMD(8, te=np.squeeze(self.mW.vals[:, 1]))
            self.theSend.send(cmd, addr=(self.main.console.ipAddress['l'], 12354))
            cmd = guT.createCMD(8, te=np.squeeze(self.mW.vals[:, 0]))
            print(self.main.console.ipAddress['r'])
            self.theSend.send(cmd, addr=(self.main.console.ipAddress['r'], 12354))

    def getPressCmd(self):
        cmd = guT.createCMD(9)
        self.theSend.send(cmd, addr=(self.main.console.ipAddress['l'], 12354))
        time.sleep(1)
        self.theSend.send(cmd, addr=(self.main.console.ipAddress['r'], 12354))

    def readBinaryCmd(self):
        shoes = tu.binaryFile2python()
        fSave = tk.filedialog.asksaveasfile(self.main.root)
        if fSave is None:
            return shoes
        np.save(fSave, shoes)

    def saveDataCmd(self):
        fSave = tk.filedialog.asksaveasfilename()
        if fSave is None:
            return 0
        ss = [self.main.leftShoe, self.main.rightShoe, self.main.model, self.main.postPred]
        for s in ss:
            # self.rightShoe.pressDisplay = self.pressDisp
            # self.leftShoe.plotP = self.lPlot
            s.saveBinaryFile(fSave)

    def enterSafeMode(self):
        cmd = guT.createCMD(10)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def resetShoes(self):
        cmd = guT.createCMD(11)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def reTrainModel(self):
        pass


class RealTimeConsole(guT.ConsoleFrame):
    def __init__(self, parent, nLines=5):
        guT.ConsoleFrame.__init__(self, parent, nLines=nLines, parseCMD=self.parseMSG)
        self.ipAddress = {'IP_BASE': '192.168.0.', 'r': RIGHT_IP, 'l': LEFT_IP}
        self.recordings = {'r': False, 'l': False}
        # self.RIGHT_IP_ADDRESS = '192.168.0.103'
        # self.LEFT_IP_ADDRESS = '192.168.0.102'

    # def set(self, text):
    #     super().set(text)
    #     serchStr = 'Hello from'
    #     if serchStr in text:
    #         newIP = text[11:14]
    #         lab = text[16]
    #         self.ipAddress[lab] = self.ipAddress['IP_BASE'] + newIP

    def parseMSG(self, text):
        self.__changeIP(text)
        self.__checkRecording(text)

    def __changeIP(self, text):
        serchStr = 'Hello from'
        if serchStr in text:
            newIP = text[11:14]
            lab = text[16]
            self.ipAddress[lab] = self.ipAddress['IP_BASE'] + newIP

    def __checkRecording(self, text):
        serchStr = 'Recording'
        if serchStr in text:
            lab = text[-1]
            self.recordings[lab] = 'Started' in text


def wrap(angles):
    return angles
    # return (angles + np.pi) % (2 * np.pi) - np.pi


# class PhaseVibrationFrame(tk.Frame):
#     def __init__(self, parent, udpSend, predictionDF, rightDF, leftDF, **kwargs):
#         # tk.Frame.__init__(parent, **kwargs)
#         super(PhaseVibrationFrame, self).__init__(parent, **kwargs)
#         self.udpSend = udpSend
#         self.predictionDF = predictionDF
#         self.rightDF = rightDF
#         self.leftDF = leftDF
#         self.buttonFrame = tk.Button(self, text='Start Vibration', command=self.setVibration)
#         self.buttonFrame.pack()
#         self.buttonFrame2 = tk.Button(self, text='Test Trigger', command=lambda: self.vibrationTrigger(True, True))
#         self.buttonFrame2.pack()
#         self.vibrate = False
#         self.lFrame = OneSideVibrationControlFrame(self, 'L', 'blue', lambda x: self.sendCommand(LEFT_IP, x))
#         self.lFrame.pack(side=tk.LEFT)
#         self.rFrame = OneSideVibrationControlFrame(self, 'R', 'red', lambda x: self.sendCommand(RIGHT_IP, x))
#         self.rFrame.pack(side=tk.RIGHT)
#         self.binaryFile = bytes(0)
#         self.windowSize = 5
#         self.counter = [0, 0]
#         self.hsDetected = [False, False]
#         self._lastPredictionTime = 0
#
#     def setVibration(self):
#         self.vibrate = not self.vibrate
#         if self.vibrate:
#             self.buttonFrame["text"] = "Stop Vibration"
#             self.buttonFrame.config(bg='green')
#         else:
#             self.buttonFrame["text"] = "Start Vibration"
#             self.buttonFrame.config(bg='lightgray')
#
#     def vibrationTrigger(self, rTrigger=False, lTrigger=False):
#         if rTrigger:
#             if self.rightDF.index.values.shape[0] == 0:
#                 self.rFrame.vibrationTrigger([True, True, True], 0)
#             else:
#                 self.rFrame.vibrationTrigger([True, True, True], self.rightDF.index.values[-1].astype(np.int))
#
#         if lTrigger:
#             if self.leftDF.index.values.shape[0] == 0:
#                 self.rFrame.vibrationTrigger([True, True, True], 0)
#             else:
#                 self.lFrame.vibrationTrigger([True, True, True], self.leftDF.index.values[-1].astype(np.int))
#
#         if self.predictionDF.shape[0] == 0:
#             return
#
#     def saveBinaryFile(self, fileN):
#         for k in [self.rFrame, self.lFrame]:
#             self.binaryFile += k.binaryFile
#         fileN += '_triggers.bin'
#         with open(fileN, 'wb') as f:
#             f.write(self.binaryFile)
#
#     def vibrateAtHS(self):
#         hss = [False, False]
#         if self.predictionDF.shape[0] < 10 or self.predictionDF.index[-1] == self._lastPredictionTime:
#             return
#         # ddf = (self.predictionDF.values[-1, :] - self.predictionDF.values[-2, :]) == -1
#         # ddf2 = (self.predictionDF.values[-1, :] - self.predictionDF.values[-2, :]) == 1
#         # for j in range(2):
#         #     # if HS detected, set as true
#         #     if ddf[j]:
#         #         self.hsDetected[j] = True
#         #     # if TO detected, set as false
#         #     if ddf2[j]:
#         #         self.hsDetected[j] = False
#         #
#         #     # if HS was detected then vibrate
#         #     if self.hsDetected[j] and self.predictionDF.values[-self.windowSize:, j].sum() == 0:
#         #         hss[j] = True
#         for j in range(2):
#             hss[j] = self.predictionDF.values[-self.windowSize:, j].sum() == 0 and self.predictionDF.values[-self.windowSize-1, j] == 1
#
#         self.vibrationTrigger(hss[0], hss[1])
#         self._lastPredictionTime = self.predictionDF.index[-1]
#
#     def vibrationUpdate(self):
#         self.vibrateAtHS()
#         self.lFrame.vibrationUpdate()
#         self.rFrame.vibrationUpdate()
#
#     def sendCommand(self, ipAddress, mVals):
#         # names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
#         vals = np.zeros(7, dtype=np.int8)
#         vals[:3] = mVals
#         cmd = guT.createCMD(8, te=np.squeeze(vals))
#         self.udpSend.send(cmd, addr=(ipAddress, 12354))


class PostPredictionFrame(dsm_tf.PostPredictionFrameProto):
    def __init__(self, master, checkCMD, udpSend, predictionDF, rightDF, leftDF, **kwargs):
        fd = {'Phase Vibration': lambda x: dsm_tf.PhaseVibrationFrame(x, udpSend, predictionDF, rightDF, leftDF,
                                                                      LEFT_IP, RIGHT_IP),
              'Threshold Vibration': lambda x: dsm_tf.ThresholdVibrationFrame(x, udpSend, predictionDF, rightDF, leftDF,
                                                                              LEFT_IP, RIGHT_IP)
              }
        super(PostPredictionFrame, self).__init__(master, checkCMD, udpSend, fd, **kwargs)
        # tk.Frame.__init__(self, parent, **kwargs)


# class PostPredictionFrame2(tk.Frame):
#     def __init__(self, parent, checkCMD, udpSend, predictionDF, rightDF, leftDF, **kwargs):
#         tk.Frame.__init__(self, parent, **kwargs)
#         self.udpSend = udpSend
#         self.rightDF = rightDF
#         self.leftDF = leftDF
#         self.predictionDF = predictionDF
#         topFame = tk.Frame(self)
#         topFame.pack()
#         tk.Label(topFame, text='Prediction processing', background='black', foreground='white').pack()
#         self.cmd = checkCMD
#         self.sendVar = tk.IntVar(self)
#         self.sendVar.set(1)
#         tk.Checkbutton(topFame, text='Send prediction', variable=self.sendVar, command=self.exeCMD).pack()
#         self.postChoice = tk.StringVar(self)
#         self.choices = {'None', 'Phase Vibration'}
#         self.postChoice.set('None')
#         self.popUp = tk.OptionMenu(topFame, self.postChoice, *self.choices)
#         self.popUp.pack()
#         self.postChoice.trace('w', self.dropDownChanged)
#         bottomFrame = tk.Frame(self)
#         bottomFrame.pack()
#         self.emptyFrame = tk.Frame(bottomFrame)
#         self.emptyFrame.pack()
#         self.currentFrame = self.emptyFrame
#         self.phaseFrame = PhaseVibrationFrame(bottomFrame, udpSend, predictionDF, rightDF, leftDF)
#
#     def dropDownChanged(self, *args):
#         val = self.postChoice.get()
#         self.currentFrame.pack_forget()
#         if val in 'Phase Vibration':
#             print(val)
#             self.phaseFrame.pack()
#             self.currentFrame = self.phaseFrame
#         else:
#             self.emptyFrame.pack()
#             self.currentFrame = self.emptyFrame
#
#     def exeCMD(self):
#         self.cmd(self.sendVar.get())
#
#     def updateFunctions(self):
#         self.phaseFrame.vibrationUpdate()
#
#     def saveBinaryFile(self, fileN):
#         self.currentFrame.saveBinaryFile(fileN)


class RealTimeApp(tk.Tk):
    def __init__(self, useThreshold, tmax):
        tk.Tk.__init__(self)
        self.title('DeepSole System, Real Time')
        self.iconbitmap(r'./GUI/deepsole.ico')
        # objects
        self.theSend = tu.UDPsend()
        self.refreshTime = 18  # ms
        self.visualizeTime = 8  # s
        self.buffer = 500
        self.startTime = [time.time()]
        self.rightShoe = tu.smartShoe(False, keepAll=False, sizeBuf=self.buffer)
        self.leftShoe = tu.smartShoe(True, keepAll=False, sizeBuf=self.buffer)
        self.root = tk.Frame(self)
        self.root.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
        self.multiplier = 1000

        bottomFrame = tk.Frame(self.root)
        self.console = RealTimeConsole(bottomFrame)
        self.theRec = tu.UDPreceive(self.leftShoe, self.rightShoe, self.console, ver=2, startTime=self.startTime)

        # self.model = KerasPredictionModel(self.rightShoe, self.leftShoe, self.theSend, sendUDP=True)
        if useThreshold:
            tk.Label(self.root, text='Using Threshold', bg='red').pack()
            self.model = ThresholdModel(self.rightShoe.dataFrame, self.leftShoe.dataFrame, self.theSend, tmax)
            # self.startPrediction()
        else:
            tk.Label(self.root, text='Using Gait Cycle Percentage', bg='red').pack()
            self.model = KerasPredictionModel(self.rightShoe.dataFrame, self.leftShoe.dataFrame, self.theSend,
                                              sendUDP=True, startTime=self.startTime, udpMultiplier=2)
        self.batchSize = 10
        # GUI
        titles = ['Press', 'Acc', 'Gyro', 'Euler']
        names = [self.rightShoe.names[3 * j:3 * j + 3] for j in range(4)]
        # names = [['Toe ', 'Ball ', 'Heel'], ['ax', 'ay', 'az'], ['gx', 'gy', 'gz'], ['ro', 'pi', 'ya']]
        plotColor = [['fuchsia', 'orange', 'purple'],
                     ['Chartreuse', 'darkgreen', 'chocolate'],
                     ['coral', 'brown', 'crimson'],
                     ['blue', 'teal', 'cyan']]
        # self.yPredR = self.realTimeModel.yR
        # self.yCorrR = self.realTimeModel.yRc
        # self.yPredL = self.realTimeModel.yL
        # self.yCorrL = self.realTimeModel.yLc
        self.allValsR = [j for j in self.rightShoe.all]
        self.allValsL = [j for j in self.leftShoe.all]
        self.timestampR = [self.rightShoe.timestamp for _ in self.rightShoe.all]
        self.timestampL = [self.leftShoe.timestamp for _ in self.leftShoe.all]
        self.scalePlots = True
        topFrame = tk.Frame(self.root)
        topFrame.pack(fill=tk.BOTH, expand=tk.YES)
        topFrameL = tk.Frame(topFrame)
        topFrameR = tk.Frame(topFrame)
        topFrameR.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.BOTH)
        topFrameL.pack(side=tk.LEFT, expand=tk.YES, fill=tk.BOTH)
        self.rPlot = guT.PlotPanelPandas(topFrameL, 'R', titles, names, plotColor, self.timestampR,
                                         self.rightShoe.dataFrame, color='red', number2Plot=500, showTime=True,
                                         useScale=self.scalePlots, multiplier=10)
        # self.rPlot = guT.PlotPanel(topFrameL, 'R', titles, names, plotColor, self.timestampR, self.allValsR, color='red',
        #                        number2Plot=500, showTime=True, useScale=self.scalePlots)
        self.rPlot.pack(fill=tk.BOTH, expand=tk.YES)
        # self.lPlot = guT.PlotPanel(topFrameL, 'L', titles, names, plotColor, self.timestampL, self.allValsL, color='blue',
        #                        number2Plot=500, showTime=True, useScale=self.scalePlots)
        self.lPlot = guT.PlotPanelPandas(topFrameL, 'L', titles, names, plotColor, self.timestampL,
                                         self.leftShoe.dataFrame, color='blue', number2Plot=500, showTime=True,
                                         useScale=self.scalePlots, multiplier=10)
        self.lPlot.pack(fill=tk.BOTH, expand=tk.YES)
        self.predPlot = guT.PlotPanelPandas(topFrameR, 'pred', ['pred'], [['R', 'L']], [['fuchsia', 'orange']],
                                            self.model.prediction.index,
                                            self.model.prediction, color='green', number2Plot=500, showTime=True,
                                            useScale=False, multiplier=10)
        if useThreshold:
            self.predPlot.pack(fill=tk.BOTH, expand=tk.YES)
        self.filesFrame = tk.Frame(topFrameL)
        # 3d plots of orientation
        # self.plot3DPanel = guT.PlotPanel3DPandas(topFrameR, 'Side', ['Left', 'Right'], [['Plot'], ['Plot']],
        #                                    [['red'], ['blue']],
        #                                [self.leftShoe.dataFrame,
        #                                 self.rightShoe.dataFrame], figsize=(5, 5))
        # self.plot3DPanel.pack(fill=tk.BOTH, expand=tk.YES)
        self.shoeParams = None
        self.subID = 0
        self.freq = 50.0
        fileNamesLabel = ['Model File']
        buttonCmds = [self.loadModel]
        browseTi = ['Select Model file']
        browseTypes = [(("Keras files", "*.h5"), ("all files", "*.*"))]
        self.inputwB = []
        for lab, bCMD, titl, typ in zip(fileNamesLabel, buttonCmds, browseTi, browseTypes):
            self.inputwB.append(guT.InputWithButton(self.filesFrame, lab, 'Load', bCMD, titl, typ))
            self.inputwB[-1].pack()
        self.filesFrame.pack()
        labels = [['Ping', 'Change Freq'],
                  ['Start Recording', 'Change Name'],
                  ['Send Start Rec', 'Send Stop Rec'],
                  ['Motor CMD', 'Start Prediction'],
                  ['Save file', 'Retrain Model'],
                  ['Flash FW', 'Reset Shoes']]
        self.theCom = ControlFunctionsContainer(labels, self)

        bottomFrame.pack(fill=tk.BOTH, expand=tk.YES)
        # bottomLeftFrame = tk.Frame(self.root).pack(side=tk.LEFT)
        # bottomRightFrame = tk.Frame(self.root).pack(side=tk.RIGHT)
        buttonsCMDs = [[self.theCom.sendPing, self.theCom.changeFreq],
                       [self.theCom.recordCmd, self.theCom.reNameCmd],
                       [self.theCom.sendSendStart, self.theCom.sendSendStop],
                       [self.theCom.sendMotorCmd, self.startPrediction],
                       [self.theCom.saveDataCmd, self.theCom.reTrainModel],
                       [self.theCom.enterSafeMode, self.theCom.resetShoes]]
        self.control = guT.ButtonPanel(bottomFrame, labels, buttonsCMDs, width=25)
        self.theCom.buttons = self.control.buttons
        self.control.grid(row=0, column=0)
        # self.pressDisp = guT.ShoePressDisp(topFrameR)
        # # self.pressDisp.grid(row=0, column=1)
        # self.pressDisp.pack()

        self.console.grid(row=0, column=2)
        self.oldTimeR = 0
        self.oldTimeL = 0

        chCMD = lambda x: self.model.changeSendOption(x == 1)
        self.postPred = PostPredictionFrame(self, chCMD, self.theSend, self.model.prediction, self.rightShoe.dataFrame,
                                            self.leftShoe.dataFrame)
        self.postPred.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)
        self._plotCounter = 1

        # self.ctrlP.pack(side=tk.LEFT)
        # self.bottomLeft = tk.Frame(self.bottom)
        # self.bottomLeft.pack(side=tk.LEFT)
        # self.now = time.time()

    def updatePlots(self, pl=4):
        fn = lambda x: wrap(x / 8000)

        if pl == 1 or pl == 4:
            self.rPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=[['EUy', 'EUz', 'EUx'],
                                                                                             [fn]])
            self.lPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=[['EUy', 'EUz', 'EUx'],
                                                                                             [fn]], justTime=True)
            self.predPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=None, justTime=True)
        elif pl == 2 or pl == 4:
            self.lPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=[['EUy', 'EUz', 'EUx'],
                                                                                             [fn]])
            self.rPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=[['EUy', 'EUz', 'EUx'],
                                                                                             [fn]], justTime=True)
            self.predPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=None, justTime=True)
        elif pl == 3 or pl == 4:
            self.predPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=None)
            self.lPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=[['EUy', 'EUz', 'EUx'],
                                                                                             [fn]], justTime=True)
            self.rPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=[['EUy', 'EUz', 'EUx'],
                                                                                             [fn]], justTime=True)
        # self.rPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0)
        # self.lPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0)
        # self.plot3DPanel.plotControlFromChecks()
        # for s in [self.rightShoe, self.leftShoe]:
        #     self.pressDisp.updatePlot(toe=s.getPressVal(s.pToe),
        #                               ball=s.getPressVal(s.pBall),
        #                               heel=s.getPressVal(s.pHeel), axLeft=s.isLeft)
        # isL = False
        # for pPlot, isL in zip([self.rPlot, self.lPlot], [False, True]):
        #     if pPlot.maxValsAux is None:
        #         continue
        #     windowVals = pPlot.all.iloc[-2:, :].copy()
        #     divAux = pPlot.maxValsAux - pPlot.minValsAux
        #     divAux[divAux == 0] = 1
        #     windowVals = (windowVals - pPlot.minValsAux) / (divAux)
        #     # 'pToe', 'pBall', 'pHeel'
        #     self.pressDisp.updatePlot(toe=np.clip(windowVals['pToe'].values[-1], 0, 1),
        #                               ball=np.clip(windowVals['pBall'].values[-1], 0, 1),
        #                               heel=np.clip(windowVals['pHeel'].values, 0, 1)
        #                               [-1], axLeft=isL)

        # if not self.theCom.isRecord and (len(self.rightShoe.timestamp) > 100 or len(self.leftShoe.timestamp) > 100):
        #
        #     if len(self.rightShoe.timestamp) is self.oldTimeR:
        #         rm = 0.0
        #     else:
        #         rm = 1000.0 / np.diff(np.array(self.rightShoe.timestamp[self.oldTimeR:])).mean()
        #     if len(self.leftShoe.timestamp) is self.oldTimeL:
        #         lm = 0.0
        #     else:
        #         lm = 1000.0 / np.diff(np.array(self.leftShoe.timestamp[self.oldTimeL:])).mean()
        #     self.oldTimeR = len(self.rightShoe.timestamp)
        #     self.oldTimeL = len(self.leftShoe.timestamp)
        #     self.theCom.buttons['Start Recording']["text"] = "[%.1fHz]Stop recording[%.1fHz]" % (lm, rm)
        if not self.theCom.isRecord:
            self.theCom.buttons['Start Recording']["text"] = "[%.1fHz]Stop recording[%.1fHz]" % (self.lPlot.dt,
                                                                                                 self.rPlot.dt)

    def exportCMD(self, exportFlag=True):
        if self.yPred is None:
            e = self.predictCMD()
            if e:
                return True
        yRshoe, yLshoe = self.yPred
        if self.mat is None:
            laps = None
        else:
            laps = self.mat.lap
        self.shoeParams = tu.getAllTemporalParamsPandas(yRshoe, yLshoe, laps=laps, freq=self.freq, subID=self.subID,
                                                        dt=0)
        root = tk.Tk()
        #
        # fileN = tk.filedialog.askopenfilename(parent=root, title='Select Shoe binary file',
        #                                       filetypes=(("Binary files", "*.bin"), ("all files", "*.*")))
        filePath = tk.filedialog.asksaveasfilename(parent=root, title='Save Paramaters as',
                                                   filetypes=(
                                                       ("Excel files", ("*.xls", "*.xlsx")), ("all files", "*.*")))
        root.withdraw()
        if filePath is None:
            return
        writer = pandas.ExcelWriter(filePath, engine='xlsxwriter')
        self.shoeParams.to_excel(writer)
        writer.save()
        writer.close()

    def exportRaw(self):
        if self.leftShoe is None or self.rightShoe is None:
            # warnings.warn('Shoe object cannot be empty, please load file')
            messagebox.showinfo('Cannot load file', 'Shoe object cannot be empty, please load file')
        sub = tu.ShoeSubject(self.leftShoe, self.rightShoe, self.subID, matInfo=self.mat)
        labels = ['Save Pickle', 'Save Matlab', 'Save Excel']
        buttonsCMDs = [sub.savePickle, sub.saveMatlab, sub.saveExcel]
        root = tk.Tk()
        control = guT.ButtonPanel(root, labels, buttonsCMDs)
        control.pack()

    def loopFunctions(self):
        now = time.time()
        self.theRec.parseQueue()
        # self.theRec.receiveData()
        self.model.updateFrame()

        # threading.Thread(target=self._GUIupdate, daemon=True).start()
        self._GUIupdate(self._plotCounter)
        self._plotCounter += 1
        if self._plotCounter > 3:
            self._plotCounter = 1
        usedTime = int((time.time() - now) * 1000)
        # print(usedTime)
        rT = self.refreshTime - usedTime
        if rT < 1:
            print('Finished late, in %f' % rT)
            rT = 1
        self.after(rT, self.loopFunctions)

    def _GUIupdate(self, pl=4):
        self.updatePlots(pl)
        self.postPred.updateFunctions()

    def startApp(self):
        self.startReceive()
        self.loopFunctions()
        self.protocol("WM_DELETE_WINDOW", self.stopApp)
        self.mainloop()

    def stopApp(self):
        # self.theRec.stopThread()
        self.destroy()

    def startReceive(self):
        self.theRec.startThread()

    def loadModel(self, modelPath):
        class ModelWindow(tk.Toplevel):
            def __init__(self, parent):
                tk.Toplevel.__init__(self, parent)
                # self.root = tk.Toplevel(self)
                self.vals = None
                fr = tk.Frame(self)
                fr.pack()
                self.valR = []
                names = ['Freq', 'Time Size']
                defVals = ['50', '25']
                self.allNames = names
                okCMD = self.register(self.validateVal)
                vld = (okCMD, '%P')
                for i, (n, v) in enumerate(zip(names, defVals)):
                    # label
                    tk.Label(fr, text=n).grid(row=0, column=i + 1)
                    # box 1
                    self.valR.append(tk.StringVar())
                    tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                            column=i + 1)
                    self.valR[-1].set(v)
                self.che = guT.CheckControl(self, '', ['Start Predicting', 'Do Argmax'], ax=None, defaultVal=[1, 0])
                self.che.pack()
                frBot = tk.Frame(self)
                tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
                tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
                frBot.pack()
                self.protocol("WM_DELETE_WINDOW", self.cancelB)
                self.transient(parent)
                self.grab_set()
                parent.wait_window(self)

            def validateVal(self, value):
                try:
                    if value:
                        # print(type(value))
                        # print(value)
                        v = int(value)
                        return True
                except ValueError:
                    return False

            def okB(self):
                self.vals = np.zeros((len(self.allNames) + 2), dtype=np.int8)
                for i, r in enumerate(self.valR):
                    self.vals[i] = int(r.get())
                self.vals[-2:] = self.che.getAllValues()[:2]
                self.destroy()

            def cancelB(self):
                self.vals = None
                self.destroy()

        params = ModelWindow(self)
        if params.vals is None:
            print('Cancel was pressed')
            return

        print(params.vals)
        print(modelPath)
        self.model.loadModel(modelPath, params.vals[0], params.vals[1], params.vals[3])
        if params.vals[2] == 1:
            self.startPrediction()

    def startPrediction(self):
        if self.model.model is None:
            return
        if self.model.predict:
            self.predPlot.pack_forget()
            self.model.stopPrediction()
            self.control.buttons['Start Prediction']["text"] = "Start Prediction"
        else:
            self.predPlot.pack(fill=tk.BOTH, expand=tk.YES)
            self.model.startPrediction()
            self.control.buttons['Start Prediction']["text"] = "Stop Prediction"


if __name__ == '__main__':
    config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.8))
    config.gpu_options.allow_growth = True
    session = tf.compat.v1.Session(config=config)
    tf.compat.v1.keras.backend.set_session(session)

    disable_eager_execution()

    USE_THRESHOLD = True
    CLIENT_IP = '192.168.0.255'
    LEFT_IP = '192.168.0.102'
    if USE_THRESHOLD:
        RIGHT_IP = '192.168.0.255'
    else:
        RIGHT_IP = '192.168.0.103'
    CLIENT_PORT = 24681
    FREQ = 50.0
    TMAX = 10
    app = RealTimeApp(USE_THRESHOLD, TMAX)
    # app.startReceive()
    app.startApp()
