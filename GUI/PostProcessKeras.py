import os
import sys
from tkinter import messagebox

from Legacy.NeuralNetworks.NNutils import runPredictions, ShoePredictionModel
from Utils import compareTemporalPandas

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from Tools.GUItools import *
from Tools.Utils import *


class PredictOptions(tk.Toplevel):
    def __init__(self, parent):
        tk.Toplevel.__init__(self, parent)
        # self.root = tk.Toplevel(self)
        self.vals = None
        fr = tk.Frame(self)
        fr.pack()
        # batchSize = 500, nSams = 100, samplesBack = 100,
        # verbose = False, sizeOld = 99, justRun = False, useOld = False, flipTime = False
        self.varNames = ['BatchSize', 'nSamples', 'sizeOld']
        stringVals = ['500', '100', '99']
        self.strVals = StringInputs(fr, self.varNames, defaultVals=stringVals)
        self.strVals.pack(side=tk.LEFT)
        self.chNames = ['JustRun', 'useOld', 'FlipTime']
        chVals = [1, 0, 0]
        self.chkVals = CheckControl(fr, '', self.chNames, ax=None, defaultVal=chVals)
        self.chkVals.pack(side=tk.RIGHT)
        frBot = tk.Frame(self)
        tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
        tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
        frBot.pack()
        self.protocol("WM_DELETE_WINDOW", self.cancelB)
        self.transient(parent)
        self.grab_set()
        parent.wait_window(self)
        # root.quit()

    def okB(self):
        self.vals = {}
        strV = self.strVals.readValues()
        for n, v in zip(self.varNames, strV):
            self.vals[n] = v
        # self.vals['BatchSize'] = strV[0]
        # self.vals['nSamples'] = strV[1]
        # self.vals['sizeOld'] = strV[2]
        chV = self.chkVals.getAllValues()
        for n, v in zip(self.chNames, chV):
            self.vals[n] = v
        # self.vals['JustRun'] = chV[0] == 1
        # self.vals['useOld'] = chV[0] == 1
        # self.vals['FlipTime'] = chV[0] == 1
        self.destroy()

    def cancelB(self):
        self.vals = None
        self.destroy()


# class ModelOptions(tk.Toplevel):
#     def __init__(self, parent):
#         tk.Toplevel.__init__(self, parent)
#         # self.root = tk.Toplevel(self)
#         self.vals = None
#         fr = tk.Frame(self)
#         fr.pack()
#         # batchSize = 500, nSams = 100, samplesBack = 100,
#         # verbose = False, sizeOld = 99, justRun = False, useOld = False, flipTime = False
#         # outputVarName = "output_node2:0", V = 2,
#         # outputVarName2 = "yPast_exp:0"
#         self.varNames = ['outputVarName', 'outputVarName2']
#         stringVals = ['output_node2:0', 'yPast_exp:0']
#         self.strVals = StringInputs(fr, self.varNames, defaultVals=stringVals, dtype=np.str, useInt=False)
#         self.strVals.pack()
#         frBot = tk.Frame(self)
#         tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
#         tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
#         frBot.pack()
#         self.protocol("WM_DELETE_WINDOW", self.cancelB)
#         self.transient(parent)
#         self.grab_set()
#         parent.wait_window(self)
#         # root.quit()
#
#     def okB(self):
#         self.vals = {}
#         strV = self.strVals.readValues()
#         for n, v in zip(self.varNames, strV):
#             self.vals[n] = v
#
#         self.destroy()
#
#     def cancelB(self):
#         self.vals = None
#         self.destroy()
#

class processApp(object):
    def __init__(self):
        self.root = tk.Tk()
        self.root.title('DeepSole System, Post-Processing')
        self.root.iconbitmap(r'./GUI/deepsole.ico')
        titles = ['Press', 'Acc', 'Gyro', 'Euler', 'Phase']
        names = [['Toe ', 'Ball ', 'Heel'], ['ax', 'ay', 'az'], ['gx', 'gy', 'gz'], ['ro', 'pi', 'ya'],
                 ['True', 'Predicted']]
        plotColor = [['fuchsia', 'orange', 'purple'],
                     ['Chartreuse', 'darkgreen', 'chocolate'],
                     ['coral', 'brown', 'crimson'],
                     ['blue', 'teal', 'cyan'],
                     ['gray', 'red']]
        self.scalePlots = True
        self.timestampR = [[] for _ in range(sum((len(it) for it in names)))]
        self.timestampL = [[] for _ in range(sum((len(it) for it in names)))]
        self.allValsR = [[] for _ in range(sum((len(it) for it in names)))]
        self.allValsL = [[] for _ in range(sum((len(it) for it in names)))]
        self.rPlot = PlotPanel(self.root, 'R', titles, names, plotColor, self.timestampR, self.allValsR, color='red',
                               number2Plot=0, showTime=False, useScale=self.scalePlots)
        self.rPlot.pack(fill=tk.BOTH, expand=tk.YES)
        self.lPlot = PlotPanel(self.root, 'L', titles, names, plotColor, self.timestampL, self.allValsL, color='blue',
                               number2Plot=0, shareAxis=self.rPlot.axis, showTime=False, useScale=self.scalePlots)
        self.lPlot.pack(fill=tk.BOTH, expand=tk.YES)
        self.filesFrame = tk.Frame(self.root)
        self.rightShoe = None
        self.leftShoe = None
        self.model = None
        self.mat = None
        self.shoeParams = None
        self.yPred = None
        self.subID = 0
        self.matParams = None
        self.freq = 100.0

        fileNamesLabel = ['Shoe R    ', 'Shoe L   ', 'Mat File  ', 'Model File']
        buttonCmds = [self.loadRightShoe, self.loadLeftShoe, self.loadMatFile, self.loadModel]
        browseTi = ['Select Right Shoe binary file', 'Select Left Shoe binary file', 'Select Mat file',
                    'Select Model file']
        browseTypes = [(("Binary files", "*.bin"), ("all files", "*.*")),
                       (("Binary files", "*.bin"), ("all files", "*.*")),
                       (("Excel files", ("*.xls", "*.xlsx")), ("all files", "*.*")),
                       (("PB files", "*.pb"), ("all files", "*.*"))]
        inputwB = []
        for lab, bCMD, titl, typ in zip(fileNamesLabel, buttonCmds, browseTi, browseTypes):
            inputwB.append(InputWithButton(self.filesFrame, lab, 'Load', bCMD, titl, typ))
            inputwB[-1].pack()
        self.filesFrame.pack()
        labels = [['Flip shoes', 'Predict', 'Export Gait Excel', 'Plot Summary', 'Export raw']]
        buttonsCMDs = [[self.flipShoes, self.predictCMD, self.exportCMD, self.plotSumCmd, self.exportRaw]]
        self.control = ButtonPanel(self.root, labels, buttonsCMDs)
        self.control.pack()
        # self.ctrlP.pack(side=tk.LEFT)
        # self.bottomLeft = tk.Frame(self.bottom)
        # self.bottomLeft.pack(side=tk.LEFT)
        # self.now = time.time()

    def _updateList(self):
        self.allValsR = self.rPlot.all
        self.allValsL = self.lPlot.all

    def updatePlots(self):
        self.rPlot.plotControlFromChecks()
        self.lPlot.plotControlFromChecks()

    def flipShoes(self):
        if self.leftShoe is None and self.rightShoe is None:
            messagebox.showinfo('Cannot load file', 'Shoe object cannot be empty, please load file')
            return

        if self.rightShoe is not None:
            self.rightShoe.isLeft = True
            self.rightShoe.makeBinaryFileAgain()
        if self.leftShoe is not None:
            self.leftShoe.isLeft = False
            self.leftShoe.makeBinaryFileAgain()
        root = tk.Tk()
        fName = tk.filedialog.asksaveasfilename(parent=root, title='Template Name')
        root.withdraw()
        if fName is None:
            return
        if self.rightShoe is not None:
            self.rightShoe.saveBinaryFile(fName)
        if self.leftShoe is not None:
            self.leftShoe.saveBinaryFile(fName)

    def predictCMD(self):
        if self.model is None:
            self.loadModel(None)
            if self.model is 0:
                # warnings.warn('Model is not loaded, cannot continue')
                messagebox.showerror('Cannot predict', 'Model is not loaded, cannot continue')
                return True
        b = self.control.buttons[1]
        # b.config(bg='red')
        feat = [0, 1, 2, 3, 4, 5, 9, 10, 11]
        tR, shoeR = self.rightShoe.makeMatrix(freq=self.freq)  # filter=True)
        tL, shoeL = self.leftShoe.makeMatrix(freq=self.freq)  # filter=True)
        shoeR = shoeR[:, feat]
        shoeL = shoeL[:, feat]
        if tR.size > tL.size:
            tS = tL
            shoeR = shoeR[:tS.size, :]
        else:
            tS = tR
            shoeL = shoeL[:tS.size, :]
        predParams = PredictOptions(self.root)
        if predParams.vals is None:
            return

        yRshoe, yLshoe = runPredictions(self.model, tS, shoeR, shoeL, batchSize=predParams.vals['BatchSize'],
                                        nSams=predParams.vals['nSamples'], samplesBack=predParams.vals['nSamples'],
                                        verbose=False, sizeOld=predParams.vals['sizeOld'],
                                        justRun=predParams.vals['JustRun'], useOld=predParams.vals['useOld'],
                                        flipTime=predParams.vals['FlipTime'])
        # self.lPlot.all.append(yRshoe)
        # self.rPlot.all.append(yLshoe)
        # self.rPlot.timestamp.append(tS)
        # self.lPlot.timestamp.append(tS)
        self.allValsR[13][:] = yRshoe
        self.allValsL[13][:] = yLshoe
        self.timestampR[13][:] = tS
        self.timestampL[13][:] = tS
        # self._updateList()
        b.config(bg='green')
        self.yPred = [yRshoe, yLshoe]
        self.updatePlots()

    def exportCMD(self, exportFlag=True):
        if self.yPred is None:
            e = self.predictCMD()
            if e:
                return True
        yRshoe, yLshoe = self.yPred
        if self.mat is None:
            laps = None
        else:
            laps = self.mat.lap
        self.shoeParams = getAllTemporalParamsPandas(yRshoe, yLshoe, laps=laps, freq=self.freq, subID=self.subID, dt=0)
        root = tk.Tk()
        #
        # fileN = tk.filedialog.askopenfilename(parent=root, title='Select Shoe binary file',
        #                                       filetypes=(("Binary files", "*.bin"), ("all files", "*.*")))
        filePath = tk.filedialog.asksaveasfilename(parent=root, title='Save Paramaters as',
                                                   filetypes=(
                                                       ("Excel files", ("*.xls", "*.xlsx")), ("all files", "*.*")))
        root.withdraw()
        if filePath is None:
            return
        writer = pandas.ExcelWriter(filePath, engine='xlsxwriter')
        self.shoeParams.to_excel(writer)
        writer.save()
        writer.close()

    def plotSumCmd(self):
        root = tk.Tk()
        #
        # fileN = tk.filedialog.askopenfilename(parent=root, title='Select Shoe binary file',
        #                                       filetypes=(("Binary files", "*.bin"), ("all files", "*.*")))
        paImg = tk.filedialog.askdirectory(parent=root, title='Directory to save images')
        root.withdraw()
        if paImg is None:
            return 0
        if self.shoeParams is None:
            e = self.exportCMD(exportFlag=False)
            if e:
                return True
        if self.matParams is None:
            self.loadMatFile(None)
        pars = compareTemporalPandas(self.matParams, self.shoeParams, verbose=False, savePath=paImg, name=self.subID)

    def exportRaw(self):
        if self.leftShoe is None or self.rightShoe is None:
            messagebox.showinfo('Cannot load file', 'Shoe object cannot be empty, please load file')

        sub = ShoeSubject(self.leftShoe, self.rightShoe, self.subID, matInfo=self.mat, cutSync=True)
        labels = ['Save Pickle', 'Save Matlab', 'Save Excel']
        buttonsCMDs = [sub.savePickle, sub.saveMatlab, sub.saveExcel]
        root = tk.Tk()
        control = ButtonPanel(root, [labels], [buttonsCMDs])
        control.pack()

    def loadRightShoe(self, fN):
        [self.rightShoe, _] = binaryFile2python(fN, V=2)
        self.rightShoe.dataFrame = None
        # t, m = self.rightShoe.makeMatrix(freq=self.freq)
        self.rightShoe.createPandasDataFrame(freq=self.freq, useSync=True)
        m = self.rightShoe.dataFrame.values
        t = self.rightShoe.dataFrame.index.values * 1000.0
        for j in range(m.shape[1]):
            self.allValsR[j][:] = m[:, j]
            self.timestampR[j][:] = t
        self.updatePlots()
        # self.rPlot.all = [m[:, j] for j in range(m.shape[1])]
        # self.rPlot.timestamp = [t for _ in range(m.shape[1])]
        # self.rightShoe.isLeft = True
        # self.rightShoe.makeBinaryFileAgain()
        # self._updateList()

    def loadLeftShoe(self, fN):
        [_, self.leftShoe] = binaryFile2python(fN, V=2)
        self.leftShoe.dataFrame = None
        # t, m = self.leftShoe.makeMatrix(freq=self.freq)
        self.leftShoe.createPandasDataFrame(freq=self.freq, useSync=True)
        m = self.leftShoe.dataFrame.values
        t = self.leftShoe.dataFrame.index.values * 1000.0
        for j in range(m.shape[1]):
            self.allValsL[j][:] = m[:, j]
            self.timestampL[j][:] = t
        self.updatePlots()
        # self.lPlot.all = [m[:, j] for j in range(m.shape[1])]
        # self.lPlot.timestamp = [t for _ in range(m.shape[1])]
        # self._updateList()

    def loadMatFile(self, fN):
        self.mat = MatObject(fN, newMat=False, freq=self.freq)
        self.allValsR[12][:] = self.mat.yR
        self.allValsL[12][:] = self.mat.yL
        self.timestampR[12][:] = self.mat.t * 1000.0
        self.timestampL[12][:] = self.mat.t * 1000.0
        self.updatePlots()
        # self.rPlot.all.append(self.mat.yR)
        # self.lPlot.all.append(self.mat.yL)
        # self.rPlot.timestamp.append(self.mat.t)
        # self.lPlot.timestamp.append(self.mat.t)
        # self._updateList()
        self.matParams = getAllTemporalParamsPandas(self.mat.yR, self.mat.yL, laps=self.mat.lap, freq=self.freq,
                                                    subID=self.subID)

    # def loadModel(self, fN):
    #     params = ModelOptions(self.root)
    #     if params.vals is None:
    #         return
    #
    #     # self.model = ShoePredictionModel(fN, isInteractive=True, outputVarName=params.vals['outputVarName'], V=2,
    #     #                                  outputVarName2=params.vals['outputVarName2'])
    #     self.model = ShoePredictionModel(fN, isInteractive=True,outputVarName = "output_node2:0", V=2,
    #     outputVarName2 = "yPast_exp:0")

    def loadModel(self, fN):
        params = ModelOptions(self.root)
        if params.vals is None:
            return
        if self.model is not None:
            self.model.closeSession()
        # ['outputVarName', 'outputVarName2', 'Time Size', 'Batch Size']
        inSize = int(params.vals['Time Size'])
        self.batchSize = int(params.vals['Batch Size'])
        # features = [0, 1, 2, 3, 4, 5, 9, 10, 11]
        features = params.vals['features'][0]
        print(features)
        inVar = params.vals['inputVarName']
        self.model = ShoePredictionModel(fN, isInteractive=True, inputVarName=inVar,
                                         outputVarName=params.vals['outputVarName'], V=2,
                                         outputVarName2=params.vals['outputVarName2'], timeSize=inSize,
                                         features=features)
        testX = np.zeros((self.batchSize, inSize * features.size), dtype=np.float32)
        try:
            self.model.runX(testX)
        except ValueError as e:
            self.inputwB[0].changeButtonColor('Error')
            messagebox.showerror('Error loading model, check size of input', e)
        except TypeError as e:
            self.inputwB[0].changeButtonColor('Error')
            messagebox.showerror('Error loading model, check variable names', e)

    def startApp(self):
        # while True:
        #   self.updatePlots()
        #  self.root.update_idletasks()
        # self.root.update()
        self.root.protocol("WM_DELETE_WINDOW", self.stopApp)
        self.root.mainloop()

    def stopApp(self):
        # self.theRec.stopThread()
        self.root.destroy()


app = processApp()
app.startApp()
