import Tools.GUItools as guT
import Tools.Utils as tu
import pandas
import struct
import tkinter as tk
import numpy as np

CLIENT_IP = '127.0.0.1'
# CLIENT_IP = '192.168.0.255'
PORT_NUM = 12345
REC_PORT = 12354


class FakeShoeBoard(object):
    def __init__(self, id, ipAddress, theSend: tu.UDPsend):
        """

        :param id:
        :param ipAddress:
        :param theSend:
        """
        self.id = id
        self.ipAddress = ipAddress
        self.theSend = theSend
        self.counter = 0
        self.dataFrame = None
        self.header = [(x).to_bytes(1, 'big') for x in [1, 2, 3]]
        self.footer = [(x).to_bytes(1, 'big') for x in [0xA, 0xB, 0xC]]

        self.fmt = '<3c I 13h 4c'
        self.nVariables = 20
        self.packetSize = struct.calcsize(self.fmt)
        self.stream = False

    def sendPing(self):
        msg = "Hello from %d, %s in mode 3, V 4, @ 50 Hz, time is 0" % (int(self.ipAddress[-3:]), self.id)
        self.theSend.send(msg.encode(), (CLIENT_IP, PORT_NUM))

    def startRecording(self, shoeDF: pandas.DataFrame):
        self.dataFrame = shoeDF
        self.stream = True
        self.counter = 0

    def stopRecording(self):
        self.stream = False

    def updateStream(self):
        if self.stream:
            tS = int(self.dataFrame.index.values[self.counter] * 1000)
            # print(tS)
            pk = struct.pack(self.fmt, *self.header, tS, *(self.dataFrame.values[self.counter].tolist()), 0,
                             self.id[0].encode(), *self.footer)
            self.theSend.send(pk, (CLIENT_IP, PORT_NUM))
            self.counter += 1
            if self.counter >= self.dataFrame.shape[0]:
                self.counter = 0


class FakeShoeFrame(tk.Frame):
    def __init__(self, parent, theSend, id=None, ipAddress=None, **kwargs):
        super(FakeShoeFrame, self).__init__(parent, **kwargs)
        self.theSend = theSend
        topFrame = tk.Frame(self)
        topFrame.pack()
        leftFrame = tk.Frame(topFrame)
        leftFrame.pack(side=tk.LEFT)
        rightFrame = tk.Frame(topFrame)
        rightFrame.pack(side=tk.RIGHT)
        fileNamesLabel = ['Shoe File']
        buttonCmds = [self._loadShoe]
        browseTi = ['Select Shoe file']
        browseTypes = [(("Shoe files", "*.bin"), ("all files", "*.*"))]
        defaultVals = pandas.DataFrame(columns=['id', 'ip Address'], index=['val'])
        if id is None:
            id = chr(np.random.randint(97, 122))
        if ipAddress is None:
            ipAddress = '192.168.0.%d' % np.random.randint(100, 255)
        defaultVals['id'] = id
        defaultVals['ip Address'] = ipAddress
        self.inputValues = guT.InputsMatrix(rightFrame, defaultVals, 15, parseFN=lambda x: x)
        self.inputValues.pack()
        self.inputwB = []
        for lab, bCMD, titl, typ in zip(fileNamesLabel, buttonCmds, browseTi, browseTypes):
            self.inputwB.append(guT.InputWithButton(leftFrame, lab, 'Load', bCMD, titl, typ))
            self.inputwB[-1].pack()
        self.shoeBoard = FakeShoeBoard(id, ipAddress, self.theSend)
        self.dataFrame = None

    def _loadShoe(self, modelPath):
        vals = np.squeeze(self.inputValues.getValues().values)
        self.shoeBoard.id = vals[0]
        self.shoeBoard.ipAddress = vals[1]
        [r, ra] = tu.binaryFile2python(modelPath)
        if len(r.ax) == 0:
            s = ra
        else:
            s = r
        rDF = s.createPandasDataFrame(useSync=True, freq=50, returnObj=True)
        rDF.loc[:, ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'EUx', 'EUy', 'EUz']] *= 8000
        rDF = rDF.astype(np.int)
        namesWrong = ['pToe', 'pBall', 'pHeel', 'ax', 'ay', 'az', 'gx', 'gy', 'gz', 'EUx', 'EUy', 'EUz']

        rDF = rDF[namesWrong]
        self.dataFrame = rDF

    def updateFrame(self):
        if self.shoeBoard is None:
            return
        self.shoeBoard.updateStream()

    def startStreaming(self):
        self.shoeBoard.startRecording(self.dataFrame)

    def stopRecording(self):
        self.shoeBoard.stopRecording()

    def sendPing(self):
        self.shoeBoard.sendPing()


class UDPFakeShoes(tu.UDPreceiveProto):
    def __init__(self, console, commands):
        super(UDPFakeShoes, self).__init__(console, REC_PORT)
        self.commands = commands

    def _parseData(self, unparsed):
        print(unparsed)
        if unparsed[0] == 0xA and unparsed[1] == 0xD and unparsed[2] == 0xA:
            if unparsed[3] in self.commands:
                self.commands[unparsed[3]]()



class FakeShoesApp(tk.Tk):
    def __init__(self, **kwargs):
        super(FakeShoesApp, self).__init__(**kwargs)
        self.title('DeepSole System, Fake Shoes')
        self.iconbitmap(r'./GUI/deepsole.ico')
        self.refreshTime = 20  # ms
        self.theSend = tu.UDPsend()
        buttonsFrame = tk.Frame(self)
        buttonsFrame.pack()
        self.addButton = tk.Button(buttonsFrame, text='Add Board', command=self.addBoard)
        self.streamButton = tk.Button(buttonsFrame, text='Start Stream', command=self.startStream)
        self.streamButton.pack(side=tk.LEFT)
        self.addButton.pack(side=tk.RIGHT)
        tk.Button(buttonsFrame, text='Send Ping', command=self.sendPing).pack(side=tk.RIGHT)
        self.boardsFrame = tk.Frame(self)
        self.boardsFrame.pack()
        self.counter = 0
        self.boards = {}
        self.addBoard('l', '192.168.0.102')
        self.addBoard('r', '192.168.0.103')
        self.streaming = False
        self.cmds = {1: self.sendPing, 2: self.startStream, 3: self.startStream}
        self.rec = UDPFakeShoes(guT.ConsoleFrame(self), self.cmds)
        self.rec.startThread()

    def addBoard(self, id=None, ipAddress=None):
        self.boards[str(self.counter)] = FakeShoeFrame(self.boardsFrame, self.theSend, id, ipAddress)
        self.boards[str(self.counter)].pack()
        self.counter += 1

    def startApp(self):
        # while True:
        #   self.updatePlots()
        #  self.root.update_idletasks()
        # self.root.update()
        # self.startReceive()
        self.loopFunctions()
        # self.protocol("WM_DELETE_WINDOW", self.stopApp)
        self.mainloop()

    def sendPing(self):
        for k in self.boards:
            self.boards[k].sendPing()

    def startReceive(self):
        # self.root.startReceive()
        pass

    def startStream(self):
        if self.streaming:
            self.streaming = False
            self.streamButton['text'] = 'Start Stream'
            for k in self.boards:
                self.boards[k].stopRecording()

        else:
            for k in self.boards:
                self.boards[k].startStreaming()
            self.streamButton['text'] = 'Stop Stream'
            self.streaming = True

    def loopFunctions(self):
        for k in self.boards:
            self.boards[k].updateFrame()
        self.after(self.refreshTime, self.loopFunctions)

    def stopApp(self):
        self.root.destroy()

app = FakeShoesApp()
app.startApp()

