import Tools.Utils as tu
import struct
import numpy as np
import time

# CLIENT_IP = '192.168.0.255'
CLIENT_IP = '127.0.0.1'

PORT_NUM = 12345

data_path = 'Z:/HealthyControl/_Experiments/6MWT/'
subNum = 15
subID = 'Sbj%03d' % subNum
freq = 50.0
period = 1.0 / freq

lPath = data_path + subID + '/' + subID + 'L.bin'
rPath = data_path + subID + '/' + subID + 'R.bin'

rPath = "Z:/Haptic/Phase/Sub001/Baseline/baseline_l.bin"
lPath = "Z:/Haptic/Phase/Sub001/Baseline/baseline_r.bin"

rfile = open(rPath, 'rb')
lfile = open(lPath, 'rb')
theSend = tu.UDPsend()

[r, ra] = tu.binaryFile2python(rPath)
[_, l] = tu.binaryFile2python(lPath)
rDF = r.createPandasDataFrame(useSync=True, freq=freq, returnObj=True)
lDF = l.createPandasDataFrame(useSync=True, freq=freq, returnObj=True)
rDF.loc[:, ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'EUx', 'EUy', 'EUz']] *= 8000
lDF.loc[:, ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'EUx', 'EUy', 'EUz']] *= 8000
rDF = rDF.astype(np.int)
lDF = lDF.astype(np.int)


# names = ['pToe', 'pBall', 'pHeel', 'ax', 'ay', 'az', 'gx', 'gy', 'gz', 'EUy', 'EUz', 'EUx']
namesWrong = ['pToe', 'pBall', 'pHeel', 'ax', 'ay', 'az', 'gx', 'gy', 'gz', 'EUx', 'EUy', 'EUz']

rDF = rDF[namesWrong]
lDF = lDF[namesWrong]

header = [(x).to_bytes(1, 'big') for x in [1, 2, 3]]
footer = [(x).to_bytes(1, 'big') for x in [0xA, 0xB, 0xC]]

fmt = '<3c I 13h 4c'
nVariables = 20
packetSize = struct.calcsize(fmt)
while True:
    for j in range(rDF.shape[0]):
        start = time.time()
        for d, s in zip([rDF, lDF], [b'r', b'l']):
            tS = int(d.index.values[j] * 1000)
            pk = struct.pack(fmt, *header, tS, *(d.values[j].tolist()), 1, s, *footer)
            theSend.send(pk, (CLIENT_IP, PORT_NUM))

        while time.time() - start < period:
            continue








