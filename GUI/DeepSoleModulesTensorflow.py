import pandas
import time
import numpy as np
import tensorflow as tf
import struct
import tkinter as tk
import Tools.GUItools as guT
import GUI.DeepSoleModules as dsm


class PredictionFrameProto(tk.Frame):
    def __init__(self, master, udpSend, predictionDF, **kwargs):
        super(PredictionFrameProto, self).__init__(master, **kwargs)
        self.udpSend = udpSend
        self.predictionDF = predictionDF
        self.binaryFile = bytes(0)

    def vibrationUpdate(self):
        pass

    def saveBinaryFile(self, fileN):
        with open(fileN, 'wb') as f:
            f.write(self.binaryFile)
        self.binaryFile = bytes(0)


class KerasPredictionModelProto(object):
    def __init__(self, udpSend, startTime=[0], sizeBuf=1000, sendUDP=True, doArgmax=False, udpMultiplier=1):
        self.model = None
        self.doArgmax = doArgmax
        self.sendUDP = sendUDP
        self.startTime = startTime
        self.sizeBuf = sizeBuf
        # self.r = rightDataFrame
        # self.l = leftDataFrame
        self._predictionDataFrames = {}
        self.freq = None
        self.tmax = None
        self.freqDF = pandas.DataFrame(index=range(50), columns=['Time'])
        self.fmt = None
        self.fmt2 = None
        self.footer = [(x).to_bytes(1, 'big') for x in [1, 2, 3]]
        self.header = [(x).to_bytes(1, 'big') for x in [0xA, 0xB, 0xC]]
        self.theSend = udpSend
        # self.prediction = pandas.DataFrame(columns=['R', 'L'])
        # self.prediction = pandas/////////////////////////.DataFrame()
        # self.rec1 = None
        # self.t1 = None
        # self.bCont = None
        # self.sizeBuf = sizeBuf
        self.predicting = False
        self.modelPath = None
        self.binaryFile = bytes(0)
        self.start = time.time()
        self.predict = False
        self.prediction = pandas.DataFrame(columns=['sync'])
        self.c = 0
        self._lastPredictionTime = []
        self.CLIENT_IP = '192.168.0.255'
        self.CLIENT_PORT = 24681
        self.udpMultiplier = udpMultiplier
        self.udpCounter = 0

    def addDataframes(self, newDataFramesDictionary):
        self._predictionDataFrames = {**self._predictionDataFrames, **newDataFramesDictionary}
        for k in newDataFramesDictionary:
            self.prediction[k] = 0

    def _predict(self):
        self._actualPrediction(self.c)
        self.c += 1
        if self.c > 50:
            self.c = 0

    def loadModel(self, modelPath, freq, tmax, doArgmax):
        self.model = modelPath
        self.modelPath = modelPath
        self.freq = freq
        self.tmax = tmax
        self.doArgmax = doArgmax
        self.model = tf.keras.models.load_model(self.modelPath)
        self.model.predict(np.zeros((2, self.tmax, 12), dtype=np.float32))

    def startPrediction(self):
        # self.rec1 = threading.Event()
        # self.rec1.set()
        # self.t1 = threading.Thread(target=self.__predict2, args=[self.rec1])
        # self.t1.daemon = True
        # self.t1.start()
        # self.model = lambda x: np.round(0.5 * np.sin(np.pi * (x / 1000)) + 0.5)
        self.predict = True
        # '>3c 4L 3c'
        self.fmt = '>3c %dL 3c' % (2 * len(self._predictionDataFrames))
        self.fmt2 = '>3c %dL 3c' % (2 * len(self._predictionDataFrames) + 1)
        print(self.fmt2)
        self.bCont = np.zeros((len(self._predictionDataFrames), self.tmax, 12), dtype=np.float32)
        self.c = 0
        # self.model = tf.keras.models.load_model(self.modelPath)
        # self.model.predict(self.bCont)
        self.binaryFile = bytes(0)
        self.start = time.time()
        # print(self.prediction.head())

    def stopPrediction(self):
        self.predict = False

    def updateFrame(self):
        if self.predict:
            self._predict()

    def _actualPrediction(self, c):
        now = time.time()
        t = []
        for j, k in enumerate(self._predictionDataFrames):
            if self._predictionDataFrames[k].shape[0] < self.tmax:
                return
            aux = self._predictionDataFrames[k].values[-self.tmax:, :-2].astype(np.float)
            aux[np.isnan(aux)] = 0.0
            self.bCont[j] = aux
            # self.bCont[1] = self.l.dataFrame.values[-self.tmax:, :-1]
            t.append(self._predictionDataFrames[k].index.values.astype(np.int)[-1])

        # if any(a == b for a, b in zip(t, self._lastPredictionTime)):
        if t == self._lastPredictionTime:
            return
        # t = self.r.dataFrame.index.values.astype(np.int)[-1]
        pred = self.model.predict(self.bCont)
        # print(pred.shape)
        if self.doArgmax:
            pred = np.argmax(pred, axis=1)
        # # print(pred.shape)
        # print(t)
        # print(pred)
        # print(self.bCont[0])
        # print(self.bCont[1])
        if np.isnan(pred).any():
            return
        msg = struct.pack(self.fmt, *self.header, *t,
                          *[(pred[j, 0] * 1000).astype(np.int) for j in range(len(self._predictionDataFrames))],
                          *self.footer)
        msg2 = struct.pack(self.fmt2, *self.header, int((now - self.startTime[0]) * 1000), *t,
                           *[(pred[j, 0] * 1000).astype(np.int) for j in range(len(self._predictionDataFrames))],
                           *self.footer)
        # msg = struct.pack(self.fmt, *self.header, t[0], t[1], (pred[0] * 1000).astype(np.int),
        #                   (pred[1] * 1000).astype(np.int), *self.footer)
        if self.sendUDP and self.udpCounter >= self.udpMultiplier:
            self.theSend.send(msg, (self.CLIENT_IP, self.CLIENT_PORT))
            self.udpCounter = 1
        else:
            self.udpCounter += 1
        self.binaryFile += msg2
        for j, s in enumerate(self._predictionDataFrames):
            self.prediction.loc[t[0], s] = pred[j]
        # self.prediction.loc[t[1], 'L'] = pred[1]
        if self.prediction.shape[0] > self.sizeBuf:
            self.prediction.drop(self.prediction.index[0], inplace=True)

        # self.freqDF.loc[c, 'Time'] = 1 / (self.prediction.index.values[-1] - self.prediction.index.values[-2])
        self._lastPredictionTime = t

    def saveBinaryFile(self, fileN):
        fileN += '_pred.bin'
        with open(fileN, 'wb') as f:
            f.write(self.binaryFile)
        self.binaryFile = bytes(0)

    def changeSendOption(self, newVal):
        self.sendUDP = newVal


class PostPredictionFrameProto(tk.Frame):
    def __init__(self, master, checkCMD, udpSend, framesDictConst, **kwargs):
        super(PostPredictionFrameProto, self).__init__(master, **kwargs)
        self.udpSend = udpSend
        self.bottomFrame = tk.Frame(self)
        # self.framesDict = dict((k, framesDictConst[k](self.bottomFrame)) for k in framesDictConst)
        self.framesDictConst = framesDictConst
        self.framesDict = dict((k, None) for k in framesDictConst)
        topFame = tk.Frame(self)
        topFame.pack()
        tk.Label(topFame, text='Prediction processing', background='black', foreground='white').pack()
        self.cmd = checkCMD
        self.sendVar = tk.IntVar(self)
        self.sendVar.set(1)
        tk.Checkbutton(topFame, text='Send prediction', variable=self.sendVar, command=self.exeCMD).pack()
        self.postChoice = tk.StringVar(self)
        self.postChoice.set('None')
        self.popUp = tk.OptionMenu(topFame, self.postChoice, 'None', *framesDictConst)
        self.popUp.pack()
        self.postChoice.trace('w', self.dropDownChanged)
        self.bottomFrame.pack()
        self.emptyFrame = PredictionFrameProto(self, None, None)
        self.emptyFrame.pack()
        self.currentFrame = self.emptyFrame
        # self.phaseFrame = PhaseVibrationFrame(bottomFrame, udpSend, predictionDF, rightDF, leftDF)

    def dropDownChanged(self, *args):
        val = self.postChoice.get()
        if val in 'None':
            self.currentFrame.pack_forget()
            self.emptyFrame.pack()
            self.currentFrame = self.emptyFrame
        else:
            # print(val)
            if self.framesDict[val] is None:
                self.framesDict[val] = self.framesDictConst[val](self.bottomFrame)
                if self.framesDict[val] is None:
                    return
            self.currentFrame.pack_forget()
            self.framesDict[val].pack()
            self.currentFrame = self.framesDict[val]

    def exeCMD(self):
        self.cmd(self.sendVar.get())

    def updateFunctions(self):
        self.currentFrame.vibrationUpdate()

    def saveBinaryFile(self, fileN):
        self.currentFrame.saveBinaryFile(fileN)


class OneSideVibrationControlFrame(tk.Frame):
    def __init__(self, parent, name, color, cmd, **kwargs):
        '''

        :param parent: TK master
        :param name: Name to be used as label
        :param color: color of the label
        :param cmd: command to be executed for the vibration. It has to be a f([vibrationIntensity*3])
        :param kwargs:
        '''
        # tk.Frame.__init__(self, parent, **kwargs)

        super(OneSideVibrationControlFrame, self).__init__(parent, **kwargs)
        topF = tk.Frame(self)
        topF.pack()
        self.name = name
        self.commands = cmd
        self.enableFrame = guT.CheckControl(topF, '', [name], None, self.packUnpack, [color])
        self.enableFrame.pack()
        bottomF = tk.Frame(self)
        bottomF.pack()
        defaultVals = pandas.DataFrame(data=np.zeros((3, 2), dtype=np.int) + np.array([1000, 80]),
                                       index=['MBall', 'LBall', 'Heel'],
                                       columns=['t [ms]', 'A [%]'])
        self.inputs = guT.InputsMatrix(bottomF, defaultVals, 5)
        # self.inputs.pack()
        # self.inputs.pack_forget()
        self.vibrate = [False, False, False]
        self.vibrationStart = [0, 0, 0]
        self.vibrationCMD = np.zeros(3)
        self.statusLabelVar = tk.StringVar(self)
        self.statusLabelVar.set('off')
        self.statusLabel = tk.Message(bottomF, textvariable=self.statusLabelVar)
        self.statusLabel.pack()
        self.binaryFile = bytes(0)

        # self.inputs.pack()

    def packUnpack(self):
        if self.enableFrame.getAllValues()[0] == 1:
            self.inputs.pack()
        else:
            self.inputs.pack_forget()

    def vibrationTrigger(self, onOffList, t):
        if self.enableFrame.getAllValues()[0] == 0:
            return
        self.vibrate = onOffList
        vals = self.inputs.getValues().values[:, 1]
        vibrationCMD = np.zeros(3)
        for i, (v, intensity) in enumerate(zip(onOffList, vals)):
            if v:
                self.vibrationStart[i] = time.time()
                # cmd(intensity)
                vibrationCMD[i] = intensity

            # else:
            #     # cmd(0)
        self.commands(vibrationCMD)
        self.vibrationCMD = vibrationCMD
        self.statusLabelVar.set('On')
        self.binaryFile += struct.pack('<Ic', t, self.name[0].encode())

    def vibrationUpdate(self):
        # check which motor should be turn off
        if any(self.vibrate):
            vals = self.inputs.getValues().values[:, 0]
            resend = False
            for i, (v, st, duration) in enumerate(zip(self.vibrate, self.vibrationStart, vals)):
                if v and time.time() - st > duration / 1000.0:
                    resend = True
                    self.vibrationCMD[i] = 0
                    self.vibrate[i] = False
            if resend:
                self.commands(self.vibrationCMD)
        else:
            self.statusLabelVar.set('off')


class OneSideThresholdVibrationControlFrame(tk.Frame):
    def __init__(self, parent, name, color, cmd, **kwargs):
        '''

        :param parent: TK master
        :param name: Name to be used as label
        :param color: color of the label
        :param cmd: command to be executed for the vibration. It has to be a f([vibrationIntensity*3])
        :param kwargs:
        '''
        # tk.Frame.__init__(self, parent, **kwargs)

        super(OneSideThresholdVibrationControlFrame, self).__init__(parent, **kwargs)
        topF = tk.Frame(self)
        topF.pack()
        self.name = name
        self.commands = cmd
        self.enableFrame = guT.CheckControl(topF, '', [name], None, self.packUnpack, [color])
        self.enableFrame.pack()
        bottomF = tk.Frame(self)
        bottomF.pack()
        defaultVals = pandas.DataFrame(data=np.zeros((2, 2), dtype=np.int) + np.array([2048, 80]),
                                       index=['Left', 'Right'],
                                       columns=['Thd', 'A [%]'])
        self.inputs = guT.InputsMatrix(bottomF, defaultVals, 5)
        # self.inputs.pack()
        # self.inputs.pack_forget()
        self.vibrate = [False, False]
        self.vibrationStart = [0, 0]
        self.vibrationCMD = np.zeros(2)
        statusFrame = tk.Frame(bottomF)
        statusFrame.pack()
        self.statusLabelVarLeft = tk.StringVar(self)
        self.statusLabelVarLeft.set('off')
        self.statusLabel = tk.Message(statusFrame, textvariable=self.statusLabelVarLeft)
        self.statusLabel.pack(side=tk.LEFT)
        self.statusLabelVarRight = tk.StringVar(self)
        self.statusLabelVarRight.set('off')
        self.statusLabel = tk.Message(statusFrame, textvariable=self.statusLabelVarRight)
        self.statusLabel.pack(side=tk.LEFT)
        self.statusLabelVarNone = tk.StringVar(self)
        self.binaryFile = bytes(0)

        # self.inputs.pack()

    def packUnpack(self):
        if self.enableFrame.getAllValues()[0] == 1:
            self.inputs.pack()
        else:
            self.inputs.pack_forget()

    def vibrationTrigger(self, onOffList, t):
        if self.enableFrame.getAllValues()[0] == 0:
            return
        self.vibrate = onOffList
        vals1 = self.inputs.getValues().values[:, 1]
        vals = [vals1[0], vals1[1], ((vals1[0] * onOffList[0]) + (vals1[1] * onOffList[1])) % 100]
        # print(onOffList)
        vibrationCMD = np.zeros(3)
        for i, (v, intensity, sL) in enumerate(zip(onOffList, vals, (self.statusLabelVarLeft, self.statusLabelVarRight,
                                                                     self.statusLabelVarNone))):
            if v:
                # self.vibrationStart[i] = time.time()
                # cmd(intensity)
                vibrationCMD[i] = intensity
                sL.set('On')
            else:
                vibrationCMD[i] = 0
                sL.set('off')

            # else:
            #     # cmd(0)
        # print(vibrationCMD)
        self.commands(vibrationCMD)
        self.vibrationCMD = vibrationCMD
        # self.statusLabelVar.set('On')
        self.binaryFile += struct.pack('<Ic', t, self.name[0].encode())

    def vibrationUpdate(self):
        # check which motor should be turn off
        # if any(self.vibrate):
        #     vals = self.inputs.getValues().values[:, 0]
        #     resend = False
        #     for i, (v, st, duration) in enumerate(zip(self.vibrate, self.vibrationStart, vals)):
        #         if v and time.time() - st > duration / 1000.0:
        #             resend = True
        #             self.vibrationCMD[i] = 0
        #             self.vibrate[i] = False
        #     if resend:
        #         self.commands(self.vibrationCMD)
        # else:
        #     self.statusLabelVar.set('off')
        pass


class PhaseVibrationFrame(PredictionFrameProto):
    def __init__(self, master, udpSend, predictionDF, rightDF, leftDF, left_IP, right_IP, **kwargs):
        # tk.Frame.__init__(parent, **kwargs)
        super(PhaseVibrationFrame, self).__init__(master, udpSend, predictionDF, **kwargs)
        self.rightDF = rightDF
        self.leftDF = leftDF
        self.buttonFrame = tk.Button(self, text='Start Vibration', command=self.setVibration)
        self.buttonFrame.pack()
        self.buttonFrame2 = tk.Button(self, text='Test Trigger', command=lambda: self.vibrationTrigger(True, True))
        self.buttonFrame2.pack()
        self.vibrate = False
        self.lFrame = OneSideVibrationControlFrame(self, 'L', 'blue', lambda x: self.sendCommand(left_IP, x))
        self.lFrame.pack(side=tk.LEFT)
        self.rFrame = OneSideVibrationControlFrame(self, 'R', 'red', lambda x: self.sendCommand(right_IP, x))
        self.rFrame.pack(side=tk.RIGHT)
        self.windowSize = 5
        self.counter = [0, 0]
        self.hsDetected = [False, False]
        self._lastPredictionTime = 0

    def setVibration(self):
        self.vibrate = not self.vibrate
        if self.vibrate:
            self.buttonFrame["text"] = "Stop Vibration"
            self.buttonFrame.config(bg='green')
        else:
            self.buttonFrame["text"] = "Start Vibration"
            self.buttonFrame.config(bg='lightgray')

    def vibrationTrigger(self, rTrigger=False, lTrigger=False):
        if rTrigger:
            if self.rightDF.index.values.shape[0] == 0:
                self.rFrame.vibrationTrigger([True, True, True], 0)
            else:
                self.rFrame.vibrationTrigger([True, True, True], self.rightDF.index.values[-1].astype(np.int))

        if lTrigger:
            if self.leftDF.index.values.shape[0] == 0:
                self.rFrame.vibrationTrigger([True, True, True], 0)
            else:
                self.lFrame.vibrationTrigger([True, True, True], self.leftDF.index.values[-1].astype(np.int))

        if self.predictionDF.shape[0] == 0:
            return

    def saveBinaryFile(self, fileN):
        for k in [self.rFrame, self.lFrame]:
            self.binaryFile += k.binaryFile
        fileN += '_triggers.bin'
        super(PhaseVibrationFrame, self).saveBinaryFile(fileN)
        # with open(fileN, 'wb') as f:
        #     f.write(self.binaryFile)

    def vibrateAtHS(self):
        hss = [False, False]
        if self.predictionDF.shape[0] < 10 or self.predictionDF.index[-1] == self._lastPredictionTime:
            return
        # ddf = (self.predictionDF.values[-1, :] - self.predictionDF.values[-2, :]) == -1
        # ddf2 = (self.predictionDF.values[-1, :] - self.predictionDF.values[-2, :]) == 1
        # for j in range(2):
        #     # if HS detected, set as true
        #     if ddf[j]:
        #         self.hsDetected[j] = True
        #     # if TO detected, set as false
        #     if ddf2[j]:
        #         self.hsDetected[j] = False
        #
        #     # if HS was detected then vibrate
        #     if self.hsDetected[j] and self.predictionDF.values[-self.windowSize:, j].sum() == 0:
        #         hss[j] = True
        for j in range(2):
            hss[j] = self.predictionDF.values[-self.windowSize:, j].sum() == 0 and self.predictionDF.values[
                -self.windowSize - 1, j] == 1

        self.vibrationTrigger(hss[0], hss[1])
        self._lastPredictionTime = self.predictionDF.index[-1]

    def vibrationUpdate(self):
        self.vibrateAtHS()
        self.lFrame.vibrationUpdate()
        self.rFrame.vibrationUpdate()

    def sendCommand(self, ipAddress, mVals):
        # names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
        vals = np.zeros(7, dtype=np.uint8)
        # print(ipAddress)
        vals[:3] = mVals
        cmd = guT.createCMD(8, te=np.squeeze(vals))
        self.udpSend.send(cmd, addr=(ipAddress, 12354))


class ThresholdVibrationFrame(PredictionFrameProto):
    def __init__(self, master, udpSend, predictionDF, rightDF, leftDF, left_IP, right_IP, **kwargs):
        # tk.Frame.__init__(parent, **kwargs)
        super(ThresholdVibrationFrame, self).__init__(master, udpSend, predictionDF, **kwargs)
        self.rightDF = rightDF
        self.leftDF = leftDF
        self.buttonFrame = tk.Button(self, text='Start Vibration', command=self.setVibration)
        self.buttonFrame.pack()
        self.buttonFrame2 = tk.Button(self, text='Test Trigger', command=lambda: self.vibrationTrigger(True, True))
        self.buttonFrame2.pack()
        self.vibrate = False
        # self.lFrame = OneSideThresholdVibrationControlFrame(self, 'L', 'blue', lambda x: self.sendCommand(left_IP, x))
        # self.lFrame.pack(side=tk.LEFT)
        self.rFrame = OneSideThresholdVibrationControlFrame(self, 'ArmRest', 'cyan',
                                                            lambda x: self.sendCommand(right_IP, x))
        self.rFrame.pack()
        self.windowSize = 5
        self.counter = [0, 0]
        self.hsDetected = [False, False]
        self._lastPredictionTime = 0
        self._lastCmd = np.zeros(7, dtype=np.int8)

    def setVibration(self):
        self.vibrate = not self.vibrate
        if self.vibrate:
            self.buttonFrame["text"] = "Stop Vibration"
            self.buttonFrame.config(bg='green')
        else:
            self.buttonFrame["text"] = "Start Vibration"
            self.buttonFrame.config(bg='lightgray')

    def vibrationTrigger(self, BLeft, BRight):
        self.rFrame.vibrationTrigger([BLeft, BRight, BLeft or BRight], 0)
        # if self.predictionDF.shape[0] == 0:
        #     return

    def saveBinaryFile(self, fileN):
        for k in [self.rFrame]:
            self.binaryFile += k.binaryFile
        fileN += '_triggers.bin'
        super(PhaseVibrationFrame, self).saveBinaryFile(fileN)
        # with open(fileN, 'wb') as f:
        #     f.write(self.binaryFile)

    def vibrateAtHS(self):
        if self.predictionDF.shape[0] < 10 or self.predictionDF.index[-1] == self._lastPredictionTime:
            return
        # print(self.predictionDF.iloc[-1])
        hss = [x < t for x, t in zip(self.predictionDF.values[-1, 1:], self.rFrame.inputs.getValues().values[:, 0])]
        # for j in range(2):
        #     hss[j] = self.predictionDF.values[-self.windowSize:, j].sum() == 0

        self.vibrationTrigger(hss[0], hss[1])
        self._lastPredictionTime = self.predictionDF.index[-1]

    def vibrationUpdate(self):
        self.vibrateAtHS()
        self.rFrame.vibrationUpdate()

    def sendCommand(self, ipAddress, mVals):
        # names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
        vals = np.zeros(7, dtype=np.int8)
        vals[:3] = mVals
        vals[3:] = [0, 5, 80, 0]
        if (vals == self._lastCmd).all():
            return
        # print(vals)
        self._lastCmd = vals
        cmd = guT.createCMD(8, te=np.squeeze(vals))
        self.udpSend.send(cmd, addr=(ipAddress, 12354))


class MuscleVibrationFrame(PredictionFrameProto):
    def __init__(self, master, udpSend, predictionDF, muscleDefIP, **kwargs):
        super(MuscleVibrationFrame, self).__init__(master, udpSend, predictionDF, **kwargs)
        self.muscleDefIP = muscleDefIP
        self.udpSend = udpSend
        self.vibrate = False
        self.buttonFrame = tk.Button(self, text='Start Vibration', command=self.setVibration)
        self.buttonFrame.pack()
        self.buttonFrame2 = tk.Button(self, text='Test Trigger', command=self.testVibration)
        self.buttonFrame2.pack()
        self.checks = {}
        self.bottomFrame = tk.Frame(self)
        self.bottomFrame.pack()
        for j, s in enumerate(['L', 'R']):
            self.checks[s] = guT.CheckControl(self.bottomFrame, s, ['Tibial', 'Rectus', 'Femoris'], None,
                                              color=['red', 'blue', 'orange'], defaultVal=[1])
            self.checks[s].grid(row=0, column=j)
        self._boardDict = {}
        for k in muscleDefIP:
            self._boardDict[muscleDefIP[k]['Board']] = {}
            self._boardDict[muscleDefIP[k]['Board']]['MotorVals'] = [0, 0, 0]
            self._boardDict[muscleDefIP[k]['Board']]['MotorVals_old'] = [0, 0, 0]
            self._boardDict[muscleDefIP[k]['Board']]['IP'] = muscleDefIP[k]['IP']
        self._lastTime = 0
        self._cmd = [0 for _ in muscleDefIP]
        self.footer = [(x).to_bytes(1, 'big') for x in [1, 2, 3]]
        self.header = [(x).to_bytes(1, 'big') for x in [0xA, 0xB, 0xC]]
        self.fmt = '>3c L %dc' % (len(self.muscleDefIP) + 3)

    def setVibration(self):
        self.vibrate = not self.vibrate
        if self.vibrate:
            self.buttonFrame["text"] = "Stop Vibration"
            self.buttonFrame.config(bg='green')
        else:
            self.buttonFrame["text"] = "Start Vibration"
            self.buttonFrame.config(bg='lightgray')
            for k in self._boardDict:
                # send command
                self._sendVibration(self._boardDict[k]['IP'], [0, 0, 0])

    def saveBinaryFile(self, fileN):
        # for k in [self.rFrame, self.lFrame]:
        #     self.binaryFile += k.binaryFile
        fileN += '_triggers.bin'
        super(MuscleVibrationFrame, self).saveBinaryFile(fileN)

    def testVibration(self):
        root = tk.Toplevel(self)
        for k in self.muscleDefIP:
            dsm.TestBoardVibration(root, k, self.muscleDefIP[k]['IP'], self.muscleDefIP[k]['Motor'],
                                   self.udpSend).pack()
        root.transient(self)
        root.grab_set()
        self.wait_window(root)

    def _sendVibration(self, ipAddress, motorVals):
        # names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
        vals = np.array([*motorVals, 1, 5, 80, 0], dtype=np.uint8)
        cmd = guT.createCMD(8, te=np.squeeze(vals))
        self.udpSend.send(cmd, addr=(ipAddress, 12354))

    def vibrationUpdate(self):
        if self.vibrate:
            predT = self.predictionDF.index.values[-1]
            if predT == self._lastTime:
                return
            self._lastTime = predT
            predVal = self.predictionDF.values[-1, 1]
            # check each muscle
            for j, k in enumerate(self.muscleDefIP):
                b = self.muscleDefIP[k]['Board']
                m = self.muscleDefIP[k]['Motor']
                # save to the other dictionary to min the number of udp sends
                # print(predVal)

                self._boardDict[b]['MotorVals'][m] = ((self.muscleDefIP[k]['Range'][0] < predVal <
                                                       self.muscleDefIP[k]['Range'][1]) * 50)
                # save to cmd to put to binary file
                self._cmd[j] = bytes([int(self._boardDict[b]['MotorVals'][m])])
            # send commands
            for k in self._boardDict:
                # check if the motor command changed from last dt
                if self._boardDict[k]['MotorVals_old'] == self._boardDict[k]['MotorVals']:
                    continue
                self._boardDict[k]['MotorVals_old'] = self._boardDict[k]['MotorVals'].copy()
                # send command
                # print(k)
                # print(self._boardDict[k]['MotorVals'])
                self._sendVibration(self._boardDict[k]['IP'], self._boardDict[k]['MotorVals'])

            # save to binary file
            self.binaryFile += struct.pack(self.fmt, *self.header, predT, *self._cmd, *self.footer)
