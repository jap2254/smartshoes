import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from Tools.Utils import *
from Tools.GUItools import createCMD


class softSyncBox(object):
    def __init__(self, port=8666, resendTimes=1):
        self.port = port
        self.resendTimes = resendTimes
        self.syncVal = False
        udpRec = UDPreceiveNoThread(port, self.parseSync)
        self.STREAM_IP_ADDRESS = '192.168.0.255'
        self.theSend = UDPsend()
        udpRec.receiveData()

    def parseSync(self, data, address):
        lenData = len(data)
        if lenData is 16:
            pData = struct.unpack('8h', data)
            self.syncVal = pData[3] is 256
            cmd = createCMD(0xC, self.syncVal)
            for _ in range(self.resendTimes):
                self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
            print('Sync is %i' % self.syncVal)


# mySync = softSyncBox(resendTimes=3)

# use this to test a sync signal
STREAM_IP_ADDRESS = '192.168.0.255'
theSend = UDPsend()
cmd = createCMD(0xC, 1)
time.sleep(2)
print('Sync start')
theSend.send(cmd, addr=(STREAM_IP_ADDRESS, 12354))
now = time.time()
time.sleep(5)
print('Sync end')
cmd = createCMD(0xC, 0)
theSend.send(cmd, addr=(STREAM_IP_ADDRESS, 12354))
