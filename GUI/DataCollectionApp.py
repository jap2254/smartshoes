# import os
# import sys
#
# sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import pandas
import threading
import time
import numpy as np
from tkinter import messagebox, simpledialog
import tkinter as tk


import Tools.GUItools as guT
import Tools.Utils as tu
import GUI.DeepSoleModules as dsm


class ControlFunctionsContainer(object):
    def __init__(self, labels, main):
        # self.RIGHT_IP_ADDRESS = '192.168.0.103'
        # self.LEFT_IP_ADDRESS = '192.168.0.102'
        self.STREAM_IP_ADDRESS = '192.168.0.255'
        # self.STREAM_IP_ADDRESS = '192.168.0.255'
        self.buttons = None
        self.labels = labels
        self.main = main
        self.theSend = main.theSend
        self.isRecord = True
        self.isStream = True
        self.isPredict = True
        self.fileName = 'file%d' % (int(time.time()))

    class motorControl(tk.Toplevel):
        def __init__(self, parent):
            tk.Toplevel.__init__(self, parent)
            # self.root = tk.Toplevel(self)
            self.vals = None
            fr = tk.Frame(self)
            fr.pack()
            tk.Label(fr, text='R').grid(row=1)
            tk.Label(fr, text='L').grid(row=2)
            self.valR = []
            self.valL = []
            names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
            self.allNames = names
            okCMD = self.register(self.validateVal)
            vld = (okCMD, '%P')
            for i, n in enumerate(names):
                # label
                tk.Label(fr, text=n).grid(row=0, column=i + 1)
                # box 1
                self.valR.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                        column=i + 1)
                self.valR[-1].set('0')
                # box 2
                self.valL.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valL[-1], validate='focusout', validatecommand=vld).grid(row=2,
                                                                                                        column=i + 1)
                self.valL[-1].set('0')
            frBot = tk.Frame(self)
            tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
            tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
            frBot.pack()
            self.protocol("WM_DELETE_WINDOW", self.cancelB)
            self.transient(parent)
            self.grab_set()
            parent.wait_window(self)
            # root.quit()

        def validateVal(self, value):
            try:
                if value:
                    # print(type(value))
                    # print(value)
                    v = int(value)
                    return True
            except ValueError:
                return False

        def okB(self):
            self.vals = np.zeros((len(self.allNames), 2), dtype=np.int8)
            for i, (r, l) in enumerate(zip(self.valR, self.valL)):
                self.vals[i, :] = np.array([int(r.get()), int(l.get())], dtype=np.int8)

            self.destroy()

        def cancelB(self):
            self.vals = None
            self.destroy()

    class FreqChange(tk.Toplevel):
        def __init__(self, parent):
            tk.Toplevel.__init__(self, parent)
            # self.root = tk.Toplevel(self)
            self.vals = None
            fr = tk.Frame(self)
            fr.pack()
            self.valR = []
            names = ['Freq']
            self.allNames = names
            okCMD = self.register(self.validateVal)
            vld = (okCMD, '%P')
            for i, n in enumerate(names):
                # label
                tk.Label(fr, text=n).grid(row=0, column=i + 1)
                # box 1
                self.valR.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                        column=i + 1)
                self.valR[-1].set('100')
            self.che = guT.CheckControl(self, '', ['Save params'], ax=None, defaultVal=[1])
            self.che.pack()
            frBot = tk.Frame(self)
            tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
            tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
            frBot.pack()
            self.protocol("WM_DELETE_WINDOW", self.cancelB)
            self.transient(parent)
            self.grab_set()
            parent.wait_window(self)
            # root.quit()

        def validateVal(self, value):
            try:
                if value:
                    # print(type(value))
                    # print(value)
                    v = int(value)
                    return True
            except ValueError:
                return False

        def okB(self):
            self.vals = np.zeros((len(self.allNames) + 1), dtype=np.int8)
            for i, r in enumerate(self.valR):
                self.vals[i] = int(r.get())
            self.vals[-1] = self.che.getAllValues()[0]
            self.destroy()

        def cancelB(self):
            self.vals = None
            self.destroy()

    class assignControl(tk.Toplevel):
        def __init__(self, parent):
            tk.Toplevel.__init__(self, parent)
            # self.root = tk.Toplevel(self)
            self.vals = None
            fr = tk.Frame(self)
            fr.pack()
            tk.Label(fr, text='R').grid(row=1)
            tk.Label(fr, text='L').grid(row=2)
            self.valR = []
            self.valL = []
            names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
            self.allNames = names
            okCMD = self.register(self.validateVal)
            vld = (okCMD, '%P')
            for i, n in enumerate(names):
                # label
                tk.Label(fr, text=n).grid(row=0, column=i + 1)
                # box 1
                self.valR.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                        column=i + 1)
                self.valR[-1].set('0')
                # box 2
                self.valL.append(tk.StringVar())
                tk.Entry(fr, textvariable=self.valL[-1], validate='focusout', validatecommand=vld).grid(row=2,
                                                                                                        column=i + 1)
                self.valL[-1].set('0')
            frBot = tk.Frame(self)
            tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
            tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
            frBot.pack()
            self.protocol("WM_DELETE_WINDOW", self.cancelB)
            self.transient(parent)
            self.grab_set()
            parent.wait_window(self)
            # root.quit()

        def validateVal(self, value):
            try:
                if value:
                    # print(type(value))
                    # print(value)
                    v = int(value)
                    return True
            except ValueError:
                return False

        def okB(self):
            self.vals = np.zeros((len(self.allNames), 2), dtype=np.int8)
            for i, (r, l) in enumerate(zip(self.valR, self.valL)):
                self.vals[i, :] = np.array([int(r.get()), int(l.get())], dtype=np.int8)

            self.destroy()

        def cancelB(self):
            self.vals = None
            self.destroy()

    def sendPing(self):
        cmd = guT.createCMD(1)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def changeFreq(self):
        self.mW = self.FreqChange(self.main.root)
        if self.mW.vals is None:
            print('Cancel was pressed')
        else:
            # send motor cmd
            print(self.mW.vals)
            cmd = guT.createCMD(14, te=self.mW.vals)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def reassign(self):
        # TODO send only to 1
        cmd = guT.createCMD(5, [108, 1])
        # cmd = guT.createCMD(14, 10)
        # self.theSend.send(cmd, addr=(self.LEFT_IP_ADDRESS, 12354))
        self.theSend.send(cmd, addr=(self.RIGHT_IP_ADDRESS, 12354))

    def recordCmd(self):
        if self.isRecord:
            n = 2
            self.buttons['Start Recording']["text"] = "Stop recording"
            # self.main.leftShoe.resetShoe()
            # self.main.rightShoe.resetShoe()
            self.main.leftShoe.binaryFile = bytes(0)
            self.main.rightShoe.binaryFile = bytes(0)
            self.buttons['Start Recording'].config(bg='green')
            cmd = guT.createCMD(n)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
        else:
            n = 3
            self.buttons['Start Recording']["text"] = "Start recording"
            self.buttons['Start Recording'].config(bg='yellow')
            cmd = guT.createCMD(n)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
            self.saveDataCmd()
        self.isRecord = not self.isRecord

        self.streamCmd()

    def streamCmd(self):
        if self.isStream:
            n = 6
            # self.streamB["text"] = "Stop streaming"
        else:
            n = 7
            # self.streamB["text"] = "Start streaming"
        self.isStream = not self.isStream
        cmd = guT.createCMD(n)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def reNameCmd(self):
        name = simpledialog.askstring('Name for log file', 'Name')
        if name is None:
            return
        print(name)
        self.fileName = '%s%d' % (name, int(time.time()))
        cmd = guT.createCMD(4, te=name)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def sendMotorCmd(self):
        self.mW = self.motorControl(self.main.root)
        if self.mW.vals is None:
            print('Cancel was pressed')
        else:
            # send motor cmd
            print(self.mW.vals)
            cmd = guT.createCMD(8, te=np.squeeze(self.mW.vals[:, 1]))
            self.theSend.send(cmd, addr=(self.main.console.ipAddress['l'], 12354))
            cmd = guT.createCMD(8, te=np.squeeze(self.mW.vals[:, 0]))
            self.theSend.send(cmd, addr=(self.main.console.ipAddress['r'], 12354))

    def getPressCmd(self):
        cmd = guT.createCMD(9)
        self.theSend.send(cmd, addr=(self.main.console.ipAddress['l'], 12354))
        time.sleep(1)
        self.theSend.send(cmd, addr=(self.main.console.ipAddress['r'], 12354))

    def readBinaryCmd(self):
        shoes = tu.binaryFile2python()
        fSave = tk.filedialog.asksaveasfile(self.main.root)
        if fSave is None:
            return shoes
        np.save(fSave, shoes)

    def saveDataCmd(self):
        fSave = tk.filedialog.asksaveasfilename()
        if fSave is None:
            return 0
        ss = [self.main.leftShoe, self.main.rightShoe]
        sided = [True, False]
        for s, sid in zip(ss, sided):
            # self.rightShoe.pressDisplay = self.pressDisp
            # self.leftShoe.plotP = self.lPlot
            s.saveBinaryFile(fSave)

    def enterSafeMode(self):
        cmd = guT.createCMD(10)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def resetShoes(self):
        cmd = guT.createCMD(11)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

    def updateParams(self):
        pass


class RealTimeConsole(guT.ConsoleFrame):
    def __init__(self, parent, nLines=5):
        guT.ConsoleFrame.__init__(self, parent, nLines=nLines, parseCMD=self.parseMSG)
        self.ipAddress = {'IP_BASE': '192.168.0.', 'r': '192.168.0.103', 'l': '192.168.0.102'}
        self.recordings = {'r': False, 'l': False}
        # self.RIGHT_IP_ADDRESS = '192.168.0.103'
        # self.LEFT_IP_ADDRESS = '192.168.0.102'

    # def set(self, text):
    #     super().set(text)
    #     serchStr = 'Hello from'
    #     if serchStr in text:
    #         newIP = text[11:14]
    #         lab = text[16]
    #         self.ipAddress[lab] = self.ipAddress['IP_BASE'] + newIP

    def parseMSG(self, text):
        self.__changeIP(text)
        self.__checkRecording(text)

    def __changeIP(self, text):
        serchStr = 'Hello from'
        if serchStr in text:
            newIP = text[11:14]
            lab = text[16]
            self.ipAddress[lab] = self.ipAddress['IP_BASE'] + newIP

    def __checkRecording(self, text):
        serchStr = 'Recording'
        if serchStr in text:
            lab = text[-1]
            self.recordings[lab] = 'Started' in text


def wrap(angles):
    return (angles + np.pi) % (2 * np.pi) - np.pi


class realTimeApp(object):
    def __init__(self):
        self.root = tk.Tk()
        self.root.title('DeepSole System, Real Time')
        self.root.iconbitmap(r'./GUI/deepsole.ico')
        # objects
        self.theSend = tu.UDPsend()
        self.refreshTime = 10  # ms
        self.visualizeTime = 8  # s
        self.rightShoe = tu.smartShoe(False, keepAll=False)
        self.leftShoe = tu.smartShoe(True, keepAll=False)
        bottomFrame = tk.Frame(self.root)
        self.console = RealTimeConsole(bottomFrame)
        self.theRec = tu.UDPreceive(self.leftShoe, self.rightShoe, self.console, ver=2)
        self.model = None
        self.batchSize = 10
        # GUI
        titles = ['Press', 'Acc', 'Gyro', 'Euler', 'Phase']
        names = [self.rightShoe.names[3*j:3*j+3] for j in range(4)]
        # names = [['Toe ', 'Ball ', 'Heel'], ['ax', 'ay', 'az'], ['gx', 'gy', 'gz'], ['ro', 'pi', 'ya']]
        plotColor = [['fuchsia', 'orange', 'purple'],
                     ['Chartreuse', 'darkgreen', 'chocolate'],
                     ['coral', 'brown', 'crimson'],
                     ['blue', 'teal', 'cyan']]
        # self.yPredR = self.realTimeModel.yR
        # self.yCorrR = self.realTimeModel.yRc
        # self.yPredL = self.realTimeModel.yL
        # self.yCorrL = self.realTimeModel.yLc
        self.allValsR = [j for j in self.rightShoe.all]
        self.allValsL = [j for j in self.leftShoe.all]
        self.timestampR = [self.rightShoe.timestamp for _ in self.rightShoe.all]
        self.timestampL = [self.leftShoe.timestamp for _ in self.leftShoe.all]
        self.scalePlots = True
        topFrame = tk.Frame(self.root)
        topFrame.pack(fill=tk.BOTH, expand=tk.YES)
        topFrameL = tk.Frame(topFrame)
        topFrameR = tk.Frame(topFrame)
        topFrameR.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.BOTH)
        topFrameL.pack(side=tk.LEFT, expand=tk.YES, fill=tk.BOTH)
        self.rPlot = guT.PlotPanelPandas(topFrameL, 'R', titles, names, plotColor, self.timestampR,
                                         self.rightShoe.dataFrame, color='red', number2Plot=500, showTime=True,
                                         useScale=self.scalePlots)
        # self.rPlot = guT.PlotPanel(topFrameL, 'R', titles, names, plotColor, self.timestampR, self.allValsR, color='red',
        #                        number2Plot=500, showTime=True, useScale=self.scalePlots)
        self.rPlot.pack(fill=tk.BOTH, expand=tk.YES)
        # self.lPlot = guT.PlotPanel(topFrameL, 'L', titles, names, plotColor, self.timestampL, self.allValsL, color='blue',
        #                        number2Plot=500, showTime=True, useScale=self.scalePlots)
        self.lPlot = guT.PlotPanelPandas(topFrameL, 'L', titles, names, plotColor, self.timestampL,
                                         self.leftShoe.dataFrame, color='blue', number2Plot=500, showTime=True,
                                         useScale=self.scalePlots)
        self.lPlot.pack(fill=tk.BOTH, expand=tk.YES)
        self.filesFrame = tk.Frame(topFrameL)
        # 3d plots of orientation
        self.plot3DPanel = guT.PlotPanel3DPandas(topFrameR, 'Side', ['Left', 'Right'], [['Plot'], ['Plot']],
                                                 [['red'], ['blue']],
                                                 [self.leftShoe.dataFrame,
                                        self.rightShoe.dataFrame], figsize=(5, 5))
        self.plot3DPanel.pack(fill=tk.BOTH, expand=tk.YES)
        self.model = None
        self.shoeParams = None
        self.subID = 0
        self.freq = 100.0
        # fileNamesLabel = ['Model File']
        # buttonCmds = [self.loadModel]
        # browseTi = ['Select Model file']
        # browseTypes = [(("PB files", "*.pb"), ("all files", "*.*"))]
        # self.inputwB = []
        # for lab, bCMD, titl, typ in zip(fileNamesLabel, buttonCmds, browseTi, browseTypes):
        #     self.inputwB.append(guT.InputWithButton(self.filesFrame, lab, 'Load', bCMD, titl, typ))
        #     self.inputwB[-1].pack()
        # self.filesFrame.pack()
        labels = [['Ping', 'Change Freq'],
                  ['Start Recording', 'Change Name'],
                  ['Motor CMD'],
                  ['Save file', 'Update Params'],
                  ['Flash FW', 'Reset Shoes']]
        self.theCom = ControlFunctionsContainer(labels, self)

        bottomFrame.pack()
        # bottomLeftFrame = tk.Frame(self.root).pack(side=tk.LEFT)
        # bottomRightFrame = tk.Frame(self.root).pack(side=tk.RIGHT)
        buttonsCMDs = [[self.theCom.sendPing, self.theCom.changeFreq],
                       [self.theCom.recordCmd, self.theCom.reNameCmd],
                       [self.theCom.sendMotorCmd],
                       [self.theCom.saveDataCmd, self.theCom.updateParams],
                       [self.theCom.enterSafeMode, self.theCom.resetShoes]]
        self.control = guT.ButtonPanel(bottomFrame, labels, buttonsCMDs, width=25)
        self.theCom.buttons = self.control.buttons
        self.control.grid(row=0, column=0)
        self.pressDisp = dsm.ShoePressDisp(bottomFrame)
        self.pressDisp.grid(row=0, column=1)

        self.console.grid(row=0, column=2)
        self.oldTimeR = 0
        self.oldTimeL = 0

        # self.ctrlP.pack(side=tk.LEFT)
        # self.bottomLeft = tk.Frame(self.bottom)
        # self.bottomLeft.pack(side=tk.LEFT)
        # self.now = time.time()

    def updatePlots(self):
        fn = lambda x: wrap(x/8000)
        self.rPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=[['EUy', 'EUz', 'EUx'],
                                                                                         [fn]])
        self.lPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0, preprocess=[['EUy', 'EUz', 'EUx'],
                                                                                         [fn]])
        # self.rPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0)
        # self.lPlot.plotControlFromChecksTime(self.visualizeTime, extraT=1.0)
        self.plot3DPanel.plotControlFromChecks()
        # for s in [self.rightShoe, self.leftShoe]:
        #     self.pressDisp.updatePlot(toe=s.getPressVal(s.pToe),
        #                               ball=s.getPressVal(s.pBall),
        #                               heel=s.getPressVal(s.pHeel), axLeft=s.isLeft)
        # isL = False
        for pPlot, isL in zip([self.rPlot, self.lPlot], [False, True]):
            if pPlot.maxValsAux is None:
                continue
            windowVals = pPlot.all.iloc[-2:, :].copy()
            divAux = pPlot.maxValsAux - pPlot.minValsAux
            divAux[divAux == 0] = 1
            windowVals = (windowVals-pPlot.minValsAux) / (divAux)
            # 'pToe', 'pBall', 'pHeel'
            self.pressDisp.updatePlot(toe=np.clip(windowVals['pToe'].values[-1], 0, 1),
                                      ball=np.clip(windowVals['pBall'].values[-1], 0, 1),
                                      heel=np.clip(windowVals['pHeel'].values, 0, 1)
                                      [-1], axLeft=isL)


        # if not self.theCom.isRecord and (len(self.rightShoe.timestamp) > 100 or len(self.leftShoe.timestamp) > 100):
        #
        #     if len(self.rightShoe.timestamp) is self.oldTimeR:
        #         rm = 0.0
        #     else:
        #         rm = 1000.0 / np.diff(np.array(self.rightShoe.timestamp[self.oldTimeR:])).mean()
        #     if len(self.leftShoe.timestamp) is self.oldTimeL:
        #         lm = 0.0
        #     else:
        #         lm = 1000.0 / np.diff(np.array(self.leftShoe.timestamp[self.oldTimeL:])).mean()
        #     self.oldTimeR = len(self.rightShoe.timestamp)
        #     self.oldTimeL = len(self.leftShoe.timestamp)
        #     self.theCom.buttons['Start Recording']["text"] = "[%.1fHz]Stop recording[%.1fHz]" % (lm, rm)
        if not self.theCom.isRecord:
            self.theCom.buttons['Start Recording']["text"] = "[%.1fHz]Stop recording[%.1fHz]" % (self.lPlot.dt,
                                                                                                 self.rPlot.dt)

    def exportCMD(self, exportFlag=True):
        if self.yPred is None:
            e = self.predictCMD()
            if e:
                return True
        yRshoe, yLshoe = self.yPred
        if self.mat is None:
            laps = None
        else:
            laps = self.mat.lap
        self.shoeParams = tu.getAllTemporalParamsPandas(yRshoe, yLshoe, laps=laps, freq=self.freq, subID=self.subID, dt=0)
        root = tk.Tk()
        #
        # fileN = tk.filedialog.askopenfilename(parent=root, title='Select Shoe binary file',
        #                                       filetypes=(("Binary files", "*.bin"), ("all files", "*.*")))
        filePath = tk.filedialog.asksaveasfilename(parent=root, title='Save Paramaters as',
                                                   filetypes=(
                                                       ("Excel files", ("*.xls", "*.xlsx")), ("all files", "*.*")))
        root.withdraw()
        if filePath is None:
            return
        writer = pandas.ExcelWriter(filePath, engine='xlsxwriter')
        self.shoeParams.to_excel(writer)
        writer.save()
        writer.close()

    def exportRaw(self):
        if self.leftShoe is None or self.rightShoe is None:
            # warnings.warn('Shoe object cannot be empty, please load file')
            messagebox.showinfo('Cannot load file', 'Shoe object cannot be empty, please load file')
        sub = tu.ShoeSubject(self.leftShoe, self.rightShoe, self.subID, matInfo=self.mat)
        labels = ['Save Pickle', 'Save Matlab', 'Save Excel']
        buttonsCMDs = [sub.savePickle, sub.saveMatlab, sub.saveExcel]
        root = tk.Tk()
        control = guT.ButtonPanel(root, labels, buttonsCMDs)
        control.pack()

    def loopFunctions(self):
        self.updatePlots()
        self.root.after(self.refreshTime, self.loopFunctions)

    def startApp(self):
        # while True:
        #   self.updatePlots()
        #  self.root.update_idletasks()
        # self.root.update()
        self.startReceive()
        self.loopFunctions()
        self.root.protocol("WM_DELETE_WINDOW", self.stopApp)
        self.root.mainloop()

    def stopApp(self):
        # self.theRec.stopThread()
        self.root.destroy()

    def startReceive(self):
        self.theRec.startThread()


app = realTimeApp()
# app.startReceive()
app.startApp()
