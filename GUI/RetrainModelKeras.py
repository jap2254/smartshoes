import tensorflow as tf
import numpy as np
import Tools.Utils as tu
import Tools.NNUtils as ntu
import pandas
import warnings
import os
import tkinter as tk
import Tools.GUItools as guT
import warnings
from tensorflow.python.framework.ops import disable_eager_execution

#
disable_eager_execution()


def HS2per(hs, t, freq):
    y = np.zeros_like(t)
    hs2 = (hs * freq).astype(np.int)
    for st, en in zip(hs2[:-1], hs2[1:]):
        if en >= y.shape[0]:
            break
        aux = np.linspace(0, 1, en - st)
        y[st:en] = aux

    y = y[:en]
    return y


def retrainModel(originalModel, newModelName, filesParents, batchSize=5000, freq=50.0, maxEpochs=200, timeSize=25):
    fileNames = []
    for file in os.listdir(filesParents):
        if file.endswith(".csv"):
            fileNames.append(file[:-4])

    # batchSize = 5000
    # freq = 50.0
    # maxEpochs = 200
    # timeSize = 25
    # initialEpoch = 500

    print('Loading the data')
    xAll = []
    yAll = []
    for f in fileNames:
        [r, l] = tu.binaryFile2python(filesParents + '/' + f + '.bin')
        hs = pandas.read_csv(filesParents + '/' + f + '.csv', index_col=0)
        if len(r.ax) != 0:
            s = r.createPandasDataFrame(useSync=True, freq=freq, returnObj=True)
        elif len(l.ax) != 0:
            s = l.createPandasDataFrame(useSync=True, freq=freq, returnObj=True)
        else:
            warnings.warn('Shoe file is empty!!!!')
        t = s.index.values
        y = HS2per(np.squeeze(hs.values), t, freq)
        tmin = min([t.shape[0], y.shape[0]])
        t = t[:tmin]
        y = y[:tmin]
        s = s.values[:tmin]
        xAll.append(ntu.createbatch(s, timeSize))
        yAll.append(y.copy())

    xVal = xAll[-1]
    yVal = yAll[-1]
    xAll = np.concatenate(xAll[:-1], axis=0)
    yAll = np.concatenate(yAll[:-1], axis=0)

    shu = tu.randomOrder(xAll.shape[0])

    xAll = xAll[shu]
    yAll = yAll[shu]

    print('Retraining the model')

    model = tf.keras.models.load_model(originalModel)

    model.fit(x=xAll, y=yAll, batch_size=batchSize, epochs=maxEpochs, validation_split=0.3)

    model.save(newModelName)

    print('Model saved to % s' % newModelName)


def retrainModelMat(originalModel, newModelName, filesParents, batchSize=5000, freq=50.0, maxEpochs=200, timeSize=25,
                    usePercenatge=False):
    fileNames = []
    for file in os.listdir(filesParents):
        if file.endswith(".xlsx"):
            fileNames.append(file[:-5])

    # batchSize = 5000
    # freq = 50.0
    # maxEpochs = 200
    # timeSize = 25
    # initialEpoch = 500
    fileNames = np.unique(fileNames)
    print('Loading the data')
    xAll = []
    yAll = []
    for f in fileNames:
        lPath = filesParents + '/' + f + '_l.bin'
        rPath = filesParents + '/' + f + '_r.bin'
        [r, _] = tu.binaryFile2python(rPath)
        [_, l] = tu.binaryFile2python(lPath)
        r.createPandasDataFrame(useSync=True, freq=freq)
        l.createPandasDataFrame(useSync=True, freq=freq)
        matPath = filesParents + '/' + f + '.xlsx'
        try:
            matO = tu.MatObject(fName=matPath, freq=freq, newMat=False)
        except:
            matO = tu.MatObject(fName=matPath, freq=freq, newMat=True)
        yR = matO.binaryFunctions['R'].values
        yL = matO.binaryFunctions['L'].values
        xR = r.dataFrame.values
        xL = l.dataFrame.values
        laps = matO.lap.copy()
        tMax = min([yR.shape[0], yL.shape[0], xR.shape[0], xL.shape[0]])
        yR = yR[:tMax]
        yL = yL[:tMax]
        xR = xR[:tMax]
        xL = xL[:tMax]
        samplesPerSubject = tMax
        x, y, _ = ntu.createTemporalSamples(yR, yL, xR, xL, laps,
                                            freq, freq, samplesPerSubject, timeSize, timeSize, useP=usePercenatge)
        xAll.append(x.copy())
        yAll.append(y[:, -1].copy())

    # xVal = xAll[-1]
    # yVal = yAll[-1]
    xAll = np.concatenate(xAll, axis=0)
    yAll = np.concatenate(yAll, axis=0)

    shu = tu.randomOrder(xAll.shape[0])

    xAll = xAll[shu]
    yAll = yAll[shu]

    print('Retraining the model')

    model = tf.keras.models.load_model(originalModel)

    model.fit(x=xAll, y=yAll, batch_size=batchSize, epochs=maxEpochs, validation_split=0.3)

    model.save(newModelName)
    print('Model saved to % s' % newModelName)


class ReTrainApp(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title('Retrain Keras Model with files')
        self.iconbitmap(r'./GUI/deepsole.ico')
        TopFrame = tk.Frame(self)
        names = ['batchSize', 'freq', 'maxEpochs', 'timeSize']
        defVals = [1000, 50, 100, 25]
        okCMD = self.register(self.validateVal)
        self.allNames = names
        vld = (okCMD, '%P')
        self.valR = []
        for i, (n, dV) in enumerate(zip(names, defVals)):
            # label
            tk.Label(TopFrame, text=n).grid(row=0, column=i + 1)
            # box 1
            self.valR.append(tk.StringVar())
            tk.Entry(TopFrame, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                          column=i + 1)
            self.valR[-1].set(str(dV))

        TopFrame.pack()

        BottomFrame = tk.Frame(self)
        self.che = guT.CheckControl(BottomFrame, '', ['Percentage', 'Use Treadmill'], ax=None, defaultVal=[1, 1])
        self.che.pack()
        self._originalModel = None
        self._newModel = None
        self._parentPath = None
        # first we need an original model one
        oriModIn = guT.InputWithButton(BottomFrame, 'Original Model', 'Load', self.__setOriginalModel,
                                       'Select Original Keras Model', (("Keras Model", "*.h5"), ("all files", "*.*")))
        oriModIn.pack()
        newModIn = guT.InputWithButton(BottomFrame, 'New Model', 'Load', self.__setNewModel,
                                       'Save New Keras Model As', (("Keras Model", "*.h5"), ("all files", "*.*")),
                                       dialogType=1)
        newModIn.pack()
        filesPathIn = guT.InputWithButton(BottomFrame, 'Training Folder', 'Load', self.__setParentPath,
                                          'Training Folder',
                                          None, dialogType=2)
        filesPathIn.pack()

        bts = guT.ButtonPanel(BottomFrame, [['Train Model']], [[self.trainModel]])
        bts.pack()

        BottomFrame.pack()

    def validateVal(self, value):
        try:
            if value:
                # print(type(value))
                # print(value)
                v = int(value)
                return True
        except ValueError:
            return False

    def __setOriginalModel(self, val):
        self._originalModel = val
        a = 1

    def __setNewModel(self, val):
        self._newModel = val
        a = 1

    def __setParentPath(self, val):
        self._parentPath = val
        a = 1

    def trainModel(self):
        if self._parentPath is None or self._newModel is None or self._originalModel is None:
            warnings.warn('Please set all parameters')
            return
        vals = []
        for i, r in enumerate(self.valR):
            vals.append(int(r.get()))
        # ['batchSize', 'freq', 'maxEpochs', 'timeSize']
        modelTypes = np.array(self.che.getAllValues())
        if (modelTypes == 1).all():
            retrainModel(self._originalModel, self._newModel, self._parentPath, batchSize=vals[0], freq=vals[1],
                         maxEpochs=vals[2], timeSize=vals[3])
        else:
            retrainModelMat(self._originalModel, self._newModel, self._parentPath, batchSize=vals[0], freq=vals[1],
                            maxEpochs=vals[2], timeSize=vals[3], usePercenatge=modelTypes[0])
        print('Training ended')
        # self.stopApp()

    def startApp(self):
        # self.root.protocol("WM_DELETE_WINDOW", self.stopApp)
        self.mainloop()

    def stopApp(self):
        # self.theRec.stopThread()
        self.destroy()


app = ReTrainApp()
app.startApp()
