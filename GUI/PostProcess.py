import os
import sys
from tkinter import messagebox

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from Tools.GUItools import *
from Tools.Utils import *


class processApp(object):
    def __init__(self):
        self.root = tk.Tk()
        self.root.title('DeepSole System, Post-Processing')
        self.root.iconbitmap(r'./GUI/deepsole.ico')
        titles = ['Press', 'Acc', 'Gyro', 'Euler', 'Phase']
        names = [['Toe ', 'Ball ', 'Heel'], ['ax', 'ay', 'az'], ['gx', 'gy', 'gz'], ['ro', 'pi', 'ya'],
                 ['True', 'Predicted']]
        plotColor = [['fuchsia', 'orange', 'purple'],
                     ['Chartreuse', 'darkgreen', 'chocolate'],
                     ['coral', 'brown', 'crimson'],
                     ['blue', 'teal', 'cyan'],
                     ['gray', 'red']]
        self.scalePlots = True
        self.timestampR = [[] for _ in range(sum((len(it) for it in names)))]
        self.timestampL = [[] for _ in range(sum((len(it) for it in names)))]
        self.allValsR = [[] for _ in range(sum((len(it) for it in names)))]
        self.allValsL = [[] for _ in range(sum((len(it) for it in names)))]
        self.rPlot = PlotPanel(self.root, 'R', titles, names, plotColor, self.timestampR, self.allValsR, color='red',
                               number2Plot=0, showTime=False, useScale=self.scalePlots, useCheckFn=True)
        self.rPlot.pack(fill=tk.BOTH, expand=tk.YES)
        self.lPlot = PlotPanel(self.root, 'L', titles, names, plotColor, self.timestampL, self.allValsL, color='blue',
                               number2Plot=0, shareAxis=self.rPlot.axis, showTime=False, useScale=self.scalePlots,

                               useCheckFn=True)
        self.lPlot.pack(fill=tk.BOTH, expand=tk.YES)
        self.filesFrame = tk.Frame(self.root)
        self.rightShoe = None
        self.leftShoe = None
        self.model = None
        self.mat = None
        self.shoeParams = None
        self.yPred = None
        self.subID = 0
        self.matParams = None
        self.freq = 100.0

        fileNamesLabel = ['Shoe R    ', 'Shoe L   ', 'Mat File  ']
        buttonCmds = [self.loadRightShoe, self.loadLeftShoe, self.loadMatFile]
        browseTi = ['Select Right Shoe binary file', 'Select Left Shoe binary file', 'Select Mat file']
        browseTypes = [(("Binary files", "*.bin"), ("all files", "*.*")),
                       (("Binary files", "*.bin"), ("all files", "*.*")),
                       (("Excel files", ("*.xls", "*.xlsx")), ("all files", "*.*"))]
        inputwB = []
        for lab, bCMD, titl, typ in zip(fileNamesLabel, buttonCmds, browseTi, browseTypes):
            inputwB.append(InputWithButton(self.filesFrame, lab, 'Load', bCMD, titl, typ))
            inputwB[-1].pack()
        self.filesFrame.pack()
        labels = [['Flip shoes', 'Export Gait Excel', 'Export raw']]
        buttonsCMDs = [[self.flipShoes, self.exportCMD, self.exportRaw]]
        self.control = ButtonPanel(self.root, labels, buttonsCMDs)
        self.control.pack()
        # self.ctrlP.pack(side=tk.LEFT)
        # self.bottomLeft = tk.Frame(self.bottom)
        # self.bottomLeft.pack(side=tk.LEFT)
        # self.now = time.time()

    def _updateList(self):
        self.allValsR = self.rPlot.all
        self.allValsL = self.lPlot.all

    def updatePlots(self):
        self.rPlot.plotControlFromChecks()
        self.lPlot.plotControlFromChecks()

    def flipShoes(self):
        if self.leftShoe is None and self.rightShoe is None:
            messagebox.showinfo('Cannot load file', 'Shoe object cannot be empty, please load file')
            return

        if self.rightShoe is not None:
            self.rightShoe.isLeft = True
            self.rightShoe.makeBinaryFileAgain()
        if self.leftShoe is not None:
            self.leftShoe.isLeft = False
            self.leftShoe.makeBinaryFileAgain()
        root = tk.Tk()
        fName = tk.filedialog.asksaveasfilename(parent=root, title='Template Name')
        root.withdraw()
        if fName is None:
            return
        if self.rightShoe is not None:
            self.rightShoe.saveBinaryFile(fName)
        if self.leftShoe is not None:
            self.leftShoe.saveBinaryFile(fName)

    def exportCMD(self, exportFlag=True):
        if self.yPred is None:
            e = self.predictCMD()
            if e:
                return True
        yRshoe, yLshoe = self.yPred
        if self.mat is None:
            laps = None
        else:
            laps = self.mat.lap
        self.shoeParams = getAllTemporalParamsPandas(yRshoe, yLshoe, laps=laps, freq=self.freq, subID=self.subID, dt=0)
        root = tk.Tk()
        #
        # fileN = tk.filedialog.askopenfilename(parent=root, title='Select Shoe binary file',
        #                                       filetypes=(("Binary files", "*.bin"), ("all files", "*.*")))
        filePath = tk.filedialog.asksaveasfilename(parent=root, title='Save Paramaters as',
                                                   filetypes=(
                                                       ("Excel files", ("*.xls", "*.xlsx")), ("all files", "*.*")))
        root.withdraw()
        if filePath is None:
            return
        writer = pandas.ExcelWriter(filePath, engine='xlsxwriter')
        self.shoeParams.to_excel(writer)
        writer.save()
        writer.close()

    def exportRaw(self):
        if self.leftShoe is None or self.rightShoe is None:
            messagebox.showinfo('Cannot load file', 'Shoe object cannot be empty, please load file')

        sub = ShoeSubject(self.leftShoe, self.rightShoe, self.subID, matInfo=self.mat, cutSync=True)
        labels = ['Save Pickle', 'Save Matlab', 'Save Excel']
        buttonsCMDs = [sub.savePickle, sub.saveMatlab, sub.saveExcel]
        root = tk.Tk()
        control = ButtonPanel(root, [labels], [buttonsCMDs])
        control.pack()

    def loadRightShoe(self, fN):
        [self.rightShoe, _] = binaryFile2python(fN, V=2)
        # self.rightShoe.dataFrame = None
        # t, m = self.rightShoe.makeMatrix(freq=self.freq)
        self.rightShoe.createPandasDataFrame(freq=self.freq, useSync=True)
        m = self.rightShoe.dataFrame.values
        t = self.rightShoe.dataFrame.index.values * 1000.0
        for j in range(m.shape[1]):
            self.allValsR[j][:] = m[:, j]
            self.timestampR[j][:] = t
        self.updatePlots()
        # self.rPlot.all = [m[:, j] for j in range(m.shape[1])]
        # self.rPlot.timestamp = [t for _ in range(m.shape[1])]
        # self.rightShoe.isLeft = True
        # self.rightShoe.makeBinaryFileAgain()
        # self._updateList()

    def loadLeftShoe(self, fN):
        [_, self.leftShoe] = binaryFile2python(fN, V=2)
        # self.leftShoe.dataFrame = None
        # t, m = self.leftShoe.makeMatrix(freq=self.freq)
        self.leftShoe.createPandasDataFrame(freq=self.freq, useSync=True)
        m = self.leftShoe.dataFrame.values
        t = self.leftShoe.dataFrame.index.values * 1000.0
        for j in range(m.shape[1]):
            self.allValsL[j][:] = m[:, j]
            self.timestampL[j][:] = t
        self.updatePlots()
        # self.lPlot.all = [m[:, j] for j in range(m.shape[1])]
        # self.lPlot.timestamp = [t for _ in range(m.shape[1])]
        # self._updateList()

    def loadMatFile(self, fN):
        self.mat = MatObject(fN, newMat=False, freq=self.freq)
        self.allValsR[12][:] = self.mat.yR
        self.allValsL[12][:] = self.mat.yL
        self.timestampR[12][:] = self.mat.t * 1000.0
        self.timestampL[12][:] = self.mat.t * 1000.0
        self.updatePlots()
        # self.rPlot.all.append(self.mat.yR)
        # self.lPlot.all.append(self.mat.yL)
        # self.rPlot.timestamp.append(self.mat.t)
        # self.lPlot.timestamp.append(self.mat.t)
        # self._updateList()
        self.matParams = getAllTemporalParamsPandas(self.mat.yR, self.mat.yL, laps=self.mat.lap, freq=self.freq,
                                                    subID=self.subID)

    def startApp(self):
        # while True:
        #   self.updatePlots()
        #  self.root.update_idletasks()
        # self.root.update()
        self.root.protocol("WM_DELETE_WINDOW", self.stopApp)
        self.root.mainloop()

    def stopApp(self):
        # self.theRec.stopThread()
        self.root.destroy()


app = processApp()
app.startApp()
