import socket
import struct
import argparse



def createCMD(inst, te=None):
    if inst == 4:
        c = struct.pack('5c ' + str(len(te)) + 's 3c', bytes([0xA]), bytes([0xD]), bytes([0xA]), bytes([inst]),
                        bytes([len(te)]), te.encode('utf-8'), bytes([0xC]), bytes([0xD]), bytes([0xF]))
    elif inst == 8:
        c = struct.pack('14c', bytes([0xA]), bytes([0xD]), bytes([0xA]),
                        bytes([inst]), bytes([te[0]]), bytes([te[1]]), bytes([te[2]]), bytes([te[3]]), bytes([te[4]]),
                        bytes([te[5]]), bytes([te[6]]), bytes([0xC]), bytes([0xD]), bytes([0xF]))
    elif inst == 5 or inst == 14:
        c = struct.pack('9c', bytes([0xA]), bytes([0xD]), bytes([0xA]), bytes([inst]), bytes([te[0]]), bytes([te[1]]),
                        bytes([0xC]), bytes([0xD]), bytes([0xF]))
    elif inst == 0xC:
        c = struct.pack('8c', bytes([0xA]), bytes([0xD]), bytes([0xA]), bytes([inst]), bytes([te]), bytes([0xC]),
                        bytes([0xD]), bytes([0xF]))
    else:
        c = struct.pack('7c', bytes([0xA]), bytes([0xD]), bytes([0xA]), bytes([inst]), bytes([0xC]), bytes([0xD]),
                        bytes([0xF]))

    return c


class UDPsend(object):
    """
    Object for sending data through UDP
    """

    def __init__(self):
        self.sock = socket.socket(socket.AF_INET,  # Internet
                                  socket.SOCK_DGRAM)  # UDP

    def send(self, msg, addr=('192.168.42.1', 12354)):
        """
        :param msg: message to send
        :param addr: tuple of length 2, (ip address, port number)
        :return:
        """
        self.sock.sendto(msg, addr)

    def closeSocket(self):
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

    def reOpenSocket(self):
        self.sock = socket.socket(socket.AF_INET,  # Internet
                                  socket.SOCK_DGRAM)  # UDP


def main(_):
    theSend = UDPsend()
    cmd = createCMD(2)
    theSend.send(cmd, addr=('192.168.0.255', 12354))
    theSend.closeSocket()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    FLAGS, unparsed = parser.parse_known_args()
    main(unparsed)