import argparse
import tkinter as tk
import Tools.GUItools as guT
from GUI.DataCollectionApp2 import DataAPP
from tensorflow.python.framework.ops import disable_eager_execution
import pandas
import time
import numpy as np
import tensorflow as tf
import struct
import GUI.DeepSoleModulesTensorflow as dms_tf
import GUI.DeepSoleModules as guiMods


def makePhaseVibrationFrame(parent, udpSend, plotsDict, ipAddressDict, predictionDF):
    if len(plotsDict) < 2:
        return None

    nickNames = guiMods.AssingBoardsNicknames(parent, [['Left', 'Right']], plotsDict, 'PhaseVibration')

    if len(nickNames.values) == 0:
        return None
    r = nickNames.values['Right']['Board']
    l = nickNames.values['Left']['Board']
    return dms_tf.PhaseVibrationFrame(parent, udpSend, predictionDF, plotsDict[r]['shoe'], plotsDict[l]['shoe'],
                                      ipAddressDict[r], ipAddressDict[l])


def makeMuscleVibartionFrame(parent, udpSend, plotsDict, ipAdressDict, predictionDF):
    if len(plotsDict) < 6:
        return
    nickNames = guiMods.AssingBoardsNicknames(parent,
                                              [[s + ' ' + m for m in ['Tibial', 'Rectus', 'Femoris']]
                                               for s in ['Left', 'Right']],
                                              plotsDict, 'MuscleVibration')
    ranges = [[.40, .60], [.60, .75], [.30, .40],
              [00, .10], [.10, .30], [.87, 1.00]]
    for k, r in zip(nickNames.values, ranges):
        nickNames.values[k]['IP'] = ipAdressDict[nickNames.values[k]['Board']]
        nickNames.values[k]['Range'] = r
    return dms_tf.MuscleVibrationFrame(parent, udpSend, predictionDF, nickNames.values)


class PredictionApp(DataAPP):
    def __init__(self, **kwargs):
        super(PredictionApp, self).__init__(self.startPrediction, self.savePreFrames, **kwargs)
        self.plotColor = ['fuchsia', 'orange', 'purple', 'Chartreuse', 'darkgreen', 'chocolate', 'coral', 'brown',
                          'crimson', 'blue', 'teal', 'cyan']
        self.model = dms_tf.KerasPredictionModelProto(self.root.theSend, sendUDP=True, startTime=self.startTime)
        self.mainFrame.pack_forget()
        self.mainFrame.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
        self.predicitonFrame = tk.Frame(self)
        self.predicitonFrame.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)
        self.predicitonFrameLeft = tk.Frame(self)
        self.predicitonFrameLeft.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
        # self.predicitonFrameRight = tk.Frame(self)
        # self.predicitonFrameRight.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)
        fileNamesLabel = ['Model File']
        buttonCmds = [self.loadModel]
        browseTi = ['Select Model file']
        browseTypes = [(("Keras files", "*.h5"), ("all files", "*.*"))]
        self.inputwB = []
        for lab, bCMD, titl, typ in zip(fileNamesLabel, buttonCmds, browseTi, browseTypes):
            self.inputwB.append(guT.InputWithButton(self.predicitonFrameLeft, lab, 'Load', bCMD, titl, typ))
            self.inputwB[-1].pack()
        self.predPlot = None
        chCMD = lambda x: self.model.changeSendOption(x == 1)
        vibrationsDict = {'Phase Vibration': lambda x: makePhaseVibrationFrame(x, self.udpSend,
                                                                               self.root.plotPanels.plots,
                                                                               self.root.console.ipAddress,
                                                                               self.model.prediction),
                          'Muscle Vibration': lambda x: makeMuscleVibartionFrame(x, self.udpSend,
                                                                                 self.root.plotPanels.plots,
                                                                                 self.root.console.ipAddress,
                                                                                 self.model.prediction)}
        self.postPredictionFrame = dms_tf.PostPredictionFrameProto(self.predicitonFrameLeft, chCMD, self.udpSend,
                                                                   vibrationsDict)
        self.postPredictionFrame.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)

    def loadModel(self, modelPath):
        class ModelWindow(tk.Toplevel):
            def __init__(self, parent):
                tk.Toplevel.__init__(self, parent)
                # self.root = tk.Toplevel(self)
                self.vals = None
                fr = tk.Frame(self)
                fr.pack()
                self.valR = []
                names = ['Freq', 'Time Size']
                defVals = ['50', '25']
                self.allNames = names
                okCMD = self.register(self.validateVal)
                vld = (okCMD, '%P')
                for i, (n, v) in enumerate(zip(names, defVals)):
                    # label
                    tk.Label(fr, text=n).grid(row=0, column=i + 1)
                    # box 1
                    self.valR.append(tk.StringVar())
                    tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                            column=i + 1)
                    self.valR[-1].set(v)
                self.che = guT.CheckControl(self, '', ['Start Predicting', 'Do Argmax'], ax=None, defaultVal=[1, 0])
                self.che.pack()
                frBot = tk.Frame(self)
                tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
                tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
                frBot.pack()
                self.protocol("WM_DELETE_WINDOW", self.cancelB)
                self.transient(parent)
                self.grab_set()
                parent.wait_window(self)

            def validateVal(self, value):
                try:
                    if value:
                        # print(type(value))
                        # print(value)
                        v = int(value)
                        return True
                except ValueError:
                    return False

            def okB(self):
                self.vals = np.zeros((len(self.allNames) + 2), dtype=np.int8)
                for i, r in enumerate(self.valR):
                    self.vals[i] = int(r.get())
                self.vals[-2:] = self.che.getAllValues()[:2]
                self.destroy()

            def cancelB(self):
                self.vals = None
                self.destroy()

        params = ModelWindow(self)
        if params.vals is None:
            print('Cancel was pressed')
            return

        # print(params.vals)
        print(modelPath)
        self.model.loadModel(modelPath, params.vals[0], params.vals[1], params.vals[3])
        if params.vals[2] == 1:
            self.startPrediction()

    def savePreFrames(self, fileName):
        self.postPredictionFrame.saveBinaryFile(fileName)
        self.model.saveBinaryFile(fileName)

    def startPrediction(self):
        if self.model.model is None:
            return
        if self.model.predict:

            self.predPlot.pack_forget()
            self.model.stopPrediction()
            self.control.buttons['Start Prediction']["text"] = "Start Prediction"
        else:
            self.control.buttons['Start Prediction']["text"] = "Stop Prediction"
            self.control.buttons['Start Prediction']["state"] = "normal"
            dataFramesdict = {}
            boards2use = guiMods.Boards4pred(self, self.root.plotPanels.plots)
            for k, useBoard in zip(self.root.plotPanels.plots, boards2use.vals):
                if useBoard == 1:
                    dataFramesdict[k] = self.root.plotPanels.plots[k]['shoe'].dataFrame
            self.model.addDataframes(dataFramesdict)
            self.predPlot = guT.PlotPanelPandas(self.predicitonFrameLeft, 'pred', ['pred'],
                                                [[k for k in self.model._predictionDataFrames]],
                                                [self.plotColor[:len((self.model._predictionDataFrames))]],
                                                self.model.prediction.index, self.model.prediction, color='green',
                                                number2Plot=500, showTime=True, useScale=False, multiplier=10)
            self.predPlot.pack(fill=tk.BOTH, expand=tk.YES)
            self.model.startPrediction()

    def loopFunctions(self):
        now = time.time()
        self.model.updateFrame()
        # tt = int((time.time() - now) * 1000)
        # print('Model update took %d ms' % tt)
        # now2 = time.time()
        self.root.updateFunction()
        # tt = int((time.time() - now2) * 1000)
        # print('Root update took %d ms' % tt)
        # now2 = time.time()
        self.postPredictionFrame.updateFunctions()
        # tt = int((time.time() - now2) * 1000)
        # print('PostPrediction update took %d ms' % tt)
        if self.model.predict:
            # now2 = time.time()
            self.predPlot.plotControlFromChecksTime(self.root.visualizeTime, extraT=1.0, preprocess=None)
            # tt = int((time.time() - now2) * 1000)
            # print('PredictionPlot update took %d ms' % tt)
        # self.root.theRec.receiveData()
        usedTime = int((time.time() - now) * 1000)
        # print(usedTime)
        rT = self.refreshTime - usedTime
        if rT < 1:
            # print('Finished late by %d ms' % -rT)
            rT = 1
        self.after(rT, self.loopFunctions)

        # super(PredictionApp, self).loopFunctions()


if __name__ == '__main__':
    config = tf.compat.v1.ConfigProto(gpu_options=tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.8))
    config.gpu_options.allow_growth = True
    session = tf.compat.v1.Session(config=config)
    tf.compat.v1.keras.backend.set_session(session)
    disable_eager_execution()

    CLIENT_IP = '192.168.0.255'
    CLIENT_PORT = 24681

    app = PredictionApp()
    # app.startReceive()
    app.startApp()
