import pickle
import time
import tkinter as tk
import matplotlib.image as mpimg
import matplotlib.patches as patches
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import Tools.GUItools as guT
import numpy as np
import Tools.Utils as tu
from os import path
from tkinter import ttk
from tkinter import simpledialog


class ShoePressDisp(tk.Frame):
    class fakeRec(object):
        def __init__(self):
            a = 1

        def remove(self):
            pass

    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        # img are 700x500
        self.rImg = mpimg.imread('right.png')
        self.lImg = mpimg.imread('left.png')
        f = plt.figure(figsize=(3.7, 2), dpi=100)
        # f = plt.figure.Figure(figsize=(3.7, 2), dpi=100)
        self.axisL = f.add_subplot(121)
        # self.axisL.axis('equal')
        self.axisL.axis('off')
        self.axisR = f.add_subplot(122)
        self.axisR.axis('off')
        self.canvas = FigureCanvasTkAgg(f, self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.canvas._tkcanvas.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.axisR.imshow(self.rImg)
        self.axisL.imshow(self.lImg)
        self.rects = [ShoePressDisp.fakeRec() for _ in range(3)]
        self.updateVal = tk.IntVar()
        self.updateVal.set(1)
        self.check = tk.Checkbutton(self, text='Update Pressure', variable=self.updateVal, foreground='black')
        self.check.pack()
        self.oldValues = [1.0, 1.0, 1.0]
        self.updatePlot(0.0, 0.0, 0.0)
        self.updatePlot(0.0, 0.0, 0.0, axLeft=False)

    def updatePlot(self, toe=1, ball=1, heel=1, axLeft=True, useAlpha=True):
        # toe is 0 - 200
        # ball is 201 - 400
        # heel is 500 - 700
        # axLeft = True is left
        # axLeft = False is right

        # for a,im in zip([self.axisL,self.axisR],[self.lImg,self.rImg]):
        if self.updateVal.get() == 0 or all([x == y for x, y in zip(self.oldValues, [toe, ball, heel])]):
            return
        self.oldValues = [toe, ball, heel]
        if axLeft:
            a = self.axisL
            im = self.lImg
        else:
            a = self.axisR
            im = self.rImg
        # a.clear()
        [p.remove() for p in reversed(self.rects)]
        if useAlpha:
            toeRec = patches.Rectangle((0, 0), 400, 200, fill=True, color='r', alpha=(1 - toe), zorder=-1)
            ballRec = patches.Rectangle((0, 200), 400, 200, fill=True, color='r', alpha=(1 - ball), zorder=-1)
            heelRec = patches.Rectangle((0, 400), 400, 300, fill=True, color='r', alpha=(1 - heel), zorder=-1)
        else:
            toeRec = patches.Rectangle((0, 0), 400, 200, fill=True, color=str(1 - toe), zorder=-1)
            ballRec = patches.Rectangle((0, 200), 400, 200, fill=True, color=str(1 - ball), zorder=-1)
            heelRec = patches.Rectangle((0, 400), 400, 300, fill=True, color=str(1 - heel), zorder=-1)
        self.rects = [toeRec, ballRec, heelRec]
        for re in self.rects:
            a.add_patch(re)
        # a.imshow(im)
        # a.axis('off')
        self.canvas.draw()


class ModelOptions(tk.Toplevel):
    def __init__(self, parent):
        tk.Toplevel.__init__(self, parent)
        # self.root = tk.Toplevel(self)
        self.vals = None
        fr = tk.Frame(self)
        fr.pack()
        # batchSize = 500, nSams = 100, samplesBack = 100,
        # verbose = False, sizeOld = 99, justRun = False, useOld = False, flipTime = False
        # outputVarName = "output_node2:0", V = 2,
        # outputVarName2 = "yPast_exp:0"
        self.varNames = ['inputVarName', 'outputVarName', 'outputVarName2', 'Time Size', 'Batch Size']
        stringVals = ['input_feed:0', 'output_node2:0', 'ys_exp:0', '100', '20']
        self.strVals = guT.StringInputs(fr, self.varNames, defaultVals=stringVals, dtype=np.str, useInt=False)
        self.strVals.pack()
        self.checkFrame = tk.Frame(self)
        self.checkFrame.pack(side=tk.LEFT)
        # self.checkFrame.grid(row=0,column=1)
        titles = ['Press', 'Acc', 'Gyro', 'Euler']
        names = [['Toe ', 'Ball ', 'Heel'], ['ax', 'ay', 'az'], ['gx', 'gy', 'gz'], ['ro', 'pi', 'ya']]
        plotColor = [['fuchsia', 'orange', 'purple'],
                     ['Chartreuse', 'darkgreen', 'chocolate'],
                     ['coral', 'brown', 'crimson'],
                     ['blue', 'teal', 'cyan']]
        self.checks = []
        for t, n, c in zip(titles, names, plotColor):
            self.checks.append(guT.CheckControl(self.checkFrame, t, n, ax=None, color=c, defaultVal=[1]))
            # self.checks[-1].grid(row=0,column=len(self.checks))
            self.checks[-1].pack(side=tk.LEFT)

        frBot = tk.Frame(self)
        tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
        tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
        frBot.pack()
        self.protocol("WM_DELETE_WINDOW", self.cancelB)
        self.transient(parent)
        self.grab_set()
        parent.wait_window(self)
        # root.quit()

    def okB(self):
        self.vals = {}
        strV = self.strVals.readValues()
        for n, v in zip(self.varNames, strV):
            self.vals[n] = v
        self.vals['features'] = np.where([v for vs in self.checks for v in vs.getAllValues()])
        self.destroy()

    def cancelB(self):
        self.vals = None
        self.destroy()


class ControlPanel(tk.Frame):
    def __init__(self, parent, theSend, console, soleObjects, plotsContainer: guT.PlotPanelPandasContainer = None,
                 predictionFn=None, saveFn=None, STREAM_IP_ADDRESS='192.168.0.255', startTime=[0], **kwargs):
        super(ControlPanel, self).__init__(parent, **kwargs)
        self.soleObjects = soleObjects
        self.theSend = theSend
        self.saveFn = saveFn
        labels = [['Ping', 'Change Freq'],
                  ['Start Recording', 'Change File Name'],
                  ['Send Start', 'Send Stop'],
                  ['Motor CMD', 'Change Boards ID'],
                  ['Save file', 'Start Prediction'],
                  ['Flash FW', 'Reset Shoes']]
        self.theCom = self.ControlFunctionsContainer(labels, theSend, console, parent, soleObjects, plotsContainer,
                                                     saveFn, STREAM_IP_ADDRESS=STREAM_IP_ADDRESS, startTime=startTime)
        buttonsCMDs = [[self.theCom.sendPing, self.theCom.changeFreq],
                       [self.theCom.recordCmd, self.theCom.reNameCmd],
                       [self.theCom.sendSendStart, self.theCom.sendSendStop],
                       [self.theCom.sendMotorCmd, self.theCom.SignalBoards],
                       [self.theCom.saveDataCmd, predictionFn],
                       [self.theCom.enterSafeMode, self.theCom.resetShoes]]
        self.control = guT.ButtonPanel(self, labels, buttonsCMDs, width=25)
        labels = ['Start Recording', 'Change File Name',
                  'Motor CMD', 'Change Boards ID',
                  'Save file', 'Start Prediction']
        self.control.enableDisableButtons(labels, "disabled")
        # if predictionFn is None:
        #     self.control.buttons['Start Prediction']["state"] = "disabled"
        self.theCom.buttons = self.control.buttons
        self.control.pack()

    def enableButtons(self):
        labels = ['Start Recording', 'Change File Name', 'Motor CMD', 'Change Boards ID', 'Save file',
                  'Flash FW', 'Reset Shoes']
        self.control.enableDisableButtons(labels, "normal")

    class ControlFunctionsContainer(object):
        def __init__(self, labels, theSend, console: guT.ConsoleFramePlus, parent, soleObjects,
                     plotsContainer: guT.PlotPanelPandasContainer = None, saveFn=None, STREAM_IP_ADDRESS='192.168.0.255',
                     startTime=[0]):
            # self.RIGHT_IP_ADDRESS = '192.168.0.103'
            # self.LEFT_IP_ADDRESS = '192.168.0.102'
            self.saveFn = saveFn
            self.plotsContainer = plotsContainer
            self.soleObjects = soleObjects
            self.STREAM_IP_ADDRESS = STREAM_IP_ADDRESS
            self.parent = parent
            # self.soleObjects = soleObjects
            self.console = console
            self.theSend = theSend
            self.buttons = None
            self.labels = labels
            self.isRecord = True
            self.isStream = True
            self.isPredict = True
            self.fileName = 'file%d' % (int(time.time()))
            self.startTime = startTime
            self.boardsRecording = None

        class motorControl(tk.Toplevel):
            def __init__(self, parent, soleObjects):
                tk.Toplevel.__init__(self, parent)
                # self.root = tk.Toplevel(self)
                self.vals = None
                names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
                self.allNames = names
                okCMD = self.register(self.validateVal)
                vld = (okCMD, '%P')
                fr = tk.Frame(self)
                fr.pack()
                self.valAll = {}
                for i, k in enumerate(soleObjects):
                    tk.Label(fr, text=k).grid(row=i + 1)
                    self.valAll[k] = {}
                # tk.Label(fr, text='L').grid(row=2)
                # self.valL = []
                for i, n in enumerate(names):
                    tk.Label(fr, text=n).grid(row=0, column=i + 1)
                    for j, k in enumerate(soleObjects):
                        # label
                        # box 1
                        self.valAll[k][n] = tk.StringVar()
                        tk.Entry(fr, textvariable=self.valAll[k][n], validate='focusout',
                                 validatecommand=vld).grid(row=j + 1, column=i + 1)
                        self.valAll[k][n].set('0')
                frBot = tk.Frame(self)
                tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
                tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
                frBot.pack()
                self.protocol("WM_DELETE_WINDOW", self.cancelB)
                self.transient(parent)
                self.grab_set()
                parent.wait_window(self)
                # root.quit()

            def validateVal(self, value):
                try:
                    if value:
                        # print(type(value))
                        # print(value)
                        v = int(value)
                        return True
                except ValueError:
                    return False

            def okB(self):
                self.vals = np.zeros((len(self.allNames), len(self.valAll)), dtype=np.int8)
                # for i, (r, l) in enumerate(zip(self.valR, self.valL)):
                #     self.vals[i, :] = np.array([int(r.get()), int(l.get())], dtype=np.int8)
                for i, row in enumerate(self.allNames):
                    for j, col in enumerate(self.valAll):
                        self.vals[i, j] = self.valAll[col][row].get()

                self.destroy()

            def cancelB(self):
                self.vals = None
                self.destroy()

        class FreqChange(tk.Toplevel):
            def __init__(self, parent):
                tk.Toplevel.__init__(self, parent)
                # self.root = tk.Toplevel(self)
                self.vals = None
                fr = tk.Frame(self)
                fr.pack()
                self.valR = []
                names = ['Freq']
                self.allNames = names
                okCMD = self.register(self.validateVal)
                vld = (okCMD, '%P')
                for i, n in enumerate(names):
                    # label
                    tk.Label(fr, text=n).grid(row=0, column=i + 1)
                    # box 1
                    self.valR.append(tk.StringVar())
                    tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                            column=i + 1)
                    self.valR[-1].set('100')
                self.che = guT.CheckControl(self, '', ['Save params'], ax=None, defaultVal=[1])
                self.che.pack()
                frBot = tk.Frame(self)
                tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
                tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
                frBot.pack()
                self.protocol("WM_DELETE_WINDOW", self.cancelB)
                self.transient(parent)
                self.grab_set()
                parent.wait_window(self)
                # root.quit()

            def validateVal(self, value):
                try:
                    if value:
                        # print(type(value))
                        # print(value)
                        v = int(value)
                        return True
                except ValueError:
                    return False

            def okB(self):
                self.vals = np.zeros((len(self.allNames) + 1), dtype=np.int8)
                for i, r in enumerate(self.valR):
                    self.vals[i] = int(r.get())
                self.vals[-1] = self.che.getAllValues()[0]
                self.destroy()

            def cancelB(self):
                self.vals = None
                self.destroy()

        class assignControl(tk.Toplevel):
            def __init__(self, parent):
                tk.Toplevel.__init__(self, parent)
                # self.root = tk.Toplevel(self)
                self.vals = None
                fr = tk.Frame(self)
                fr.pack()
                tk.Label(fr, text='R').grid(row=1)
                tk.Label(fr, text='L').grid(row=2)
                self.valR = []
                self.valL = []
                names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
                self.allNames = names
                okCMD = self.register(self.validateVal)
                vld = (okCMD, '%P')
                for i, n in enumerate(names):
                    # label
                    tk.Label(fr, text=n).grid(row=0, column=i + 1)
                    # box 1
                    self.valR.append(tk.StringVar())
                    tk.Entry(fr, textvariable=self.valR[-1], validate='focusout', validatecommand=vld).grid(row=1,
                                                                                                            column=i + 1)
                    self.valR[-1].set('0')
                    # box 2
                    self.valL.append(tk.StringVar())
                    tk.Entry(fr, textvariable=self.valL[-1], validate='focusout', validatecommand=vld).grid(row=2,
                                                                                                            column=i + 1)
                    self.valL[-1].set('0')
                frBot = tk.Frame(self)
                tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
                tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
                frBot.pack()
                self.protocol("WM_DELETE_WINDOW", self.cancelB)
                self.transient(parent)
                self.grab_set()
                parent.wait_window(self)
                # root.quit()

            def validateVal(self, value):
                try:
                    if value:
                        # print(type(value))
                        # print(value)
                        v = int(value)
                        return True
                except ValueError:
                    return False

            def okB(self):
                self.vals = np.zeros((len(self.allNames), 2), dtype=np.int8)
                for i, (r, l) in enumerate(zip(self.valR, self.valL)):
                    self.vals[i, :] = np.array([int(r.get()), int(l.get())], dtype=np.int8)

                self.destroy()

            def cancelB(self):
                self.vals = None
                self.destroy()

        def SignalBoards(self):
            # cmd = guT.createCMD(15)
            # self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
            ReassignBoards(self.parent, self.console.ipAddress, self.theSend, self.plotsContainer)

        def sendPing(self):
            cmd = guT.createCMD(1)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

        def changeFreq(self):
            self.mW = self.FreqChange(self.parent)
            if self.mW.vals is None:
                print('Cancel was pressed')
            else:
                # send motor cmd
                print(self.mW.vals)
                cmd = guT.createCMD(14, te=self.mW.vals)
                self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

        # def reassign(self):
        #     cmd = guT.createCMD(5, [108, 1])
        #     # cmd = guT.createCMD(14, 10)
        #     # self.theSend.send(cmd, addr=(self.LEFT_IP_ADDRESS, 12354))
        #     self.theSend.send(cmd, addr=(self.RIGHT_IP_ADDRESS, 12354))

        def recordCmd(self):
            if self.isRecord:
                n = 2
                self.buttons['Start Recording']["text"] = "Stop recording"
                for k in self.soleObjects:
                    self.soleObjects[k]['shoe'].resetShoe()
                    self.soleObjects[k]['shoe'].binaryFile = bytes(0)
                # self.main.leftShoe.resetShoe()
                # self.main.rightShoe.resetShoe()
                # self.main.leftShoe.binaryFile = bytes(0)
                # self.main.rightShoe.binaryFile = bytes(0)
                if self.boardsRecording is None:
                    self.boardsRecording = Boards4pred(self.parent, self.soleObjects)
                self.buttons['Start Recording'].config(bg='green')
                cmd = guT.createCMD(n)
                for k, useBoard in zip(self.soleObjects, self.boardsRecording.vals):
                    if useBoard == 1:
                        self.theSend.send(cmd, addr=(self.console.ipAddress[k], 12354))
                # self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
                self.startTime[0] = time.time()
            else:
                n = 3
                self.buttons['Start Recording']["text"] = "Start recording"
                self.buttons['Start Recording'].config(bg='yellow')
                cmd = guT.createCMD(n)
                self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
                self.saveDataCmd()
            self.isRecord = not self.isRecord

            self.streamCmd()

        def streamCmd(self):
            if self.isStream:
                n = 6
                # self.streamB["text"] = "Stop streaming"
            else:
                n = 7
                # self.streamB["text"] = "Start streaming"
            self.isStream = not self.isStream
            cmd = guT.createCMD(n)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

        def reNameCmd(self):
            name = simpledialog.askstring('Name for log file', 'Name')
            if name is None:
                return
            print(name)
            self.fileName = '%s%d' % (name, int(time.time()))
            cmd = guT.createCMD(4, te=name)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

        def sendMotorCmd(self):
            self.mW = self.motorControl(self.parent, self.soleObjects)
            if self.mW.vals is None:
                print('Cancel was pressed')
            else:
                # send motor cmd
                # print(self.mW.vals)
                for j, k in enumerate(self.console.ipAddress):
                    cmd = guT.createCMD(8, te=np.squeeze(self.mW.vals[:, j]))
                    self.theSend.send(cmd, addr=(self.console.ipAddress[k], 12354))
                # cmd = guT.createCMD(8, te=np.squeeze(self.mW.vals[:, 1]))
                # self.theSend.send(cmd, addr=(self.console.ipAddress['l'], 12354))
                # cmd = guT.createCMD(8, te=np.squeeze(self.mW.vals[:, 0]))
                # self.theSend.send(cmd, addr=(self.console.ipAddress['r'], 12354))

        def getPressCmd(self):
            cmd = guT.createCMD(9)
            self.theSend.send(cmd, addr=(self.main.console.ipAddress['l'], 12354))
            time.sleep(1)
            self.theSend.send(cmd, addr=(self.main.console.ipAddress['r'], 12354))

        def readBinaryCmd(self):
            shoes = tu.binaryFile2python()
            fSave = tk.filedialog.asksaveasfile(self.main.root)
            if fSave is None:
                return shoes
            np.save(fSave, shoes)

        def saveDataCmd(self):
            fSave = tk.filedialog.asksaveasfilename()
            if fSave is None:
                return 0
            # ss = [self.main.leftShoe, self.main.rightShoe]
            # sided = [True, False]
            for k in self.soleObjects:
                # self.rightShoe.pressDisplay = self.pressDisp
                # self.leftShoe.plotP = self.lPlot
                self.soleObjects[k]['shoe'].saveBinaryFile(fSave)
            self.saveFn(fSave)

        def enterSafeMode(self):
            cmd = guT.createCMD(10)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

        def resetShoes(self):
            cmd = guT.createCMD(11)
            self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

        def updateParams(self):
            pass

        def sendSendStart(self):
            n = 2
            self.buttons['Start Recording']["text"] = "Stop recording"
            for k in self.soleObjects:
                self.soleObjects[k]['shoe'].resetShoe()
                self.soleObjects[k]['shoe'].binaryFile = bytes(0)
            # self.main.leftShoe.resetShoe()
            # self.main.rightShoe.resetShoe()
            # self.main.leftShoe.binaryFile = bytes(0)
            # self.main.rightShoe.binaryFile = bytes(0)
            if self.boardsRecording is None:
                self.boardsRecording = Boards4pred(self.parent, self.soleObjects)
            self.buttons['Start Recording'].config(bg='green')
            cmd = guT.createCMD(n)
            for k, useBoard in zip(self.soleObjects, self.boardsRecording.vals):
                if useBoard == 1:
                    self.theSend.send(cmd, addr=(self.console.ipAddress[k], 12354))
            # self.buttons['Start Recording'].config(bg='green')
            # cmd = guT.createCMD(n)
            # self.startTime[0] = time.time()
            # self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))

        def sendSendStop(self):
            n = 3
            cmd = guT.createCMD(n)
            if self.boardsRecording is None:
                self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
            # self.main.theRec.startStreamRec()
            for k, useBoard in zip(self.soleObjects, self.boardsRecording.vals):
                if useBoard == 1:
                    self.theSend.send(cmd, addr=(self.console.ipAddress[k], 12354))


class DataCollectionFrame(tk.Frame):
    def __init__(self, parent, startTime, predictionFn=None, saveFn=None, STREAM_IP_ADDRESS='192.168.0.255', **kwargs):
        super(DataCollectionFrame, self).__init__(parent, **kwargs)
        self.STREAM_IP_ADDRESS = STREAM_IP_ADDRESS
        self.theSend = tu.UDPsend()
        self.visualizeTime = 8  # s
        self.startTime = startTime
        topFrame = tk.Frame(self)
        topFrame.pack(fill=tk.BOTH, expand=tk.YES)
        # topFrameR = tk.Frame(topFrame)
        # topFrameR.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)
        topFrameL = tk.Frame(topFrame)
        topFrameL.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)
        bottomFrame = tk.Frame(self)
        bottomFrame.pack(fill=tk.BOTH, expand=tk.YES)
        self.console = guT.ConsoleFramePlus(bottomFrame)
        titles = ['Press', 'Acc', 'Gyro', 'Euler', 'Phase']
        names = [tu.DeepSoleObject().names[3 * j:3 * j + 3] for j in range(4)]
        plotColor = [['fuchsia', 'orange', 'purple'],
                     ['Chartreuse', 'darkgreen', 'chocolate'],
                     ['coral', 'brown', 'crimson'],
                     ['blue', 'teal', 'cyan']]
        self.plotPanels = guT.PlotPanelPandasContainer(topFrameL, self.console, self.sendPing, titles, names,
                                                   plotColor, lambda: self.sendPing(True), self.visualizeTime)
        self.plotPanels.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
        self.console.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)
        # pingB = tk.Button(bottomFrame, command=lambda: self.sendPing(True), text='Send Ping')
        # pingB.pack()
        self.theRec = tu.UDPreceiveAllObjects(self.console, self.plotPanels.plots, self.startTime)
        self.controlP = ControlPanel(bottomFrame, self.theSend, self.console, self.plotPanels.plots, self.plotPanels,
                                     predictionFn, saveFn=saveFn, STREAM_IP_ADDRESS=STREAM_IP_ADDRESS, startTime=startTime)
        self.plotPanels.postPopulateFn = self.controlP.enableButtons
        self.controlP.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)

    def sendPing(self, recordIDs=True):
        cmd = guT.createCMD(1)
        self.theSend.send(cmd, addr=(self.STREAM_IP_ADDRESS, 12354))
        if recordIDs:
            self.console.recordNewAdress = True

    def startReceive(self):
        self.theRec.startThread()

    def updateFunction(self):
        self.theRec.parseQueue()
        self.plotPanels.updatePlots()
        # self.theRec.receiveData()



class ReassignBoards(tk.Toplevel):
    def __init__(self, master, soleObjectsIPs, theSend: tu.UDPsend, plotsContainer: guT.PlotPanelPandasContainer = None,
                 **kwargs):
        super(ReassignBoards, self).__init__(master, **kwargs)
        self.soleObjects = soleObjectsIPs
        for k in soleObjectsIPs:
            self.ReassignSubFrame(self, k, soleObjectsIPs[k], theSend, soleObjectsIPs).pack()
        self.protocol("WM_DELETE_WINDOW", self.cancelB)
        self.transient(master)
        self.grab_set()
        master.wait_window(self)
        if plotsContainer is not None:
            plotsContainer.populatePlotPanel(True)
        # root.quit()

    def cancelB(self):
        self.destroy()

    class ReassignSubFrame(tk.Frame):
        def __init__(self, master, name, ipAdress, theSend: tu.UDPsend, soleObjectsIPs, **kwargs):
            super(master.ReassignSubFrame, self).__init__(master, **kwargs)
            self.name = name
            self.theSend = theSend
            self.soleObjectsIPs = soleObjectsIPs
            self.ipAddress = ipAdress
            self.consoleVar = tk.StringVar()
            self.titleLabel = tk.Message(self, textvariable=self.consoleVar)
            self.titleLabel.grid(row=0, column=0)
            self.consoleVar.set(name)
            self.nameInputVar = tk.StringVar()
            self.motorVibrating = 3 * [False]
            tk.Entry(self, textvariable=self.nameInputVar).grid(row=0, column=1)
            texts = ['Signal', 'Reassign ID', 'mBall', 'lBall', 'Heel']
            cmd = [self._signal, self._reassign, lambda: self._vibrationCMD(0), lambda: self._vibrationCMD(1),
                   lambda: self._vibrationCMD(2)]
            self.buttons = []
            for j, (t, c) in enumerate(zip(texts, cmd)):
                self.buttons.append(tk.Button(self, text=t, command=c))
                self.buttons[-1].grid(row=0, column=j+2)
            # tk.Button(self, text='Signal', command=self._signal).grid(row=0, column=2)
            # tk.Button(self, text='Reassign ID', command=self._reassign).grid(row=0, column=3)

        def _reassign(self):
            val = self.nameInputVar.get()
            self.soleObjectsIPs[val] = self.ipAddress
            self.soleObjectsIPs.pop(self.name)
            self.name = val
            self.consoleVar.set(val)
            cmd = guT.createCMD(5, [ord(val[0]), 1])
            # cmd = guT.createCMD(14, 10)
            # self.theSend.send(cmd, addr=(self.LEFT_IP_ADDRESS, 12354))
            self.theSend.send(cmd, addr=(self.ipAddress, 12354))

        def _signal(self):
            cmd = guT.createCMD(15)
            self.theSend.send(cmd, addr=(self.ipAddress, 12354))

        def _vibrationCMD(self, mid):
            ms = self.motorVibrating[mid]
            print(mid)
            if ms:
                self.buttons[mid + 2].config(bg='red')
            else:
                self.buttons[mid + 2].config(bg='green')

            self._sendVibration(ms, mid)
            self.motorVibrating[mid] = not ms

        def _sendVibration(self, turnOn, mid):
            # names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
            vals = np.array([0, 0, 0, 1, 80, 80, 0], dtype=np.int8)
            if turnOn:
                vals[mid] = 50

            cmd = guT.createCMD(8, te=np.squeeze(vals))
            self.theSend.send(cmd, addr=(self.ipAddress, 12354))


class AssingBoardsNicknames(tk.Toplevel):
    def __init__(self, master, labels, soleObjectsIPs, configID, **kwargs):
        super(AssingBoardsNicknames, self).__init__(master, **kwargs)
        self.soleObjects = soleObjectsIPs
        self.configID = configID
        self.nicknamesDict = {}
        self.labels = labels
        self.boardNames = [k for k in soleObjectsIPs]
        labelNames = [k for aa in labels for k in aa]
        self.values = {}
        topFrame = tk.Frame(self)
        topFrame.pack()
        lowFrame = tk.Frame(self)
        lowFrame.pack()
        if path.exists('./%s_boardConfig.pickle' % self.configID):
            with open('./%s_boardConfig.pickle' % self.configID, 'rb') as f:
                defaults = pickle.load(f)['values']
            for k in defaults.copy():
                if k not in labelNames:  # or defaults[k]['Board'] not in self.boardNames:
                    defaults.pop(k)
        else:
            defaults = dict((k, None) for k in labelNames)
        for col, kk in enumerate(labels):
            for row, k in enumerate(kk):
                self.nicknamesDict[k] = self.AssingBoardsSubFrame(topFrame, k, soleObjectsIPs, default=defaults[k])
                self.nicknamesDict[k].grid(row=row, column=col)
        tk.Button(lowFrame, text='OK', command=self.okB).pack(side=tk.LEFT)
        tk.Button(lowFrame, text='cancel', command=self.cancelB).pack(side=tk.LEFT)
        self.protocol("WM_DELETE_WINDOW", self.cancelB)
        self.transient(master)
        self.grab_set()
        if master is not None:
            master.wait_window(self)
        # root.quit()

    def cancelB(self):
        self.destroy()

    def okB(self):
        for k in self.nicknamesDict:
            self.values[k] = {'Board': self.nicknamesDict[k].boardChoice.get(),
                              'Motor': self.nicknamesDict[k].vibrationChoice.get()}
        with open('./%s_boardConfig.pickle' % self.configID, 'wb') as f:
            pickle.dump({'labels': self.labels, 'boardNames': self.boardNames, 'values': self.values}, f)
        self.destroy()

    class AssingBoardsSubFrame(tk.Frame):
        def __init__(self, master, label, soleObjects, default=None, **kwargs):
            super(AssingBoardsNicknames.AssingBoardsSubFrame, self).__init__(master, **kwargs)
            tk.Label(self, text=label).pack(side=tk.LEFT)
            self.boardChoice = tk.StringVar(self)
            self.vibrationChoice = tk.IntVar(self)
            if default is None:
                self.boardChoice.set(list(soleObjects.keys())[0])
                self.vibrationChoice.set(0)
            else:
                self.boardChoice.set(default['Board'])
                self.vibrationChoice.set(default['Motor'])
            # self.plotChoice.trace('w', self.dropDownChanged)
            self.popUp = ttk.OptionMenu(self, self.boardChoice, None, *soleObjects)
            self.popUp.pack(side=tk.LEFT)
            self.popUp2 = ttk.OptionMenu(self, self.vibrationChoice, None, 0, 1, 2)
            self.popUp2.pack(side=tk.LEFT)


class Boards4pred(tk.Toplevel):
    def __init__(self, master, soleObjectsDataFrame, **kwargs):
        super(Boards4pred, self).__init__(master, **kwargs)
        self.soleObjectsDataFrame = soleObjectsDataFrame
        self.check = guT.CheckControl(self, 'Boards', [k for k in soleObjectsDataFrame], None)
        self.check.pack()
        frBot = tk.Frame(self)
        tk.Button(frBot, text='OK', command=self.okB).pack(side=tk.LEFT)
        tk.Button(frBot, text='Cancel', command=self.cancelB).pack(side=tk.LEFT)
        frBot.pack()
        self.vals = np.zeros((len(soleObjectsDataFrame)), dtype=np.int8)
        self.protocol("WM_DELETE_WINDOW", self.cancelB)
        self.transient(master)
        self.grab_set()
        master.wait_window(self)

        # root.quit()

    def cancelB(self):
        self.destroy()

    def okB(self):
        self.vals[:] = self.check.getAllValues()[:]
        self.destroy()


class TestBoardVibration(tk.Frame):
    def __init__(self, master, label, ipAddress, idInBoard, udpSend, **kwargs):
        super(TestBoardVibration, self).__init__(master, **kwargs)
        self.label = label
        self.udpSend = udpSend
        self.ipAddress = ipAddress
        self.idInBoard = idInBoard
        self._vibrating = True
        tk.Label(self, text=label).pack(side=tk.LEFT)
        self.button = tk.Button(self, text='Start Vibration', bg='red', command=self._vibrate)
        self.button.pack()

    def _vibrate(self):
        if self._vibrating:
            self.button['text'] = 'Stop Vibration'
            self.button.config(bg='green')
            self._sendVibration(True)
        else:
            self.button['text'] = 'Start Vibration'
            self.button.config(bg='red')
            self._sendVibration(False)

        self._vibrating = not self._vibrating

    def _sendVibration(self, turnOn):
        # names = ['M Ball', 'L Ball', 'Heel', 'Noise', 'Min Freq', 'Max Freq', 'Use log scale']
        vals = np.array([0, 0, 0, 1, 80, 80, 0], dtype=np.int8)
        if turnOn:
            vals[self.idInBoard] = 50
        print(vals)
        print(self.ipAddress)
        cmd = guT.createCMD(8, te=np.squeeze(vals))
        self.udpSend.send(cmd, addr=(self.ipAddress, 12354))

