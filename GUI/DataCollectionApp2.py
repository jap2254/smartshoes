# import argparse
#
# import pandas
# import time
# import numpy as np
# from tkinter import messagebox, simpledialog
import tkinter as tk
import GUI.DeepSoleModules as dsm
import time
import warnings

CLIENT_IP = '192.168.0.255'
LOCAL_DEBUG = False

if LOCAL_DEBUG:
    CLIENT_IP = '127.0.0.1'
    warnings.warn('Local host debugging is active. If you are using real boards change the flag!!!!')


class DataAPP(tk.Tk):
    def __init__(self, predictionFn=None, saveFn=None, **kwargs):
        super(DataAPP, self).__init__(**kwargs)
        self.title('DeepSole System, Data collection')
        self.iconbitmap(r'./GUI/deepsole.ico')
        self.refreshTime = 20  # ms
        self.mainFrame = tk.Frame(self)
        self.startTime = [time.time()]
        self.mainFrame.pack()
        self.root = dsm.DataCollectionFrame(self.mainFrame, self.startTime, predictionFn=predictionFn, saveFn=saveFn,
                                            STREAM_IP_ADDRESS=CLIENT_IP)
        self.root.pack(fill=tk.BOTH, expand=tk.YES)
        self.control = self.root.controlP.control
        self.udpSend = self.root.theSend

    def startApp(self):
        # while True:
        #   self.updatePlots()
        #  self.root.update_idletasks()
        # self.root.update()
        self.startReceive()
        self.loopFunctions()
        # self.protocol("WM_DELETE_WINDOW", self.stopApp)
        self.mainloop()

    def startReceive(self):
        self.root.startReceive()

    def loopFunctions(self):
        self.root.updateFunction()
        self.after(self.refreshTime, self.loopFunctions)

    def stopApp(self):
        # self.theRec.stopThread()
        self.root.destroy()


if __name__ == '__main__':
    # parser = argparse.ArgumentParser()
    # FLAGS, unparsed = parser.parse_known_args()
    # main(unparsed)
    app = DataAPP()
    # app.startReceive()
    app.startApp()
