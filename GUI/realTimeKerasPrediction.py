
import struct
import numpy as np
import Tools.Utils as tu
import tensorflow as tf
import time
import Tools.GUItools as guT


def checkAndInterp(t, A, freq):
    tt = t / 1000
    tt -= tt[0]
    if tt[-1] <= (A.shape[0]-2)/freq:
        # interp data
        t2 = np.arange(0, A.shape[0]) / freq
        A2 = tu.reSampleMatrix(A, 0, tt, t2)
        print('*************************************************************')
        return A2
    else:
        return A


class fakeConsole(object):
    def __init__(self):
        self.me = 0

    def set(self, trash):
        pass

CLIENT_IP = '192.168.0.255'
CLIENT_PORT = 24681
timeMax = 25
fmt = '>4L'
# shoe objects
rightShoe = tu.smartShoe(False, keepAll=False)
leftShoe = tu.smartShoe(True, keepAll=False)
# receive data from the shoe
theSend = tu.UDPsend()
theRec = tu.UDPreceive(leftShoe, rightShoe, fakeConsole(), ver=2)
theRec.startThread()
modelPath = "D:/smartshoes/GUI/GaitPercentage_Corey.h5"
model = tf.keras.models.load_model(modelPath)

cmd = guT.createCMD(2)
theSend.send(cmd, addr=(CLIENT_IP, 12354))

while rightShoe.dataFrame.shape[0] < timeMax and leftShoe.dataFrame.shape[0] < timeMax:
    pass

# create a batch container
bCont = np.zeros((2, timeMax, 12), dtype=np.float32)
tCount = np.zeros(10)
freq = 50
c = 0
print('prediction started')
while True:
    now = time.time()
    tR = rightShoe.dataFrame.index.values[-timeMax:]
    tL = leftShoe.dataFrame.index.values[-timeMax:]
    # bCont[0] = checkAndInterp(tR, rightShoe.dataFrame.values[-timeMax:, :-1], freq)
    # bCont[1] = checkAndInterp(tL, leftShoe.dataFrame.values[-timeMax:, :-1], freq)
    r = rightShoe.dataFrame.iloc[-timeMax:, :-1].copy()
    r.loc[:, ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'EUx', 'EUy', 'EUz']] *= 1.0 / 8000
    l = leftShoe.dataFrame.iloc[-timeMax:, :-1].copy()
    l.loc[:, ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'EUx', 'EUy', 'EUz']] *= 1.0 / 8000

    # bCont[0] = checkAndInterp(tR, r.values, freq)
    # bCont[1] = checkAndInterp(tL, l.values, freq)
    bCont[0] = r.values
    bCont[1] = l.values
    t = [rightShoe.dataFrame.index.values.astype(np.int)[-1], leftShoe.dataFrame.index.values.astype(np.int)[-1]]
    pred = model.predict(bCont)
    print('%f, %f, %f' % (1/(time.time()-now), pred[0], pred[1]))
    msg = struct.pack(fmt, t[0], t[1], (pred[0]*1000).astype(np.int)[0], (pred[1]*1000).astype(np.int)[0])
    theSend.send(msg, (CLIENT_IP, CLIENT_PORT))
    # while time.time()-now < .1:
    #     a=1







