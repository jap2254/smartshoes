import pickle

import numpy as np
import pandas
import tensorflow as tf

import Tools.Utils as tu
import Tools.NNUtils as ntu


# tf.compat.v1.disable_eager_execution()

def modifyPhaseY(matO: tu.MatObject, freezingE: pandas.DataFrame, ignoreMicro):
    for i, val in freezingE.iterrows():
        if ignoreMicro and val['Event'] == 3:
            continue
        m = np.logical_and(val['Start'] <= matO.binaryFunctions.index.values,
                           matO.binaryFunctions.index.values <= val['End'])
        matO.binaryFunctions.iloc[m, :2] = val['Event']
        # extend lap time if necessary
        isInLap = np.logical_and(val['Start'] > matO.lap[:, 0],
                                 val['End'] <= matO.lap[:, 1])
        if (~isInLap).all():
            isInLap = val['End'] > matO.lap[:, 1]
            m = np.where(isInLap)[0][-1]
            matO.lap[m, 1] = val['End']


def evaluateSub(subID: str, model: tf.keras.Model):
    lPath = PD_path + subID + '/6MWT_L.bin'
    rPath = PD_path + subID + '/6MWT_R.bin'
    [r, _] = tu.binaryFile2python(rPath)
    [_, l] = tu.binaryFile2python(lPath)
    r.createPandasDataFrame(useSync=True, freq=freq)
    l.createPandasDataFrame(useSync=True, freq=freq)
    matPath = PD_path + subID + '/6MWT.xlsx'
    # get sub ID number
    if int(subID[2:]) < 11:
        matO = tu.MatObject(fName=matPath, freq=freq)
    else:
        matO = tu.MatObject(fName=matPath, freq=freq, newMat=True)
    # matO = tu.MatObject(fName=matPath, freq=freq)
    freezePath = PD_path + subID + '/FreezeLog.csv'
    frO = pandas.read_csv(freezePath)
    modifyPhaseY(matO, frO, ignoreMicro=False)
    yR = matO.binaryFunctions['R'].values
    yL = matO.binaryFunctions['L'].values
    xR = r.dataFrame.values
    xL = l.dataFrame.values
    laps = matO.lap.copy()
    tMax = min([yR.shape[0], yL.shape[0], xR.shape[0], xL.shape[0]])
    yR = yR[:tMax]
    yL = yL[:tMax]
    xR = xR[:tMax]
    xL = xL[:tMax]
    # Do True or False for FoG
    m = yR == 3
    yR[m] = 2

    m = yR == 1
    yR[m] = 0

    m = yR == 2
    yR[m] = 1

    xRbatch = ntu.createbatch(xR, 50).astype(np.float32)
    xLbatch = ntu.createbatch(xL, 50).astype(np.float32)
    xx = np.transpose(np.stack((xRbatch, xLbatch), axis=3), (0, 1, 3, 2))
    xx[:, :, :, :3] /= 4096
    # yRpred = model.predict(xRbatch, batch_size=500)
    yLpred = model.predict(xx, batch_size=500)

    data = { #'rShoe': r.dataFrame, 'lShoe': l.dataFrame,
            'yTruth': yR, 'yPred': yLpred,
            'laps': laps, 'subID': subID}
    return data


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'evaluatedSubsAll2.pickle'
modelParentPath = "E:/PDclass/LOO4/LOO_"
modelTypes = ['CNN', 'ED']
validSubs = np.array([j for j in range(1, 6)] + [7] + [j for j in range(11, 25)])
otherSub = validSubs.copy()
while np.any(np.divide(validSubs, otherSub) == 1):
    np.random.shuffle(otherSub)

freq = 100.0
# samplesPerSubject = 6000
allSubs = {}
for k in validSubs:
    subID = 'PD%03d' % k
    print('Loading: %s' % subID)
    oneSub = {}
    for modT in modelTypes:
        modelPath = '%s%s_%s/LOO_%s_%s.h5' % (modelParentPath, subID, modT, subID, modT)
        model = tf.keras.models.load_model(modelPath)
        oneSub[modT] = {}
        for k2 in validSubs:
            nloo = evaluateSub('PD%03d' % k2, model)
            oneSub[modT]['PD%03d' % k2] = nloo
    allSubs[subID] = oneSub.copy()

with open(allSubsPath, 'wb') as f:
    pickle.dump(allSubs, f)
