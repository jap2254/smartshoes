import pickle

import numpy as np
import pandas
import seaborn as sn


def removedataOutsideLaps(data: dict):
    laps = data['laps']
    m = []
    for st, en in laps:
        m.append(np.arange(st * 100, en * 100).astype(np.int))
    m = np.concatenate(m)
    # remove where m is less than size
    m = m[m < data['yTruth'].size]
    for k in ['yTruth', 'yPred']:
        data[k] = data[k][m]


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'evaluatedSubs.pickle'

with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)

accuracyData = []
for subID in allSubs:
    sub = allSubs[subID]
    for modelT in sub:
        mod = sub[modelT]
        for sType in mod:
            mod[sType]['yPred'] = np.argmax(mod[sType]['yPred'], axis=1)
            data = mod[sType]
            removedataOutsideLaps(data)
            aux = pandas.DataFrame(index=[0])
            aux['SubID'] = data['subID']
            aux['TestType'] = sType
            aux['Accuracy'] = np.equal(data['yPred'], data['yTruth']).sum() / data['yTruth'].size
            aux['ModelType'] = modelT
            fog = np.where(data['yTruth'] == 1)[0]
            try:
                aux['TruePositive'] = ((data['yTruth'][fog] - data['yPred'][fog]) == 0).sum() / fog.size
            except:
                a = 1
            nfog = np.where(data['yTruth'] == 0)[0]
            aux['TrueNegative'] = ((data['yTruth'][nfog] - data['yPred'][nfog]) == 0).sum() / nfog.size
            accuracyData.append(aux.copy())
            accuracyData.append(aux.copy())

accuracyData = pandas.concat(accuracyData).reset_index(drop=True)
# sn.catplot(data=accuracyData, hue='TestType', y='value', kind='boxen', x='ModelType')

# allSubsPath = PD_path + 'evaluatedSubsAll.pickle'
#
# with open(allSubsPath, 'rb') as f:
#     allSubsAll = pickle.load(f)
#
# looMatrixCNN = []
# looMatrixED = []
# for subID in allSubsAll:
#     sub = allSubsAll[subID]
#     for modelT in sub:
#         mod = sub[modelT]
#         aux = pandas.DataFrame(index=[subID])
#         for testSub in mod:
#             data = mod[testSub]
#             removedataOutsideLaps(data)
#             aux[testSub] = np.equal(np.argmax(data['yPred'], axis=1), data['yTruth']).sum() / data['yTruth'].size
#         if 'CNN' in modelT:
#             looMatrixCNN.append(aux.copy())
#         else:
#             looMatrixED.append(aux.copy())
#
# looMatrixCNN = pandas.concat(looMatrixCNN)
# looMatrixED = pandas.concat(looMatrixED)
#
# sn.heatmap(looMatrixED, vmin=0.75, vmax=1)
