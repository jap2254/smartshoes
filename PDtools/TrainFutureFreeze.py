import pickle
import tensorflow as tf
import numpy as np
import Tools.Utils as tu
from tensorflow.python.framework.ops import disable_eager_execution

disable_eager_execution()


def createModel(DATA_SHAPE) -> tf.keras.Model:
    kernels1D = [30, 20, 10, 5]
    kernels1Dpost = [30, 20, 10, 5]
    # kernels1Dpost = []
    fullyCon = [32, 64]
    # num_gru_units = None
    num_gru_units = [5 for _ in range(1)]
    learningRate = 1e-3
    dropout = 0.5
    # DATA_SHAPE = ()
    CLASSES_NUM = 2
    model = tf.keras.Sequential()
    # model.add(tf.keras.layers.Dense(DATA_SHAPE[1]*2, use_bias=False, input_shape=DATA_SHAPE))
    # model.add(tf.keras.layers.BatchNormalization())
    # model.add(tf.keras.layers.Activation("relu"))
    # Encoder
    model.add(tf.keras.layers.Conv2D(DATA_SHAPE[-1], kernels1D[0], activation=tf.nn.relu, padding='same',
                                     input_shape=DATA_SHAPE))
    for k in kernels1D[1:]:
        model.add(tf.keras.layers.Conv2D(DATA_SHAPE[-1], k, activation=tf.nn.relu, padding='same'))
        model.add(tf.keras.layers.Dropout(dropout))

    model.add(tf.keras.layers.Reshape((DATA_SHAPE[0], DATA_SHAPE[2] * DATA_SHAPE[1])))

    for k in fullyCon:
        model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))
        model.add(tf.keras.layers.Dropout(dropout))

    # RNN
    model.add(tf.keras.layers.RNN([tf.keras.layers.GRUCell(n, dropout=dropout) for n in num_gru_units],
                                  return_sequences=True))

    # dense layers
    for k in fullyCon + [CLASSES_NUM * 2]:
        model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))
        model.add(tf.keras.layers.Dropout(dropout))

    model.add(tf.keras.layers.Reshape((DATA_SHAPE[0], CLASSES_NUM, 2)))

    # model.add(tf.keras.layers.Dense(32, activation=tf.nn.relu))
    # model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
    # model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))

    # model.add(tf.keras.layers.Dense(CLASSES_NUM, activation=tf.nn.relu))

    # Decoder
    # for k in kernels1Dpost:
    #     model.add(tf.keras.layers.Conv1D(DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))

    for k in kernels1Dpost:
        model.add(tf.keras.layers.Conv2D(DATA_SHAPE[-1], k, activation=tf.nn.relu, padding='same'))
        model.add(tf.keras.layers.Dropout(dropout))

    model.add(tf.keras.layers.Flatten())

    # Flatten

    # Softmax
    model.add(tf.keras.layers.Dense(CLASSES_NUM, activation=tf.nn.softmax))
    opt = tf.keras.optimizers.Adam(lr=learningRate)

    model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    return model


def creatreTrainData(data):
    x = []
    y = []
    xT = []
    yT = []
    labels = []
    labelsT = []
    for i, k in enumerate(data):
        if 'db' in k:
            continue

        x.append(data[k]['x'])
        y.append(data[k]['y'])
        xT.append(data[k]['xT'])
        yT.append(data[k]['yT'])
        labels.append(np.ones_like(data[k]['y']) * i)
        labelsT.append(np.ones_like(data[k]['yT']) * i)
    x = np.concatenate(x, 0)
    y = np.concatenate(y, 0)
    xT = np.concatenate(xT, 0)
    yT = np.concatenate(yT, 0)
    labels = np.concatenate(labels, 0)
    labelsT = np.concatenate(labelsT, 0)
    return x, y, xT, yT, labels, labelsT

batchSize = 1000
trainSize = 140000
testSize = 50000
maxEpochs = 200
tmax = 25
save_path = 'E:/PDclass/Future/'
PD_path = 'Z:/PD/PD_val/_Experiments/'
data2save = {}

for futureSamples in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50]:
    allSubsPath = save_path + 'allSubs_Feb2021_%dFuture.pickle' % futureSamples

    with open(allSubsPath, 'rb') as f:
        allSubs = pickle.load(f)

    x, y, xT, yT, labels, labelsT = creatreTrainData(allSubs)
    x = x[:, tmax:]
    xT = xT[:, tmax:]
    model = createModel(x.shape[1:])

    model.fit(x, y, batch_size=batchSize, epochs=maxEpochs, validation_split=0.1)
    tu.createDirIfNotExist(save_path)
    model.save('%sFuture%d.h5' % (save_path, futureSamples))

    # run the test
    yP = model.predict(xT, batch_size=batchSize)
    data2save[futureSamples] = {'gt': yT, 'pred': yP, 'labels': labelsT}

with open('%sFuture%d.pickle' % (save_path, futureSamples), 'wb') as f:
    pickle.dump(data2save, f)
