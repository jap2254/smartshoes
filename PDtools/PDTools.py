import pandas
import numpy as np
import Tools.NNUtils as ntu
import Tools.Utils as tu
import tensorflow as tf


def evaluateSub(lPath, rPath, freezePath, freq, model: tf.keras.Model):
    # lPath = PD_path + subID + '/6MWT_L.bin'
    # rPath = PD_path + subID + '/6MWT_R.bin'
    [r, _] = tu.binaryFile2python(rPath)
    [_, l] = tu.binaryFile2python(lPath)
    r.createPandasDataFrame(useSync=True, freq=freq)
    l.createPandasDataFrame(useSync=True, freq=freq)

    # freezePath = PD_path + subID + '/FreezeLog.csv'
    frO = pandas.read_csv(freezePath)
    xR = r.dataFrame.values
    xL = l.dataFrame.values
    tMax = min([xR.shape[0], xL.shape[0]])
    ytt = np.zeros(tMax)
    y = ntu.createbatch(np.expand_dims(modifyPhaseY(ytt, frO, ignoreMicro=False), 1), 50)
    y = np.squeeze(y)
    xR = xR[:tMax]
    xL = xL[:tMax]
    xx = np.empty((tMax, 50, 12, 2))
    xx[:, :, :, 0] = ntu.createbatch(xR, 50)
    xx[:, :, :, 0] = ntu.createbatch(xL, 50)
    xx = np.transpose(xx, (0, 1, 3, 2))
    xx[:, :, :, :3] /= 4096
    # yRpred = model.predict(xRbatch, batch_size=500)
    yLpred = model.predict(xx, batch_size=500)

    data = { #'rShoe': r.dataFrame, 'lShoe': l.dataFrame,
            'yTruth': y, 'yPred': yLpred, 'subID': subID}
    return data


def modifyPhaseY(y, freezingE: pandas.DataFrame, ignoreMicro):
    for i, val in freezingE.iterrows():
        if ignoreMicro and val['Event'] == 3:
            continue
        if val['Start'] > y.size or val['End'] > y.size:
            continue
        y[int(val['Start']*100):int(val['End']*100)] = 1
    return y