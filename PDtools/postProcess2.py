import pickle

import numpy as np
import pandas
import seaborn as sn
import matplotlib.pyplot as plt
import Tools.Utils as tu


def removedataOutsideLaps(data: dict):
    laps = data['laps']
    m = []
    for st, en in laps:
        m.append(np.arange(st * freq, en * freq).astype(np.int))
    m = np.concatenate(m)
    # remove where m is less than size
    m = m[m < data['yTruth'].size]
    for k in ['yTruth', 'yPred']:
        data[k] = data[k][m]


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'evaluatedSubsSingleLarge.pickle' #--> this is good, 20% split
# allSubsPath = PD_path + 'evaluatedSubsSingleSmaller.pickle'


with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)

# del allSubs['PD010']
validSubs = allSubs['dbPars']['validSubs']
freq = allSubs['dbPars']['freq']
allSubs.pop('dbPars')
# validSubs = [int(k[2:]) for k in allSubs]
otherSub = validSubs.copy()
# while np.any(np.divide(validSubs, otherSub) == 1):
#     np.random.shuffle(otherSub)

NITSub = ['PD%03d' % k for k in otherSub]
# f, ax = plt.subplots(10, sharex=True, sharey=True)
c = 0
accuracyData = []
for subID, NItID in zip(allSubs, NITSub):
    sub = allSubs[subID]
    for modelT in sub:
        mod = sub[modelT]
        del mod['PD010']
        for testSub in mod:
            # del mod['PD010']
            data = mod[testSub]
            t = np.arange(0, mod[testSub]['yTruth'].shape[0])/freq
            data['yTruth'] = mod[testSub]['yTruth'][:, -1]
            data['yPred'] = np.argmax(mod[testSub]['yPred'], axis=1)
            testSize = int(data['yTruth'].shape[0]*.8)
            data['yTruth'] = data['yTruth'][-testSize:]
            data['yPred'] = data['yPred'][-testSize:]
            # mod[testSub]['yPred'] = (mod[testSub]['yPred'][:, 1] > 0.7).astype(np.float)
            # mod[testSub]['yPred'] = tu.myDigitalFilter(mod[testSub]['yPred'], t, 1)
            # mod[testSub]['yPred'] = tu.myDigitalFilter(mod[testSub]['yPred'], t, 5)
            # mod[testSub]['yPred'] = tu.myDigitalFilter(mod[testSub]['yPred'], t, 10)


            # ax[c].plot(mod[testSub]['yTruth'], label='True')
            # ax[c].plot(mod[testSub]['yPred'], label='Pred', linestyle=':')
            # ax[c].plot(mod[testSub]['yPred'], label='Filter')
            c += 1
            # if testSub in subID or testSub in NItID:
            aux = pandas.DataFrame(index=[0, 1, 2, 3])

            fog = np.where(data['yTruth'] == 1)[0]
            fog2 = np.where(data['yPred'] == 1)[0]
            nfog = np.where(data['yTruth'] == 0)[0]
            nfog2 = np.where(data['yPred'] == 0)[0]


            TP = ((data['yTruth'][fog] - data['yPred'][fog]) == 0).sum()
            TN = ((data['yTruth'][nfog] - data['yPred'][nfog]) == 0).sum()
            FP = ((data['yTruth'][fog2] - data['yPred'][fog2]) != 0).sum()
            FN = ((data['yTruth'][nfog2] - data['yPred'][nfog2]) != 0).sum()

            # aux.loc[0, 'val'] = ((data['yTruth'][fog] - data['yPred'][fog]) == 0).sum() / fog.size
            aux.loc[0, 'val'] = TP / (TP + FN) * 100.0
            aux.loc[0, 'Metric'] = 'Sensitivity '

            # aux.loc[2, 'val'] = np.equal(data['yPred'], data['yTruth']).sum() / data['yTruth'].size
            aux.loc[3, 'val'] = (TP + TN) / (TP + FN + TN + FP) * 100.0
            aux.loc[3, 'Metric'] = 'Accuracy'

            # aux.loc[3, 'val'] = ((data['yTruth'][fog2] - data['yPred'][fog2]) != 0).sum() / fog2.size
            aux.loc[1, 'val'] = TN / (TN + FP) * 100.0
            aux.loc[1, 'Metric'] = 'Specificity '

            # aux.loc[5, 'val'] = ((data['yTruth'][fog] - data['yPred'][fog]) == 0).sum() /\
            #                     (((data['yTruth'][fog2] - data['yPred'][fog2]) != 0).sum() +
            #                      ((data['yTruth'][fog] - data['yPred'][fog]) == 0).sum())
            aux.loc[2, 'val'] = TP / (TP + FP) * 100.0
            aux.loc[2, 'Metric'] = 'Precision'
            # accuracyData.append(aux.copy())


            aux.loc[4, 'val'] = FP / fog.size * 100
            aux.loc[4, 'Metric'] = 'False Positives p'

            aux.loc[5, 'val'] = FN / nfog.size * 100
            aux.loc[5, 'Metric'] = 'False Negatives p'

            aux.loc[6, 'val'] = FP
            aux.loc[6, 'Metric'] = 'False Positives'

            aux.loc[7, 'val'] = FN
            aux.loc[7, 'Metric'] = 'False Negatives'

            aux.loc[8, 'val'] = TP
            aux.loc[8, 'Metric'] = 'True Positives'

            aux.loc[9, 'val'] = TN
            aux.loc[9, 'Metric'] = 'True Negatives'

            aux.loc[10, 'val'] = TP / fog.size * 100
            aux.loc[10, 'Metric'] = 'True Positives p'

            aux.loc[11, 'val'] = TN / nfog.size * 100
            aux.loc[11, 'Metric'] = 'True Negatives p'

            # aux['FOG'] = FP / fog.size
            # aux['REG'] = FN / nfog.size
            aux['SubID'] = data['subID']
            if testSub in subID:
                aux['TestType'] = 'NIT'
            else:
                aux['TestType'] = 'IT'
            aux['ModelType'] = modelT



            accuracyData.append(aux.copy())

accuracyData = pandas.concat(accuracyData).reset_index(drop=True)
# sn.catplot(data=accuracyData, hue='TestType', y='value', kind='boxen', x='ModelType')
f, ax = plt.subplots(1)
sn.catplot(data=accuracyData, x='Metric', y='val', kind='bar', ax=ax)
m = accuracyData.groupby('Metric').mean()
s = accuracyData.groupby('Metric').std()
accuracyDataF = accuracyData.where(np.logical_or(accuracyData['Metric']=='False Positives', accuracyData['Metric']=='False Negatives')).dropna()
accuracyDataF['val'] *= 100.0
sn.catplot(data=accuracyDataF, x='Metric', y='val', kind='bar', ax=ax)



# modify to have SUBID, TruePos, TruyeNeg, FalsePos, FalseNeg
# values = ['TruePositive', 'TrueNegative', 'FalsePositive', 'FalseNegative', 'Accuracy']
values = accuracyData['Metric'].unique()
subs = accuracyData['SubID'].unique()
accuracyDataSum = pandas.DataFrame(columns=values, index=subs)
for v in values:
    accuracyDataSum.loc[:, v] = accuracyData['val'].where(accuracyData['Metric'] == v).dropna().values

# accuracyDataSum['FOG'] = accuracyData['FOG'].unique()
# accuracyDataSum['REG'] = accuracyData['REG'].unique()

accuracyDataSum.describe()





# f, ax = plt.subplots(1)
# sn.catplot(data=accuracyData, x='TestType', y='TrueNegative', kind='box', ax=ax)


# allSubsPath = PD_path + 'evaluatedSubsAll.pickle'
#
# with open(allSubsPath, 'rb') as f:
#     allSubsAll = pickle.load(f)
#

# looMatrixTN = []
# looMatrixTP = []
# for subID in allSubs:
#     sub = allSubs[subID]
#     for modelT in sub:
#         mod = sub[modelT]
#         aux = pandas.DataFrame(index=[subID])
#         aux1 = pandas.DataFrame(index=[subID])
#         for testSub in mod:
#             data = mod[testSub]
#             # removedataOutsideLaps(data)
#             nfog = np.where(data['yTruth'] == 0)[0]
#             aux[testSub] = ((data['yTruth'][nfog] - data['yPred'][nfog]) == 0).sum() / nfog.size
#
#             fog = np.where(data['yTruth'] == 1)[0]
#             # removedataOutsideLaps(data)
#             aux1[testSub] = ((data['yTruth'][fog] - data['yPred'][fog]) == 0).sum() / fog.size
#         looMatrixTN.append(aux.copy())
#         looMatrixTP.append(aux1.copy())
#
# looMatrixTN = pandas.concat(looMatrixTN)
# looMatrixTP = pandas.concat(looMatrixTP)
# f, ax = plt.subplots(1)
# sn.heatmap(looMatrixTP, vmin=0.5, vmax=1, ax=ax)
# f, ax = plt.subplots(1)
# sn.heatmap(looMatrixTN, vmin=0.5, vmax=1, ax=ax)