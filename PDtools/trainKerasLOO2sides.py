import pickle

import numpy as np
import tensorflow as tf

import Tools.NNUtils as nnU
import Tools.Utils as tu


class PDDataset(nnU.DatasetProto):
    def __init__(self, allSubs, trainsize, testSize, batchSize, tmax=50, channels=None):
        self.allSubs = allSubs
        x = []
        y = []
        xT = []
        yT = []
        useSubTestMode = type(testSize) is not int
        if useSubTestMode:
            # This means that you want to split
            totalSamsPerSub = np.ceil(trainsize / len(allSubs)).astype(np.int)
        else:
            totalSamsPerSub = np.ceil((testSize + trainsize) / len(allSubs)).astype(np.int)

        for subName in allSubs:
            sub = allSubs[subName].copy()
            if len(sub['y'].shape) > 2:
                sub['y'] = sub['y'][:, :, 0]
            # transpose the x such that data is (samples, time, shoe, sensor) instead of (samples, time, sensor, shoe)
            sub['x'] = np.transpose(sub['x'], (0, 1, 3, 2))
            if useSubTestMode and subName in testSize:
                xT.append(sub['x'])
                yT.append(sub['y'][:, -1])
            else:
                shu1 = sub['y'][:, -1] == 2
                x.append(sub['x'][shu1])
                y.append(sub['y'][shu1, -1])
                shu = tu.randomOrder(sub['x'].shape[0])
                x.append(sub['x'][shu[:totalSamsPerSub-shu1.sum()]])
                y.append(sub['y'][shu[:totalSamsPerSub-shu1.sum()], -1])

        if useSubTestMode:
            xx = np.concatenate(x, axis=0)
            yy = np.concatenate(y, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            x = xx[shu].copy()
            y = yy[shu].copy()
            xx = np.concatenate(xT, axis=0)
            yy = np.concatenate(yT, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            xT = xx[shu]
            yT = yy[shu]
        else:
            xx = np.concatenate(x, axis=0)
            yy = np.concatenate(y, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            if trainSize + testSize < shu.size:
                x = xx[shu][:trainsize]
                y = yy[shu][:trainsize]
                xT = xx[shu][-testSize:]
                yT = yy[shu][-testSize:]
            else:
                x = xx[shu][:trainsize]
                y = yy[shu][:trainsize]
                xT = xx[shu][trainsize:]
                yT = yy[shu][trainsize:]


        x = x[:, -tmax:]
        xT = xT[:, -tmax:]
        numClasses = 2

        # reduceFreeze = True
        # numClasses = 3
        # if reduceFreeze:
        #     m = y == 3
        #     y[m] = 2
        #     m = yT == 3
        #     yT[m] = 2
        #     numClasses = 3
        #
        # freezeNoFreeze = True
        # if freezeNoFreeze:
        #     m = y == 1
        #     y[m] = 0
        #     m = yT == 1
        #     yT[m] = 0
        #     m = y == 2
        #     y[m] = 1
        #     m = yT == 2
        #     yT[m] = 1
        #     numClasses = 2

        # x[:, :, :, :3] /= 4096
        # xT[:, :, :, :3] /= 4096

        if channels is not None:
            x = x[:, :, :, channels]
            xT = xT[:, :, :, channels]

        nnU.DatasetProto.__init__(self, x, y, xT,
                                  yT,
                                  batchSize, numclases=numClasses)


class modelContainer(object):
    def __init__(self, dataset: nnU.DatasetProto, kernels1D, kernels1Dpost, num_gru_units, fullyCon, resultsPath):
        self.dataset = dataset
        self.learningRate = 1e-3
        self.dropout = 0.5
        self.ED_RNN = self._buildCRNN(kernels1D, kernels1Dpost, num_gru_units, fullyCon, self.learningRate)
        # self.RNN = self._buildRNN(num_gru_units, fullyCon, self.learningRate)
        # self.CNN = self._buildCNN(kernels1D, fullyCon, self.learningRate)
        self.CNN2D = self._buildCNN2D(kernels1D, fullyCon, self.learningRate)
        # self.Ln = self._buildLn(fullyCon, self.learningRate)
        # self.ED_Ln = self._buildEDLn(kernels1D, kernels1Dpost, fullyCon, self.learningRate)

    def _buildCRNN(self, kernels1D, kernels1Dpost, num_gru_units, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()
        # model.add(tf.keras.layers.Dense(dataset.DATA_SHAPE[1]*2, use_bias=False, input_shape=dataset.DATA_SHAPE))
        # model.add(tf.keras.layers.BatchNormalization())
        # model.add(tf.keras.layers.Activation("relu"))
        # Encoder
        model.add(tf.keras.layers.Conv2D(dataset.DATA_SHAPE[-1], kernels1D[0], activation=tf.nn.relu, padding='same',
                                         input_shape=dataset.DATA_SHAPE))
        for k in kernels1D[1:]:
            model.add(tf.keras.layers.Conv2D(dataset.DATA_SHAPE[-1], k, activation=tf.nn.relu, padding='same'))
            # model.add(tf.keras.layers.Dropout(self.dropout))

        model.add(tf.keras.layers.Reshape((dataset.DATA_SHAPE[0], dataset.DATA_SHAPE[2]*dataset.DATA_SHAPE[1])))

        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))
        # RNN
        model.add(tf.keras.layers.RNN([tf.keras.layers.GRUCell(n, dropout=self.dropout) for n in num_gru_units],
                                      return_sequences=True))


        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))


        # model.add(tf.keras.layers.Dense(32, activation=tf.nn.relu))
        # model.add(tf.keras.layers.Dense(64, activation=tf.nn.relu))
        # model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))



        # model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.relu))

        # Decoder
        for k in kernels1Dpost:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))

        model.add(tf.keras.layers.Flatten())


        # Flatten

        # Softmax
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.softmax))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        return model

    def _buildCNN2D(self, kernels1D, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()
        # Encoder
        model.add(tf.keras.layers.Conv2D(dataset.DATA_SHAPE[-1], kernels1D[0], activation=tf.nn.relu, padding='same',
                                         input_shape=dataset.DATA_SHAPE))
        model.add(tf.keras.layers.Dropout(self.dropout))
        for k in kernels1D[1:]:
            model.add(tf.keras.layers.Conv2D(dataset.DATA_SHAPE[-1], k, activation=tf.nn.relu, padding='same'))
            model.add(tf.keras.layers.Dropout(self.dropout))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        # Softmax
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.softmax))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        return model

# tensorboard --logdir=E:/PDclass/LOO_ED5/scalars
PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'allSubs_Freeze2SidesVideo.pickle'

with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)

kernels1D = [30, 20, 10, 5]
kernels1Dpost = [30, 20, 10, 5]
# kernels1Dpost = []
fullyCon = [32, 64]
# num_gru_units = None
num_gru_units = [5 for _ in range(1)]
batchSize = 500
trainSize = 40000
# testSize = 6000
resultsParent = "E:/PDclass/LOO_ED5/"
maxEpochs = 100

for sub in allSubs:
    # if 'PD010' in sub:
    modelNameP = 'LOO_%s' % sub
    testSize = [sub]
    dataset = PDDataset(allSubs, trainSize, testSize, batchSize, tmax=50) #, channels=range(9))
    modelC = modelContainer(dataset, kernels1D, kernels1Dpost, num_gru_units, fullyCon, resultsParent)
    # model = modelC.ED_RNN
    for model, mn in zip([modelC.ED_RNN, modelC.CNN2D], ['ED', 'CNN']):
        if 'CNN' in mn:
            continue
        modelName = '%s_%s' % (modelNameP, mn)
        resultsPath = resultsParent + modelName + '/'
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=resultsParent + 'scalars/' + modelName)

        model.fit(dataset.train.x, dataset.train.y, batch_size=batchSize, epochs=maxEpochs,
                  validation_data=(dataset.test.x, dataset.test.y), callbacks=[tensorboard_callback])

        tu.createDirIfNotExist(resultsPath)
        model.save(resultsPath + modelName + '.h5')
