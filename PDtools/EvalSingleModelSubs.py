import pickle

import numpy as np
import pandas
import tensorflow as tf

import Tools.Utils as tu
import Tools.NNUtils as ntu


# tf.compat.v1.disable_eager_execution()

def modifyPhaseY(y, freezingE: pandas.DataFrame, ignoreMicro):
    for i, val in freezingE.iterrows():
        if ignoreMicro and val['Event'] == 3:
            continue
        if val['Start'] > y.size or val['End'] > y.size:
            continue
        y[int(val['Start']*freq):int(val['End']*freq)] = 1
    return y


def evaluateSub(subID: str, model: tf.keras.Model, timeSize, normalPress, transposeX):
    lPath = PD_path + subID + '/6MWT_L.bin'
    rPath = PD_path + subID + '/6MWT_R.bin'
    [r, _] = tu.binaryFile2python(rPath)
    [_, l] = tu.binaryFile2python(lPath)
    filterv = False
    r.createPandasDataFrame(useSync=True, freq=freq, filter=filterv)
    l.createPandasDataFrame(useSync=True, freq=freq, filter=filterv)

    freezePath = PD_path + subID + '/FreezeLog.csv'
    frO = pandas.read_csv(freezePath)
    channels = range(0, 12)
    xR = r.dataFrame.values[:, channels]
    xL = l.dataFrame.values[:, channels]
    tMax = min([xR.shape[0], xL.shape[0]])
    ytt = np.zeros(tMax)
    y = ntu.createbatch(np.expand_dims(modifyPhaseY(ytt, frO, ignoreMicro=False), 1), timeSize)
    y = np.squeeze(y)
    xR = xR[:tMax]
    xL = xL[:tMax]
    xx = np.empty((tMax, timeSize, xR.shape[1], 2))
    xx[:, :, :, 0] = ntu.createbatch(xR, timeSize)
    xx[:, :, :, 1] = ntu.createbatch(xL, timeSize)
    if transposeX:
        xx = np.transpose(xx, (0, 1, 3, 2))
    if normalPress:
        if transposeX:
            xx[:, :, :, :3] /= 4096
        else:
            xx[:, :, :3, :] /= 4096
    # yRpred = model.predict(xRbatch, batch_size=500)
    yLpred = model.predict(xx, batch_size=batchSize)

    data = { #'rShoe': r.dataFrame, 'lShoe': l.dataFrame,
            'yTruth': y, 'yPred': yLpred, 'subID': subID}
    return data


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'allSubs_Freeze2SidesVideoSplit40_params.pickle'

with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'evaluatedSubsSingle40.pickle'
# modelTypes = ['CNN', 'ED']
modelTypes = ['ED']
# # timeMax = 50
# # freq = 100
batchSize = allSubs['batchSize']
tmax = allSubs['tmax']
channels = allSubs['channels']
normalPress = allSubs['normalPress']
validSubs = allSubs['validSubs']
freq = allSubs['freq']
modelName = allSubs['modelName']
transposeX = allSubs['transposeX']
# ss = [k for k in allSubs][1:]
# validSubs = [int(k[2:]) for k in ss]
# allSubs['validSubs'] = validSubs
# samplesPerSubject = 6000
allSubs2 = {'dbPars': allSubs}
# allSubs2 = {}

for k in [0]:
    subID = 'PD%03d' % k
    print('Loading: %s' % subID)
    oneSub = {}
    for modT in modelTypes:
        modelPath = modelName
        print('Loading: %s' % modelPath)
        model = tf.keras.models.load_model(modelPath)
        oneSub[modT] = {}
        for k2 in validSubs:
            print('Evaluating PD%03d' % k2)
            nloo = evaluateSub('PD%03d' % k2, model, tmax, normalPress, transposeX)
            oneSub[modT]['PD%03d' % k2] = nloo
    allSubs2[subID] = oneSub.copy()

with open(allSubsPath, 'wb') as f:
    pickle.dump(allSubs2, f)
