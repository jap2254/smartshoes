import pickle

import numpy as np
import pandas
import warnings

import Tools.Utils as tu
import Tools.NNUtils as nnTU


def modifyPhaseY(y, freezingE: pandas.DataFrame, ignoreMicro):
    for i, val in freezingE.iterrows():
        if ignoreMicro and val['Event'] == 3:
            continue
        if val['Start'] > y.size or val['End'] > y.size:
            continue
        y[int(val['Start']*freq):int(val['End']*freq)] = 1
    return y


def addLaps2Fog(laps, fogOb, matRaw):
    fogOb['Laps'] = np.nan
    fogOb['InLap'] = np.nan
    matRaw['FogLap'] = 0
    for i, ff in fogOb.iterrows():
        aux = (ff['Start'] >= laps[:-1, 0]) & (ff['Start'] <= laps[1:, 0])
        if aux.sum() != 1:
            print('FoG detected in %d laps' % aux.sum())
        fogOb.loc[i, 'Laps'] = np.argmax(aux)
        fogOb.loc[i, 'InLap'] = int(ff['End'] <= laps[int(fogOb.loc[i, 'Laps']), 1])
        matRaw.loc[matRaw['Laps'] == np.argmax(aux) + 1, 'FogLap'] = 1
    a = 1

PD_path = 'Z:/PD/PD_val/_Experiments/'
validSubs1 = [j for j in range(1, 8)] + [j for j in range(10, 25)]
validSubs = []
freq = 50.0
windowSize = 50
# futureSamples = 50
allSubsPath = PD_path + 'allSubs_Feb2021_RawData.pickle'
samplesSplit = 1
fogSplit = 0.4
validation_split = 0.2

dbPars = {'freq': freq, 'windowSize': windowSize}
allSubs = {'dbPars': dbPars}
totSamples = []
fogEvents = []
matData = []
for k in validSubs1:
    # try:
    subID = 'PD%03d' % k
    print('Loading: %s' % subID)
    lPath = PD_path + subID + '/6MWT_L.bin'
    rPath = PD_path + subID + '/6MWT_R.bin'
    [r, _] = tu.binaryFile2python(rPath)
    [_, l] = tu.binaryFile2python(lPath)
    r.createPandasDataFrame(useSync=True, freq=freq)
    l.createPandasDataFrame(useSync=True, freq=freq)
    freezePath = PD_path + subID + '/FreezeLog.csv'
    frO = pandas.read_csv(freezePath)
    matPath = PD_path + subID + '/6MWT.xlsx'
    matO = tu.MatObject(fName=matPath, freq=freq)
    matO.raw['Sub'] = subID
    xR = r.dataFrame.values
    xL = l.dataFrame.values
    tMax = min([xR.shape[0], xL.shape[0]])
    ytt = np.zeros(tMax)
    fog = modifyPhaseY(ytt, frO, ignoreMicro=False)
    frO['Event'] = 1
    frO['Sub'] = subID
    addLaps2Fog(matO.lap, frO, matO.raw)
    xR = xR[:tMax]
    xL = xL[:tMax]
    x = np.empty((tMax, windowSize, 12, 2))
    x[:, :, :, 0] = nnTU.createbatch(xR, windowSize)
    x[:, :, :, 1] = nnTU.createbatch(xL, windowSize)
    x[:, :, :, :3] /= 4096
    mFoG = np.where(fog == 1)[0]
    if mFoG.size < 100:
        warnings.warn('%s has %d samples of FoG, skip!!!!!' % (subID, mFoG.size))
        continue
    fogEvents.append(frO)
    matData.append(matO.raw)
    allSubs[subID] = {'x': x.copy(), 'fogCont': fog, 'mat': matO, 'fog': frO}
    validSubs.append(k)
    # except:
    #     print('Error in %s' % subID)
    #     pass

allSubs['dbPars']['validSubs'] = validSubs
matData = pandas.concat(matData)
fogEvents = pandas.concat(fogEvents)
allSubs['DataFrames'] = {'mat': matData, 'fog': fogEvents}
print('Dataset has %d samples' % np.array(totSamples).sum())

with open(allSubsPath, 'wb') as f:
    pickle.dump(allSubs, f)

cols = ['Step Length (cm.)', 'Stride Length (cm.)', 'Stride Width (cm.)']
