import os
import pickle

import numpy as np
import tensorflow as tf
from tensorflow.python.framework import graph_io

import Tools.NNUtils as nnU
import Tools.Utils as tu


class FilterRNNmodelClassifier(nnU.NeuralNetworkModel):
    def __init__(self, dataset: nnU.DatasetProto, kernels1D, kernels1Dpost, num_gru_units, fullCon, modelName,
                 activationFn=None):
        self.input_name = 'input_node'
        self.output_name = 'output_node'
        self.input_size = dataset.DATA_SHAPE
        self.activationFn = activationFn
        self.modelName = modelName
        self.fullConnected = fullCon
        input_feed = tf.placeholder(tf.float32, [None, *dataset.DATA_SHAPE], name=self.input_name)
        output_truth = tf.placeholder(tf.int32, [None], name='y_')
        keep_prob = tf.placeholder(tf.float32, name='keep_prob')
        keep_prob_bool = tf.placeholder(tf.bool, name='keep_prob_bool')
        # isTrainV = tf.placeholder(tf.int32, name='isTrainV')
        learn_rate = tf.placeholder(tf.float32, name='learn_rate')
        accThr = tf.placeholder(tf.int32, name='accuracyThreshold')
        self.learn_rate = learn_rate
        self.kernels1Dpost = kernels1Dpost
        self.kernels1D = kernels1D
        self.num_gru_units = num_gru_units
        self.samples = dataset.DATA_SHAPE[0]
        self.channels = dataset.DATA_SHAPE[1]
        nnU.NeuralNetworkModel.__init__(self, input_feed, output_truth, keep_prob, keep_prob_bool, learn_rate,
                                        dataset.CLASSES_NUM, accThr,
                                        builder=self.__buildModel, input_name=self.input_name,
                                        output_name=self.output_name,
                                        input_size=self.input_size)
        self.output_name = 'softmax/' + self.output_name
        # self.num_class = 3
        # self.__buildModel()

    def __buildModel(self):
        # x_image = tf.reshape(self.input_feed, [-1, self.samples, self.channels])
        x_image = self.input_feed
        # x_image = tf.identity(x_image, self.input_name)
        cnn1 = x_image
        # 1D CNN, a.k.a. filters
        for k in self.kernels1D:
            cnn1 = tf.contrib.layers.convolution1d(inputs=cnn1, num_outputs=self.samples, kernel_size=k)
        self.filterOut = cnn1
        if self.num_gru_units is None:
            outputs = cnn1
        else:
            rnn_cell = tf.contrib.rnn.MultiRNNCell([self.RNNcell(nUnits) for nUnits in self.num_gru_units])
            outputs, state = tf.nn.dynamic_rnn(cell=rnn_cell, inputs=cnn1, dtype=tf.float32)
        # cnn_out = tf.contrib.layers.fully_connected(inputs=outputs, num_outputs=3)
        cnn_out = outputs
        for k in self.kernels1Dpost:
            cnn_out = tf.contrib.layers.convolution1d(inputs=cnn_out, num_outputs=self.samples, kernel_size=k)

        y_outputs = tf.contrib.layers.flatten(cnn_out)
        # flatOutputs = tf.reshape(cnn_out, [-1, self.samples * k])
        for j in self.fullConnected:
            with tf.variable_scope('FL%d' % j) as scope:
                y_outputs = tf.contrib.layers.fully_connected(inputs=y_outputs, num_outputs=j,
                                                              activation_fn=self.activationFn)
        with tf.variable_scope('logits') as scope:
            logits = tf.contrib.layers.fully_connected(inputs=y_outputs, num_outputs=self.num_class,
                                                       activation_fn=None)

        self.logits = logits
        with tf.variable_scope('softmax') as scope:
            self.preds = tf.nn.softmax(logits)
            self.output = tf.cast(tf.argmax(self.preds, 1), tf.int32, name=self.output_name)
        self.correct = tf.equal(self.output, self.output_truth, name='AllPred')
        self.loss = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=self.output_truth))
        self.accuracy = tf.reduce_mean(tf.cast(self.correct, tf.float32), axis=0, name='accuracy')
        self.train_step = tf.train.AdamOptimizer(self.learn_rate).minimize(self.loss)
        self.s = tf.shape(logits, name='shapeOf')

    def RNNcell(self, n):
        cell = tf.contrib.rnn.GRUCell(num_units=n)
        cell = tf.contrib.rnn.DropoutWrapper(cell, output_keep_prob=self.keep_prob)
        return cell

    def trainNetwork(self, maxEpochs, dataset, modelName, isRestore, restoreCP, resultsPath, restoreName, learn_rate,
                     earlyStop, pltTestN, initialAccuracy=1.0, saveChk=50):
        print('Training for %d epochs' % maxEpochs)
        if not os.path.exists(resultsPath):
            os.makedirs(resultsPath)
        # t, matR, matL, yyR, yyL = loadTestSubject(pltTestN)
        thrVal = self.accThr
        logInfo = ""
        stopVal = 5
        thr2 = initialAccuracy
        with tf.Session() as sess:
            if restoreCP:
                self.restoreModel(sess, resultsPath + 'chkp')
            elif isRestore:
                self.restoreModel(sess, resultsPath + restoreName)
            else:
                sess.run(tf.global_variables_initializer())
            print('variables init')
            c = -1
            for i in range(maxEpochs):
                tTraainVal = 1e-4
                dataset.train.sameBatch = True
                train_cost = 0.0
                train_accuracy = 0.0
                thisC = c
                while dataset.train.sameBatch:
                    dataset.train.nextBatch(np.random.randint(dataset.batchsize * 0.1))
                    batch = [dataset.train.subX, dataset.train.subY]
                    dictInput = {self.input_feed: batch[0], self.output_truth: batch[1], self.keep_prob: 1.0,
                                 thrVal: 0.0, learn_rate: tTraainVal}
                    train_accuracy += self.accuracy.eval(feed_dict=dictInput)
                    train_cost += self.loss.eval(feed_dict=dictInput)
                    dictInput[self.keep_prob] = 0.5
                    self.train_step.run(feed_dict=dictInput)
                    c += 1
                train_accuracy = train_accuracy / (c - thisC)
                train_cost = train_cost / (c - thisC)
                dataset.test.sameBatch = True
                cTest = 0
                test_accuracy = 0.0
                while dataset.test.sameBatch:
                    dataset.test.nextBatch()
                    batch = [dataset.test.subX, dataset.test.subY]
                    dictInput = {self.input_feed: batch[0], self.output_truth: batch[1], self.keep_prob: 1.0,
                                 thrVal: 0.0, learn_rate: tTraainVal}
                    test_accuracy += self.accuracy.eval(feed_dict=dictInput)
                    cTest += 1
                test_accuracy /= cTest
                nowLog = "Epoch %d, step %d, training accuracy %.3g, test accuracy %.3g, cost mean %.6g" % \
                         (i, c, train_accuracy, test_accuracy, train_cost)
                print(nowLog)
                logInfo = logInfo + nowLog + '\n'
                if i % saveChk == 0:
                    print(self.saver.save(sess, resultsPath + 'chkp', global_step=i))

                if earlyStop and os.path.isfile(resultsPath + 'STOP.txt'):
                    break
            print(self.saver.save(sess, resultsPath + 'chkp', global_step=i))

            dataset.test.sameBatch = True
            cTest = 0
            test_accuracy = 0.0
            while dataset.test.sameBatch:
                dataset.test.nextBatch()
                batch = [dataset.test.subX, dataset.test.subY]
                dictInput = {self.input_feed: batch[0], self.output_truth: batch[1], self.keep_prob: 1.0,
                             thrVal: 0.0, learn_rate: tTraainVal}
                test_accuracy += self.accuracy.eval(feed_dict=dictInput)
                cTest += 1
            lastLog = "Final test accuracy %.3g" % (test_accuracy / cTest)
            print(lastLog)
            logInfo = logInfo + lastLog
            # print("saving model in " + resultsPath + modelName + '.ckpt')
            print(self.saveModel(sess, resultsPath + modelName))
            # print(saver.save(sess, resultsPath + modelName))
            # self.__optimizeModel(sess.graph)
            graph_io.write_graph(sess.graph, resultsPath, modelName + '_graph.pb')
            with open(resultsPath + modelName + '_Log.txt', 'w') as f:
                f.write(logInfo)


class PDDataset(nnU.DatasetProto):
    def __init__(self, allSubs, trainsize, testSize, batchSize):
        self.allSubs = allSubs
        x = []
        y = []
        xT = []
        yT = []
        useSubTestMode = type(testSize) is not int
        if useSubTestMode:
            # This means that you want to split
            totalSamsPerSub = np.ceil(trainsize / len(allSubs)).astype(np.int)
        else:
            totalSamsPerSub = np.ceil((testSize + trainsize) / len(allSubs)).astype(np.int)

        for subName in allSubs:
            sub = allSubs[subName]
            if useSubTestMode and sub in testSize:
                xT.append(sub['x'])
                yT.append(sub['y'][:, -1])
            else:
                shu = tu.randomOrder(sub['x'].shape[0])
                x.append(sub['x'][shu[:totalSamsPerSub]])
                y.append(sub['y'][shu[:totalSamsPerSub], -1])

        if useSubTestMode:
            xx = np.concatenate(x, axis=0)
            yy = np.concatenate(y, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            x = xx[shu].copy()
            y = yy[shu].copy()
            xx = np.concatenate(xT, axis=0)
            yy = np.concatenate(yT, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            xT = xx[shu]
            yT = yy[shu]
        else:
            xx = np.concatenate(x, axis=0)
            yy = np.concatenate(y, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            xT = xx[shu][trainsize:]
            yT = yy[shu][trainsize:]

            x = xx[shu][:trainsize]
            y = yy[shu][:trainsize]

        reduceFreeze = False
        numClasses = 3
        if reduceFreeze:
            m = y == 3
            y[m] = 2
            m = yT == 3
            yT[m] = 2
            numClasses = 3

        nnU.DatasetProto.__init__(self, x, y, xT, yT, batchSize, numclases=numClasses)


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'allSubs_Freeze.pickle'

with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)

kernels1D = [16, 32, 64]
kernels1Dpost = [16, 32, 64]
fullyCon = [512]
# num_gru_units = None
num_gru_units = [10 for _ in range(1)]
batchSize = 5000
trainSize = 40000
testSize = 10000
dataset = PDDataset(allSubs, trainSize, testSize, batchSize)
modelName = 'CNN'
model = FilterRNNmodelClassifier(dataset, kernels1D, kernels1Dpost, num_gru_units, fullyCon, modelName, tf.nn.relu)

maxEpochs = 1
isRestore = False
restoreCP = False
resultsPath = "E:/PDclass/" + modelName + '/'
tu.createDirIfNotExist(resultsPath)
with open(resultsPath + modelName + 'DBoptions.pickle', 'wb') as f:
    dbOptions = {'num_gru_units': num_gru_units, 'kernels1D': kernels1D, 'kernels1Dpost': kernels1Dpost,
                 'trainSize': trainSize, 'testSize': testSize}
    pickle.dump(dbOptions, f)
# restoreName = modelName
restoreName = 'chkp-%d' % 999
learn_rate = model.learn_rate
earlyStop = True
model.trainNetwork(maxEpochs, dataset, modelName, isRestore, restoreCP, resultsPath, restoreName, learn_rate, earlyStop,
                   pltTestN=False)

model.freezeModel(resultsPath, modelName, optimize=True)
