import pickle

import numpy as np
import tensorflow as tf

import Tools.NNUtils as nnU
import Tools.Utils as tu


class PDDataset(nnU.DatasetProto):
    def __init__(self, allSubs, trainsize, testSize, batchSize):
        self.allSubs = allSubs
        x = []
        y = []
        xT = []
        yT = []
        useSubTestMode = type(testSize) is not int
        if useSubTestMode:
            # This means that you want to split
            totalSamsPerSub = np.ceil(trainsize / len(allSubs)).astype(np.int)
        else:
            totalSamsPerSub = np.ceil((testSize + trainsize) / len(allSubs)).astype(np.int)

        for subName in allSubs:
            sub = allSubs[subName]
            if useSubTestMode and subName in testSize:
                xT.append(sub['x'])
                yT.append(sub['y'][:, -1])
            else:
                shu = tu.randomOrder(sub['x'].shape[0])
                x.append(sub['x'][shu[:totalSamsPerSub]])
                y.append(sub['y'][shu[:totalSamsPerSub], -1])

        if useSubTestMode:
            xx = np.concatenate(x, axis=0)
            yy = np.concatenate(y, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            x = xx[shu].copy()
            y = yy[shu].copy()
            xx = np.concatenate(xT, axis=0)
            yy = np.concatenate(yT, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            xT = xx[shu]
            yT = yy[shu]
        else:
            xx = np.concatenate(x, axis=0)
            yy = np.concatenate(y, axis=0)
            shu = tu.randomOrder(xx.shape[0])
            xT = xx[shu][trainsize:]
            yT = yy[shu][trainsize:]

            x = xx[shu][:trainsize]
            y = yy[shu][:trainsize]

        reduceFreeze = False
        numClasses = 2
        if reduceFreeze:
            m = y == 3
            y[m] = 2
            m = yT == 3
            yT[m] = 2
            numClasses = 3

        nnU.DatasetProto.__init__(self, x, y, xT,
                                  yT,
                                  batchSize, numclases=numClasses)


class modelContainer(object):
    def __init__(self, dataset: nnU.DatasetProto, kernels1D, kernels1Dpost, num_gru_units, fullyCon, resultsPath):
        self.dataset = dataset
        self.learningRate = 1e-4
        self.ED_RNN = self._buildCRNN(kernels1D, kernels1Dpost, num_gru_units, fullyCon, self.learningRate)
        self.RNN = self._buildRNN(num_gru_units, fullyCon, self.learningRate)
        self.CNN = self._buildCNN(kernels1D, fullyCon, self.learningRate)
        self.Ln = self._buildLn(fullyCon, self.learningRate)
        self.ED_Ln = self._buildEDLn(kernels1D, kernels1Dpost, fullyCon, self.learningRate)

    def _buildCRNN(self, kernels1D, kernels1Dpost, num_gru_units, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()
        # Encoder
        model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], kernels1D[0], activation=tf.nn.relu, padding='same',
                                         input_shape=dataset.DATA_SHAPE))
        for k in kernels1D[1:]:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))
        # RNN
        model.add(tf.keras.layers.RNN([tf.keras.layers.GRUCell(n, dropout=0.5) for n in num_gru_units],
                                      return_sequences=True))

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.relu))

        # Decoder
        for k in kernels1Dpost:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[1], k, activation=tf.nn.relu, padding='same'))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # Softmax
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.softmax))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        return model

    def _buildEDLn(self, kernels1D, kernels1Dpost, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()
        # Encoder
        model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[0], kernels1D[0], activation=tf.nn.relu, padding='same',
                                         input_shape=dataset.DATA_SHAPE))
        for k in kernels1D[1:]:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[0], k, activation=tf.nn.relu, padding='same'))

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.relu))

        # Decoder
        for k in kernels1Dpost:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[0], k, activation=tf.nn.relu, padding='same'))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # Softmax
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.softmax))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        return model

    def _buildRNN(self, num_gru_units, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()

        # RNN
        model.add(tf.keras.layers.RNN([tf.keras.layers.GRUCell(n, dropout=0.5) for n in num_gru_units],
                                      return_sequences=True, input_shape=dataset.DATA_SHAPE))

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # Softmax
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.softmax))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        return model

    def _buildCNN(self, kernels1D, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()
        # Encoder
        model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[0], kernels1D[0], activation=tf.nn.relu, padding='same',
                                         input_shape=dataset.DATA_SHAPE))
        for k in kernels1D[1:]:
            model.add(tf.keras.layers.Conv1D(dataset.DATA_SHAPE[0], k, activation=tf.nn.relu, padding='same'))

        # Flatten
        model.add(tf.keras.layers.Flatten())

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        # Softmax
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.softmax))
        opt = tf.keras.optimizers.Adam(lr=learningRate)

        model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        return model

    def _buildLn(self, fullyCon, learningRate) -> tf.keras.Model:
        dataset = self.dataset
        model = tf.keras.Sequential()

        # Flatten
        model.add(tf.keras.layers.Flatten(input_shape=dataset.DATA_SHAPE))

        # dense layers
        for k in fullyCon:
            model.add(tf.keras.layers.Dense(k, activation=tf.nn.relu))

        # Softmax
        model.add(tf.keras.layers.Dense(dataset.CLASSES_NUM, activation=tf.nn.softmax))

        opt = tf.keras.optimizers.Adam(lr=learningRate)
        model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        return model


PD_path = 'E:/Drive/JAP2254/Lab/SmartShoes/Healthy/'
allSubsPath = PD_path + 'allSubsHealthy.pickle'

with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)

kernels1D = [20, 10, 5]
kernels1Dpost = [20, 10, 5]
fullyCon = [2]
# num_gru_units = None
num_gru_units = [5 for _ in range(1)]
batchSize = 2000
HEALTHY_SUBJECTS = [1, 2, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 19, 21, 22, 25, 27, 28, 29, 30, 31, 34, 35, 36, 37,
                    39]
SYMMETRIC_SUBJECTS = [50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60]
ASYMMETRIC_SUBJECTS = [61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71]
for j in SYMMETRIC_SUBJECTS:
    del allSubs['Sbj%03d' % j]
trainSize = 100000
testSize = ['Sbj%03d' % k for k in ASYMMETRIC_SUBJECTS]
dataset = PDDataset(allSubs, trainSize, testSize, batchSize)
modelName = 'ED_RNN_Healthy_Assy3'
resultsParent = "E:/PDclass/"
resultsPath = resultsParent + modelName + '/'
reTrain = True
if reTrain:
    maxEpochs = 50
    modelC = modelContainer(dataset, kernels1D, kernels1Dpost, num_gru_units, fullyCon, resultsParent)
    model = modelC.ED_RNN
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=resultsParent + 'scalars/' + modelName)

    model.fit(dataset.train.x, dataset.train.y, batch_size=batchSize, epochs=maxEpochs,
              validation_data=(dataset.test.x, dataset.test.y), callbacks=[tensorboard_callback])

    tu.createDirIfNotExist(resultsPath)
    model.save(resultsPath + modelName + '.h5')
else:
    model = tf.keras.models.load_model(resultsPath + modelName + '.h5')

for l in model.layers:
    print(l._name)
    print(l.output_shape)
