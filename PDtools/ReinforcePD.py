import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import tensorflow as tf


class reModel(object):
    def __init__(self, samples, channels):
        self.samples = samples
        self.channels = channels
        self.kernels1D = [20, 10, 5]
        self.kernels1Dpost = [20, 10, 5]
        self.num_gru_units = 11

    def __buildModel(self):
        x_image = tf.reshape(self.input_feed, [-1, self.samples, self.channels])
        # 1D CNN, a.k.a. filters
        for k in self.kernels1D:
            x_image = tf.contrib.layers.convolution1d(inputs=x_image, num_outputs=self.samples, kernel_size=k)

        rnn_cell = tf.contrib.rnn.GRUCell(num_units=self.num_gru_units)
        rnn_cell = tf.contrib.rnn.DropoutWrapper(rnn_cell, output_keep_prob=self.config.train_keep_prob)
        outputs, state = tf.nn.dynamic_rnn(cell=rnn_cell, inputs=x_image, dtype=tf.float32)
        y_outputs = tf.contrib.layers.fully_connected(inputs=outputs,
                                                      num_outputs=self.config.num_class)  # it is before softmax
        for k in self.kernels1Dpost:
            y_outputs = tf.contrib.layers.convolution1d(inputs=y_outputs, num_outputs=self.config.num_class,
                                                        kernel_size=k)

        # sigmoid to make it between 0-1
        currentVal = tf.squeeze(y_outputs[:, -1, :])
        self.output = tf.nn.sigmoid(currentVal, name='output_node')
