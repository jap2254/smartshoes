import numpy as np
import pandas
import Tools.Utils as tu
import warnings


def wrap(angles):
    return (angles + np.pi) % (2 * np.pi) - np.pi


class pdSub(tu.ShoeSubject):
    def __init__(self, leftShoe: tu.smartShoe, rightShoe: tu.smartShoe, subID: int, freezer: bool, freq: float,
                 testType: str = 'Sub', matInfo: tu.MatObject = None, cutSync=False, n=400):
        self.freq = freq
        leftShoe.createPandasDataFrame(useSync=cutSync, freq=freq)
        rightShoe.createPandasDataFrame(useSync=cutSync, freq=freq)
        for shoe in [leftShoe, rightShoe]:
            eu = np.array(shoe.all[9:]).T
            # do for each vector
            for j in range(3):
                vAux = tu.body2spaceEuler(np.array(shoe.all[3 * j:3 * j + 3]).T, eu, freeze=True)
                # print(vAux.shape)
                for i in range(vAux.shape[1]):
                    shoe.all[3 * j + i] = [k for k in vAux[:, i]]

        tu.ShoeSubject.__init__(self, leftShoe, rightShoe, subID, testType, matInfo, cutSync)
        self.freezer = freezer
        self.__cutShoesPerStep(n)

    def __cutShoesPerStep(self, n):
        allSteps = []
        for shoe, side in zip((self.leftShoe, self.rightShoe), ('L', 'R')):
            y = self.matInfo.binaryFunctions[side]
            print('Doing ' + side)
            hs = y.index.where(y.diff() == -1).dropna().values
            for st, en in zip(hs[:-1], hs[1:]):
                # print('Stride from %f:%f' % (st, en))
                if en - st > 2:
                    continue
                lap = np.where(np.logical_and(st >= self.matInfo.lap[:, 0], st <= self.matInfo.lap[:, 1]))[0]
                if len(lap.shape) > 1:
                    warnings.warn('HS detected in 2 laps???')
                    lap = lap[0]
                if len(lap.shape) == 0:
                    a = 1
                stI = int(st * self.freq)
                enI = int(en * self.freq)
                if stI > shoe.dataFrame.shape[0] or enI > shoe.dataFrame.shape[0]:
                    warnings.warn('HS outside of shoe recording')
                    continue
                stride = pandas.DataFrame(data=tu.reSampleMatrix(shoe.dataFrame.values[stI:enI, :], n), columns=shoe.names)
                stride['lap'] = lap[0]
                stride['side'] = side
                allSteps.append(stride.copy())

        self.allStrides = pandas.concat(allSteps)
        self.allStrides.reset_index(inplace=True)



PD_path = 'Z:/PD/PD_val/_Experiments/'
params2record = ['Stride Length (cm.)', 'Stride Time (sec.)']
meanPars = pandas.DataFrame(columns=params2record)
# these are the subjects that froze during the test
# freezer = [True, True, True, False, True, False, False, True, False, True, False, False, True, False, False, True, True,
#            True, False, False, False, False, True, True, False]
# these are the subjects that froze during the test or where diagnosed as freezers
freezer = [True, True, True, False, True, True, True, True, False, True, False, False, True, True, False, True, True,
           True, False, False, False, True, True, True, False]
freq = 100.0
allSubs = {}
for k, fr in zip(range(1, 2), freezer):
    subID = 'PD%03d' % k
    print('Loading: %s' % subID)
    # try:
    lPath = PD_path + subID + '/' + '6MWT_L.bin'
    rPath = PD_path + subID + '/' + '6MWT_R.bin'
    matPath = PD_path + subID + '/' + '6MWT.xlsx'
    allSubs[subID] = pdSub(tu.binaryFile2python(lPath)[1], tu.binaryFile2python(rPath)[0], k, fr, freq, '6MWT',
                           tu.MatObject(matPath, freq=freq, newMat=True), True)
    # [r, _] = tu.binaryFile2python(rPath)
    # [_, l] = tu.binaryFile2python(lPath)
    # rDt = np.diff(np.array(r.timestamp))
    # lDt = np.diff(np.array(l.timestamp))
    # print('DT %f +- %f, %f +- %f' % (rDt.mean(), rDt.std(), lDt.mean(), lDt.std()))
    matO = allSubs[subID].matInfo
    meanPars.loc[subID, params2record] = matO.raw[params2record].std() / matO.raw[params2record].mean()

# meanPars['Freezer'] = np.array(freezer, dtype=np.int)

# except:
#     print('Error in %s' % subID)
