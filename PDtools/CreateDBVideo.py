import pickle

import numpy as np
import pandas
import warnings

import Tools.Utils as tu
import Tools.NNUtils as nnTU


def modifyPhaseY(y, freezingE: pandas.DataFrame, ignoreMicro):
    for i, val in freezingE.iterrows():
        if ignoreMicro and val['Event'] == 3:
            continue
        if val['Start'] > y.size or val['End'] > y.size:
            continue
        y[int(val['Start']*freq):int(val['End']*freq)] = 1
    return y


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'allSubs_Freeze2SidesVideoLarge.pickle'
validSubs1 = [j for j in range(1, 8)] + [j for j in range(10, 25)]
validSubs = []
freq = 50.0
samplesPerSubject = 16000
dbPars = {'freq': freq}
allSubs = {'dbPars': dbPars}
for k in validSubs1:
    # try:
    subID = 'PD%03d' % k
    print('Loading: %s' % subID)
    lPath = PD_path + subID + '/6MWT_L.bin'
    rPath = PD_path + subID + '/6MWT_R.bin'
    [r, _] = tu.binaryFile2python(rPath)
    [_, l] = tu.binaryFile2python(lPath)
    r.createPandasDataFrame(useSync=True, freq=freq)
    l.createPandasDataFrame(useSync=True, freq=freq)
    freezePath = PD_path + subID + '/FreezeLog.csv'
    frO = pandas.read_csv(freezePath)
    xR = r.dataFrame.values
    xL = l.dataFrame.values
    tMax = min([xR.shape[0], xL.shape[0]])
    ytt = np.zeros(tMax)
    y = nnTU.createbatch(np.expand_dims(modifyPhaseY(ytt, frO, ignoreMicro=False), 1), 50)
    y = np.squeeze(y)
    xR = xR[:tMax]
    xL = xL[:tMax]
    x = np.empty((tMax, 50, 12, 2))
    x[:, :, :, 0] = nnTU.createbatch(xR, 50)
    x[:, :, :, 1] = nnTU.createbatch(xL, 50)
    m = tu.randomOrder(x.shape[0])[:samplesPerSubject]
    # m = np.empty(samplesPerSubject, dtype=np.int)
    mFoG = np.where(y == 1)[0]
    shu = tu.randomOrder(mFoG.size)
    mFoG = mFoG[shu]
    mNFoG = np.where(y == 0)[0]
    shu = tu.randomOrder(mNFoG.size)
    mNFoG = mNFoG[shu]
    if mFoG.size < samplesPerSubject//2:
        warnings.warn('%s has %d samples of FoG, skip!!!!!' % (subID, mFoG.size))
        continue
    # m[:mFoG.size] = mFoG
    # m[mFoG.size:] = mNFoG[samplesPerSubject-mFoG.size]
    # else:
    validSubs.append(k)
    m[:samplesPerSubject//2] = mFoG[:samplesPerSubject//2]
    m[samplesPerSubject // 2:] = mNFoG[:1*samplesPerSubject//2]
    # m = tu.randomOrder(x.shape[0])[:samplesPerSubject]
    allSubs[subID] = {'x': x[m].copy(), 'y': y[m].copy()}
    # except:
    #     print('Error in %s' % subID)
    #     pass

allSubs['dbPars']['validSubs'] = validSubs

with open(allSubsPath, 'wb') as f:
    pickle.dump(allSubs, f)
