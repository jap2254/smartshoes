import pickle
import pandas
import numpy as np

save_path = 'E:/PDclass/Future/'
# for futureSamples in [10, 20, 30, 40, 50]:
futureSamples = 50
with open('%sFuture%d.pickle' % (save_path, 50), 'rb') as f:
    data2save = pickle.load(f)
allData = []
for futureSamples in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50]:
    data = pandas.DataFrame(data={'gt': data2save[futureSamples]['gt'],
                                  'pred': np.argmax(data2save[futureSamples]['pred'], 1),
                                  'Sub': data2save[futureSamples]['labels']})
    mFog = data['pred'] == 1
    mReg = data['pred'] == 0
    data['Pos'] = data['pred'].loc[mFog] == data['gt'].loc[mFog]
    data['Neg'] = data['pred'].loc[mReg] == data['gt'].loc[mReg]
    data['TP'] = data['Pos'] == True
    data['FP'] = data['Pos'] == False
    data['TN'] = data['Neg'] == True
    data['FN'] = data['Neg'] == False
    dataMetrics = data.groupby('Sub')[['TP', 'FP', 'TN', 'FN']].sum().reset_index()
    dataMetrics['Sensitivity'] = dataMetrics['TP'] / (dataMetrics['TP'] + dataMetrics['FN']) * 100
    dataMetrics['Specificity'] = dataMetrics['TN'] / (dataMetrics['TN'] + dataMetrics['FP']) * 100
    dataMetrics['Precision'] = dataMetrics['TP'] / (dataMetrics['TP'] + dataMetrics['FP']) * 100
    dataMetrics['Accuracy'] = (dataMetrics['TP'] + dataMetrics['TN']) / (dataMetrics['TP'] + dataMetrics['FP'] +
                                                                         dataMetrics['TN'] + dataMetrics['FN']) * 100
    dataMetrics['Future'] = futureSamples
    allData.append(dataMetrics)

allData = pandas.concat(allData)
