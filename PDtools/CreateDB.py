import pickle

import numpy as np
import pandas

import Tools.Utils as tu
import Tools.NNUtils as nnTU


def modifyPhaseY(matO: tu.MatObject, freezingE: pandas.DataFrame, ignoreMicro):
    for i, val in freezingE.iterrows():
        if ignoreMicro and val['Event'] == 3:
            continue
        m = np.logical_and(val['Start'] <= matO.binaryFunctions.index.values,
                           matO.binaryFunctions.index.values <= val['End'])
        matO.binaryFunctions.iloc[m, :2] = val['Event']
        # extend lap time if necessary
        isInLap = np.logical_and(val['Start'] > matO.lap[:, 0],
                                 val['End'] <= matO.lap[:, 1])
        if (~isInLap).all():
            isInLap = val['End'] > matO.lap[:, 1]
            m = np.where(isInLap)[0][-1]
            matO.lap[m, 1] = val['End']


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'allSubs_Freeze2Sides.pickle'
validSubs = [j for j in range(1, 8)] + [j for j in range(10, 25)]

freq = 100.0
samplesPerSubject = 6000
allSubs = {}
for k in validSubs:
    try:
        subID = 'PD%03d' % k
        print('Loading: %s' % subID)
        lPath = PD_path + subID + '/6MWT_L.bin'
        rPath = PD_path + subID + '/6MWT_R.bin'
        [r, _] = tu.binaryFile2python(rPath)
        [_, l] = tu.binaryFile2python(lPath)
        r.createPandasDataFrame(useSync=True, freq=freq)
        l.createPandasDataFrame(useSync=True, freq=freq)
        matPath = PD_path + subID + '/6MWT.xlsx'
        if k < 11:
            matO = tu.MatObject(fName=matPath, freq=freq)
        else:
            matO = tu.MatObject(fName=matPath, freq=freq, newMat=True)
        freezePath = PD_path + subID + '/FreezeLog.csv'
        frO = pandas.read_csv(freezePath)
        modifyPhaseY(matO, frO, ignoreMicro=False)
        yR = matO.binaryFunctions['R'].values
        yL = matO.binaryFunctions['L'].values
        xR = r.dataFrame.values
        xL = l.dataFrame.values
        laps = matO.lap.copy()
        tMax = min([yR.shape[0], yL.shape[0], xR.shape[0], xL.shape[0]])
        yR = yR[:tMax]
        yL = yL[:tMax]
        xR = xR[:tMax]
        xL = xL[:tMax]

        x, y, _ = nnTU.createTemporalSamples2Sides(yR, yL, xR, xL, laps,
                                                   freq, freq, samplesPerSubject, 50, 50)
        allSubs[subID] = {'x': x.copy(), 'y': y.copy()}
    except:
        print('Error in %s' % subID)
        pass

with open(allSubsPath, 'wb') as f:
    pickle.dump(allSubs, f)
