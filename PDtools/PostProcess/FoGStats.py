import Tools.Stats as mSt
import pickle
import Tools.Utils as tu
import Tools.PlotUtils as put
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt


PD_path = 'Z:/PD/PD_val/_Experiments/'
PD_path = 'E:/Drive/JAP2254/Lab/SmartShoes/PD/'
figPath = 'E:/Drive/JAP2254/Lab/SmartShoes/PD/Publications/Feb/Gait/Normal/'
allSubsPath = PD_path + 'allSubs_Feb2021_dataframe.pickle'

with open(allSubsPath, 'rb') as f:
    allData = pickle.load(f)

matData = allData['mat']
fogEvents = allData['fog']
cols = ['Step Length (cm.)', 'Stride Length (cm.)', 'Stride Width (cm.)', 'Stance Time (sec.)',
        'Swing Time (sec.)', 'Toe In/Out Angle (degrees)', 'Stance COP Dist. (cm.)', 'Step Time (sec.)',
        'Stride Time (sec.)', 'Stride Velocity (cm./sec.)']
colsSave = ['Step Length', 'Stride Length', 'Stride Width', 'Stance Time',
            'Swing Time', 'Toe InOut Angle', 'Stance COP Dist', 'Step Time',
            'Stride Time', 'Stride Velocity']
colsSig = [False for _ in cols]
colsSig[2] = True
colsSig[5] = True
colsSig[6] = True

spatial = ['Step Length (cm.)', 'Stride Length (cm.)', 'Stride Width (cm.)']
temporal = ['Stance Time (sec.)', 'Swing Time (sec.)', 'Step Time (sec.)', 'Stride Time (sec.)',
            'Stride Velocity (cm./sec.)']
balance = ['Toe In/Out Angle (degrees)', 'Stance COP Dist. (cm.)']
colsPars = ['Laps', 'isLeft', 'StepIndex', 'num', 'Index', 'Sub', 'FogLap']

# cols = ['Foot Length (cm.)', 'Foot Width (cm.)', 'Step Length (cm.)', 'Stride Length (cm.)',
#         'Stride Width (cm.)', 'Step Time (sec.)', 'Stride Time (sec.)','Stride Velocity (cm./sec.)',
#         'Stance Time (sec.)', 'Swing Time (sec.)', 'Single Support (sec.)', 'Initial D. Support (sec.)',
#         'Terminal D. Support (sec.)', 'Total D. Support (sec.)', 'CISP Time (sec.)']
# cols = ['Integ. Pressure (p x sec.)', 'Foot Length (cm.)', 'Foot Length %', 'Foot Width (cm.)',
#         'Foot Area (cm. x cm.)', 'Foot Angle (degrees)', 'Toe In/Out Angle (degrees)',
#         'Foot Toe X Location (cm.)', 'Foot Toe Y Location (cm.)', 'Foot Heel X Location (cm.)',
#         'Foot Heel Y Location (cm.)', 'Foot Center X Location (cm.)', 'Foot Center Y Location (cm.)',
#         'Step Length (cm.)', 'Absolute Step Length (cm.)', 'Stride Length (cm.)', 'Stride Width (cm.)',
#         'Step Time (sec.)', 'Stride Time (sec.)', 'Stride Velocity (cm./sec.)', 'DOP (degrees)',
#         'Gait Cycle Time (sec.)', 'Stance Time (sec.)', 'Stance %', 'Swing Time (sec.)', 'Swing %',
#         'Single Support (sec.)', 'Single Support %', 'Initial D. Support (sec.)', 'Initial D. Support %',
#         'Terminal D. Support (sec.)', 'Terminal D. Support %', 'Total D. Support (sec.)',
#         'Total D. Support %', 'CISP Time (sec.)', 'CISP AP (%)', 'CISP ML (%)', 'Stance COP Dist. (cm.)',
#         'SS COP Dist. (cm.)', 'DS COP Dist. (cm.)', 'Stance COP Dist. %', 'SS COP Dist. %',
#         'Stance COP Path Eff. %', 'SS COP Path Eff. %', 'DS COP Path Eff.%']

print('Mean')
means = mSt.rmANOVA_rmFriedman(matData, 'Sub', 'FogLap', cols)
print('CV')
cv = mSt.rmANOVA_rmFriedman(matData, 'Sub', 'FogLap', cols, aggregatFn=lambda x: x.std() / x.mean())

matData['Gait'] = ''
matData['x'] = ''
matData.loc[matData['FogLap'].astype(np.bool), 'Gait'] = 'FOG'
matData.loc[~matData['FogLap'].astype(np.bool), 'Gait'] = 'REG'
matStacked = tu.pandasPivot(matData, cols, colsPars)
matStacked['DataType'] = ''

for name, typ in zip(['Spatial', 'Temporal', 'Balance'], [spatial, temporal, balance]):
    m = np.logical_or.reduce([matStacked['Type'] == n for n in typ])
    matStacked.loc[m, 'DataType'] = name
for typ in ['Spatial', 'Temporal', 'Balance']:
    sns.catplot(data=matStacked.loc[matStacked['DataType'] == typ], x='Type', y='Vals', hue='FogLap', kind='box')

for c, s, cs in zip(cols, colsSig, colsSave):
    ax = sns.catplot(data=matData, x='x', y=c, hue='Gait', kind='box', showfliers=False)
    if s:
        put.addSignificanceLevel(ax.ax, 0, 1, matData, '*', c)
    ax.tight_layout()

    ax.savefig(figPath + cs + '.svg')
    plt.close()

for c, s, cs in zip(cols, colsSig, colsSave):
    ax = sns.catplot(data=matData, x='x', y=c, hue='Gait', kind='bar', ci='sd')
    if s:
        put.addSignificanceLevel(ax.ax, 0, 1, matData, '*', c)
    ax.tight_layout()

    ax.savefig(figPath + cs + '_bar.svg')
    plt.close()
