import pickle

import numpy as np
import pandas
import tensorflow as tf

import Tools.Utils as tu
import Tools.NNUtils as ntu


# tf.compat.v1.disable_eager_execution()

def modifyPhaseY(y, freezingE: pandas.DataFrame, ignoreMicro):
    for i, val in freezingE.iterrows():
        if ignoreMicro and val['Event'] == 3:
            continue
        if val['Start'] > y.size or val['End'] > y.size:
            continue
        y[int(val['Start']*100):int(val['End']*100)] = 1
    return y


def evaluateSub(subID: str, model: tf.keras.Model):
    lPath = PD_path + subID + '/6MWT_L.bin'
    rPath = PD_path + subID + '/6MWT_R.bin'
    [r, _] = tu.binaryFile2python(rPath)
    [_, l] = tu.binaryFile2python(lPath)
    r.createPandasDataFrame(useSync=True, freq=freq)
    l.createPandasDataFrame(useSync=True, freq=freq)

    freezePath = PD_path + subID + '/FreezeLog.csv'
    frO = pandas.read_csv(freezePath)
    xR = r.dataFrame.values
    xL = l.dataFrame.values
    tMax = min([xR.shape[0], xL.shape[0]])
    ytt = np.zeros(tMax)
    y = ntu.createbatch(np.expand_dims(modifyPhaseY(ytt, frO, ignoreMicro=False), 1), 50)
    y = np.squeeze(y)
    xR = xR[:tMax]
    xL = xL[:tMax]
    xx = np.empty((tMax, 50, 12, 2))
    xx[:, :, :, 0] = ntu.createbatch(xR, 50)
    xx[:, :, :, 1] = ntu.createbatch(xL, 50)
    xx = np.transpose(xx, (0, 1, 3, 2))
    xx[:, :, :, :3] /= 4096
    # yRpred = model.predict(xRbatch, batch_size=500)
    yLpred = model.predict(xx, batch_size=500)

    data = { #'rShoe': r.dataFrame, 'lShoe': l.dataFrame,
            'yTruth': y, 'yPred': yLpred, 'subID': subID}
    return data


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'allSubs_Freeze2SidesVideo.pickle'

with open(allSubsPath, 'rb') as f:
    allSubs = pickle.load(f)


PD_path = 'Z:/PD/PD_val/_Experiments/'
allSubsPath = PD_path + 'evaluatedSubsAll3.pickle'
modelParentPath = "E:/PDclass/LOO_ED2/LOO_"
# modelTypes = ['CNN', 'ED']
modelTypes = ['ED']
validSubs = [int(k[2:]) for k in allSubs]
otherSub = validSubs.copy()
while np.any(np.divide(validSubs, otherSub) == 1):
    np.random.shuffle(otherSub)

freq = 100.0
# samplesPerSubject = 6000
allSubs = {}
for k in validSubs:
    subID = 'PD%03d' % k
    print('Loading: %s' % subID)
    oneSub = {}
    for modT in modelTypes:
        modelPath = '%s%s_%s/LOO_%s_%s.h5' % (modelParentPath, subID, modT, subID, modT)
        model = tf.keras.models.load_model(modelPath)
        oneSub[modT] = {}
        for k2 in validSubs:
            print('Evaluating PD%03d' % k2)
            nloo = evaluateSub('PD%03d' % k2, model)
            oneSub[modT]['PD%03d' % k2] = nloo
    allSubs[subID] = oneSub.copy()

with open(allSubsPath, 'wb') as f:
    pickle.dump(allSubs, f)
