import pickle

import numpy as np
import pandas
import warnings

import Tools.Utils as tu
import Tools.NNUtils as nnTU


def modifyPhaseY(y, freezingE: pandas.DataFrame, ignoreMicro):
    for i, val in freezingE.iterrows():
        if ignoreMicro and val['Event'] == 3:
            continue
        if val['Start'] > y.size or val['End'] > y.size:
            continue
        y[int(val['Start']*freq):int(val['End']*freq)] = 1
    return y


for futureSamples in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50]:
    PD_path = 'Z:/PD/PD_val/_Experiments/'
    saveP = 'E:/PDclass/Future/'
    validSubs1 = [j for j in range(1, 8)] + [j for j in range(10, 25)]
    validSubs = []
    freq = 50.0
    windowSize = 50
    # futureSamples = 50
    allSubsPath = saveP + 'allSubs_Feb2021_%dFuture.pickle' % futureSamples
    samplesSplit = 1
    fogSplit = 0.4
    validation_split = 0.2

    dbPars = {'freq': freq, 'windowSize': windowSize}
    allSubs = {'dbPars': dbPars}
    totSamples = []
    for k in validSubs1:
        # try:
        subID = 'PD%03d' % k
        print('Loading: %s' % subID)
        lPath = PD_path + subID + '/6MWT_L.bin'
        rPath = PD_path + subID + '/6MWT_R.bin'
        [r, _] = tu.binaryFile2python(rPath)
        [_, l] = tu.binaryFile2python(lPath)
        r.createPandasDataFrame(useSync=True, freq=freq)
        l.createPandasDataFrame(useSync=True, freq=freq)
        freezePath = PD_path + subID + '/FreezeLog.csv'
        frO = pandas.read_csv(freezePath)
        xR = r.dataFrame.values
        xL = l.dataFrame.values
        tMax = min([xR.shape[0], xL.shape[0]])
        ytt = np.zeros(tMax)
        y = modifyPhaseY(ytt, frO, ignoreMicro=False)
        # move the y into the future
        # y = np.zeros(tMax+futureSamples)
        # y[futureSamples:] = ytt
        # # remove samples over the shoe time
        # y = y[:futureSamples]
        xR = xR[:tMax]
        xL = xL[:tMax]
        x = np.empty((tMax, windowSize, 12, 2))
        x[:, :, :, 0] = nnTU.createbatch(xR, windowSize)
        x[:, :, :, 1] = nnTU.createbatch(xL, windowSize)
        x[:, :, :, :3] /= 4096
        x = np.transpose(x, (0, 1, 3, 2))
        # there is no y values for the last futureSeamples
        x = x[:-futureSamples]
        y = y[futureSamples:]
        nSamples = int(y.shape[0] * (1 - validation_split))
        xT = x[nSamples:]
        yT = y[nSamples:]
        x = x[:nSamples]
        y = y[:nSamples]
        # m = tu.randomOrder(x.shape[0])[:int(tMax*samplesSplit)]
        m = tu.randomOrder(x.shape[0])
        # m = np.empty(samplesPerSubject, dtype=np.int)
        yy = y
        mFoG = np.where(yy == 1)[0]
        shu = tu.randomOrder(mFoG.size)
        mFoG = mFoG[shu]
        mNFoG = np.where(yy == 0)[0]
        shu = tu.randomOrder(mNFoG.size)
        mNFoG = mNFoG[shu]
        if mFoG.size < 100:
            warnings.warn('%s has %d samples of FoG, skip!!!!!' % (subID, mFoG.size))
            continue
        # do the new sampling
        newM = np.concatenate([mFoG, mNFoG[:int(mFoG.shape[0]*(1 - fogSplit)/fogSplit)]])
        allSubs[subID] = {'x': x[newM].copy(), 'y': y[newM].copy(), 'xT': xT.copy(), 'yT': yT.copy()}
        totSamples.append(newM.shape[0])
        validSubs.append(k)
        # except:
        #     print('Error in %s' % subID)
        #     pass

    allSubs['dbPars']['validSubs'] = validSubs
    print('Dataset has %d samples' % np.array(totSamples).sum())

    with open(allSubsPath, 'wb') as f:
        pickle.dump(allSubs, f)
